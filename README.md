<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>
<br>
<h2>Abinf_api</h2>

## Sobre a Api Abinf

Esta Api tem por objetivo fornecer dados por verbos Http.
<br>
A api é muita simples de usar, utiliza-se de um ambiente feito em Laravel,
<br>
Node.js, Redis entre outras aplicações e plugins compoe o mesmo.

## Requerimentos
PHP> = 7.0.0 - Minimo
<br>
Laravel 5.5 - Minimo
<br>
Apache2 - Minimo
<br>
Extensão PHP OpenSSL
<br>
PDO PHP Extensão
<br>
Mbstring PHP Extensão
<br>
Extensão do PHP Tokenizer
<br>
XML Extensão PHP
<br>

## Pacotes
Barryvdh Cors - Cross-Origin Resource Sharing
  <br>
  composer require barryvdh/laravel-cors
<br>
Intervention Image
  <br>
  composer require intervention/image
<br>
Predis /Broadcasting and Cache
  <br>
  composer required predis/predis
  <br>
Repository Api
  <br>
  composer required codens/repository
  <br>
  //Or install location folder Package
  <br>

## Instações no Servidor
  Instalar Redis
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis
  <br>
  Node.js
    <br>
    apt-get install nodejs
    <br>
    or
    <br>
    sudo apt-get install nodejs
  Npm
    <br>
    apt-get install npm
    <br>
    or
    <br>
    sudo apt-get install npm
  <br>
  Composer
  <br>
  https://getcomposer.org/  
  <br>

## Testes
Api Sendo testada constamente no Postman -> documentação da api no PostMan ->
