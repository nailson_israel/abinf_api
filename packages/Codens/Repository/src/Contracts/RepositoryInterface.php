<?php

namespace Codens\Repository\Contracts;

interface RepositoryInterface{
	
    public function all($columns = array('*'));

    public function allWhereNull($columns = array('*'));

    public function allNoPaginate($columns = array('*'));

    public function allWhereNullNopaginate($columns = array('*'));

    public function first($tenantid = null);    

    public function count();

    public function getWith($field, $value);

    public function create(array $data);
 
    public function update(array $data, $id);
 
    public function updateNoTenant(array $data, $id);

    public function delete($id, $tenantid);
   
    public function deleteNoTenant($id, $attribute = 'id');    

    //public function applySQL($sql);

}

