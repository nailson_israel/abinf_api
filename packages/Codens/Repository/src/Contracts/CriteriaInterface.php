<?php 

namespace Codens\Repository\Contracts;

use Codens\Repository\Criteria\Criteria;

Interface CriteriaInterface{

    public function skipCriteria($status = true);
 
    public function getCriteria();
 
    public function getByCriteria(Criteria $criteria);
 
    public function pushCriteria(Criteria $criteria);
 
    public function  applyCriteria();
}