<?php

namespace Codens\Repository\Orm;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as AppContainer;

use Codens\Repository\Contracts\CriteriaInterface;
use Codens\Repository\Criteria\Criteria;
use Codens\Repository\Contracts\RepositoryInterface;
use Codens\Repository\Exceptions\RepositoryException;

use Cache;

abstract class RepositoryElo implements RepositoryInterface, CriteriaInterface{

	private $app;

	protected $model;

    protected $criteria;

    protected $skipCriteria = false;


	public function __construct(AppContainer $app, Collection $collection){
		$this->app = $app;
		$this->criteria = $collection;
        $this->resetScope();
		$this->makeModel();
	}

	abstract function model();

    public function all($currentPage = 1, $key = null, $minutes = 120, $total = 10){
    	$tag = $key;
		$key .= $currentPage;
    	$this->applyCriteria();
	 	//return Cache::tags([$tag])->remember($key, $minutes, function () use ($total){
        	return $this->model->paginate($total);
		//});
    }

    public function allWhereNull($currentPage = 1, $key = null, $minutes = 120, $total = 10){
    	//$tag = $key;
		//$key .= $currentPage;
    	$this->applyCriteria();
	 	///return Cache::tags([$tag])->remember($key, $minutes, function () use ($total){
        	return $this->model->whereNull('deleted_at')->paginate($total);
		///});
    }

    public function allNoPaginate($columns = array('*')){
    	$this->applyCriteria();
        return $this->model->get($columns);
    }

    public function allWhereNullNopaginate($columns = array('*')){
    	$this->applyCriteria();
        return $this->model->whereNull('deleted_at')->get();
    }

    public function first($tenantid = null){
    	return $this->model->where('tenantid', $tenantid)->first();
    }

    public function count(){
    	return $this->model->count();
    }

    public function getWith($field, $value){
    	return $this->model->where($field, $value)->whereNull('deleted_at')->get();
    }

    public function create(array $data, $key = null) {
    	if($key != null){
			//Cache::tags($key)->flush();
    	}
        return $this->model->create($data);
    }

    public function update(array $data, $id, $attribute = 'id', $tenantid = null, $key = null) {
    	if($key != null){
			Cache::tags($key)->flush();
    	}
        return $this->model->where($attribute, $id)->where('tenantid', $tenantid)->update($data);
    }

    public function updateNoTenant(array $data, $id, $attribute = 'id', $key = null) {
        if($key != null){
			Cache::tags($key)->flush();
    	}
        return $this->model->where($attribute, $id)->update($data);
    }

    public function delete($id, $attribute = 'id', $tenantid = null, $key = null) {
        if($key != null){
			Cache::tags($key)->flush();
    	}
       	return $this->model->where($attribute, $id)->where('tenantid', $tenantid)->delete();
    }

    public function deleteNoTenant($id, $attribute = 'id', $key = null) {
        if($key != null){
			Cache::tags($key)->flush();
    	}
        return $this->model->where($attribute, $id)->delete();
    }


    /*public function SqlBasic($sql){
    	return DB::select( DB::raw($sql));
    }*/

	public function makeModel(){
		$model = $this->app->make($this->model());

		if(!$model instanceof Model)
			throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

		return $this->model = $model->newQuery();
	}

	//Criteria

    public function resetScope() {
        $this->skipCriteria(false);
        return $this;
    }

    public function skipCriteria($status = true){
        $this->skipCriteria = $status;
        return $this;
    }

    public function getCriteria() {
        return $this->criteria;
    }

    public function getByCriteria(Criteria $criteria) {
        $this->model = $criteria->apply($this->model, $this);
        return $this;
    }

    public function pushCriteria(Criteria $criteria) {
        $this->criteria->push($criteria);
        return $this;
    }

    public function  applyCriteria() {
        if($this->skipCriteria === true)
            return $this;

        foreach($this->getCriteria() as $criteria) {
            if($criteria instanceof Criteria)
                $this->model = $criteria->apply($this->model, $this);
        }

        return $this;
    }


}
