<?php 

namespace Codens\Repository\Criteria;

use Codens\Repository\Contracts\RepositoryInterface as Repository;

abstract class Criteria {
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public abstract function apply($model, Repository $repository);
}