<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Api\Talk::class, function (Faker $faker) {
    return [
        'tenantid' => '1',
        'clientid' => '513',
        'subject' => $faker->sentence(5),
        'userid' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'represented' => '7',
        'contactcl' => '7',
        'date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'type' => 'vf',
        'description' => $faker->paragraph
    ];
});
