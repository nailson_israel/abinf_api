<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Opportunit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('businnes')->unsigned();
            $table->string('title', 255);
            $table->string('value', 255)->nulabble();
            $table->string('description', 255)->nulabble();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->foreign('tenantid')->references('id')->on('tenants'); 
            $table->foreign('businnes')->references('id')->on('funnels');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunities');
    }
}
