<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Upirepresentedclient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('representedclients', function (Blueprint $table) {
            $table->renameColumn('id_client', 'clientid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('representedclients', function (Blueprint $table) {
            $table->renameColumn('clientid', 'id_client');
        });
    }
}
