<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updateitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('items', function($table) {
         $table->dropColumn('brand');
         $table->dropColumn('group');
      });
    }

    public function down()
    {
      Schema::table('items', function($table) {
         $table->integer('brand');
         $table->integer('group');
      });
    }
}
