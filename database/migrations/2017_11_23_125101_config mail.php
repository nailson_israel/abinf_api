<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConfigMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configmails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->string('serversmtp');
            $table->string('portsmtp');
            $table->string('serverpop');
            $table->string('portpop');
            $table->string('security');
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 

            $table->foreign('tenantid')->references('id')->on('tenants'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configmails');
    }
}
