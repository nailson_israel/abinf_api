<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Represented extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representeds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();            
            $table->integer('id_client')->unsigned();
            $table->string('cod')->nullable();
            $table->string('representada');
            $table->string('reference')->nullable();    
            $table->string('cnpjcpf');
            $table->string('group');
            $table->string('branch')->nullable();    
            $table->string('route');
            $table->string('ie')->nullable();
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('cep');
            $table->string('street')->nullable();
            $table->string('number')->nullable();
            $table->string('complements')->nullable();
            $table->string('neighborhood')->nullable();                                                        
            $table->string('city')->nullable();
            $table->string('uf')->nullable();
            $table->string('email')->nullable();
            $table->string('site')->nullable();
            $table->string('logo')->nullable();
            $table->string('textfix')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 
            $table->index('id_client'); 
            
            $table->foreign('tenantid')->references('id')->on('tenants');             
            $table->foreign('id_client')->references('id')->on('clients'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('ab_represent_cli');
    }
}
