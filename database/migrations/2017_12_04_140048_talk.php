<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Talk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('id_client')->unsigned();
            $table->string('subject');
            $table->date('date');
            $table->string('type');                        
            $table->string('contactcl')->nullable();    
            $table->string('contactante')->nullable();
            $table->string('represented')->nullable();
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
            $table->index('tenantid'); 
            $table->index('id_client');
            
            $table->foreign('id_client')->references('id')->on('clients'); 
            $table->foreign('tenantid')->references('id')->on('tenants');  

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('talks');
    }
}
