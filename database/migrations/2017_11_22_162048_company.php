<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Company extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->string('cnpj');
            $table->string('reason');
            $table->string('namefantasy');
            $table->string('cep');
            $table->string('address');
            $table->string('number');
            $table->string('city');
            $table->string('district');
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('email');                                                                        
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid');            

            $table->foreign('tenantid')->references('id')->on('tenants'); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
