<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cod');            
            $table->integer('tenantid')->unsigned();
            $table->string('client');
            $table->string('reference')->nullable();    
            $table->string('cnpjcpf');
            $table->string('group');
            $table->string('branch')->nullable();    
            $table->string('route');
            $table->string('ie')->nullable();
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('cep');
            $table->string('street')->nullable();
            $table->string('number')->nullable();
            $table->string('complements')->nullable();
            $table->string('neighborhood')->nullable();                                                         
            $table->string('city')->nullable();
            $table->string('uf')->nullable();
            $table->string('email')->nullable();
            $table->string('site')->nullable();
            $table->string('logo')->nullable();
            $table->string('textfix')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('client');            
            $table->index('tenantid'); 

            $table->foreign('tenantid')->references('id')->on('tenants');              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('clients');
    }
}
