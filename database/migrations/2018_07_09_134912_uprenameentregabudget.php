<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Uprenameentregabudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budgets', function (Blueprint $table) {
            $table->renameColumn('entrega', 'delivery');
            $table->renameColumn('concluido', 'completed'); 
            $table->renameColumn('dataaltera', 'datamodify');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budgets', function (Blueprint $table) {
            $table->renameColumn('delivery', 'entrega');
            $table->renameColumn('completed', 'concluido');
            $table->renameColumn('datamodify', 'dataaltera');
        });
    }
}
