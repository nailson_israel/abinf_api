<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('notifications');
            $table->string('iten1')->nullable();
            $table->string('iten2')->nullable();
            $table->string('iten3')->nullable();
            $table->string('iten4')->nullable();
            $table->string('iten5')->nullable();
            $table->string('iten6')->nullable();
            $table->string('iten7')->nullable();
            $table->string('iten8')->nullable();
            $table->string('iten9')->nullable();
            $table->string('iten10')->nullable();
            $table->string('visto')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('notifications');
    }
}
