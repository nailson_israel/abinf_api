<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Permission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('id_group')->unsigned();
            $table->string('page')->nullable();
            $table->string('view')->nullable(); 
            $table->string('create')->nullable();
            $table->string('update')->nullable(); 
            $table->string('delete')->nullable();                                   
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid');             

            $table->foreign('tenantid')->references('id')->on('tenants');  
            $table->foreign('id_group')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
