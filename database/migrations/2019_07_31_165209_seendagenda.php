<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Seendagenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendagendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agendaid');
            $table->integer('3days')->nullable();
            $table->integer('2days')->nullable();
            $table->integer('1day')->nullable(); 
            $table->integer('2_8hours')->nullable();
            $table->integer('1_2hours')->nullable();
            $table->integer('1_30hours')->nullable();
            $table->integer('30_0hours')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendagendas');
    }
}
