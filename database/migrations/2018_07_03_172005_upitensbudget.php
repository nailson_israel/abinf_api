<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Upitensbudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itenbudgets', function (Blueprint $table) {
            $table->renameColumn('id_budget', 'budgetid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itenbudgets', function (Blueprint $table) {
            $table->renameColumn('budgetid', 'id_budget');
        });
    }
}
