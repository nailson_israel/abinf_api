<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->string('cod');
            $table->string('product');
            $table->double('ipi')->nullable();
            $table->string('unit')->nullable();
            $table->double('price');
            $table->double('commission')->nullable();
            $table->integer('groupprod')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 
            $table->index('product');             

            $table->foreign('tenantid')->references('id')->on('tenants');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('products');
    }
}
