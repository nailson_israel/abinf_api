<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->string('plan')->default('Free');
            $table->string('referenceplan')->default('free_absl');
            $table->string('codeplan')->default('fr33');                                       
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid');            

            $table->foreign('tenantid')->references('id')->on('tenants'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
