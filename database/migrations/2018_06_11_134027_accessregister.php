<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class accessregister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessregisters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('iduser')->unsigned();
            $table->datetime('datetime');
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid');
            $table->index('iduser');

            $table->foreign('tenantid')->references('id')->on('tenants'); 
            $table->foreign('iduser')->references('id')->on('users');              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('accessregisters');
    }
}
