<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('id_client')->unsigned();
            $table->string('comment');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
            $table->index('tenantid'); 
            $table->index('id_client');
            
            $table->foreign('id_client')->references('id')->on('clients'); 
            $table->foreign('tenantid')->references('id')->on('tenants');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('comments');
    }
}
