<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Salesfunnel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columnsfunnel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('columnfunnel');
            $table->integer('idfunnel');
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 
            $table->foreign('tenantid')->references('id')->on('tenants'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columnsfunnel');
    }
}
