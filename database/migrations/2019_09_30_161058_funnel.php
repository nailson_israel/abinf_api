<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Funnel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funnels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->string('funnel', 255);
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid');            
            $table->foreign('tenantid')->references('id')->on('tenants'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funnels');
    }
}
