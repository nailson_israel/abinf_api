<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Custonorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customorders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('photorepresented')->nullable();
            $table->integer('namerepresented')->nullable();
            $table->integer('inforepresented')->nullable();
            $table->integer('code')->nullable();
            $table->integer('item')->nullable();
            $table->integer('ipi')->nullable();
            $table->integer('percent')->nullable();
            $table->integer('descont')->nullable();
            $table->integer('subtotal')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 

            $table->foreign('tenantid')->references('id')->on('tenants'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customorders');
    }
}
