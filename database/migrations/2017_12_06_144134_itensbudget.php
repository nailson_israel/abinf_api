<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Itensbudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itenbudgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenantid')->unsigned();
            $table->integer('id_budget')->unsigned();
            $table->string('product');
            $table->string('amount');
            $table->string('ipi');
            $table->string('unitary');
            $table->string('total');
            $table->string('nf')->nullable();
            $table->string('date')->nullable();
            $table->string('entregue')->nullable();
            $table->string('comission')->nullable();
            $table->string('verify_del')->nullable();   
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
            $table->index('tenantid'); 
            $table->index('product');
            
            $table->foreign('id_budget')->references('id')->on('budgets'); 
            $table->foreign('tenantid')->references('id')->on('tenants'); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('itenbudgets');
    }
}
