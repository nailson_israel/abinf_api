<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Budgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('control');           
            $table->integer('tenantid')->unsigned();
            $table->integer('client')->unsigned();
            $table->string('date');
            $table->string('seller')->nullable();
            $table->string('form_pay');
            $table->string('comment')->nullable();
            $table->string('contact')->nullable();
            $table->string('entrega');
            $table->string('transport')->nullable();
            $table->string('type');
            $table->string('descont_value')->nullable();
            $table->string('descont')->nullable();
            $table->string('represented');
            $table->string('dataaltera')->nullable();
            $table->string('order')->nullable();
            $table->string('concluido')->nullable();
            $table->string('situation');
            $table->string('user');
            $table->string('mot_descart')->nullable();                               
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('tenantid'); 
            $table->index('client');

            $table->foreign('client')->references('id')->on('clients'); 
            $table->foreign('tenantid')->references('id')->on('tenants'); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('budgets');
    }
}
