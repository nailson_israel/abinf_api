<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Upidusertalkactive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talks', function (Blueprint $table) {
            $table->integer('contactcl')->unsigned()->after('subject');
            $table->integer('represented')->unsigned()->after('subject');
            $table->integer('iduser')->unsigned()->after('subject');

            $table->foreign('contactcl')->references('id')->on('contacts');
            $table->foreign('represented')->references('id')->on('representeds');
            $table->foreign('iduser')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('talks');
    }
}
