<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Representedclient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representedclients', function (Blueprint $table) {
            $table->increments('idtable');
            $table->integer('tenantid')->unsigned();
            $table->integer('id_client')->unsigned();
            $table->integer('idrepresented')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
            $table->index('idtable');
            $table->index('tenantid'); 
            $table->index('id_client'); 
            $table->index('idrepresented');
                        
            $table->foreign('tenantid')->references('id')->on('tenants');                             
            $table->foreign('id_client')->references('id')->on('clients');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('representedclients');
    }
}
