<?php

use Illuminate\Database\Seeder;

class TalkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  		factory(App\Models\Api\Talk::class, 10)->create()->each(function($u) {});
    }
}
