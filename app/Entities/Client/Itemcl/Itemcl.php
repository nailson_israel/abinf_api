<?php

namespace App\Entities\Client\Itemcl;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Itemcl.
 *
 * @package namespace App\Entities\Client\Itemcl;
 */
class Itemcl extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
