<?php

namespace App\Entities\Client\Fillselects;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Departamentcontact.
 *
 * @package namespace App\Entities\Client\Fillselects;
 */
class Departamentcontact extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
