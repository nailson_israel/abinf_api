<?php

namespace App\Entities\Budget\Itenbudget;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Itenbudget.
 *
 * @package namespace App\Entities\Budget\Itenbudget;
 */
class Itenbudget extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
