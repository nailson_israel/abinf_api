<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    use SoftDeletes;
    //protected $connection = 'connect_tenancy_one';

    protected $table = 'tenants';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenant', 'cnpj', 'email', 'name', 'phone1', 'phone2', 'datebeginfree', 'datefinishfree', 'amountuser'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    protected $dates = ['deleted_at'];
}
