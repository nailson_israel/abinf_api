<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes, HasApiTokens, Notifiable;

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    //Function for access the passport auth2 api laravel with email or user. 
    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('user', $identifier)->first();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenantid', 'ismaster', 'isadmin', 'responsible', 'name', 'user', 'email', 'active', 'groupus', 'region', 'extern', 'routeuser', 'departament', 'change', 'phone1', 'phone2', 'phone3', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];
}
