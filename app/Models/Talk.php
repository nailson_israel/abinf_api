<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talk extends Model
{
    use SoftDeletes; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenantid', 'clientid', 'isactivity', 'subject', 'date', 'hour', 'type', 'contactcl', 'represented', 'description', 'userid', 'notification', 'notification_complet', 'updated_at', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];

    //acessors
    public function getHourAttribute($value)
    {
        return date('H:i', strtotime($this->attributes['hour']));

    }

    public function getUpdatedAtAttribute($value)
    {
        return date('H:i', strtotime($this->attributes['updated_at']));
    }
}
