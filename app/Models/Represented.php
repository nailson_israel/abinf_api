<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Represented extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenantid', 'cod', 'represented', 'reference', 'cnpjcpf', 'group', 'branch', 'route', 'phone1', 'phone2', 'cep', 'street', 'number', 'complements', 'neighborhood', 'city', 'uf', 'email', 'site', 'logo', 'textfix'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    protected $dates = ['deleted_at'];
}
