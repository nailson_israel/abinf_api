<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Groupclient extends Model
{
    use SoftDeletes;

    //protected $connection = 'connect_tenancy_one';

    protected $table = 'groupclients';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenantid', 'groupclient',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];
}
