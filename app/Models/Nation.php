<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Nation extends Model
{

    //protected $connection = 'connect_tenancy_one';

    protected $table = 'pais';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'sigla',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];


    protected $dates = [];
}
