<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Budget extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'control', 'tenantid', 'client', 'date', 'seller', 'form_pay', 'comment', 'contact', 'delivery', 'transport', 'type', 'descont_value', 'descont', 'representedid', 'datemodify', 'order', 'completed', 'situation', 'userid', 'mot_descart'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];


    //acessors
    public function getDateAttribute($value)
    {
        return date('d/m/Y', strtotime($this->attributes['date']));

    }    

    public function getCompletedAttribute($value)
    {
        return date('d/m/Y', strtotime($this->attributes['completed']));

    } 

    //mutators

    public function setMotDescartAttribute($value)
    {
        if($value){
            $this->attributes['mot_descart'] = $value;
        }else{
            $this->attributes['mot_descart'] = 0;
        }
    }

    public function setDateAttribute($value)
    {
        if($value){
            if($pos = strpos( $value, '/')){
                            //data_inicio
                $dt1 = explode('/', $value);
                $this->attributes['date'] = $dt1[2].'-'.$dt1[1].'-'.$dt1[0];
            }else{
                $this->attributes['date'] = $value;                
            }
        }else{
            $this->attributes['date'] = '0000-00-00';
        }        
    }

    public function setCompletedAttribute($value)
    {
        if($value){
            if($pos = strpos( $value, '/')){
                $dtconcl1 = explode('/', $value);
                $this->attributes['completed'] = $dtconcl1[2].'-'.$dtconcl1[1].'-'.$dtconcl1[0];  
            }else{
                $this->attributes['completed'] = $value;
            }
        }else{
           $this->attributes['completed'] = '0000-00-00';
        }
    }

    //Se quiser usar o relacionamento do laravel
    public function itens()
    {
        return $this->hasMany('App\Models\Api\Itenbudget', 'budgetid');

    }

}
