<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itenbudget extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenantid', 'budgetid', 'product', 'amount', 'ipi', 'unitary', 'total', 'nf', 'date', 'completed', 'comission', 'verify_del',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];

    //acessors
    public function getDateAttribute($value)
    {
        return date('d/m/Y', strtotime($this->attributes['date']));

    } 
   

    public function setNfAttribute($value)
    {
        if($value){
            $this->attributes['nf'] = $value;
        }else{
            $this->attributes['nf'] = 0;
        }
    }

    public function setComissionAttribute($value)
    {
        if($value){
            $this->attributes['comission'] = $value;
        }else{
            $this->attributes['comission'] = 0;
        }
    }


    
    public function setDateAttribute($value)
    {
        if($value){
            if($pos = strpos( $value, '/')){
                //data_inicio
                $dt1 = explode('/', $value);
                $this->attributes['date'] = $dt1[2].'-'.$dt1[1].'-'.$dt1[0];
            }else{
                $this->attributes['date'] = $value;
            }
        }else{
            $this->attributes['date'] = '0000-00-00';
        }        
    }

}
