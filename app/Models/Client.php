<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenantid', 'cod', 'dateupdate', 'client', 'reference', 'cnpjcpf', 'group', 'branch', 'route', 'ie', 'phone1', 'phone2', 'cep', 'street', 'number', 'complements', 'neighborhood', 'city', 'uf', 'email', 'site', 'logo', 'textfix', 'userid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];
}
