<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{


    //protected $connection = 'connect_tenancy_one';

    protected $table = 'cidade';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];


    protected $dates = [];
}
