<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{

    //protected $connection = 'connect_tenancy_one';

    protected $table = 'estado';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'uf', 'pais',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
 
    ];


    protected $dates = [];
}
