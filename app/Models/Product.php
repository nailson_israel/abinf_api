<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tenantid', 'cod', 'product', 'ipi', 'unit', 'price', 'commission', 'groupprod', 'idrep', 'checkstock', 'quantity'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'tenantid'
    ];


    protected $dates = ['deleted_at'];

    public function groupproduct()
    {
        return $this->belongsTo('App\Models\Groupproduct', 'groupprod');
    }

    public function represented()
    {
        return $this->belongsTo('App\Models\Represented', 'idrep');
    }
}
