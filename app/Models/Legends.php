<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Legends extends Model
{
  use ConectionTrait;

    protected $connection;

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $conn = $this->getConnect();
        return $this->connection = $conn;
    }

    //protected $connection = 'connect_tenancy_one';

    protected $table = 'legends';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idlegends', 'legends', 'descriptionlegend',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];


    protected $dates = [];
}
