<?php

namespace App\Http\Traits;
use App\Models\Tenant;
use Illuminate\Support\Facades\Auth;

trait ConectionTrait{

    public function getConnect(){
        $tenantid = Auth::guard('api')->user()->tenantid;
        $tenant = Tenant::where('idtenant', $tenantid)->get();
        foreach ($tenant as $tn) {
           $conn =  $tn->conn;
        }
        return $conn;
    }

    public function getDatabase(){
        $tenantid = Auth::guard('api')->user()->tenantid;
        $tenant = Tenant::where('idtenant', $tenantid)->get();
        foreach ($tenant as $tn) {
           $db =  $tn->dbconn;
        }
        return $db; 
    }
}