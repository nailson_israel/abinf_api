<?php

namespace App\Http\Traits;
use App\Models\Tenant;
use Illuminate\Support\Facades\Auth;

trait GetTenantid{

    public function getTenant(){
        return $tenantid = Auth::guard('api')->user()->tenantid;
    }
}