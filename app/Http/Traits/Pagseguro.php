<?php

namespace App\Http\Traits;
use App\Models\Tenant;
use Illuminate\Support\Facades\Auth;

trait PagSeguro{

    public function Uri(){
        //return 'https://ws.sandbox.pagseguro.uol.com.br/';
        return 'https://ws.pagseguro.uol.com.br/';
    }

    public function Auth(){
    	return 'codens@codens.com.br';
    }

    public function Token(){
    	//return '0A1EF96E12C24BDB81BF4893EB1FB760';
        return '9a046ad1-e0ff-4582-a67d-1467ebc64f076014a7c2449a95e8c54c814262302e318b59-0330-4509-9336-36eed49f767f';
    }

    public function Contenttype(){
    	return 'Content-type: application/json;charset=ISO-8859-1';
    }

    public function Accept(){
        return 'Accept: application/vnd.pagseguro.com.br.v3+xml;charset=ISO-8859-1';
    }

    public function ContenttypeTwo(){
    	return 'Content-type: application/x-www-form-urlencoded;charset=ISO-8859-1';
    }

    public function AcceptTwo(){
    	return 'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1';
    }

    public function planFree(){
        return 'abinfsales_free';
    }
}