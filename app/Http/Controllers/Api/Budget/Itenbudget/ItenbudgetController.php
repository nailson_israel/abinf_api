<?php

namespace App\Http\Controllers\Api\Budget\Itenbudget;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostItenbudget;

use App\Models\Budget;
use App\Models\Itenbudget as Itenbudgets;
use App\Models\Product;


use DB;

class ItenbudgetController extends Controller
{
    use GetTenantid;  

    private $pagination;

	public function __construct(Itenbudgets $itenbudget){
    	$this->middleware('privilege.budget');
        $this->itenbudget = $itenbudget;
        $this->pagination = config('responses.pagination');
	}

	public function itenbudget(Request $request, $id){
        $tenantid = $this->GetTenant(); 

        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'change';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'change';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $result = $this->itenbudget::select('products.product as productdescription', 'date', 'budgetid', 'cod', 'amount', 'itenbudgets.ipi', 'unitary', 'total', 'nf', 'completed', 'unit', 'comission', 'itenbudgets.product', 'itenbudgets.id', 'products.checkstock as checkstock', 'products.quantity as quantity')
        ->leftJoin('products', 'itenbudgets.product', '=', 'products.id')
        ->whereNull('itenbudgets.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['itenbudgets.tenantid' => $tenantid, 'budgetid' => $id])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->itenbudget::whereNull('itenbudgets.deleted_at')->where(['itenbudgets.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
        return response()->json($result, 200);	
	}

    public function itenbudgetorder(Request $request, $id){
        $tenantid = $this->GetTenant(); 
        $attribute = 'itenbudgets.tenantid';

        $result = $this->itenbudget::select('products.product as productdescription', 'date', 'budgetid', 'cod', 'amount', 'itenbudgets.ipi', 'unitary', 'total', 'nf', 'completed', 'unit', 'comission', 'itenbudgets.product', 'itenbudgets.id', 'products.checkstock as checkstock', 'products.quantity as quantity')
        ->leftJoin('products', 'itenbudgets.product', '=', 'products.id')
        ->whereNull('itenbudgets.deleted_at')
        ->where(['itenbudgets.tenantid' => $tenantid, 'budgetid' => $id])
        ->get();

        return response()->json($result, 200);  
    }

    

	public function additenbudget(RulesPostItenbudget $request){ 
        $tenantid = $this->GetTenant(); 
        $userloged = \Auth::guard('api')->user();

        /*$product = Product::where(['id' => $request['product'], 'tenantid' => $tenantid])->first();
        $qtd = intval($product->quantity);
        $amount = intval($request['amount']);

        if ($product->checkstock == 1) {
            if($amount > $qtd){
                return response()->json([
                    'ERROR' => 'A quantidade é maior do que o estoque do produto'
                ], 422);
            }

            if($amount == 0){
                return response()->json([
                    'ERROR' => 'A quantidde não pode ser 0'
                ], 422);
            }

            if($amount < 0){
                return response()->json([
                    'ERROR' => 'Não pode numero negativo'
                ], 422);
            }

            if($amount < $qtd){
                $sub = $qtd - $amount;
                Product::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $request['product']])->update(['quantity' => $sub]);
            }
        }*/

        $data = [
            'tenantid' => $tenantid,   
            'budgetid' => $request['budgetid'],
            'product' => $request['product'],           
            'amount' => $request['amount'],
            'ipi' => $request['ipi'],
            'unitary' => $request['unitary'],                        
            'total' => $request['total'], 
            'nf' => $request['nf'],
            'date' => $request['date'],     
            'completed' => $request['completed'],                                            
            'comission' => $request['comission'],             
        ];

        $create = $this->itenbudget::create($data);

        activity('itensbudget-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'idbudget' => $request['budgetid'], 'view' => '1'])
         ->log('Adicionou item ao orçamento ');

		return response()->json([
		    'SUCESS' => 'Iten do orçamento salvo com sucesso'
		], 200);  
	} 

	public function upitenbudget(RulesPostItenbudget $request, $id){
        $tenantid = $this->GetTenant(); 
        $userloged = \Auth::guard('api')->user();

        if(strpos($request->input('unitary'), ',')) {
            $unitary = str_replace('.', '', $request->input('unitary')); $unitary = str_replace(',', '.', $unitary); $unitary = (double)$unitary;  
        }else{
            $unitary = $request->input('unitary');
        }


        if(strpos($request->input('total'), ',')) {
            $total = str_replace('.', '', $request->input('total')); $total = str_replace(',', '.', $total); $total = (double)$total;  
        }else{
            $total = $request->input('total');
        } 

        $data = [
            'tenantid' => $tenantid,   
            'budgetid' => $request['budgetid'],
            'product' => $request['product'],           
            'amount' => $request['amount'],
            'ipi' => $request['ipi'],
            'unitary' => $unitary,                        
            'total' => $total, 
            'nf' => $request['nf'],
            'date' => $request['date'],     
            'completed' => $request['completed'],                                            
            'comission' => $request['comission'],             
        ];

        $update = $this->itenbudget::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

    	if($update){
            activity('itensbudget-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itenbudget' => $request['budgetid'], 'view' => '1'])
             ->log('Atualizou item no orçamento ');
			return response()->json([
			    'SUCESS' => 'Iten do orçamento editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Iten do orçamento não alterado, este iten não existe'
			], 422);         		
    	}

	}

	public function delitenbudget(Request $request, $id){
        $tenantid = $this->GetTenant();
        $user = auth()->user();

        $attribute = 'id';
        
        $data = [
            'verify_del' => '1',
        ];
        $update = $this->itenbudget::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);
        $delete = $this->itenbudget::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        /*
        $ib = Itensbudget::where('iditenbudget', $id)->update(['verify_del' => '1',]);
        $itenbudget = Itensbudget::where('iditenbudget', $id)->delete();
        */
        if($delete){
            activity('itensbudget-delete')
             ->causedBy($user)
             ->withProperties(['tenantid' => $tenantid, 'user' => $user->id, 'itenbudget' => $id, 'view' => '1'])
             ->log('Deletou item no orçamento ');
            return response()->json([
                'SUCESS' => 'Iten do orçamento excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Iten do orçamento não excluido, este iten não existe'
            ], 422);                 
        }         
	}


    public function delallitenbudget(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
       
        $attribute = 'id';
        $checkiten = $request->input('checkiten');

        $delete = $this->itenbudget::whereIn('id', $checkiten)->where(['tenantid' => $tenantid])->delete();     
        
        if($delete){
            activity('itensbudget-deleteall')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itenbudget' => $checkiten, 'view' => '1'])
             ->log('Deletou vários itens no orçamento ');

            return response()->json([
            'SUCESS' => 'Iten(s) excluido(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Iten(s) não excluidos, esta(s) itens(s) não existe(m)'
            ], 422);                 
        }
    }
}
