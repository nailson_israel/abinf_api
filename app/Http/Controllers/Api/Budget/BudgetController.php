<?php

namespace App\Http\Controllers\Api\Budget;
/*
    Requeriments inputs
	idbudget*,
	type*,
	date*,
	represented*,
	client*,
	contact
	delivery*,
	situation*,
	completed,
	form_pay*,
	order,
	transport,
	comment,
	seller,
	mot_descart,
	user*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostBudget;

use App\Models\Budget as Budgets;
use App\Models\Contact;
use App\Models\Client;
use App\Models\Represented;
use App\Models\Representedclient;


use App\Models\Itenbudget;
use DB;

class BudgetController extends Controller
{
    use GetTenantid;

    private $budget;
    private $represented;
    private $itenbudget;
    private $pagination;
    private $limittalk;

    public function __construct(Budgets $budget, Represented $represented, Itenbudget $itenbudget){
    	$this->middleware('privilege.budget');
        $this->budget = $budget;
        $this->represented = $represented;
        $this->itenbudget = $itenbudget;
        $this->pagination = config('responses.pagination');
        $this->limittalk = config('responses.limittalk');
    }

    public function budget(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $extern = $userloged['extern'];
        $route = $userloged['routeuser'];
        $attribute = 'budgets.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'date';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'control';
        $search = $request['search'];
        $type = $request['type'] != null ? $request['type'] : 'default';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->budget::select('budgets.date', 'budgets.id as id', 'control', 'budgets.client', 'budgets.order', 'descont_value', 
            'descont', 'situation', 'mot_descart', 'clients.client as client_description', 
            'clients.id as idclient', 'clients.route', 'completed', 'seller', 'budgets.contact', 
            'contacts.contact', 'form_pay', 'comment', 'delivery', 'transport', 'type', 
            'contacts.id as idcontact', 'representedid', 'represented as represented_', 'motivedescart')
        ->leftJoin('clients', 'budgets.client', '=', 'clients.id')
        ->leftJoin('motivedescarts', 'budgets.mot_descart', '=', 'motivedescarts.id')
        ->leftJoin('contacts', 'budgets.contact', '=', 'contacts.id')
        ->leftJoin('representeds', 'budgets.representedid', '=', 'representeds.id')
        ->whereNull('budgets.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));        
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('budgets.userid', $iduser);
            }
        })
        ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'budget' ? $query->where('type', "0") : $query->where('type', "1"); 
            }
        })
        ->where(['budgets.tenantid' => $tenantid])
        ->groupBy('budgets.id')
                ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->budget::whereNull('budgets.deleted_at')->where(['budgets.tenantid' => $tenantid])
        ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'budget' ? $query->where('type', "0") : $query->where('type', "1"); 
            }
        })->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function budgetClientOrder(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $extern = $userloged['extern'];
        $route = $userloged['routeuser'];
        $attribute = 'budgets.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'date';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'control';
        $type = $request['type'] != null ? $request['type'] : 'default';
        $search = $request['search'];

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->budget::select('budgets.date', 'budgets.id as id', 'control', 'budgets.client', 'budgets.order', 'descont_value', 
            'descont', 'situation', 'mot_descart', 'clients.client as client_description', 
            'clients.id as idclient', 'clients.route', 'completed', 'seller', 'budgets.contact', 
            'contacts.contact', 'form_pay', 'comment', 'delivery', 'transport', 'type', 
            'contacts.id as idcontact', 'representedid', 'represented as represented_', 'motivedescart')
        ->leftJoin('clients', 'budgets.client', '=', 'clients.id')
        ->leftJoin('motivedescarts', 'budgets.mot_descart', '=', 'motivedescarts.id')
        ->leftJoin('contacts', 'budgets.contact', '=', 'contacts.id')
        ->leftJoin('representeds', 'budgets.representedid', '=', 'representeds.id')
        ->whereNull('budgets.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));        
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('budgets.userid', $iduser);
            }
        })
        ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'budget' ? $query->where('type', "0") : $query->where('type', "1"); 
            }
        })
        ->where(['budgets.tenantid' => $tenantid, 'budgets.client' => $id, 'type' => 1])
        ->groupBy('budgets.id')
        ->limit($this->limittalk)->get();

        return response()->json($result, 200);
    }

    public function budgetPending(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $extern = $userloged['extern'];
        $route = $userloged['routeuser'];
        $attribute = 'budgets.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'date';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'control';
        $type = $request['type'] != null ? $request['type'] : 'default';
        $search = $request['search'];

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->budget::select('budgets.date', 'budgets.id as id', 'control', 'budgets.client', 'budgets.order', 'descont_value', 
            'descont', 'situation', 'mot_descart', 'clients.client as client_description', 
            'clients.id as idclient', 'clients.route', 'completed', 'seller', 'budgets.contact', 
            'contacts.contact', 'form_pay', 'comment', 'delivery', 'transport', 'type', 
            'contacts.id as idcontact', 'representedid', 'represented as represented_', 'motivedescart')
        ->leftJoin('clients', 'budgets.client', '=', 'clients.id')
        ->leftJoin('motivedescarts', 'budgets.mot_descart', '=', 'motivedescarts.id')
        ->leftJoin('contacts', 'budgets.contact', '=', 'contacts.id')
        ->leftJoin('representeds', 'budgets.representedid', '=', 'representeds.id')
        ->whereNull('budgets.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));        
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('budgets.userid', $iduser);
            }
        })
        ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'budget' ? $query->where('type', "0") : $query->where('type', "1"); 
            }
        })
        ->where(['budgets.tenantid' => $tenantid, 'budgets.client' => $id, 'type' => 0])
        ->groupBy('budgets.id')
        ->limit($this->limittalk)->get();

        return response()->json($result, 200);
    }

    public function budgetid(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $extern = $userloged['extern'];
        $route = $userloged['routeuser'];
        $attribute = 'budgets.tenantid';
        $attribute2 = 'budgets.id';

        $result = $this->budget::select('budgets.date', 'budgets.id as id', 'control', 'budgets.client', 'budgets.order', 'descont_value', 
            'descont', 'situation', 'mot_descart', 'clients.client as client_description', 
            'clients.id as idclient', 'clients.route', 'completed', 'seller', 'budgets.contact', 
            'contacts.contact', 'form_pay', 'comment', 'delivery', 'transport', 'type', 
            'contacts.id as idcontact', 'representedid', 'represented as represented_', 'motivedescart')
        ->leftJoin('clients', 'budgets.client', '=', 'clients.id')
        ->leftJoin('motivedescarts', 'budgets.mot_descart', '=', 'motivedescarts.id')
        ->leftJoin('contacts', 'budgets.contact', '=', 'contacts.id')
        ->leftJoin('representeds', 'budgets.representedid', '=', 'representeds.id')
        ->whereNull('budgets.deleted_at')
        ->where(['budgets.tenantid' => $tenantid, 'budgets.id' => $id])
        ->groupBy('budgets.id')
        ->get();

        return response()->json($result, 200);
    }

    public function nextnumber(){
        $tenantid = $this->GetTenant();        
        $query = "SELECT MAX(control)+1 AS control FROM budgets WHERE tenantid = '$tenantid'";
        $nextnumber = DB::select($query);

        if($nextnumber[0]->control === null){
            $next = [[
                'control' => 1,
            ]];
        }else {
            $next = $nextnumber;
        }

        return response()->json($next, 200);
    }

    public function sellerClient(Request $request, $idclient){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT SUM(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient' AND budgets.userid = '$iduser'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT SUM(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient'";
           $totalbudgets = DB::select($query);
        }      
       return response()->json($totalbudgets, 200);
    }

    public function topSeller(Request $request, $idclient){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT MAX(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient' AND budgets.userid = '$iduser'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT MAX(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient'";
           $totalbudgets = DB::select($query);
        }      
       return response()->json($totalbudgets, 200);
    }

    public function bottomSeller(Request $request, $idclient){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT MIN(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient' AND budgets.userid = '$iduser'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT MIN(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND client = '$idclient'";
           $totalbudgets = DB::select($query);
        }      
       return response()->json($totalbudgets, 200);
    }

    public function totalorderClient(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL and type = 1 AND client = "'.$idclient.'" AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL and type = 1 AND client = "'.$idclient.'" AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    }

    public function showtotal(Request $request, $id){//total do valor do orçamento
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT SUM(total) as total, SUM(amount * unitary)  as subtotal, SUM(ipi) as ipi, SUM(amount) as amount, SUM(itenbudgets.completed) as completed, budgetid FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND budgetid = '$id' AND budgets.userid = '$iduser'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT SUM(total) as total, SUM(amount * unitary)  as subtotal, SUM(ipi) as ipi, SUM(amount) as amount, SUM(itenbudgets.completed) as completed, budgetid FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND budgetid = '$id'";
           $totalbudgets = DB::select($query);
        }       
       return response()->json($totalbudgets, 200);
    }

    //wallert

    public function showtotalbudget(){//conta total de orçamentos no mes
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as budget FROM budgets WHERE deleted_at IS NULL and type = 0 AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as budget FROM budgets WHERE deleted_at IS NULL and type = 0 AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200); 
    }

    public function showtotalaguard(){ //conta total de orçamentos aguardando no mês
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as aguard FROM budgets WHERE deleted_at IS NULL and type = 0 AND situation = "aguardando" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as aguard FROM budgets WHERE deleted_at IS NULL and type = 0 AND  situation = "aguardando" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    }

    public function showtotalconcluido(){ //conta total de orçamentos concluidos no mÊs
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as completed FROM budgets WHERE deleted_at IS NULL and type = 0 AND  situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as completed FROM budgets WHERE deleted_at IS NULL and type = 0 AND  situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    } 

    public function showtotaldescarted(){//conta total de descartados no mÊs
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as descart FROM budgets WHERE deleted_at IS NULL and type = 0 AND  situation = "descartado" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as descart FROM budgets WHERE deleted_at IS NULL and type = 0 AND  situation = "descartado" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);       
    }

    public function showtotalorder(){ //conta total de pedidos no mÊs
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    }


    public function showtotalaguardOrder(){ //conta total de pedidos aguardando no mÊs
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as aguard FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND situation = "aguardando" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as aguard FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND  situation = "aguardando" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    }

    public function showtotalconcluidoOrder(){// conta total de pedidos concluidos no mês
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as completed FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND  situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as completed FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND  situation = "concluido" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);
    } 

    public function showtotaldescartedOrder(){//conta total de pedidos descartados no mÊs
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as descart FROM budgets WHERE deleted_at IS NULL and type = 1 AND budgets.situation = "concluido" AND  situation = "descartado" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'" AND budgets.userid = "'.$iduser.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as descart FROM budgets WHERE type = 1 AND budgets.situation = "concluido" AND  situation = "descartado" AND MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);       
    }

    //panel central

    public function showcountbudget(){ //conta os orçamentos
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select("SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, COUNT(id) as budget FROM budgets WHERE deleted_at IS NULL AND type = 0 AND MONTH(budgets.updated_at) = MONTH(CURDATE()) AND YEAR(budgets.updated_at) = YEAR(CURDATE()) AND tenantid = '$tenantid' AND budgets.userid = '$iduser'");
        }else{
            $result = DB::select("SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, COUNT(id) as budget FROM budgets WHERE deleted_at IS NULL AND type = 0 AND MONTH(budgets.updated_at) = MONTH(CURDATE()) AND YEAR(budgets.updated_at) = YEAR(CURDATE()) AND tenantid = '$tenantid'");     
        }
        return response()->json($result, 200);      
    }

    public function showcountorder(){ //conta os pedidos
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
            $result = DB::select("SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL AND type = 1 AND budgets.situation = 'concluido' AND MONTH(budgets.updated_at) = MONTH(CURDATE()) AND YEAR(budgets.updated_at) = YEAR(CURDATE()) AND tenantid = '$tenantid' AND budgets.userid = '$iduser'");
        }else{
            $result = DB::select("SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, COUNT(id) as orderb FROM budgets WHERE deleted_at IS NULL AND type = 1 AND budgets.situation = 'concluido' AND MONTH(budgets.updated_at) = MONTH(CURDATE()) AND YEAR(budgets.updated_at) = YEAR(CURDATE()) AND tenantid = '$tenantid'");     
        }
        return response()->json($result, 200);      
    }

    //left join com budgets para ir verificar se é pedido ou orçamento
    public function showtotalorderValue(){ //soma total de pedidos realizado(valores)
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT SUM(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND budgets.type = '1' AND budgets.situation = 'concluido' AND budgets.userid = '$iduser'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT SUM(total) as total FROM itenbudgets LEFT JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND budgets.type = '1' AND budgets.situation = 'concluido' ";
           $totalbudgets = DB::select($query);
        }       
       return response()->json($totalbudgets, 200);      
    }


    public function showtotalmonth(Request $request){ //total em valores do mês
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
           $iduser = $userloged['id'];
           $query = "SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, SUM(total) as total, SUM(amount * unitary)  as subtotal FROM itenbudgets INNER JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND itenbudgets.tenantid = '$tenantid' AND budgets.type = '1' AND MONTH(itenbudgets.updated_at) = MONTH(CURDATE()) AND YEAR(itenbudgets.updated_at) = YEAR(CURDATE()) AND budgets.userid = '$iduser' AND budgets.situation = 'concluido'";
           $totalbudgets = DB::select($query);
        }else{
           $query = "SELECT DATE_FORMAT(CURDATE(), '%M') as month, DATE_FORMAT(CURDATE(), '%Y') as year, SUM(total) as total, SUM(amount * unitary)  as subtotal FROM itenbudgets INNER JOIN budgets ON itenbudgets.budgetid = budgets.id WHERE itenbudgets.deleted_at IS NULL AND MONTH(itenbudgets.updated_at) = MONTH(CURDATE()) AND YEAR(itenbudgets.updated_at) = YEAR(CURDATE()) AND itenbudgets.tenantid = '$tenantid' AND budgets.type = '1' AND budgets.situation = 'concluido'";
           $totalbudgets = DB::select($query);
        }       
       return response()->json($totalbudgets, 200);
    }

    // panel of client

    public function showclientbuy(){

        /*$results = DB::select( DB::raw("SELECT COUNT(ab_budgets.idbudget) as count, abc.client FROM $db.ab_budgets LEFT JOIN $db.ab_client as abc ON ab_budgets.client = abc.idclient WHERE ab_budgets.client = ab_budgets.client"));
        return response()->json($results);*/
    }


    public function addbudget(RulesPostBudget $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

		$data = [
            'control' => $request['control'],
            'tenantid' => $tenantid,
            'type' => $request['type'],
            'date' => $request['date'],            
            'representedid' => $request['represented'],
            'client' => $request['client'],
            'contact' => $request['contact'],
            'delivery' => $request['delivery'],                        
            'situation' => $request['situation'], 
            'completed' => $request['completed'],
            'form_pay' => $request['form_pay'],
            'order' => $request['order'],                                                
            'transport' => $request['transport'],                                              
            'comment' => $request['comment'],        
            'seller' => $request['seller'], 
            'mot_descart' => $request['mot_descart'],
            'userid' => $userloged->id,
        ];

        $create = $this->budget::create($data);

        activity('budget-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'budget' => $create->id, 'view' => '1'])
         ->log('Criou orçamento ou pedido');


		return response()->json([
		    'SUCESS' => 'Orçamento salvo com sucesso',
            'ID' => $create->id
		], 200);  
    }

    public function upbudget(RulesPostBudget $request, $id){
        $tenantid = $this->GetTenant();        
        $textfix = $request['textfix'];   
        $idrepresent = $request['represented'];

        $userloged = \Auth::guard('api')->user();
	    
        if(strpos($request->input('descont_value'), ',')) {
            $descont_value = str_replace('.', '', $request->input('descont_value')); $descont_value = str_replace(',', '.', $descont_value); $descont_value = (double)$descont_value;  
        }else{
            $descont_value = $request->input('descont_value');
        } 

        if(strpos($request->input('descont'), ',')) {
            $descont = str_replace('.', '', $request->input('descont')); $descont = str_replace(',', '.', $descont); $descont = (double)$descont;  
        }else{
            $descont = $request->input('descont');
        } 

        $data = [
            //'control' => $request['control'],            
            'type' => $request['type'],
            'descont_value' => $descont_value,
            'descont' => $descont,            
            'date' => $request['date'],            
            'representedid' => $request['represented'],
            'client' => $request['client'],
            'contact' => $request['contact'],
            'delivery' => $request['delivery'],                        
            'situation' => $request['situation'], 
            'completed' => $request['completed'],
            'form_pay' => $request['form_pay'],
            'order' => $request['order'],                                                
            'transport' => $request['transport'],                                              
            'comment' => $request['comment'],        
            'seller' => $request['seller'], 
            'mot_descart' => $request['mot_descart'],
            'userid' => $userloged->id,            
        ];


        $datatext = ['textfix' => $textfix];
        $attribute = 'id';

        $update = $this->budget::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

        $attributeclient = 'id';

        $attributerepresented = 'clientid';

        $updatetextrepresented = $this->represented->whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $idrepresent])->update($datatext);

    	if($update){
            activity('budget-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'budget' => $id, 'view' => '1'])
             ->log('Alterou orçamento ou pedido ');

			return response()->json([
			    'SUCESS' => 'Orçamento editado com sucesso'
			], 200);
    	}else{
			return response()->json([
			    'ERROR' => 'Orçamento não alterado, este orçamento não existe'
			], 422);
    	}
    }

    public function uptypebudget(Request $request, $id){
        $tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();

        $data = [
            'type' => $request['type'],
        ];

        $update = $this->budget::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

        if($update){
            activity('budget-typeupdate')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'budget' => $id, 'view' => '1'])
             ->log('Alterou o tipo do orçamento ou pedido ');

            return response()->json([
                'SUCESS' => 'Orçamento editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Orçamento não alterado, este orçamento não existe'
            ], 422);                 
        }
    }

    public function delbudget(Request $request, $id){
        $tenantid = $this->GetTenant(); 
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';        

        $delete = $this->budget::where(['tenantid' => $tenantid, 'id' => $id])->delete();
        $delete2 = $this->itenbudget::where(['tenantid' => $tenantid, 'budgetid' => $id])->delete();

		//$budget = Budgets::where('idbudget', $id)->delete();
        //$itensbud = Itensbudget::where('idbudget_fk', $id)->delete();
        
        if($delete){
            activity('budget-delete')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'budget' => $id, 'view' => '1'])
             ->log('Deletou orçamento ou pedido ');

            return response()->json([
                'SUCESS' => 'Orçamento excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Orçamento não excluido, este orçamento não existe'
            ], 422);                 
        }
    }

    public function delallbudget(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';
        $checkbudget = $request->input('checkbudget');

        $delete = $this->budget::whereIn('id', $checkbudget)->where(['tenantid' => $tenantid])->delete();     
        $delete2 = $this->itenbudget::whereIn('budgetid', $checkbudget)->where(['tenantid' => $tenantid])->delete();     

        if($delete){
            activity('budget-deleteall')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged, 'budget' => $checkbudget, 'view' => '1'])
             ->log('Deletou vários orçamentos ou pedidos ');

            return response()->json([
                'SUCESS' => 'Orçamento(s) excluida(s) com sucesso'
            ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Orçamento(s) não excluidas, esta(s) orçamento(s) não existe(m)'
            ], 422);                 
        }
    }
}
