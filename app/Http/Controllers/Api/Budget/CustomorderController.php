<?php

namespace App\Http\Controllers\Api\Budget;
/*
    Requeriments inputs
    
    Motivedescart*    
    
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostCustomorder;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Budget\CustomorderRepository;
//Criteria
use App\Criteria\TenantCriteria;
use App\Criteria\SearchCriteria;
use App\Criteria\SortCriteria;

use App\Models\Customorder;

class CustomorderController extends Controller
{
    use GetTenantid;

    private $custom;

    public function __construct(Customorder $custom){
        $this->middleware('privilege.budget');
        $this->custom = $custom;
    }

    public function customorder(Request $request){
        $tenantid = $this->GetTenant();
        $result = $this->custom->whereNull('deleted_at')->where('tenantid', $tenantid)->get();
        return response()->json($result, 200);  
    }

    public function upcustomorder(RulesPostCustomorder $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'photorepresented' => $request->input('photorepresented'),
            'namerepresented' => $request->input('namerepresented'),
            'inforepresented' => $request->input('inforepresented'),
            'code' => $request->input('code'),
            'item' => $request->input('item'),
            'ipi' => $request->input('ipi'),
            'percent' => $request->input('percent'),
            'descont' => $request->input('descont'),
            'subtotal' => $request->input('subtotal'),
            'subtotalproduct' => $request->input('subtotalproduct'),
            'totalproducttext' => $request->input('totalproducttext'),
            'comment' => $request->input('comment'),
        ];
        $attribute = 'id';
        $update = $this->custom->where(['tenantid' => $tenantid, 'id' => $id])->update($data);           

        if($update){
            activity('custonorder-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $id, 'view' => '1'])
             ->log('Personalizou informações do pdf');

            return response()->json([
                'SUCESS' => 'Configuração editada com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Configuração não alterada, esta configuração não existe'
            ], 422);                 
        }
    }
}
