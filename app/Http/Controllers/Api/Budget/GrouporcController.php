<?php

namespace App\Http\Controllers\Api\Budget;
/*
    Requeriments inputs
    
    grouporc*    
    
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostGrouporc;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Api\Budget\GrouporcRepository as Grouporc;
//Criteria
use App\Repositories\Criteria\TenantidCriteria;
use App\Repositories\Criteria\Api\Budget\GrouporcCriteria;

use App\Models\Grouporc;

class GrouporcController extends Controller
{
    use GetTenantid;

    private $grouporc;

    public function __construct(Grouporc $grouporc){
        $this->middleware('privilege.fillselects');
        $this->grouporc = $grouporc;
    }

    public function grouporc(){
        $tenantid = $this->GetTenant();

        $result = $this->grouporc::whereNull('delete_at')
        ->where('tenantid', $tenantid)
        ->get();

        return response()->json($result, 200);
    }

    public function grouporcselectsbox(){
        $tenantid = $this->GetTenant();
        $result = $this->grouporc->pushCriteria(new TenantidCriteria($tenantid))->pushCriteria(new GrouporcCriteria())->allWhereNull();
        return response()->json($result, 200);     
    }

    public function addgrouporc(RulesPostGrouporc $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'grouporcclient' => $request->input('grouporcclient')];
        $create = $this->grouporc->create($data);

        activity('grouporc-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'grouporc' => $create->id, 'view' => '1'])
         ->log('Criou um grupo de orçamento');

		return response()->json([
		    'SUCESS' => 'Grupo salvo com sucesso'
		]);
    }

    public function upgrouporc(RulesPostGrouporc $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'grouporcclient' => $request->input('grouporcclient')];
        $attribute = 'id';
        $update = $this->grouporc->update($data, $id, $attribute, $tenantid);

        if($update){
            activity('grouporc-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged, 'grouporc' => $update->id, 'view' => '1'])
             ->log('Alterou o grupo de orçamento');

            return response()->json([
                'SUCESS' => 'Grupo editado com sucesso'
            ]);             
        }else{
            return response()->json([
                'ERROR' => 'Grupo não alterado, este grupo não existe'
            ]);                 
        }
    }

    public function delgrouporc(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'grouporcclient' => $request->input('grouporcclient')];
        $attribute = 'id';
        $delete = $this->grouporc->delete($id, $attribute, $tenantid);

    	if($delete){
            activity('grouporc-delete')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'grouporc' => $id, 'view' => '1'])
             ->log('Deletou o grupo de orçamento');

  			return response()->json([
  			    'SUCESS' => 'Grupo excluido com sucesso'
  			]);        		
    	}else{
  			return response()->json([
  			    'ERROR' => 'Grupo não excluido, este grupo não existe'
  			]);         		
    	}
    }

}
