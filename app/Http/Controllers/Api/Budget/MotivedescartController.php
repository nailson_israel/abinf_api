<?php

namespace App\Http\Controllers\Api\Budget;
/*
    Requeriments inputs
    
    Motivedescart*    
    
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostMotivedescart;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Budget\MotivedescartRepository;
//Criteria
use App\Criteria\TenantCriteria;
use App\Criteria\SearchCriteria;
use App\Criteria\SortCriteria;

use App\Models\Motivedescart;

class MotivedescartController extends Controller
{
    use GetTenantid;

    private $motivedescart;

    public function __construct(Motivedescart $motivedescart){
        $this->middleware('privilege.fillselects');
        $this->motivedescart = $motivedescart;
    }

    public function motivedescart(Request $request){
        $tenantid = $this->GetTenant();

        $sort = 'asc';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'motivedescart';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'motivedescart';


        $result = $this->motivedescart::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200);
    }

    public function motivedescartselectsbox(Request $request){
        $tenantid = $this->GetTenant();

        $sort = 'asc';
        $typesort = 'motivedescart';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'motivedescart';
        $search = $request['search'];

        $result = $this->motivedescart::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid])
        ->get();

        $search == '' ? $result = '' : $result = $result;

        return response()->json($result, 200);       
    }

    public function addmotivedescart(RulesPostMotivedescart $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'motivedescart' => $request->input('motivedescart')];
        $create = $this->motivedescart::create($data);

        activity('motivedescart-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'motivedescart' => $create->id, 'view' => '1'])
         ->log('Criou o motivo de descarte ');
      		return response()->json([
      		    'SUCESS' => 'Motivo salvo com sucesso'
      		], 200);
    }

    public function upmotivedescart(RulesPostMotivedescart $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'motivedescart' => $request->input('motivedescart')];
        $attribute = 'id';
        $update = $this->motivedescart::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

        if($update){
            activity('motivedescart-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'motivedescart' => $id, 'view' => '1'])
             ->log('Alterou o motivo de descarte ');

            return response()->json([
                'SUCESS' => 'Motivo editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Motivo não alterado, este motivo não existe'
            ], 422);                 
        }
    }

    public function delmotivedescart(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'motivedescart' => $request->input('motivedescart')];
        $attribute = 'id';

      $delete = $this->motivedescart::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('motivedescart-delete')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'motivedescart' => $id, 'view' => '1'])
             ->log('Alterou o motivo de descarte ');

  			return response()->json([
  			    'SUCESS' => 'Motivo excluido com sucesso'
  			], 200);        		
    	}else{
  			return response()->json([
  			    'ERROR' => 'Motivo não excluido, este motivo não existe'
  			], 422);         		
    	}
    }
}
