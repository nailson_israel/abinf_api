<?php

namespace App\Http\Controllers\Api\Client;
/*
    Requeriments inputs
    cod,
    client*,
    reference,
    cnpjcpf*,
    group*,
    branch,
    route*,
    ie,
    phone1*,
    phone2,
    cep*,
    street,
    number,
    complements,
    neighborhood,
    city,
    uf,
    email,
    site,
    logo,
    textfx,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\RulesPostClient;
use App\Http\Traits\GetTenantid;

use App\Models\Representedclients as CustonRepresentedclients;
use App\Models\Represented as CustonRepresented;
use App\Models\Contact as ModelContact;
use App\Models\Talk as ModelTalk;
use App\Models\Comment as ModelComment;
use App\Models\Client as ModelClient;
use App\Models\Itemcl as ModelItemcl;

use DB;

class ClientController extends Controller
{
    use GetTenantid;

    private $client;
    private $represented;
    private $representedclient;
    private $comment;
    private $itemcl;
    private $contact;
    private $pagination;

    public function __construct(ModelClient $client, CustonRepresented $represented, CustonRepresentedclients $representedclient, ModelTalk $talk, ModelComment $comment, ModelItemcl $itemcl, ModelContact $contact){
    	$this->middleware('privilege.client');
        $this->client = $client;
        $this->represented = $represented;
        $this->representedclient = $representedclient;
        $this->talk = $talk;
        $this->comment = $comment;
        $this->itemcl = $itemcl;
        $this->contact = $contact;
        $this->pagination = config('responses.pagination');
    }

    public function client(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'client';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'client';
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->client::select('clients.id as id', 'cod', 'dateupdate', 'clients.tenantid as tenantid', 'client', 'reference', 'cnpjcpf', 'group', 'branch', 'route', 'ie', 'clients.phone1', 'clients.phone2', 'cep', 'street', 'number', 'complements', 'neighborhood', 'city', 'uf', 'clients.email', 'site', 'logo', 'textfix', 'userid', 'users.name', 'users.user')
        ->leftJoin('users', 'clients.userid', '=', 'users.id')
        ->leftJoin('representedclients', 'clients.id', '=', 'representedclients.clientid')
        ->whereNull('clients.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('clients.userid', $iduser);
            }
        })
        ->where(['clients.tenantid' => $tenantid])
        ->groupBy('clients.cod') 
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->client::whereNull('clients.deleted_at')->where(['clients.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200); 
    }

    public function clientid(Request $requet, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->client::withTrashed()->select('clients.id as id', 'cod', 'dateupdate', 'clients.tenantid as tenantid', 'client', 'reference', 'cnpjcpf', 'group', 'branch', 'route', 'ie', 'clients.phone1', 'clients.phone2', 'cep', 'street', 'number', 'complements', 'neighborhood', 'city', 'uf', 'clients.email', 'site', 'logo', 'textfix', 'userid', 'users.name', 'users.user', 'clients.deleted_at as dlt')
        ->leftJoin('users', 'clients.userid', '=', 'users.id')
        ->leftJoin('representedclients', 'clients.id', '=', 'representedclients.clientid')
//        ->whereNull('clients.deleted_at')
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('clients.userid', $iduser);
            }
        })
        ->where(['clients.tenantid' => $tenantid, 'clients.id' => $id])
        ->groupBy('clients.cod') 
        ->get();

        return response()->json($result, 200);               
    }

    public function clientselectbox(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'client';
        $sort = 'asc';
        $search = $request['search'];
        $typesort = 'client';

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->client::select('id', 'client', 'reference')
        ->whereNull('clients.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('clients.userid', $iduser);
            }
        })
        ->where(['clients.tenantid' => $tenantid])
        ->groupBy('clients.cod') 
        ->get();

        $search == '' ? $result = '' : $result = $result;

        return response()->json($result, 200);          
    }

    public function nextnumber(){
        $tenantid = $this->GetTenant();        
        $query = "SELECT MAX(cod)+1 AS cod FROM clients WHERE tenantid = '$tenantid'";
        $nextnumber = DB::select($query);

        if($nextnumber[0]->cod === null){
            $next = [[
                'cod' => 1,
            ]];
        }else {
            $next = $nextnumber;
        }

    	return response()->json($next, 200);
    }

    public function totalclientmonth(){
        $tenantid = $this->GetTenant();  
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
            $iduser = $userloged['id'];
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.userid = "'.$iduser.'" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }else{
            $result = DB::select('SELECT DATE_FORMAT(CURDATE(), "%M") as month, DATE_FORMAT(CURDATE(), "%Y") as year, COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }     
        return response()->json($result, 200);          
    }

    public function clientsmonth(){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
            $iduser = $userloged['id'];
            $result = DB::select('SELECT COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group = "cliente" AND clients.userid = "'.$iduser.'" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }else{
            $result = DB::select('SELECT COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group = "cliente" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');            
        }
        return response()->json($result, 200); 
    }

    public function prospects(){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
            $iduser = $userloged['id'];
            $result = DB::select('SELECT COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group = "prospecto" AND clients.userid = "'.$iduser.'" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }else{
            $result = DB::select('select COUNT(id) as totalmonth from clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group = "prospecto" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');            
        }
        return response()->json($result, 200);   
    }

    public function others(){
        $tenantid = $this->GetTenant(); 
        $userloged = Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
            $iduser = $userloged['id'];
            $result = DB::select('SELECT COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group <> "prospecto" AND clients.group <> "cliente" AND clients.userid = "'.$iduser.'" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }else{
            $result = DB::select('SELECT COUNT(id) as totalmonth FROM clients WHERE MONTH(updated_at) = MONTH(CURDATE()) AND YEAR(updated_at) = YEAR(CURDATE()) AND clients.group <> "prospecto" AND clients.group <> "cliente" AND deleted_at IS NULL AND tenantid = "'.$tenantid.'"');
        }
        return response()->json($result, 200);           
    }

    public function addclient(RulesPostClient $request){
        $date = date('d/m/Y');
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $userloged['id'];

        $data = [
            'cod' => $request['cod'],
            'dateupdate' => $date,
            'tenantid' => $tenantid,
            'client' => $request['client'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'group' => $request['group'],
            'branch' => $request['branch'],
            'route' => $request['route'],                        
            'ie' => $request['ie'], 
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],
            'userid' => isset($request['userid']) ? $request['userid'] : $userloged['id'],
        ];        

        $createclient = $this->client::create($data);

        //select o ultimo client
        $field = 'client';
        $value = $request->input('client');
        $result = $this->client::where(['clients.tenantid' => $tenantid, $field => $value])->get();

        ['result' => $result];
        foreach ($result as $response) {
            $response->id;
        }

        $id = $response->id;

        if(isset($request['idrepresents'])){           
            $represent = $request->input('idrepresents');
            $id;
            for ($i=0;$i<count($represent);$i++){
                $representedclients = new CustonRepresentedclients();            
                $representedclients->clientid = $id;
                $representedclients->tenantid = $tenantid;
                $representedclients->representedid = $represent[$i];
                $representedclients->save();                        
            }   
        }

        activity('client-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'client' => $createclient->id, 'view' => '1'])
        ->log('Cadastrou o cliente ');

		return response()->json([
		    'SUCESS' => 'Cliente salvo com sucesso'
		], 200);  
    } 

   	public function upclient(RulesPostClient $request, $id){
        $date = date('d/m/Y');
        $userloged = Auth::guard('api')->user();
        $userloged['id'];
        $tenantid = $this->GetTenant();
        $data = [
            //'cod' => $request['cod'],
            'tenantid' => $tenantid,
            'dateupdate' => $date,            
            'client' => $request['client'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'group' => $request['group'],
            'branch' => $request['branch'],
            'route' => $request['route'],                        
            'ie' => $request['ie'], 
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],
            'userid' => isset($request['userid']) ? $request['userid'] : $userloged['id'],    
        ];
        $attribute = 'id';        

        $update = $this->client::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

	    if(isset($request['idrepresents'])){
	        $represent = $request->input('idrepresents');
	        $id;
	        //$represent = ["2855", "2723", "2166", "1000", "24000"];
	        $results = DB::select( DB::raw("DELETE FROM representedclients WHERE clientid = '$id'"));
	        for ($i=0;$i<count($represent);$i++){                        
	            $representadasnovas = $represent[$i];
	            $representedclients = new CustonRepresentedclients();
	            //salva dentro do update repersentadas que não foram adicionadas inicialmente e agora estão sendo adicionadas;            
                $representedclients->tenantid = $tenantid;
	            $representedclients->clientid = $id;
	            $representedclients->representedid = $representadasnovas;
	            $representedclients->save();     
	        }
	    }else{
			$results = DB::select( DB::raw("DELETE FROM representedclients WHERE clientid = '$id'"));	    	
	    }

    	if($update){
            activity('client-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'client' => $id, 'view' => '1'])
            ->log('Alterou o cliente ');

			return response()->json([
			    'SUCESS' => 'Cliente editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Cliente não alterado, este cliente não existe'
			], 422);         		
    	}

   	}

   	public function delclient(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $deleterepresentedclient = $this->representedclient::where(['tenantid' => $tenantid, 'clientid' => $id])->delete();
        $deletetalk = $this->talk::where(['tenantid' => $tenantid, 'clientid' => $id])->delete();
        $deletecontact = $this->contact::where(['tenantid' => $tenantid, 'clientid' => $id])->delete();
        $deletecomment = $this->comment::where(['tenantid' => $tenantid, 'clientid' => $id])->delete();
        $itemcl = $this->itemcl::where(['tenantid' => $tenantid, 'clientid' => $id])->delete();
        $deletecliente = $this->client::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($deletecliente){
            activity('client-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'client' => $id, 'view' => '1'])
            ->log('Deletou o cliente ');

            return response()->json([
                'SUCESS' => 'Cliente excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Cliente não excluido, este cliente não existe'
            ], 422);                 
        }

   	}

    public function delallclient(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $attribute = 'id';
        $checkclient = $request->input('checkclient');

        $delete = $this->client::whereIn('id', $checkclient)->where(['tenantid' => $tenantid])->delete();        
        $deleterepresentedclient = $this->representedclient::whereIn('clientid', $checkclient)->where(['tenantid' => $tenantid])->delete();
        $deletetalk = $this->talk::whereIn('clientid', $checkclient)->where(['tenantid' => $tenantid])->delete();
        $deletecomment = $this->comment::whereIn('clientid', $checkclient)->where(['tenantid' => $tenantid])->delete();
        $deleteitemcl = $this->itemcl::whereIn('clientid', $checkclient)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('client-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'client' => $checkclient, 'view' => '1'])
            ->log('Deletou vários clientes ');

            return response()->json([
            'SUCESS' => 'Cliente(s) excluido(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Cliente(s) não excluido, este(s) cliente(s) não existe(m)'
            ], 422);                 
        }
    }
}
