<?php
/*
    Requeriments inputs
    transport*,
    reference,
    cnpjcpf*,
    ie,
    phone1*,
    phone2,
    cep,
    street,
    number,
    complements,
    neighborhood,
    city,
    uf,
    email,
    site,
*/
namespace App\Http\Controllers\Api\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostTransport;

use App\Models\Transport as ModelTransport;

class TransportController extends Controller
{
    use GetTenantid; 

    private $transport;
    private $pagination;

    public function __construct(ModelTransport $transport){
    	$this->middleware('privilege.transport');    	
        $this->transport = $transport;
        $this->pagination = config('responses.pagination');
    }

    public function transport(Request $request){
        $tenantid = $this->GetTenant();
        $attribute = 'transports.tenantid';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'transport';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'transport';
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->transport::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->transport::whereNull('transports.deleted_at')->where(['transports.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function transportid(Request $request, $id){
        $tenantid = $this->GetTenant();
        $attribute = 'id';

        $result = $this->transport::whereNull('deleted_at')
        ->where(['tenantid' => $tenantid, 'id' => $id])
        ->get();

        return response()->json($result, 200);           
    }

    public function addtransport(RulesPostTransport $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
  
        $data = [
            'tenantid' => $tenantid,
            'transport' => $request['transport'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],            
        ];        

        $createtransport = $this->transport::create($data);

        activity('transport-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'transport' => $createtransport->id, 'view' => '1'])
        ->log('Criou a transportadora ');      

        return response()->json([
            'SUCESS' => 'Transportadora salva com sucesso'
        ], 200);  
    }

    public function uptransport(RulesPostTransport $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'transport' => $request['transport'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],            
        ];
        $attribute = 'id';        

        $update = $this->transport::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);        
        
        if($update){
            activity('transport-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'transport' => $id, 'view' => '1'])
            ->log('Alterou a transportadora ');

            return response()->json([
                'SUCESS' => 'Transportadora editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Transportadora não alterado, esta transportadora não existe'
            ], 422);                 
        }    	
    }

    public function transportselectsbox(Request $request){
        $tenantid = $this->GetTenant();

        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'transport';
        $sort = 'asc';
        $typesort = 'transport';

        $result = $this->transport::select('id', 'transport', 'reference')
        ->whereNull('deleted_at')
        ->where(['tenantid' => $tenantid])
        ->get();
      
        $search == '' ? $result = '' : $result = $result;

        return response()->json($result, 200);
    }

    public function deltransport(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->transport::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($delete){
            activity('transport-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'transport' => $id, 'view' => '1'])
            ->log('Deletou a transportadora ');    

            return response()->json([
                'SUCESS' => 'Transportadora excluida com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Transportadora não excluida, esta transportadora não existe'
            ], 422);                 
        }
    }

    public function delalltransport(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $attribute = 'id';
        $checktransport = $request->input('checktransport');

        $delete = $this->transport::whereIn('id', $checktransport)->where(['tenantid' => $tenantid])->delete();        

        if($delete){
            activity('transport-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'transport' => $checktransport, 'view' => '1'])
            ->log('Deletou várias transportadoras ');

            return response()->json([
            'SUCESS' => 'Transportadora(s) excluida(s) com sucesso'
            ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Transportadora(s) não excluidas, esta(s) representada(s) não existe(m)'
            ], 422);                 
        }
    }
}
