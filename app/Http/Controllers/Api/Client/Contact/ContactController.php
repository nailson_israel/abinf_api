<?php

namespace App\Http\Controllers\Api\Client\Contact;
/*
    Requeriments inputs
    
    contacts*,
    idclient_fk*,
    departament,
    telefone,
    email

*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostContact;
use App\Http\Traits\GetTenantid;

use App\Models\Contact;

class ContactController extends Controller
{
    use GetTenantid;
    
    private $contact;

    public function __construct(Contact $contact){
    	$this->middleware('privilege.client');
        $this->contact = $contact;
    }

    public function contactidclient(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'contact';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'contact';

        $result = $this->contact::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid, 'clientid' => $idclient])->get();

        return response()->json($result, 200);
    }

    public function contactselectbox(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $sort = 'asc';
        $typesort = 'contact';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'contact';     

        $result = $this->contact::select('id', 'contact')->whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid, 'clientid' => $idclient])->get();

        return response()->json($result, 200);        
    }

    public function contact(Request $request, $id){
        $tenantid = $this->GetTenant();

        $result = $this->contact::select('id', 'contact')
        ->whereNull('deleted_at')
        ->where(['tenantid' => $tenantid, 'id' => $id])
        ->get();

        return response()->json($result, 200);
    }

    public function addcontact(RulesPostContact $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $data = [
            'contact' => $request['contact'],
            'tenantid' => $tenantid,
            'clientid' => $request['clientid'],            
            'departament' => $request['departament'],
            'phone2' => $request['phone2'],
            'phone' => $request['phone'],
            'email' => $request['email'], 
        ];
        $create = $this->contact::create($data);

        activity('contact-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'contato' => $create->id, 'view' => '1'])
         ->log('Criou a contato ');

		return response()->json([
		    'SUCESS' => 'Contato salvo com sucesso'
		], 200); 

    }

    public function addcontactid(RulesPostContact $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'contact' => $request['contact'],
            'tenantid' => $tenantid,
            'clientid' => $idclient,            
            'departament' => $request['departament'],
            'phone' => $request['phone'],
            'phone2' => $request['phone2'],
            'email' => $request['email'],
        ];

        $create = $this->contact->create($data);

        activity('contact-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'contato' => $create->id, 'view' => '1'])
         ->log('Criou a contato ');

		return response()->json([
		    'SUCESS' => 'Contato salvo com sucesso'
		], 200); 

    }

    public function upcontact(RulesPostContact $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'contact' => $request['contact'],
            'tenantid' => $tenantid,
            'clientid' => $request['clientid'],           
            'departament' => $request['departament'],
            'phone' => $request['phone'],
            'phone2' => $request['phone2'],
            'email' => $request['email'], 
        ];
        $attribute = 'id';

        $update = $this->contact::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('contact-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'contato' => $id, 'view' => '1'])
             ->log('Alterou a contato ');

			return response()->json([
			    'SUCESS' => 'Contato editado com sucesso'
			], 200);      		
    	}else{
			return response()->json([
			    'ERROR' => 'Contato não alterado, este contato não existe'
			], 422);         		
    	}            	
    }

    public function delcontact(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->contact::where(['tenantid' => $tenantid, 'id' => $id])->delete();
        
        if($delete){
            activity('contact-delete')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'contato' => $id, 'view' => '1'])
             ->log('Deletou a contato ');

            return response()->json([
                'SUCESS' => 'Contato excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Contato não excluido, este contato não existe'
            ], 422);                 
        } 
    }

    public function delallcontact(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $checkcontact = $request->input('checkcontact');

        $delete = $this->contact::whereIn('id', $checkcontact)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('contact-deleteall')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'contato' => $checkcontact, 'view' => '1'])
             ->log('Deletou vários contato ');

            return response()->json([
            'SUCESS' => 'Contato(s) excluida(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Contato(s) não excluidas, esta(s) contato(s) não existe(m)'
            ], 422);                 
        }
    }

}
