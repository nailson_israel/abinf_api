<?php

namespace App\Http\Controllers\Api\Client\Itemcl;
/*
    Requeriments inputs
    idclient_fk*,
    itemcl,
    groupcl,
    brandcl,
    amountcl,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use App\Models\Itemcl;

class ItemclController extends Controller
{
    use GetTenantid;

    private $itemcl;

    public function __construct(Itemcl $itemcl){
    	$this->middleware('privilege.client');
        $this->itemcl = $itemcl;
    }

    public function itemcl(Request $request, $idclient){	 
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'itemcl';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'itemcl';

        $result = $this->itemcl::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid, 'clientid' => $idclient])->get();

    	return response()->json($result, 200);
    }

    public function additemcl(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'itemcl' => $request->input('itemcl'),  
            'clientid' => $request->input('clientid'),                       
            'groupcl' => $request->input('groupcl'),
            'brandcl' => $request->input('brandcl'),
            'amountcl' => $request->input('amountcl'),             
        ];
        $create = $this->itemcl::create($data);

        activity('itemcl-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itemcl' => $create->id, 'view' => '1'])
        ->log('Criou o item do cliente ');

		return response()->json([
		    'SUCESS' => 'Iten do cliente salvo com sucesso'
		], 200);          
    }

    public function additemclid(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'itemcl' => $request->input('itemcl'),  
            'clientid' => $idclient,                       
            'groupcl' => $request->input('groupcl'),
            'brandcl' => $request->input('brandcl'),
            'amountcl' => $request->input('amountcl'),             
        ];
        $create = $this->itemcl::create($data);

        activity('itemcl-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itemcl' => $create->id, 'view' => '1'])
        ->log('Criou o item do cliente ');

        return response()->json([
            'SUCESS' => 'Iten do cliente salvo com sucesso'
        ], 200); 
    }

    public function upitemcl(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,            
            'itemcl' => $request['itemcl'],
            'clientid' => $request['clientid'],
            'groupcl' => $request['groupcl'],
            'brandcl' => $request['brandcl'],
            'amountcl' => $request['amountcl'], 
        ];

        $update = $this->itemcl::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('itemcl-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itemcl' => $id, 'view' => '1'])
            ->log('Alterou o item do cliente ');

			return response()->json([
			    'SUCESS' => 'Iten do cliente editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Iten do cliente não alterado, este iten do cliente não existe'
			], 422);         		
    	}  
    }

    public function delitemcl(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
    
        $delete = $this->itemcl::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($delete){
            activity('itemcl-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itemcl' => $id, 'view' => '1'])
            ->log('Deletou o item do cliente ');

            return response()->json([
                'SUCESS' => 'Iten do cliente excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Iten do cliente não excluido, este Iten do cliente não existe'
            ], 422);                 
        } 
    }

    public function delallitemcl(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';
        $checkitemcl = $request->input('checkitemcl');

        $delete = $this->itemcl::whereIn('id', $checkitemcl)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('itemcl-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'itemcl' => $checkitemcl, 'view' => '1'])
            ->log('Deletou vários itens do cliente ');

            return response()->json([
            'SUCESS' => 'Item(ns) excluido(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Item(ns) não excluido, este(s) Item(ns) não existe(m)'
            ], 422);                 
        }   
    }
}
