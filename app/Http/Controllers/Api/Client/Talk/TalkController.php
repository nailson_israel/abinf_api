<?php

namespace App\Http\Controllers\Api\Client\Talk;
/*
    Requeriments inputs
    
    idclient_fk*,
    subject*,
    date*,
    type*,
    contactcl,
    contactante,
    represented,
    description*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostTalk;
use App\Http\Traits\GetTenantid;
use Illuminate\Support\Facades\Auth;
//Repository
use App\Models\Talk;

class TalkController extends Controller
{
	use GetTenantid;

    private $talk;
    private $pagination;
    private $limittalk;

    public function __construct(Talk $talk){
    	$this->middleware('privilege.client');
        $this->talk = $talk;
        $this->pagination = config('responses.pagination');
        $this->limittalk = config('responses.limittalk');
    }

    public function talk(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'talks.created_at';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'date';
        $type = $request['type'] != null ? $request['type'] : 'default';
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select(
            'date', 'hour', 'talks.tenantid', 'talks.id', 'talks.clientid', 'isactivity', 
            'subject', 'type', 'contactcl', 'talks.userid as userid', 'talks.represented', 
            'description', 'legends', 'descriptionlegend', 'users.name', 'users.user as user', 
            'contacts.email as emailcontact', 'representeds.represented as rep', 'contacts.contact', 
            'clients.client', 'talks.updated_at'
        ) 
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'activity' ? $query->where('isactivity', '1') : $query->where('isactivity', '0'); 
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0m'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['talks.tenantid' => $tenantid, ])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->talk::whereNull('talks.deleted_at')->where(['talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])
                ->when($type, function($query) use ($type){
            if($type != 'default'){
                $type == 'activity' ? $query->where('isactivity', '1') : $query->where('isactivity', '0'); 
            }
        })->count();
        $totalsearch = $result->count();

        $datecurrent = date('Y-m-d');
        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch, 'datecurrent' => $datecurrent], 200);
    }

    public function addtalk(RulesPostTalk $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $userloged['id'];
        $data = [
            'tenantid' => $tenantid,
            'clientid' => $request['clientid'],            
            'subject' => $request['subject'],            
            'date' => $request['date'],
            'hour' => $request['hour'],
            'type' => $request['type'],
            'contactcl' => $request['contactcl'],
            'represented' => $request['represented'],
            'description' => $request['description'],
            'userid' => $userloged['id'],
            'isactivity' => $request['isactivity'],
            'notification' => $request['notification']
        ];
        $create = $this->talk::create($data);        

        if($request['isactivity'] === 1){
            activity('talk-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity' => $create->id, 'view' => '1'])
            ->log('Criou a atividade do cliente ');
        }else{
            activity('agenda-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'agenda' => $create->id, 'view' => '1'])
            ->log('Criou a agenda do cliente ');
        }

		return response()->json([
		    'SUCESS' => 'Conversa salva com sucesso'
		]); 
    }

    public function activity(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $attribute = 'talks.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'talks.created_at';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'date';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select(
            'date', 'hour', 'talks.tenantid', 'talks.id', 'talks.clientid', 'isactivity', 
            'subject', 'type', 'contactcl', 'talks.userid as userid', 'talks.represented', 
            'description', 'legends', 'descriptionlegend', 'users.name', 'users.user as user', 
            'contacts.email as emailcontact', 'representeds.represented as rep', 'contacts.contact', 
            'clients.client', 'talks.updated_at'
        )
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '1', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();


        $total = $this->talk::whereNull('talks.deleted_at')->where(['isactivity' => '1', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function activityall(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $attribute = 'talks.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'talks.created_at';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'date';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select(
            'date', 'hour', 'talks.tenantid', 'talks.id', 'talks.clientid', 'isactivity', 
            'subject', 'type', 'contactcl', 'talks.userid as userid', 'talks.represented', 
            'description', 'legends', 'descriptionlegend', 'users.name', 'users.user as user', 
            'contacts.email as emailcontact', 'representeds.represented as rep', 'contacts.contact', 
            'clients.client', 'talks.updated_at'
        ) 
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '1', 'talks.tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->talk::whereNull('talks.deleted_at')->where(['isactivity' => '1', 'talks.tenantid' => $tenantid])->count();

        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function agenda(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $attribute = 'talks.tenantid';

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'talks.created_at';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'date';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select(
            'date', 'hour', 'talks.tenantid', 'talks.id', 'talks.clientid', 'isactivity', 
            'subject', 'type', 'contactcl', 'talks.userid as userid', 'talks.represented', 
            'description', 'legends', 'descriptionlegend', 'users.name', 'users.user as user', 
            'contacts.email as emailcontact', 'representeds.represented as rep', 'contacts.contact', 
            'clients.client', 'talks.updated_at'
        ) 
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '0', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->talk::whereNull('talks.deleted_at')->where(['isactivity' => '0', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function agendaall(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'talks.created_at';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'date';
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select(
            'date', 'hour', 'talks.tenantid', 'talks.id', 'talks.clientid', 'isactivity', 
            'subject', 'type', 'contactcl', 'talks.userid as userid', 'talks.represented', 
            'description', 'legends', 'descriptionlegend', 'users.name', 'users.user as user', 
            'contacts.email as emailcontact', 'representeds.represented as rep', 'contacts.contact', 
            'clients.client', 'talks.updated_at'
        ) 
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                if($typesearch === 'date'){
                    $date = str_replace('/', '-', $search);
                    $search = date('Y-m-d', strtotime($date));
                }
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '0', 'talks.tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->talk::whereNull('talks.deleted_at')->where(['isactivity' => '0', 'talks.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function activitypanel(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $sort = 'desc';
        $typesort = 'talks.created_at';

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select('date', 'hour', 'client', 'descriptionlegend', 'subject', 'type')
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '1', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])->limit($this->limittalk)->get();        

        return response()->json($result, 200);
    }

    public function agendapanel(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $sort = 'desc';
        $typesort = 'talks.created_at';

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->talk::select('date', 'hour', 'client', 'descriptionlegend', 'subject', 'type')
        ->leftJoin('legends', 'talks.type', '=', 'legends.legends')
        ->leftJoin('clients', 'talks.clientid', '=', 'clients.id')
        ->leftJoin('users', 'talks.userid', '=', 'users.id')
        ->leftJoin('representeds', 'talks.represented', '=', 'representeds.id')
        ->leftJoin('contacts', 'talks.contactcl', '=', 'contacts.id')
        ->whereNull('talks.deleted_at')->orderby($typesort, $sort)
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('talks.userid', $iduser);
            }
        })
        ->where(['isactivity' => '0', 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])->limit($this->limittalk)->get();        

        return response()->json($result, 200);
    }

    public function addtalkid(RulesPostTalk $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $userloged['id'];
        $data = [
            'tenantid' => $tenantid,
            'clientid' => $idclient,            
            'subject' => $request['subject'],            
            'date' => $request['date'],
            'hour' => $request['hour'],
            'type' => $request['type'],
            'contactcl' => $request['contactcl'],
            'represented' => $request['represented'],
            'description' => $request['description'],
            'userid' => $request['userid'],
            'isactivity' => $request['isactivity'],
            'notification' => $request['notification']
        ];
        $create = $this->talk::create($data);        

        if($request['isactivity'] == 1){
            activity('talk-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity' => $create->id, 'view' => '1'])
            ->log('Criou a atividade do cliente ');
        }else{
            activity('agenda-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'agenda' => $create->id, 'view' => '1'])
            ->log('Criou a agenda do cliente ');
        }
        return response()->json([
            'SUCESS' => 'Conversa salva com sucesso'
        ], 200); 
    }

    public function uptalk(RulesPostTalk $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
        $userloged['id'];
        $data = [
            'tenantid' => $tenantid,
            'clientid' => $request['clientid'],            
            'subject' => $request['subject'],            
            'date' => $request['date'],
            'hour' => $request['hour'],
            'type' => $request['type'],
            'contactcl' => $request['contactcl'],
            'represented' => $request['represented'],
            'description' => $request['description'],
            'userid' => $request['userid'],
            'isactivity' => $request['isactivity'],
            'notification' => $request['notification']
        ];
        $attribute = 'id';

        $update = $this->talk::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            if($request['isactivity'] == 1){
                activity('talk-update')
                ->causedBy($userloged)
                ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity' => $id, 'view' => '1'])
                ->log('Alterou a atividade do cliente ');
            }else{
                activity('agenda-update')
                ->causedBy($userloged)
                ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'agenda' => $id, 'view' => '1'])
                ->log('Alterou a agenda do cliente ');
            }
			return response()->json([
			    'SUCESS' => 'Conversa editada com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Conversa não alterada, esta conversa não existe'
			], 422);         		
    	} 
    }

    public function upstatus(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        if ($request['type'] == 'v') {
            $type = 'vr';
        }else if($request['type'] == 'e'){
            $type = 'ef';
        }else if($request['type'] == 'l'){
            $type = 'lf';
        }else if($request['type'] == 'w'){
            $type = 'ew';
        }else{
            $type = $request['type'];
        }

        if($request['isactivity']){
            $data = [
                'isactivity' => 1,
                'type' => $type,
            ];
            $attribute = 'id';

            $update = $this->talk::where(['tenantid' => $tenantid, 'id' => $id])
            ->whereNull('deleted_at')
            ->update($data);

            if($update){
                if($request['isactivity'] == 1){
                    activity('talk-update-status')
                    ->causedBy($userloged)
                    ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity' => $id, 'view' => '1'])
                    ->log('Alterou o status atividade do cliente ');
                }else{
                    activity('agenda-update-status')
                    ->causedBy($userloged)
                    ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'agenda' => $id, 'view' => '1'])
                    ->log('Alterou o status agenda do cliente ');
                }             
                return response()->json([
                    'SUCESS' => 'Status alterado com sucesso'
                ], 200);             
            }else{
                return response()->json([
                    'ERROR' => 'Status não alterado'
                ], 422);                 
            }
        }else{
                return response()->json([
                    'ERROR' => 'Status não alterado'
                ]);               
        }
    }

    public function deltalk(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $attribute = 'id';

        $delete = $this->talk::where(['tenantid' => $tenantid, 'id' => $id])->delete();
        
        if($delete){
            activity('talk-agenda-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity_agenda' => $id, 'view' => '1'])
            ->log('Deletou atividade ou agenda do cliente ');
        
            return response()->json([
                'SUCESS' => 'Conversa excluida com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Conversa não excluida, esta conversa não existe'
            ], 422);                 
        }     	
    }

    public function delalltalk(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        $attribute = 'id';
        $checkactivity = $request->input('checkactivity');

        $delete = $this->talk::whereIn('id', $checkactivity)->where(['tenantid' => $tenantid])->delete();        

        if($delete){
            activity('talk-agenda-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'activity_agenda' => $checkactivity, 'view' => '1'])
            ->log('Deletou várias atividades ou agendas do cliente ');

            return response()->json([
            'SUCESS' => 'Atividade(s) excluido(s) com sucesso'
            ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Atividade(s) não excluido, esta(s) Atividades(s) não existe(m)'
            ], 422);                 
        }   
    }
}
