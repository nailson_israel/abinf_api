<?php

namespace App\Http\Controllers\Api\Client\Fillselects;
/*
    Requeriments inputs
    
    routeclient*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostRouteclient;
use App\Http\Traits\GetTenantid;

use App\Models\Routeclient;

class RouteclientController extends Controller
{
    use GetTenantid;

    private $routeclient;

    public function __construct(Routeclient $routeclient){
        $this->middleware('privilege.fillselects');
        $this->routeclient = $routeclient;
    }

    public function routeclient(Request $request){
      $tenantid = $this->GetTenant();
      $tenantid = $this->GetTenant();
      $sort = $request['sort'] != null ? $request['sort'] : 'asc';
      $typesort = $request['typesort'] != null ? $request['typesort'] : 'routeclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'routeclient';

      $result = $this->routeclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
      })
      ->where(['tenantid' => $tenantid])->get();

    	return response()->json($result, 200);
    }

    public function routeclientselectsbox(Request $request){
      $tenantid = $this->GetTenant();
      $sort = 'asc';
      $typesort = 'routeclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'routeclient';     

      $result = $this->routeclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
        if($typesearch){
            $query->where($typesearch, 'like', "%$search%");
        }
      })
      ->where(['tenantid' => $tenantid])->get();

      return response()->json($result, 200);       
    }

    public function addrouteclient(RulesPostRouteclient $request){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = ['tenantid' => $tenantid, 'routeclient' =>$request->input('routeclient')];

      $create = $this->routeclient::create($data);

      activity('routeclient-create')
      ->causedBy($userloged)
      ->withProperties(['tenantid' => $tenantid, 'user' => $userloged, 'routeclient' => $create->id, 'view' => '1'])
      ->log('Criou a rota de cliente ');

  		return response()->json([
  		    'SUCESS' => 'Rota salva com sucesso'
  		], 200);
    }

    public function uprouteclient(RulesPostRouteclient $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = ['tenantid' => $tenantid, 'routeclient' => $request->input('routeclient')];

      $update = $this->routeclient::where(['tenantid' => $tenantid, 'id' => $id])
      ->whereNull('deleted_at')
      ->update($data);

      if($update){
        activity('routeclient-update')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'routeclient' => $id, 'view' => '1'])
        ->log('Alterou a rota de cliente ');

          return response()->json([
              'SUCESS' => 'Rota editada com sucesso'
          ], 200);             
      }else{
          return response()->json([
              'ERROR' => 'Rota não alterada, esta rota não existe'
          ], 422);                 
      }    	
    }

    public function delrouteclient(Request $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $delete = $this->routeclient::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
        activity('routeclient-delete')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'routeclient' => $id, 'view' => '1'])
        ->log('Deletou a rota de cliente ');

  			return response()->json([
  			    'SUCESS' => 'Rota excluida com sucesso'
  			], 200);        		
    	}else{
  			return response()->json([
  			    'ERROR' => 'Rota não excluida, esta rota não existe'
  			], 422);         		
    	}
    }
}
