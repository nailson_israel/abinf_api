<?php

namespace App\Http\Controllers\Api\Client\Fillselects;
/*
    Requeriments inputs
    
    departamentclient*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostDepartamentcontact;
use App\Http\Traits\GetTenantid;

use App\Models\Departamentcontact;

class DepartamentcontactController extends Controller
{
    use GetTenantid;

    private $departamentcontact;

    public function __construct(Departamentcontact $departamentcontact){
        $this->middleware('privilege.fillselects');
        $this->departamentcontact = $departamentcontact;
    }

    public function departamentcontact(Request $request){
      $tenantid = $this->GetTenant();
      $sort = $request['sort'] != null ? $request['sort'] : 'asc';
      $typesort = $request['typesort'] != null ? $request['typesort'] : 'departamentcontact';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'departamentcontact';

      $result = $this->departamentcontact::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
      })
      ->where(['tenantid' => $tenantid])->get();

      return response()->json($result, 200);
    }

    public function departamentcontactselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $sort = 'asc';
        $typesort = 'departamentcontact';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'departamentcontact';     

        $result = $this->departamentcontact::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
        })
        ->where(['tenantid' => $tenantid])->get();

        return response()->json($result, 200);       
    }

    public function adddepartamentcontact(RulesPostDepartamentcontact $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'departamentcontact' => $request->input('departamentcontact')];
        $create = $this->departamentcontact::create($data);

        activity('departamentcontact-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'departamentcontact' => $create->id, 'view' => '1'])
        ->log('Criou o departamento de contato ');

		return response()->json([
		    'SUCESS' => 'Departamento salvo com sucesso'
		], 200);  
    }

    public function updepartamentcontact(RulesPostDepartamentcontact $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'departamentcontact' => $request->input('departamentcontact')];
        
        $update = $this->departamentcontact::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('departamentcontact-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'departamentcontact' => $id, 'view' => '1'])
            ->log('Alterou o departamento de contato ');

            return response()->json([
                'SUCESS' => 'Departamento editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Departamento não alterado, este departamento não existe'
            ], 422);                 
        }
    }

    public function deldepartamentcontact(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->departamentcontact::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('departamentcontact-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'departamentcontact' => $id, 'view' => '1'])
            ->log('Deletou o departamento de contato ');

			return response()->json([
			    'SUCESS' => 'Departamento excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Departamento não excluido, este departamento não existe'
			], 422);         		
    	}	
    }
}
