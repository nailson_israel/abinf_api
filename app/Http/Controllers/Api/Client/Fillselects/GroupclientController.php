<?php

namespace App\Http\Controllers\Api\Client\Fillselects;
/*
    Requeriments inputs
    groupclient*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostGroupclient;
use App\Http\Traits\GetTenantid;

use App\Models\Groupclient;

class GroupclientController extends Controller
{
    use GetTenantid;

    private $groupclient;

    public function __construct(Groupclient $groupclient){
        $this->middleware('privilege.fillselects');
        $this->groupclient = $groupclient;
    }

   	public function groupclient(Request $request){
      $tenantid = $this->GetTenant();
      $sort = $request['sort'] != null ? $request['sort'] : 'asc';
      $typesort = $request['typesort'] != null ? $request['typesort'] : 'groupclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'groupclient';

      $result = $this->groupclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
      })
      ->where(['tenantid' => $tenantid])->get();

   		return response()->json($result, 200);
   	}

    public function groupclientselectsbox(Request $request){
      $tenantid = $this->GetTenant();
      $sort = 'asc';
      $typesort = 'groupclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'groupclient';     

      $result = $this->groupclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
        if($typesearch){
            $query->where($typesearch, 'like', "%$search%");
        }
      })
      ->where(['tenantid' => $tenantid])->get();

      return response()->json($result, 200);     
    }

   	public function addgroupclient(RulesPostGroupclient $request){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = [
        'tenantid' => $tenantid, 
        'groupclient' => $request->input('groupclient'),
      ];      
      $create = $this->groupclient::create($data);

      activity('groupclient-create')
      ->causedBy($userloged)
      ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'grupoclient' => $create->id, 'view' => '1'])
      ->log('Criou o grupo de cliente ');


  		return response()->json([
  		    'SUCESS' => 'Grupo salvo com sucesso'
  		], 200);  	   		
   	}

   	public function upgroupclient(RulesPostGroupclient $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = [
        'tenantid' => $tenantid, 
        'groupclient' => $request->input('groupclient'),
      ];      

      $update = $this->groupclient::where(['tenantid' => $tenantid, 'id' => $id])
      ->whereNull('deleted_at')
      ->update($data);

      if($update){
          activity('groupclient-update')
          ->causedBy($userloged)
          ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'grupoclient' => $id, 'view' => '1'])
          ->log('Alterou o grupo de cliente ');

          return response()->json([
              'SUCESS' => 'Grupo editado com sucesso'
          ], 200);             
      }else{
          return response()->json([
              'ERROR' => 'Grupo não alterado, este grupo não existe'
          ], 422);                 
      }
   	}

   	public function delgroupclient(Request $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();
      
      $delete = $this->groupclient::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
        activity('groupclient-delete')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'grupoclient' => $id, 'view' => '1'])
        ->log('Deletou o grupo de cliente ');

  			return response()->json([
  			    'SUCESS' => 'Grupo excluido com sucesso'
  			], 200);        		
    	}else{
  			return response()->json([
  			    'ERROR' => 'Grupo não excluido, este grupo não existe'
  			], 422);         		
    	}	
   	}
}
