<?php

namespace App\Http\Controllers\Api\Client\Fillselects;
/*
    Requeriments inputs
    
    branchclient*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostBranchclient;

use App\Models\Branchclient;

class BranchclientController extends Controller
{
    use GetTenantid;

    private $branchclient;

    public function __construct(Branchclient $branchclient){
        $this->middleware('privilege.fillselects');
        $this->branchclient = $branchclient;
    }

   	public function branchclient(Request $request){
      $tenantid = $this->GetTenant();
      $sort = $request['sort'] != null ? $request['sort'] : 'asc';
      $typesort = $request['typesort'] != null ? $request['typesort'] : 'branchclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'branchclient';

      $result = $this->branchclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
      })
      ->where(['tenantid' => $tenantid])->get();

      return response()->json($result, 200);
   	}

    public function branchclientselectsbox(Request $request){
      $tenantid = $this->GetTenant();
      $sort = 'asc';
      $typesort = 'branchclient';
      $search = $request['search'];
      $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'branchclient';     

      $result = $this->branchclient::whereNull('deleted_at')->orderby($typesort, $sort)
      ->when($search, function($query) use ($search, $typesearch){
          if($typesearch){
              $query->where($typesearch, 'like', "%$search%");
          }
      })
      ->where(['tenantid' => $tenantid])->get();

      return response()->json($result, 200);     
    }

   	public function addbranchclient(RulesPostBranchclient $request){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = ['tenantid' => $tenantid, 'branchclient' => $request->input('branchclient')];
      $create = $this->branchclient::create($data);

      activity('branch-create')
       ->causedBy($userloged)
       ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'branch' => $create->id, 'view' => '1'])
       ->log('Criou a marca ');

  		return response()->json([
  		    'SUCESS' => 'Ramo salvo com sucesso'
  		], 200); 
   	}

   	public function upbranchclient(RulesPostBranchclient $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $data = ['tenantid' => $tenantid, 'branchclient' => $request->input('branchclient')];
      $attribute = 'id';

      $update = $this->branchclient::where(['tenantid' => $tenantid, 'id' => $id])
      ->whereNull('deleted_at')
      ->update($data);

      if($update){
          activity('branch-update')
           ->causedBy($userloged)
           ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'branch' => $id, 'view' => '1'])
           ->log('Alterou a marca ');

          return response()->json([
              'SUCESS' => 'Ramo editado com sucesso'
          ], 200);             
      }else{
          return response()->json([
              'ERROR' => 'Ramo não alterado, este rota não existe'
          ], 422);                 
      }
   	}

   	public function delbranchclient(Request $request, $id){
      $tenantid = $this->GetTenant();
      $userloged = \Auth::guard('api')->user();

      $attribute = 'id';

      $delete = $this->branchclient::where(['tenantid' => $tenantid, 'id' => $id])->delete();
        
      if($delete){
        activity('branch-delete')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'branch' => $id, 'view' => '1'])
         ->log('Deletou a marca ');

        return response()->json([
            'SUCESS' => 'Ramo excluido com sucesso'
        ], 200);        		
      }else{
        return response()->json([
            'ERROR' => 'Ramo não excluido, este rota não existe'
        ], 422);         		
      }
   	}
}
