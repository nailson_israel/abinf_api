<?php

namespace App\Http\Controllers\Api\Client\Logoclient;
/*
    Requeriments inputs
    
    logomarca*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetTenantid;

use App\Models\Client as Clientcuston;
use App\Models\Tenant;
use \App\Models\Represented;

use File;


class LogomarcaController extends Controller
{
	use GetTenantid;

	private $client;
	private $tenant;
	private $represented;

    public function __construct(Clientcuston $client, Tenant $tenant, Represented $represented){
    	$this->middleware('privilege.logomarca');
    	$this->client = $client;
    	$this->tenant = $tenant;
    	$this->represented = $represented;
    }

    public function logomarca(Request $request, $id){
    	$tenantid = $this->GetTenant();

    	$result = $this->client::select('id', 'client', 'logo')
    	->whereNull('delete_at')
    	->where(['tenantid' => $tenantid, 'id' => $id])
    	->get()

        return response()->json($result, 200); 
    }

    //logo marca with post
    public function addlogomarca(Request $request){
    	$tenantid = $this->GetTenant();
		$id = $request->input('id');

    	if($request->hasFile('image')){
			$tenantid = Auth::user()->tenantid;

			$result = $this->tenant::whereNull('delete_at')
			->where(['tenantid' => $tenantid, 'id' => $id])->get(); 

	        foreach ($result as $response) {
	           $enterprise =  $response->prefix;
	        }

    		 $result = $this->client::select('id', 'client', 'logo')
    		 ->whereNull('delete_at')->where(['tenantid' => $tenantid, 'id' => $id]);
    		 ->get();

    		foreach ($result as $response) {
				$client = $response->client;
    		}

    		 $client = $response->client;

			$folder = $enterprise.'/';
	        $path = 'image/enterprise/';        
	        $directory = $path.$folder.'clients/';

	       	if(!is_dir($directory)){
				mkdir($directory, 0755, true); 
	        }	

	 		$file = $request->file('image');
	 		$namelogo = $request->file('image')->getClientOriginalName(); 
			$array = explode('.',$namelogo);
			$nome_arquivo = md5($array[0]);
			$nome_arquivo .= '.'.$array[1];

    		$logoantiga = $this->client::select('id', 'client', 'logo')
    		 ->whereNull('delete_at')->where(['tenantid' => $tenantid, 'id' => $id]);
    		 ->get();

			if($logoantiga != '[]'){
				foreach ($logoantiga as $logoatg) {
					$logoatg->logo;
				}

				if($logoatg->logo){
					File::delete($directory.$logoatg->logo);	
				}
			}

			$attribute = 'id';
			$data = ['tenantid' => $tenantid, 'logo' => $nome_arquivo];
			$update = $this->client::->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

			//$update2 = $this->represented->update($data, $id);

			//$client = Client::where('idclient', $idclient)->update(['logo' => $nome_arquivo]);

			list($width, $height, $type, $attr) = getimagesize($file);
			$image = Image::make($request->file('image')->getRealPath());
			$image->resize(250, 150, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			$image->resizeCanvas(250, 150, 'center', false, array(255, 255, 255, 0));		
	   		$image->save($directory.$nome_arquivo);				
			
            return response()->json([
                'SUCESS' => 'Logo do cliente salvo com sucesso'
            ], 200); 			
    	}else{
            return response()->json([
                'ERRO' => 'Nenhuma imagem selecionada'
            ], 422);     		
    	}	
    }

    //logo marca with put update
    public function uplogomarca(Request $request, $id){
    	$tenantid = $this->GetTenant();
		$id = $id;

    	if($request->hasFile('image')){
			$tenantid = Auth::user()->tenantid;

			$result = $this->tenant::whereNull('delete_at')
			->where(['tenantid' => $tenantid, 'id' => $id])->get(); 

	        foreach ($result as $response) {
	           $enterprise =  $response->prefix;
	        }

    		 $result = $this->client::select('id', 'client', 'logo')
    		 ->whereNull('delete_at')->where(['tenantid' => $tenantid, 'id' => $id]);
    		 ->get();

    		foreach ($result as $response) {
				$client = $response->client;
    		}

    		 $client = $response->client;

			$folder = $enterprise.'/';
	        $path = 'image/enterprise/';        
	        $directory = $path.$folder.'clients/';

	       	if(!is_dir($directory)){
				mkdir($directory, 0755, true); 
	        }	

	 		$file = $request->file('image');
	 		$namelogo = $request->file('image')->getClientOriginalName(); 
			$array = explode('.',$namelogo);
			$nome_arquivo = md5($array[0]);
			$nome_arquivo .= '.'.$array[1];

    		$logoantiga = $this->client::select('id', 'client', 'logo')
    		 ->whereNull('delete_at')->where(['tenantid' => $tenantid, 'id' => $id]);
    		 ->get();

			if($logoantiga != '[]'){
				foreach ($logoantiga as $logoatg) {
					$logoatg->logo;
				}

				if($logoatg->logo){
					File::delete($directory.$logoatg->logo);	
				}
			}

			$attribute = 'id';
			$data = ['tenantid' => $tenantid, 'logo' => $nome_arquivo];
			$update = $this->client::->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

			//$client = Client::where('idclient', $idclient)->update(['logo' => $nome_arquivo]);

			list($width, $height, $type, $attr) = getimagesize($file);
			$image = Image::make($request->file('image')->getRealPath());
			$image->resize(250, 150, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			$image->resizeCanvas(250, 150, 'center', false, array(255, 255, 255, 0));		
	   		$image->save($directory.$nome_arquivo);				
			
            return response()->json([
                'SUCESS' => 'Logo do cliente salvo com sucesso'
            ], 200); 			
    	}else{
            return response()->json([
                'ERRO' => 'Nenhuma imagem selecionada'
            ], 422);     		
    	}
    }
}
