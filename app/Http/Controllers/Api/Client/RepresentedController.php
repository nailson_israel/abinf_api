<?php
/*
    Requeriments inputs
    transport*,
    reference,
    cnpjcpf*,
    group,
    branch,
    route,
    ie,
    phone1*,
    phone2,
    cep,
    street,
    number,
    complements,
    neighborhood,
    city,
    uf,
    email,
    site,
    logo,
    textfx,
*/
namespace App\Http\Controllers\Api\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostTextfix;
use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostRepresented;

use App\Models\Representedclients;
use App\Models\Represented as ModelRepresented;
use App\Models\Product;

class RepresentedController extends Controller
{
    use GetTenantid; 

    private $client;
    private $representedclient;
    private $represented;
    private $product;
    private $pagination;

    public function __construct(Representedclients $representedclient, ModelRepresented $represented, Product $product){
    	$this->middleware('privilege.represented');    	
        $this->representedclient = $representedclient;
        $this->represented = $represented;
        $this->product = $product;
        $this->pagination = config('responses.pagination');
    }

    public function represented(Request $request){
        $tenantid = $this->GetTenant();
        $attribute = 'representeds.tenantid';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'represented';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'represented';
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->represented::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->represented::whereNull('representeds.deleted_at')->where(['representeds.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200); 
    }

    public function productrepresented(Request $request, $idrep){
        $tenantid = $this->GetTenant();
        $attribute = 'products.tenantid';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'product';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'product';
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->product::select('products.id', 'products.cod', 'product', 'ipi', 'unit', 'price', 'commission', 'groupprod', 'groupproduct', 'representeds.id as idrep', 'represented')
        ->leftJoin('groupproducts', 'products.groupprod', '=', 'groupproducts.id')
        ->leftJoin('representeds', 'products.idrep', '=', 'representeds.id')
        ->whereNull('products.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['products.tenantid' =>$tenantid, 'idrep' => $idrep])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->product::whereNull('products.deleted_at')->where(['products.tenantid' => $tenantid, 'idrep' => $idrep])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200); 

        return response()->json($result, 200);
    }

    public function representedid(Request $request, $id){
        $tenantid = $this->GetTenant();
        $attribute = 'id';

        $result = $this->represented::withTrashed()
        ->where(['tenantid' => $tenantid, 'id' => $id])
        ->get();

        return response()->json($result, 200);           
    }

    public function logorepresented(Request $request, $id){
        $tenantid = $this->GetTenant();

        $result = $this->represented::select('logo', 'tenantid')->whereNull('deleted_at')
        ->where(['tenantid' => $tenantid, 'id' => $id])
        ->get();

        return response()->json($result, 200);  
    }

    public function updatelogorepresented(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [                                         
            'logo' => $request['logo']        
        ];
        $attribute = 'id';        

        $update = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);
        
        if($update){
            activity('represented-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
            ->log('Alterou a logo da representada ');

            return response()->json([
                'SUCESS' => 'Logo editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Logo não alterado'
            ], 422);                 
        }        
    }

    public function addrepresented(RulesPostRepresented $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'represented' => $request['represented'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'group' => 'represented',
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],            
        ];        

        $createrepresented = $this->represented::create($data);

        activity('represented-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $createrepresented->id, 'view' => '1'])
        ->log('Criou a representada ');

        return response()->json([
            'SUCESS' => 'representada salva com sucesso'
        ] ,200);  
    }

    public function uprepresented(RulesPostRepresented $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'represented' => $request['represented'],
            'reference' => $request['reference'],            
            'cnpjcpf' => $request['cnpjcpf'],
            'group' => 'represented',
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'cep' => $request['cep'],                                                
            'street' => $request['street'],                                              
            'number' => $request['number'],        
            'complements' => $request['complements'], 
            'neighborhood' => $request['neighborhood'], 
            'city' => $request['city'],
            'uf' => $request['uf'],
            'email' => $request['email'],
            'site' => $request['site'],            
        ];
        $attribute = 'id';        

        $update = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('represented-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
            ->log('Alterou a representada ');

            return response()->json([
                'SUCESS' => 'representada editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'representada não alterado, esta representada não existe'
            ], 422);                 
        }
    }

    public function delrepresented(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $delete = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($delete){
            activity('represented-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
            ->log('Deletou a representada ');

            return response()->json([
                'SUCESS' => 'representada excluida com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'representada não excluida, esta representada não existe'
            ], 422);                 
        }
    }

    public function delallrepresented(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $checkrepresented = $request->input('checkrepresented');

        $delete = $this->represented::whereIn('id', $checkrepresented)->where(['tenantid' => $tenantid])->delete();        

        if($delete){
            activity('represented-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $checkrepresented, 'view' => '1'])
            ->log('Deletou várias representada ');

            return response()->json([
            'SUCESS' => 'representada(s) excluida(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'representada(s) não excluidas, esta(s) representada(s) não existe(m)'
            ], 422);                 
        }
    }

    //da parte das representeds
    public function representedclient(Request $request, $id){
        $tenantid = $this->GetTenant();
        $attribute = 'representedclients.tenantid';

        $result = $this->representedclient::select('clients.client')
        ->leftJoin('representeds', 'representeds.id', '=', 'representedclients.representedid')
        ->leftJoin('clients', '.clients.id', '=', 'representedclients.clientid')
        ->whereNull('representedclients.deleted_at')
        ->where(['representedclients.tenantid' => $tenantid, 'representedclients.representedid' => $id])->get();

      	return response()->json($result, 200); 
    }

    public function representedselectbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'represented';
        $sort = 'asc';
        $search = $request['search'];
        $typesort = 'represented';

        $result  = $this->represented::select('id', 'represented', 'reference')
        ->whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })        
        ->where(['tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200);       
    }

    public function representedcl(Request $request, $id){
        $tenantid = $this->GetTenant();
       
        $result = $this->representedclient::select('representedid as id', 'represented')
        ->leftJoin('representeds', 'representedclients.representedid', '=', 'representeds.id')
        ->whereNull('representedclients.deleted_at')
        ->where(['representedclients.tenantid' => $tenantid, 'representedclients.clientid' => $id])
        ->get();

        return response()->json($result, 200);    	
    }


    public function textfix(Request $request, $id){
        $tenantid = $this->GetTenant();

        $result = $this->represented::withTrashed()->select('textfix', 'id', 'represented', 'deleted_at as dlt')
        ->where(['tenantid' => $tenantid, 'id' => $id])
        ->get();

        return response()->json($result, 200);
    }

    public function addtextfix(RulesPostTextfix $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $id = $request['id'];

        $datarepresented = ['textfix' => $request['textfix']];
        $updaterepresented = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])->update($datarepresented);  

        activity('textfix-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
        ->log('Criou o texto da representada ');     

		return response()->json([
		    'SUCESS' => 'Texto salvo com sucesso'
		], 200);            
    }

    public function uptextfix(RulesPostTextfix $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $attributerepresented = 'id';
        $datarepresented = ['textfix' => $request['textfix']];
        $updaterepresented = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])->update($datarepresented);  

        activity('textfix-update')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
        ->log('Alterou o texto da representada ');

        return response()->json([
            'SUCESS' => 'Texto  editado com sucesso'
        ], 200);  
    }

    public function deltextfix(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $datarepresented = ['textfix' => NULL];
        $updaterepresented = $this->represented::where(['tenantid' => $tenantid, 'id' => $id])->update($datarepresented);  

        activity('textfix-delete')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'represented' => $id, 'view' => '1'])
        ->log('Alterou o texto da representada ');  

		return response()->json([
		    'SUCESS' => 'Texto Excluido com sucesso'
		]);
    }
}
