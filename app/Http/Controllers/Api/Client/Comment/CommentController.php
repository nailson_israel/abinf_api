<?php

namespace App\Http\Controllers\Api\Client\Comment;
/*
    Requeriments inputs
    
    idclient_fk*,
	comment*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostComment;
use App\Http\Traits\GetTenantid;

use App\Models\Comment;

class CommentController extends Controller
{
    use GetTenantid;

    private $comment;

    public function __construct(Comment $comment){
    	$this->middleware('privilege.client');
        $this->comment = $comment;
    }

    public function comment(Request $request, $idclient){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'desc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'comment';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'comment';

        $result = $this->comment::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['tenantid' => $tenantid, 'clientid' => $idclient])->get();

		return response()->json($result, 200); 
    }

    public function addcomment(RulesPostComment $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid, 
            'clientid' => $request['clientid'],
            'comment' => $request['comment'],
        ];
        $create = $this->comment::create($data);

        activity('comment-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'comment' => $create->id, 'view' => '1'])
         ->log('Criou a observação ');

		return response()->json([
		    'SUCESS' => 'Observação salva com sucesso'
		]); 

    }

    public function addcommentid(RulesPostComment $request, $idclient){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid, 
            'clientid' => $idclient,
            'comment' => $request['comment'],
        ];
        $create = $this->comment::create($data);

        activity('comment-create')
         ->causedBy($userloged)
         ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'comment' => $create->id, 'view' => '1'])
         ->log('Criou a observação ');

        return response()->json([
            'SUCESS' => 'Observação salva com sucesso'
        ], 200);  	
    }

    public function upcomment(RulesPostComment $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid, 
            'clientid' => $request['clientid'],
            'comment' => $request['comment'],
        ];
        $attribute = 'id';

        $update = $this->comment::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('comment-update')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'comment' => $id, 'view' => '1'])
             ->log('Alterou a observação ');

			return response()->json([
			    'SUCESS' => 'Observação editada com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Observação não alterada, esta observação não existe'
			], 422);         		
    	}        
    }

    public function delcomment(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';

        $delete = $this->comment::where(['tenantid' => $tenantid, 'id' => $id])->delete();
         
        if($delete){
            activity('comment-delete')
             ->causedBy($userloged)
             ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'comment' => $id, 'view' => '1'])
             ->log('Delete a observação ');

            return response()->json([
                'SUCESS' => 'Observação excluida com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Observação não excluida, esta observação não existe'
            ], 422);                 
        }     	
    }

    public function delallcomment(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $checkcomment = $request->input('checkcomment');

        $delete = $this->comment::whereIn('id', $checkcomment)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('comment-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'comment' => $checkcomment, 'view' => '1'])
            ->log('Delete várias observações ');

            return response()->json([
            'SUCESS' => 'Observação(ções) excluida(s) com sucesso'
        ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Observação(ções) não excluidas, esta(s) obervação(ções) não existe(m)'
            ], 422);                
        }
    }
}
