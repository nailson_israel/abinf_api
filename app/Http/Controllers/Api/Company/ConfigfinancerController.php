<?php

namespace App\Http\Controllers\Api\Company;
/*
    Requeriments inputs
    
	date,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostFinancer;
use App\Http\Traits\GetTenantid;

use App\Models\Configfinancer;

class ConfigfinancerController extends Controller
{
    use GetTenantid;

    private $configfinancer;

    public function __construct(Configfinancer $configfinancer){
        $this->middleware('privilege.companypermission');    
        $this->configfinancer = $configfinancer;	
    }

    public function financer(){	
        $tenantid = $this->GetTenant();
    	
        $result = $this->configfinancer::whereNull('delete_at')->where(['tenantid' => $tenantid]);

        return response()->json($result, 200);    	
    }

    public function addfinancer(RulesPostFinancer $request){
        $tenantid = $this->GetTenant();
        $result = $this->configfinancer::where(['tenantid' => $tenantid])->get();

        if(count($result)){
            return response()->json([
                'ERROR' => 'Você não pode salvar 2 ou mais vezes a data do financeiro, se quiser mudar, use a opção editar'
            ], 422);
        }else{
            $data = ['tenantid' => $tenantid, 'datef' => $request->input('datef')];
            $create = $this->configfinancer::create($data);

    		return response()->json([
    		    'SUCESS' => 'Financeiro salvo com sucesso'
    		], 200);  
        }   	
    }

    public function upfinancer(RulesPostFinancer $request, $id){
        $tenantid = $this->GetTenant();        
        $data = ['tenantid' => $tenantid, 'datef' => $request->input('datef')];

        $update = $this->configfinancer::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

    	if($update){
			return response()->json([
			    'SUCESS' => 'Financeiro editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Financeiro não alterado, este financeiro não existe'
			], 422);         		
    	}
    }

    public function delfinancer(Request $request, $id){
        $tenantid = $this->GetTenant();

        $delete = $this->configfinancer::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
			return response()->json([
			    'SUCESS' => 'Financeiro excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Financeiro não excluido, este financeiro não existe'
			], 422);         		
    	}	
    }
}
