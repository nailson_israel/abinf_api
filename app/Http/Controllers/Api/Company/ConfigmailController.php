<?php

namespace App\Http\Controllers\Api\Company;
/*
    Requeriments inputs
    
    serversmtp*,
    portsmtp*,
    serverpop*,
    portpop*,
    security*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RulesPostConfigMail;
use App\Http\Traits\GetTenantid;

use App\Models\Configmail;

class ConfigmailController extends Controller
{
    use GetTenantid;

    private $configmail;

    public function __construct(Configmail $configmail){
        $this->middleware('privilege.companypermission');
        $this->configmail = $configmail;    	
    }

    public function configmail(){	  
        $tenantid = $this->GetTenant();

        $result = $this->configmail::whereNull('deleted_at')->where(['tenantid' => $tenantid])->get();

        return response()->json($result, 200);    	
    }

    public function addconfigmmail(RulesPostConfigMail $request){
        $tenantid = $this->GetTenant();     
        $result = $this->configmail::whereNull('deleted_at')->where(['tenantid' => $tenantid])->get();
        
        if(count($result)){
            return response()->json([
                'ERROR' => 'Você não pode salvar 2 ou mais as configurações de email da empresa, se quiser mudar, use a opção editar'
            ], 422);
        }else{
            $data = [
                'tenantid' => $tenantid,
                'serversmtp' => $request->input('serversmtp'),
                'portsmtp' => $request->input('portsmtp'),            
                'serverpop' => $request->input('serverpop'),
                'portpop' => $request->input('portpop'),
                'security' => $request->input('security'),
            ];

            $create = $this->configmail::create($data);

            return response()->json([
                'SUCESS' => 'Configurações de email da empresa salvo com sucesso'
            ], 200);                 
        }	
    }

    public function upconfigmail(RulesPostConfigMail $request, $id){
        $tenantid = $this->GetTenant();    
        $data = [
            'tenantid' => $tenantid,
            'serversmtp' => $request->input('serversmtp'),
            'portsmtp' => $request->input('portsmtp'),            
            'serverpop' => $request->input('serverpop'),
            'portpop' => $request->input('portpop'),
            'security' => $request->input('security')            
        ];
        $attribute = 'id';

        $update = $this->configmail::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

        if($update){
            return response()->json([
                'SUCESS' => 'Configuração de email editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Configuração de email não alterado, esta configuração de email não existe'
            ], 422);                 
        }
    }

    public function delconfigmail(Request $request, $id){
        $tenantid = $this->GetTenant();
        $attribute = 'id'; 

        $delete = $this->configmail::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($delete){
            return response()->json([
                'SUCESS' => 'Configuração de email excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Configuração de email não excluido,  esta configuração de email não existe'
            ], 422);                 
        }  
    }
}
