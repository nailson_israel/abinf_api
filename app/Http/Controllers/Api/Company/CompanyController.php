<?php

namespace App\Http\Controllers\Api\Company;
/*
    Requeriments inputs
    
    cnpj*,
    reason*,
    namefantasy*,
    cep*,
    address*,
    number*,
    city*,
    district*,
    phone1*,
    phone2,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostCompany;
use App\Http\Traits\GetTenantid;

use App\Models\Company;

class CompanyController extends Controller
{
    use GetTenantid;

    private $copany;

    public function __construct(Company $company)
    {
        $this->middleware('privilege.companypermission');
        $this->company = $company;
    }

    public function company(){
        $tenantid = $this->GetTenant();

        $result = $this->company::whereNull('deleted_at')
        ->where(['tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200);
    }

    public function addcompany(RulesPostCompany $request){
        $tenantid = $this->GetTenant();     
        $userloged = \Auth::guard('api')->user();

        $result = $this->company::whereNull('deleted_at')->where(['tenantid' => $tenantid])->get();

        if(count($result)){
            return response()->json([
                'ERROR' => 'Você não pode salvar 2 ou mais vezes os dados da empresa, se quiser mudar, use a opção editar'
            ], 422);
        }else{
            $data = [
                'tenantid' => $tenantid,        
                'cnpj' => $request->input('cnpj'),
                'reason' => $request->input('reason'),            
                'namefantasy' => $request->input('namefantasy'),
                'cep' => $request->input('cep'),
                'address' => $request->input('address'),
                'number' => $request->input('number'),
                'city' => $request->input('city'),                   
                'district' => $request->input('district'),  
                'phone1' => $request->input('phone1'),                                        
                'phone2' => $request->input('phone2'),
                'email' => $request->input('email'),                
            ];

            $create = $this->company::create($data);

            activity('company-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'company' => $create->id, 'view' => '1'])
            ->log('Criou a compania ');

            return response()->json([
                'SUCESS' => 'Dados da empresa salvo com sucesso'
            ], 200);                         
        }
    }

    public function upcompany(RulesPostCompany $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid, 
            'cnpj' => $request->input('cnpj'),
            'reason' => $request->input('reason'),            
            'namefantasy' => $request->input('namefantasy'),
            'cep' => $request->input('cep'),
            'address' => $request->input('address'),
            'number' => $request->input('number'),
            'city' => $request->input('city'),                   
            'district' => $request->input('district'),  
            'phone1' => $request->input('phone1'),                                        
            'phone2' => $request->input('phone2'),
            'email' => $request->input('email'),
        ];        

        $update = $this->company::whereNull('deleted_at')->where(['tenantid' => $tenantid, 'id' => $id])->update($data);

        if($update){
            activity('company-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'company' => $id, 'view' => '1'])
            ->log('Alterou a compania ');

            return response()->json([
                'SUCESS' => 'Dados da empresa editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Dados da empresa não alterado, este dados não existe'
            ], 422);                 
        }

    }

    public function delcompany(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->company::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($delete){
            activity('company-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'company' => $id, 'view' => '1'])
            ->log('Deletou a compania ');

            return response()->json([
                'SUCESS' => 'Dados da empresa excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Dados da empresa não excluido, estes dados não existe'
            ], 422);                 
        }   
    }
}
