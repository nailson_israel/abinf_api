<?php

namespace App\Http\Controllers\Api\Company;
/*
    Requeriments inputs
    
	image*
*/
use Intervention\Image\ImageManagerStatic as Image;	
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostImagecompany;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetTenantid;

use App\Models\Imagecompany;
use App\Models\Tenant;


use File;

class ImgcompanyController extends Controller
{
	use GetTenantid;

	private $imagecompany;
	private $tenant;

    public function __construct(ImageCompany $imagecompany, Tenant $tenant)
    {
        $this->middleware('privilege.companypermission');
        $this->imagecompany = $imagecompany;
        $this->tenant = $tenant;
    }

    public function imagecompany(){
    	$tenantid = $this->GetTenant();

    	$result = $this->imagecompany::whereNull('deleted_at')
    	->where(['tenantid' => $tenantid])
    	->get();

    	return response()->json($result, 200);
    }

    public function addimagecompany(RulesPostImagecompany $request){
    	$tenantid = $this->GetTenant();
    	if($request->hasFile('image')){
			$tenantid = \Auth::user()->tenantid;

			$result = $this->tenant::whereNull('deleted_at')->where(['tenantid' => $tenantid])->get(); 

	        foreach ($result as $response) {
	           $enterprise =  $response->prefix;
	        }
			
			$folder = $enterprise.'/';
	        $path = 'image/logos/enterprise';        
	        $directory = $path.$folder;
	        
	        if(!is_dir($directory)){
				mkdir($directory, 0755, true); 
	        }
	        
	 		$file = $request->file('image');
	 		$namelogo = $request->file('image')->getClientOriginalName(); 
			
			$array = explode('.',$namelogo);
			$nome_arquivo = md5($array[0]);
			$nome_arquivo .= '.'.$array[1];

        	$result = $this->imagecompany->whereNull('deleted_at')->where('tenantid', $tenantid);
        	if(count($result)){
	            return response()->json([
	                'ERROR' => 'Você não pode salvar 2 ou mais vezes a logo, se quiser mudar, use a opção editar'
	            ], 422);
	        }else{

	        	$data = ['tenantid' => $tenantid, 'image' => $nome_arquivo];
	        	$create = $this->imagecompany::create($data);
				/*$imagecompany = Imagecompany::firstOrCreate([
				    'image' => $nome_arquivo,           
				]);*/ 

				list($width, $height, $type, $attr) = getimagesize($file);
				$image = Image::make($request->file('image')->getRealPath());
				$image->resize(250, 150, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				});
				$image->resizeCanvas(250, 150, 'center', false, array(255, 255, 255, 0));		
		   		$image->save($directory.$nome_arquivo);				

	            return response()->json([
	                'SUCESS' => 'Logo da empresa salvo com sucesso'
	            ], 200);  				        	
	        }	   		
    	}
    }

    public function upimagecompany(RulesPostImagecompany $request, $id){	
    	$tenantid = $this->GetTenant();
    	if($request->hasFile('image')){
			$tenantid = Auth::user()->tenantid;

			$result = $this->tenant::whereNull('deleted_at')->where(['tenantid' => $tenantid])->get(); 
	        //$tenant = Tenant::where('idtenant', $tenantid)->get();
	        foreach ($result as $response) {
	           $enterprise =  $response->prefix;
	        }
			
			$folder = $enterprise.'/';
	        $path = 'image/logos/enterprise/';        
	       	$directory = $path.$folder;

	        if(!is_dir($directory)){
				mkdir($directory, 0755, true); 
	        }
	        
	 		$file = $request->file('image');
	 		$namelogo = $request->file('image')->getClientOriginalName(); 
			
			$array = explode('.',$namelogo);
			$nome_arquivo = md5($array[0]);
			$nome_arquivo .= '.'.$array[1];

        	$resultc = $this->imagecompany->whereNull('deleted_at')->where('tenantid', $tenantid);
	        $count = count($resultc);

	        $count;
	        if($count == 0){
	        	$data = ['tenantid' => $tenantid, 'image' => $nome_arquivo];
	        	$createup = $this->imagecompany::create($data);
	        }else{
	        	$attribute = 'id';

	        	$data = ['tenantid' => $tenantid, 'image' => $nome_arquivo];
	        	$update = $this->imagecompany::whereNull('deleted_at')->where('tenantid', $tenantid)->update($data);	
	            /*$imgcompany = Imagecompany::where('idimg', '1')->update([
	                'image' => $nome_arquivo, 
	            ]);*/
	        }

			if(is_dir($directory)){

			    $directory_path = dir($directory);
			    
			    while($archive=$directory_path->read()){
			        if(($archive !='.') && ($archive !='..')){
			            File::delete($directory.$archive);
			        }
			    }
			}

			list($width, $height, $type, $attr) = getimagesize($file);
			$image = Image::make($request->file('image')->getRealPath());
			$image->resize(250, 150, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			$image->resizeCanvas(250, 150, 'center', false, array(255, 255, 255, 0));		
				$image->save($directory.$nome_arquivo);				

			return response()->json([
			    'SUCESS' => 'Logo da empresa editado com sucesso'
			], 200);  

	   }
    }

    //Name image
    public function nameimagecompany(){
    	$tenantid = $this->GetTenant();
    	$result = $this->imagecompany::where('tenantid', $tenantid)
    	->get(); 
      	return response()->json($result, 200);
    } 

    public function addnameimagecompany(RulesPostImagecompany $request){
    	$tenantid = $this->GetTenant();
    	$this->imagecompany::where('tenantid', $tenantid)->get(); 
    	
    	$data = ['tenantid' => $tenantid, 'company_id' => $request['company_id'], 'image' => $request['image']];
    	$createup = $this->imagecompany::create($data);
    }

    public function upnameimagecompany(RulesPostImagecompany $request, $id){
    	$tenantid = $this->GetTenant();
    	$this->imagecompany::where('tenantid', $tenantid)->get(); 

    	$resultc = $this->imagecompany::where('tenantid', $tenantid)->get();
        $count = count($resultc);

    	$attribute = 'id';

    	$data = ['tenantid' => $tenantid, 'company_id' => $request['company_id'], 'image' => $request['image']];
    	$update = $this->imagecompany::where('tenantid', $tenantid)->update($data);	
        /*$imgcompany = Imagecompany::where('idimg', '1')->update([
            'image' => $nome_arquivo, 
        ]);*/
    }

    public function delnameimagecompany(Request $request, $id){
    }
}
