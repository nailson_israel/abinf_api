<?php

namespace App\Http\Controllers\Api\Product;
/*
	Requeriments inputs
	
	groupproduct*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostGroupproduct;
use App\Http\Traits\GetTenantid;

use App\Models\Groupproduct;

class GroupproductController extends Controller
{
    use GetTenantid;

    private $grouproduct;

    public function __construct(Groupproduct $groupproduct)
    {
        $this->middleware('privilege.product');        
        $this->groupproduct = $groupproduct;
        $this->pagination = config('responses.pagination');
    }

    public function groupproduct(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'groupproduct';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'groupproduct';
        $search = $request['search'];
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->groupproduct::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)
                ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->groupproduct::whereNull('groupproducts.deleted_at')->where(['groupproducts.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function grouproductselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $sort = 'asc';
        $typesort = 'groupproduct';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'groupproduct';
        $search = $request['search'];

        $result = $this->groupproduct::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();
        return response()->json($result, 200);  	
    }

    public function addgroupproduct(RulesPostGroupproduct $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

		$data = ['tenantid' => $tenantid, 'groupproduct' => $request->input('groupproduct')];
        $create = $this->groupproduct::create($data);

        activity('groupproduct-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupproduct' => $create->id, 'view' => '1'])
        ->log('Criou o categoria :id de produto ');


		return response()->json([
		    'SUCESS' => 'Grupo de produto salvo com sucesso'
		], 200);  
    }

    public function upgroupproduct(RulesPostGroupproduct $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'groupproduct' => $request->input('groupproduct')];

        $update = $this->groupproduct::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('groupproduct-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupproduct' => $id, 'view' => '1'])
            ->log('Alterou o categoria :id de produto ');

			return response()->json([
			    'SUCESS' => 'Grupo editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Grupo não alterado, este grupo não existe'
			], 422);         		
    	}
    }

    public function delgroupproduct(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->groupproduct::where(['tenantid' => $tenantid, 'id' => $id])->delete();  	

    	if($delete){
            activity('groupproduct-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupproduct' => $id, 'view' => '1'])
            ->log('Deletou o categoria :id de produto ');

			return response()->json([
			    'SUCESS' => 'Grupo excluido com sucesso'
			]);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Grupo não excluido, este grupo não existe'
			]);         		
    	}
    }

    public function delallgroupproduct(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';
        $checkgroupproduct = $request->input('checkgroupproduct');

        $delete = $this->groupproduct::whereIn('id', $checkgroupproduct)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('groupproduct-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupproduct' => $checkgroupproduct, 'view' => '1'])
            ->log('Deletou várias categorias :id de produto ');

            return response()->json([
            'SUCESS' => 'Grupo excluido com sucesso'
        ]);             
        }else{
            return response()->json([
            'ERROR' => 'Grupo não excluido, este grupo não existe'
            ]);                 
        }
    }
}
