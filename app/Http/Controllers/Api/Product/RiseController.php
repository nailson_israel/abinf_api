<?php

namespace App\Http\Controllers\Api\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostRise;
use App\Http\Traits\GetTenantid;
//Model
use App\Models\Risegroup;
use App\Models\Product;

class RiseController extends Controller
{
    use GetTenantid;

    private $risegroup;
    private $product;

    public function __construct(Risegroup $risegroup, Product $product)
    {
        $this->middleware('privilege.product');
        $this->risegroup = $risegroup;
        $this->product = $product;
    }

    public function rise(){
        $tenantid = $this->GetTenant();

        $result = $this->risegroup->where('tenantid', $tenantid)->get();
        
		return response()->json($result, 200);
    }

    public function addrise(RulesPostRise $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'groupprod';
    	$result = $this->product->whereNull('deleted_at')->where([$attribute => $request['group'], 'tenantid' => $tenantid ])->get();

        if($result == '[]'){
                return response()->json([
                    'ERROR' => 'Grupo não encontrado'
                ], 422);
        }else{
            if(strpos($request->input('rise'), ',')) {
                $rise = str_replace('.', '', $request->input('rise')); $rise = str_replace(',', '.', $rise); $rise = (double)$rise;  
            }else{
                $rise = $request->input('rise');
            }
            
            $data = [
                'tenantid' => $tenantid,
                'rise' => $rise,
                'group' => $request->input('group'),            
                'user' => $userloged->user
            ];

            $risegroup = $this->risegroup->create($data);


            foreach ($result as $prod) {
                
                $porcentagem = ($prod->price * $rise) / 100;
                $idproduct = $prod->id;


                 $valorfinal = $prod->price + $porcentagem; 
                 $idproduct;
         
                $productsup = Product::where(['id' => $idproduct, 'tenantid' => $tenantid])->update(
                [
                'price' => $valorfinal,
                ]);
            }
            if($productsup >= 1){
                activity('product-rise')
                ->causedBy($userloged)
                ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupproduct' => $request['group'], 'view' => '1'])
                ->log('Aumentou o preco dos produtos da categoria ');

                return response()->json([
                    'SUCESS' => 'Aumento realizado com sucesso'
                ], 200);             
            }
        }
    }
}
