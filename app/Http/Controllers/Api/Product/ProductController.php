<?php

namespace App\Http\Controllers\Api\Product;

/*
	Requeriments inputs
	
	cod*,
	product*,
	ipi,
	unit,
	price*,
	commission,
	groupprod
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostProduct;
use App\Http\Traits\GetTenantid;

use DB;

use App\Models\Product;

class ProductController extends Controller
{
    use GetTenantid;

    private $product;
    private $pagination;

    public function __construct(Product $product)
    {
        $this->middleware('privilege.product');
        $this->product = $product;
        $this->pagination = config('responses.pagination');
    }

    public function product(Request $request){
        $tenantid = $this->GetTenant();
        $attribute = 'products.tenantid';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'product';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'product';
        $search = $request['search'];
        $limit = $request['limit'] != null ? $request['limit'] : 50;

        $result = $this->product::select('products.id', 'products.cod', 'product', 'ipi', 'unit', 'price', 'commission', 'groupprod', 'groupproduct', 'representeds.id as idrep', 'represented', 'checkstock', 'quantity')
        ->leftJoin('groupproducts', 'products.groupprod', '=', 'groupproducts.id')
        ->leftJoin('representeds', 'products.idrep', '=', 'representeds.id')
        ->whereNull('products.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where(['products.tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();
        
        $total = $this->product::whereNull('products.deleted_at')->where(['products.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);
    }

    public function productbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'product';
        if($search = $request['search']){
          $where = " AND $typesearch like '%".$search."%' ";
        }else{
            $where = '';
        }
        $sql = "SELECT CONCAT(cod,' ',product) as product, id FROM products WHERE tenantid = $tenantid $where ORDER BY product asc";
        $result = DB::select( DB::raw($sql));	
        return response()->json($result, 200);
    }

    public function productid(Request $request, $id){
        $tenantid = $this->GetTenant();

        $result = $this->product::select('products.id', 'products.cod', 'product', 'ipi', 'unit', 'price', 'commission', 'groupprod', 'groupproduct', 'representeds.id as idrep', 'represented', 'checkstock', 'quantity')
        ->leftJoin('groupproducts', 'products.groupprod', '=', 'groupproducts.id')
        ->leftJoin('representeds', 'products.idrep', '=', 'representeds.id')
        ->whereNull('products.deleted_at')
        ->where(['products.tenantid' => $tenantid, 'products.id' => $id])
        ->get();

        return response()->json($result, 200);
    }

    //Rever essa feature pois precisa do iten do orçamento
    public function productmoreseller(){
    }

    public function productcod(Request $request){
        $tenantid = $this->GetTenant();
        $cod = $request['cod'];

        $result = $this->product::whereNull('deleted_at')
        ->when($cod, function($query) use ($cod){
            $query->where('products.cod', 'like', "%$cod%");
        })
        ->where('tenantid', $tenantid)
        ->get();

        return response()->json($result, 200);        
    }

    public function addproduct(RulesPostProduct $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        //convert double mysql
        if(strpos($request->input('price'), ',')) {
            $price = str_replace('.', '', $request->input('price')); $price = str_replace(',', '.', $price); $price = (double)$price;  
        }else{
            $price = $request->input('price');
        }

        if(strpos($request->input('ipi'), ',')) {
            $ipi = str_replace('.', '', $request->input('ipi')); $ipi = str_replace(',', '.', $ipi); $ipi = (double)$ipi;
        }else{
            $ipi = $request->input('ipi');
        }

        if(strpos($request->input('commission'), ',')){
            $commission = str_replace('.', '', $request->input('commission')); $commission = str_replace(',', '.', $commission); $commission = (double)$commission;
        }else{
            $commission = $request->input('commission');
        }

        $data = [
            'tenantid' => $tenantid,
            'cod' => $request->input('cod'),
            'product' => $request->input('product'),            
            'ipi' => $ipi,
            'unit' => $request->input('unit'),
            'price' => $price,
            'commission' => $commission,                
            'groupprod' => $request->input('groupprod'),
            'idrep' => $request->input('idrep'),
            'checkstock' => $request->input('checkstock'),
            'quantity' => $request->input('quantity'),            
        ];

        $create = $this->product::create($data);

        activity('product-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'product' => $create->id, 'view' => '1'])
        ->log('Create o produto ');

		return response()->json([
		    'SUCESS' => 'Produto salvo com sucesso'
		]);    	
    }

    public function upproduct(RulesPostProduct $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        //convert double mysql
        if(strpos($request->input('price'), ',')) {
            $price = str_replace('.', '', $request->input('price')); $price = str_replace(',', '.', $price); $price = (double)$price;  
        }else{
            $price = $request->input('price');
        }

        if(strpos($request->input('ipi'), ',')) {
            $ipi = str_replace('.', '', $request->input('ipi')); $ipi = str_replace(',', '.', $ipi); $ipi = (double)$ipi;
        }else{
            $ipi = $request->input('ipi');
        }

        if(strpos($request->input('commission'), ',')){
            $commission = str_replace('.', '', $request->input('commission')); $commission = str_replace(',', '.', $commission); $commission = (double)$commission;
        }else{
            $commission = $request->input('commission');
        }

        $data = [
            'tenantid' => $tenantid,
            'cod' => $request->input('cod'),
            'product' => $request->input('product'),            
            'ipi' => $ipi,
            'unit' => $request->input('unit'),
            'price' => $price,
            'commission' => $commission,                
            'groupprod' => $request->input('groupprod'),
            'idrep' => $request->input('idrep'),
            'checkstock' => $request->input('checkstock'),
            'quantity' => $request->input('quantity'),
        ];
        $attribute = 'id';

        $update = $this->product::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);
    	
        if($update){
            activity('product-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'product' => $id, 'view' => '1'])
            ->log('Alterou o produto ');

			return response()->json([
			    'SUCESS' => 'Produto editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Produto não alterado, este produto não existe'
			], 422);         		
    	}      	
    }

    public function delproduct(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'tenantid';

        $delete = $this->product::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('product-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'product' => $id, 'view' => '1'])
            ->log('Deletou o produto ');

			return response()->json([
			    'SUCESS' => 'Produto excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Produto não excluido, este produto não existe'
			], 422);         		
    	}
    }

    public function delallproduct(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'tenantid';
        $checkproduct = $request->input('checkproduct');

        $delete = $this->product::whereIn('id', $checkproduct)->where(['tenantid' => $tenantid])->delete();                

        if($delete){
            activity('product-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'product' => $checkproduct, 'view' => '1'])
            ->log('Deletou vários produtos ');

            return response()->json([
            'SUCESS' => 'Produto(s) excluido(s) com sucesso'
            ]);             
        }else{
            return response()->json([
            'ERROR' => 'Produto(s) não excluido, este(s) produto(s) não existe(m)'
            ]);                 
        }
    }    
}
