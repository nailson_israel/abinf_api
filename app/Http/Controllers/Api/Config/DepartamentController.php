<?php

namespace App\Http\Controllers\Api\Config;
/*
    Requeriments inputs
    
    Departament*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostDepartament;
use App\Http\Traits\GetTenantid;

use App\Models\Departament;

class DepartamentController extends Controller
{
    use GetTenantid;

    private $departament;

    public function __construct(Departament $departament)
    {
        $this->middleware('privilege.config');
        $this->departament = $departament;
    }

    public function departaments(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'departament';
        $search = $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'departament';

        $result = $this->departament::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function departamentselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'departament';
        $sort = 'asc';
        $typesort = 'departament';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'departament';     

        $result = $this->departament::whereNull('deleted_at')->orderby($typesort, $sort,)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);        
    }

    public function adddepartament(RulesPostDepartament $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'departament' => $request->input('departament')];
        $create = $this->departament::create($data);

        activity('departament-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'departament' => $create->id, 'view' => '1'])
        ->log('Create o departament ');


		return response()->json([
		    'SUCESS' => 'Departamento salvo com sucesso'
		], 200); 
    }

    public function updepartament(RulesPostDepartament $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'departament' => $request->input('departament')];

        $update = $this->departament::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('departament-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'departament' => $id, 'view' => '1'])
            ->log('Alterou o departament ');

			return response()->json([
			    'SUCESS' => 'Departamento editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Departamento não alterado, este departamnto não existe'
			], 422);         		
    	}
    }

    public function deldepartament(Request $requet, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->departament::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('departament-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'departament' => $id, 'view' => '1'])
            ->log('Deletou o departamento ');

			return response()->json([
			    'SUCESS' => 'Departament excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Departament não excluido, este departamnto não existe'
			], 422);         		
    	}
    }

    public function delalldepartament(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'tenantid';
        $checkdepartament = $request->input('checkdepartament');

        $delete = $this->departament::whereIn('id', $checkdepartament)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('departament-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'departament' => $checkdepartament, 'view' => '1'])
            ->log('Deletou vários departamentos ');

            return response()->json([
            'SUCESS' => 'Departamento(s) excluido(s) com sucesso'
            ]);             
        }else{
            return response()->json([
            'ERROR' => 'Departamento(s) não excluido, este(s) departamento(s) não existe(m)'
            ]);                 
        }        
    }
}
