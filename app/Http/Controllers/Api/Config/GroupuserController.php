<?php

namespace App\Http\Controllers\Api\Config;
/*
    Requeriments inputs
    
    groupuser*,
    page*,
    view,
    create,
    update,
    delete
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostGroupuser;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Config\GroupRepository;
use App\Repositories\Config\PermissionRepository;
//Criteria
use App\Criteria\TenantCriteria;
use App\Criteria\SearchCriteria;
use App\Criteria\SortCriteria;
use App\Criteria\WhereAttributeCriteria;
use App\Criteria\Config\PermissionCriteria;
//Model Custon collection
use App\Models\Group;
use App\Models\Permission as PermissionCuston;


class GroupuserController extends Controller
{   
    use GetTenantid;

    private $group;
    private $permission;

    public function __construct(Group $group, PermissionCuston $permission)
    {
        $this->group = $group;
        $this->permission = $permission;

    }
    
    public function groups(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = 'groupuser';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'groupuser';

        $result = $this->group::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function groupselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesort = 'groupuser';
        $sort = 'asc';
        $search = $request['search'];
        $typesearch = 'groupuser';

        $result = $this->group::whereNull('deleted_at')->orderby($typesort, $sort,)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);  
    }


    public function permission(){
        $tenantid = $this->GetTenant();
        
        $result = $this->permission::whereNull('deleted_at')->where('tenantid', $tenantid)
        ->get();
        return response()->json($result, 200); 
    }

    public function groupuserid(Request $request, $id){
        $tenantid = $this->GetTenant();
        $attribute = 'groups.id';
        $result = $this->group::whereNull('deleted_at')->where('tenantid', $tenantid)
        ->get();
        return response()->json($result, 200);      
    }

    public function permissionid(Request $request, $id){
        $tenantid = $this->GetTenant();
        
        $result = $this->permission::select(
            'permissions.id', 'page', 'view', 'create', 
            'update', 'delete', 'permissions.groupid', 
            'groups.groupuser'
        )->leftJoin('groups', 'permissions.groupid', '=', 'groups.id')
        ->whereNull('permissions.deleted_at')
        ->where(['permissions.tenantid' => $tenantid, 'groupid' => $id])
        ->get();

        return response()->json($result, 200); 
    }

    public function addgroup(RulesPostGroupuser $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'groupuser' => $request->input('groupuser')];
        $create = $this->group::create($data);

        $idc = $create->id;
        //get last id
        $groupuser = $request->input('groupuser');
        $field = 'groupuser';
        $value = $request->input('groupuser');

        $result = $this->group::whereNull('deleted_at')
        ->where(['tenantid' => $tenantid, $field => $value])
        ->get(); 

        ['result' => $result];
        
        foreach ($result as $response) {
            $response->id;
        }
        $id_group = $response->id;
    
        $pages = $request['page'];
        $view = $request['view'];
        $create = $request['create'];
        $update = $request['update'];
        $delete = $request['delete'];

        for ($i=0;$i<count($pages);$i++){

            $permission = new PermissionCuston(); 
            $permission->tenantid = $tenantid;                                  
            $permission->groupid = $id_group;
            $permission->page = $pages[$i];
            $permission->view = $view[$i];
            $permission->create = $create[$i];
            $permission->update = $update[$i];
            $permission->delete = $delete[$i];                                              
            $permission->save();
        }

        activity('group-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'group' => $idc, 'view' => '1'])
        ->log('Cadastrou o grupo :id e permissões ');

        return response()->json([
            'SUCESS' => 'Grupo salvo com sucesso'
        ], 200);
    }

    public function upgroup(RulesPostGroupuser $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $datagroup = ['tenantid' => $tenantid, 'groupuser' => $request->input('groupuser')];

        $updategroup = $this->group::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($datagroup);

        $id = $request['id'];   
        $pages = $request['page'];
        $view = $request['view'];
        $create = $request['create'];
        $update = $request['update'];
        $delete = $request['delete'];

        for ($i=0;$i<count($pages);$i++){
            $datapermission = [
                'tenantid' => $tenantid, 
                'page' => $pages[$i], 
                'view' => $view[$i], 
                'create' => $create[$i], 
                'update' => $update[$i], 
                'delete' => $delete[$i],
            ];
            $updatepermission = PermissionCuston::where('id', $id[$i])->update($datapermission);
        }
    
        if($updategroup && $updatepermission){
            activity('group-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'group' => $id, 'view' => '1'])
            ->log('Alterou o grupo :id e permissões ');

            return response()->json([
                'SUCESS' => $id
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Grupo e permissões não editado, este grupo não existe'
            ], 422);                 
        }           
    }

    public function delgroup(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $deletepermissions = $this->permission::where(['tenantid' => $tenantid, 'groupid' => $id])->delete();
        $deletegroup = $this->group::where(['tenantid' => $tenantid, 'id' => $id])->delete();

        if($deletegroup){
            activity('group-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'group' => $id, 'view' => '1'])
            ->log('Deletou o grupo :id e permissões ');

            return response()->json([
                'SUCESS' => 'Grupo e permissões excluido com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Grupo e permissões não excluido, este grupo não existe'
            ], 422);                 
        }

    }
}
