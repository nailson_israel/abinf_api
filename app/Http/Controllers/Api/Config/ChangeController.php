<?php

namespace App\Http\Controllers\Api\Config;
/*
	Requeriments inputs
	
	Change*,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostChange;
use App\Http\Traits\GetTenantid;

use App\Models\Change;

class ChangeController extends Controller
{
    use GetTenantid;

    private $change;

    public function __construct(Change $change)
    {
        $this->middleware('privilege.config');
        $this->change = $change;
    }

    public function changes(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'change';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'change';

        $result = $this->change::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    //change for selects box 
    public function changeselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $sort = 'asc';
        $typesort = 'change';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'change';     

        $result = $this->change::whereNull('deleted_at')->orderby($typesort, $sort,)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function addchange(RulesPostChange $request){
        $tenantid = $this->GetTenant();  
        $userloged = \Auth::guard('api')->user();

        $data = ['tenantid' => $tenantid, 'change' => $request->input('change')];

        $create = $this->change::create($data);

        activity('change-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'change' => $create->id, 'view' => '1'])
        ->log('Criou o cargo ');

		return response()->json([
		    'SUCESS' => 'Cargo salvo com sucesso'
		], 200);  		

    }

    public function upchange(RulesPostChange $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = ['change' => $request->input('change')];
        $update = $this->change::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('change-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'change' => $id, 'view' => '1'])
            ->log('Alterou o cargo ');

			return response()->json([
			    'SUCESS' => 'Cargo editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Cargo não alterado, este cargo não existe'
			], 422);         		
    	}
    }

    public function delchange(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->change::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('change-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'change' => $id, 'view' => '1'])
            ->log('Deletou o cargo ');

			return response()->json([
			    'SUCESS' => 'Cargo excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Cargo não excluido, este cargo não existe'
			], 422);         		
    	}	

    }

    public function delallchange(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'tenantid';
        $checkchange = $request->input('checkchange');

        $delete = $this->change::whereIn('id', $checkchange)->where(['tenantid' => $tenantid])->delete();

        if($delete){
            activity('change-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'changes' => $checkchange, 'view' => '1'])
            ->log('Deletou vários cargos ');

            return response()->json([
            'SUCESS' => 'Cargos(s) excluido(s) com sucesso'
            ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'Cargo(s) não excluido, este(s) cargo(s) não existe(m)'
            ], 422);                 
        }        
    }
}
