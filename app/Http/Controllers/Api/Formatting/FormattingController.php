<?php

namespace App\Http\Controllers\Api\Formatting;
/*
    Requeriments inputs
    
	table,
	caixatext,
	relatpage,
	relatpdf,
	header,
	pdfbudget
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostFormatting;
use App\Http\Requests\Api\RulesPostFormatt;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Formatt\FormattRepository;
use App\Repositories\Formatt\FormattextRepository;
//Criteria
use App\Criteria\TenantCriteria;
use App\Criteria\WhereAttributeCriteria;

use App\Models\Formatt;
use App\Models\Formattext;

class FormattingController extends Controller
{
    use GetTenantid;

    private $formatt;

    public function __construct(Formattext $formattext, Formatt $formatt){
        $this->middleware('privilege.config');    
        $this->formatt = $formatt;
        $this->formattext = $formattext;
    }

    public function format(){
    	$tenantid = $this->GetTenant();

        $result = $this->formatt::whereNull('deleted_at')
        ->where(['tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200);  
    }

    public function addformat(RulesPostFormatting $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $result = $this->formatt->whereNull('deleted_at')->where('tenantid', $tenantid)->get();

        if(count($result)){
            return response()->json([
                'ERROR' => 'Você não pode salvar 2 ou mais formatações, se quiser mudar, use a opção editar'
            ], 422);
        }else{
            $data = [
                'tenantid' => $tenantid,
                'tables' => $request->input('tables'),
                'caixatext' => $request->input('caixatext'),            
                'relatpage' => $request->input('relatpage'),
                'relatpdf' => $request->input('relatpdf'),
                'header' => $request->input('header'),
                'pdfbudget' => $request->input('pdfbudget'),                
            ];
            $create = $this->formatt::create($data);

            activity('format-create')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'format' => $create->id, 'view' => '1'])
            ->log('Cadastrou o formatação ');

            return response()->json([
                'SUCESS' => 'Formatação salvo com sucesso'
            ], 200);                 
        }	    	
    }

    public function upformat(RulesPostFormatting $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'tables' => $request->input('tables'), 
            'caixatext' => $request->input('caixatext'),
            'relatpage' => $request->input('relatpage'),
            'relatpdf' => $request->input('relatpdf'),
            'header' => $request->input('header'),
            'pdfbudget' => $request->input('pdfbudget'),
        ];

        $update = $this->formatt::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

    	if($update){
            activity('format-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'format' => $id, 'view' => '1'])
            ->log('Alterou o formatação ');

			return response()->json([
			    'SUCESS' => 'Formatação editada com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Formatação não alterada, esta formatação não existe'
			], 422);         		
    	}
    }

    public function delformat(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->formatt::where(['tenantid' => $tenantid, 'id' => $id])->delete();
    	
    	if($delete){
            activity('format-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'format' => $id, 'view' => '1'])
            ->log('Deletou o formatação ');

			return response()->json([
			    'SUCESS' => 'Formatação excluida com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Formatação não excluida, este Formatação não existe'
			], 422);         		
    	}   
    }

    public function formattext(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $result = $this->formattext
        ->where('tenantid', $tenantid)
        ->get();
        return response()->json($result, 200);  
    }

    public function upformattext(RulesPostFormatt $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'type' => $request->input('type')
        ];
        $update = $this->formattext::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('format-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'format' => $id, 'view' => '1'])
            ->log('Alterou o formatação ');

            return response()->json([
                'SUCESS' => 'Formatação editada com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Formatação não alterada, esta formatação não existe'
            ], 422);                 
        }
    }
}
