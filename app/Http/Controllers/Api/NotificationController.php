<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use App\Models\Notifications;


class NotificationController extends Controller
{
    use GetTenantid;

    private $notification;

    public function __construct(Notification $notification){
        $this->notification = $notification;
    }

    public function notification(){
        $result = $this->notification->get();
    	return response()->json($result, 200);
	}

	public function addnotification(Request $request){
        $data = [
            'notifications' => $request['notifications'],
            'iten1' => $request['iten1'],            
            'iten2' => $request['iten2'],
            'iten3' => $request['iten3'],
            'iten4' => $request['iten4'],
            'iten5' => $request['iten5'],                        
            'iten6' => $request['iten6'], 
            'iten7' => $request['iten7'],
            'iten8' => $request['iten8'],
            'iten9' => $request['iten9'],                                                
            'iten10' => $request['iten10'], 
        ];
        $create = $this->notification::create($data);

		return response()->json([
		    'SUCESS' => 'Notificação salva com sucesso'
		]);  

	}

	public function upnotification(Request $request, $id){
        $attribute = 'id';
        $data = [
            'notifications' => $request['notifications'],
            'iten1' => $request['iten1'],            
            'iten2' => $request['iten2'],
            'iten3' => $request['iten3'],
            'iten4' => $request['iten4'],
            'iten5' => $request['iten5'],                        
            'iten6' => $request['iten6'], 
            'iten7' => $request['iten7'],
            'iten8' => $request['iten8'],
            'iten9' => $request['iten9'],                                                
            'iten10' => $request['iten10'],            
        ];

        $update = $this->notification::where('id', $id)->update($data);

    	if($update){
			return response()->json([
			    'SUCESS' => 'Notificação editada com sucesso'
			]);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Notificação não alterada, esta notificação não existe'
			]);         		
    	}

	}

	public function delnotification(Request $request, $id){
        $delete = $this->notification::delete($id);

        if($delete){
            return response()->json([
                'SUCESS' => 'A notificação excluida com sucesso'
            ]);             
        }else{
            return response()->json([
                'ERROR' => 'Notificação não excluida, esta notificação não existe'
            ]);                 
        }		
	}
}
