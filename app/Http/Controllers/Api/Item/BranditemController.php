<?php

namespace App\Http\Controllers\Api\Item;
/*
    Requeriments inputs
    brand,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostBranditem;
use App\Http\Traits\GetTenantid;

use App\Models\Branditem;

class BranditemController extends Controller
{
    use GetTenantid;

    private $brand;

	public function __construct(Branditem $brand){
		$this->middleware('privilege.item');
        $this->brand = $brand;
	}

    public function brand(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'brand';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'brand';

        $result = $this->brand::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function brandselectbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'brand';
        $sort = 'asc';
        $search = $request['search'];
        $typesort = 'brand';

        $result = $this->brand::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);  
    }

    public function addbrand(RulesPostBranditem $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $data = [
            'tenantid' => $tenantid,
            'brand' => $request->input('brand'),            
        ];        

        $create = $this->brand::create($data);

        activity('brand-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'brand' => $create->id, 'view' => '1'])
        ->log('Criou a marca ');

        return response()->json([
            'SUCESS' => 'Grupo de item salvo com sucesso'
        ], 200);  
    }

    public function upbrand(RulesPostBranditem $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'brand' => $request->input('brand'),
        ]; 
        $attribute = 'id';

        $update = $this->brand::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('brand-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'brand' => $id, 'view' => '1'])
            ->log('Alterou a marca ');

            return response()->json([
                'SUCESS' => 'Grupo de item editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Grupo de item não alterado, este Grupo de item não existe'
            ], 422);                 
        }
    }

    public function delbrand(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'id';

        $delete = $this->brand::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('brand-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'brand' => $id, 'view' => '1'])
            ->log('Deletou a marca ');

			return response()->json([
			    'SUCESS' => 'Grupo de item excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Grupo de item não excluido, este Grupo de item não existe'
			], 422);         		
    	}	
    }
}
