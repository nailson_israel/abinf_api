<?php

namespace App\Http\Controllers\Api\Item;
/*
    Requeriments inputs
    
    item,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostItem;
use App\Http\Traits\GetTenantid;

use App\Models\Item;

class ItemController extends Controller
{
    use GetTenantid;

    private $item;

	public function __construct(Item $item){
		$this->middleware('privilege.item');
        $this->item = $item;
	}

    public function item(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'item';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'item';
        $search = $request['search'];

        $result = $this->item::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function itemselectbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'item';
        $sort = 'asc';
        $typesort = 'item';
        $search = $request['search'];

        $result = $this->item::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();
        
        return response()->json($result, 200);  
    }

    public function additem(RulesPostItem $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'item' => $request->input('item')
        ];        

        $create = $this->item::create($data);

        activity('item-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'item' => $create->id, 'view' => '1'])
        ->log('Criou o item ');

        return response()->json([
            'SUCESS' => 'Item salvo com sucesso'
        ], 200);  
    }

    public function upitem(RulesPostItem $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $data = [
            'tenantid' => $tenantid,
            'item' => $request->input('item')
        ]; 
        $attribute = 'id';

        $update = $this->item::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('item-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'item' => $id, 'view' => '1'])
            ->log('Alterou o item ');

            return response()->json([
                'SUCESS' => 'Item editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Item não alterado, este item não existe'
            ], 422);                 
        }
    }

    public function delitem(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $delete = $this->item::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('item-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'item' => $id, 'view' => '1'])
            ->log('Deletou o item ');

			return response()->json([
			    'SUCESS' => 'Item excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Item não excluido, este item não existe'
			], 422);         		
    	}	
    }
}
