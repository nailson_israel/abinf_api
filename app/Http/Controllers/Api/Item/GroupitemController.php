<?php

namespace App\Http\Controllers\Api\Item;
/*
    Requeriments inputs
    group,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RulesPostGroupitem;
use App\Http\Traits\GetTenantid;

use App\Models\Groupitem;

class GroupitemController extends Controller
{
    use GetTenantid;

    private $group;

	public function __construct(Groupitem $group){
		$this->middleware('privilege.item');
        $this->group = $group;
	}

    public function group(Request $request){
        $tenantid = $this->GetTenant();
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'group';
        $search = $request['search'];
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'group';

        $result = $this->group::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);
    }

    public function groupselectbox(Request $request){
        $tenantid = $this->GetTenant();
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'group';
        $sort = 'asc';
        $typesort = 'group';
        $search = $request['search'];

        $result = $this->group::whereNull('deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->where('tenantid', $tenantid)->get();

        return response()->json($result, 200);  
    }

    public function addgroup(RulesPostGroupitem $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $data = [
            'tenantid' => $tenantid,
            'group' => $request->input('group'),            
        ];        

        $create = $this->group::create($data);

        activity('groupitem-create')
        ->causedBy($userloged)
        ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupitem' => $create->id, 'view' => '1'])
        ->log('Criou o grupo de item ');

        return response()->json([
            'SUCESS' => 'Grupo de item salvo com sucesso'
        ], 200);  
    }

    public function upgroup(RulesPostGroupitem $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
                
        $data = [
            'tenantid' => $tenantid,
            'group' => $request->input('group'),
        ]; 
        $attribute = 'id';

        $update = $this->group::where(['tenantid' => $tenantid, 'id' => $id])
        ->whereNull('deleted_at')
        ->update($data);

        if($update){
            activity('groupitem-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupitem' => $id, 'view' => '1'])
            ->log('Alterou o grupo de item ');

            return response()->json([
                'SUCESS' => 'Grupo de item editado com sucesso'
            ], 200);             
        }else{
            return response()->json([
                'ERROR' => 'Grupo de item não alterado, este Grupo de item não existe'
            ], 422);                 
        }
    }

    public function delgroup(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        
        $attribute = 'id';

        $delete = $this->group::where(['tenantid' => $tenantid, 'id' => $id])->delete();

    	if($delete){
            activity('groupitem-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'groupitem' => $id, 'view' => '1'])
            ->log('Deletou o grupo de item ');

			return response()->json([
			    'SUCESS' => 'Grupo de item excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Grupo de item não excluido, este Grupo de item não existe'
			], 422);         		
    	}	
    }
}
