<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use DB;


use App\Models\Client;
use App\Models\Contact;
use App\Models\Talk;
use App\Models\Comment;
use App\Models\Itemcl;
use App\Models\Budget;
use App\Models\Itenbudget;

class ReportrecordController extends Controller
{
    use GetTenantid;  

    private $client;
    private $contact;
    private $talk;
    private $comment;
    private $itemcl;
    private $budget;

    public function __construct(Client $client, Contact $contact, Talk $talk, Comment $comment, Itemcl $itemcl, Budget $budget){
    	$this->middleware('privilege.reportrecord');
      $this->client = $client;
      $this->contact = $contact;
      $this->talk = $talk;
      $this->comment = $comment;     
      $this->itemcl =$itemcl;
      $this->budget = $budget;
    }

    public function recordclient(Request $request){
      $tenantid = $this->GetTenant();
      $attribute = 'talks.clientid';

    	$idclient = $request['client'];
    	$dados = $request['data'];
    	$contactr = $request['contacts'];
    	$talkr = $request['talk'];
    	$commentr = $request['comment'];
    	$itemclr = $request['itemcl'];
        $budget = $request['budget'];   	    	    	

    	if($request['data']){
        $client = $this->client::select('id', 'cod', 'client', 'reference', 'phone1', 'phone2', 'street', 'clients.number', 'complements', 'neighborhood', 'cep', 'city', 'uf', 'cnpjcpf', 'email', 'site', 'ie')
        ->whereNull('deleted_at')
        ->where(['id' => $idclient, 'tenantid' => $tenantid])
        ->get();

        }else{
    		$client = null;
    	}

    	if($request['contacts']){
            $contact = $this->contact::whereNull('deleted_at')
            ->where(['clientid' => $idclient, 'tenantid' => $tenantid])
            ->orderBy('contact', 'asc')
            ->get();    
    	}else{
    		$contact = null;
      }

    	if($request['talk']){

        $talk = $this->talk::select(DB::raw('talks.id, talks.userid, talks.clientid, subject, type, contactcl, userid, description, contacts.id as idcontact, contact, phone, contacts.email as emailcontact, contacts.departament as departament, date, TIME_FORMAT(hour, "%H:%i") as hour, legends.id as legendid, descriptionlegend, Day(talks.date) as day, date_format(talks.updated_at, "%H:%i") as hours, users.name as nameus, representeds.represented as representedrep'))
        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
        ->leftJoin('legends', 'legends', '=', 'talks.type')
        ->leftJoin('users', 'users.id', '=', 'talks.userid')
        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented')
        ->where(['isactivity' => 1, 'talks.tenantid' => $tenantid, 'talks.clientid' => $idclient])
        ->limit(5)
        ->get();

    	}else{
    		$talk = null;
    	}

    	if($request['comment']){

        $comment = $this->comment::whereNull('deleted_at')
            ->where(['clientid' => $idclient, 'tenantid' => $tenantid])
            ->orderBy('comment', 'asc')
            ->get();    
    	}else{
    		$comment = null;
    	}

    	if($request['itemcl']){
            
            $itemcl = $this->itemcl::whereNull('deleted_at')
            ->where(['clientid' => $idclient, 'tenantid' => $tenantid])
            ->orderBy('itemcl', 'asc')
            ->get();

    	}else{
    		$itemcl = null;
    	}

    	if($request['budget']){
            $budget =  $this->budget::whereNull('deleted_at')
            ->where(['client' => $idclient, 'tenantid' => $tenantid])
            ->orderBy('comment', 'asc')
            ->limit(4)
            ->get();
    	}else{
        $budget = null;
      }	

    	return response()
    	->json([
    		'client' => $client,
    		'contact' => $contact,
    		'talk' => $talk,
    		'comment' => $comment,
    		'itemcl' => $itemcl,
    		'budget' => $budget,
    		'dados' => $dados,
    		'contactr' => $contactr,
    		'talkr' => $talkr,
    		'commentr' => $commentr,
    		'itemclr' => $itemclr,
    	]);
    }
}
