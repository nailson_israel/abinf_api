<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use App\Models\Client;
use App\Models\Represented;
use App\Models\Product;

use DB;

class ReportbudgetController extends Controller
{
    use GetTenantid;

    private $client;
    private $represented;

    public function __construct(Client $client, Represented $represented, Product $product){
    	$this->middleware('privilege.reportbudget');
        $this->client = $client;
        $this->represented = $represented;
        $this->product = $product;
    }

    public function reportbudget(Request $request){
        $tenantid = $this->GetTenant();        

    	$represented = $request['represented'];
    	$client = $request['client'];
		$seller = $request['seller'];
    	$product = $request['product'];
    	$date1 = $request['date1'];
    	$date2 = $request['date2'];
    	$currentdata = $request['currentdata'];
    	$nf= $request['nf'];
    	$branch = $request['branch'];
    	$situation = $request['situation']; 
    	$dados = $request['dados'];     	 	
    	$mproduct = $request['mproduct'];     	
    	$mcomission = $request['mcomission'];     	
    	$itensab = $request['itensab'];
    	$type = $request['type'];   	
    	$phone = $request['phone'];

        if($request['date1'] != ''){
            //data_inicio
            $dt1 = explode('/', $request['date1']);
            $dateam1 = $dt1[2].'-'.$dt1[1].'-'.$dt1[0];
        }

        if($request['date2'] != ''){
            //data_fim
            $dt2 = explode('/', $request['date2']);
            $dateam2 = $dt2[2].'-'.$dt2[1].'-'.$dt2[0];

        }

        $query2 = "SELECT SUM(total) as totalfull, abi.id, budgetid, abcl.client, abcl.branch, representedid 
        FROM itenbudgets as abi 
        LEFT JOIN budgets as abb ON abb.id = abi.budgetid
        LEFT JOIN clients as abcl ON abcl.id = abb.client 
        WHERE abi.deleted_at IS NULL";

        if($request['represented'] != ''){
        	$and_represented = " AND abb.representedid = '$represented' ";
        	$query2 .= " AND abb.representedid = '$represented' ";
        }else{
        	$and_represented = '';
        }

        if($request['client'] != ''){
			$and_client = " AND abb.client = '$client' ";
			$query2 .= " AND abb.client = '$client' ";
        }else{
        	$and_client = '';
        }

        if($request['seller'] != ''){
			$and_seller = " AND abb.seller = '$seller' ";
            $query2 .= " AND abb.seller = '$seller' ";           
        }else{
        	$and_seller = '';
        }

        if($request['product'] != ''){
        	$var_product = ',abp.product ';
			$sql_product = '';
			$and_product = " AND abi.product = '$product' ";
            $query2 .= " AND abi.product = '$product' AND abi.deleted_at IS NULL ";
        }else{
        	$and_product = '';
        	$sql_product = '';
        	$var_product = '';
        }

        if($request['nf'] != ''){
        	$var_nf = ',nf ';
			$and_nf = " AND abi.nf = '$nf' ";
            $query2 .= " AND abi.nf = '$nf' ";
        }else{
        	$and_nf = '';
        } 

        if($request['branch'] != ''){
        	$var_branch = ',abcl.branch ';
			$and_branch = " AND abcl.branch = '$branch' ";
            $query2 .= " AND abcl.branch = '$branch' ";            
        }else{
        	$var_branch = '';        	
        	$and_branch = '';
        }  

        if($request['situation'] != ''){
        	$var_situation = ',abb.situation ';
			$and_situation = " AND abb.situation = '$situation' ";
            $query2 .= " AND abb.situation = '$situation' "; 
        }else{
        	$var_situation = '';        	
        	$and_situation = '';
        }                        

        if($request['type'] != ''){
        	$var_type = ',abb.type ';
			$and_type = " AND abb.type = '$type' ";
            $query2 .= " AND abb.type = '$type' ";             
        }else{
        	$var_type = '';        	
        	$and_type = '';
        }

		$query = "SELECT abb.id, abb.client, 
		DATE_FORMAT(abb.date, '%d/%m/%Y') as date, SUM(total) as totalorc,
        seller, form_pay, delivery, comment, delivery, transport, type, descont_value, abc.contact, 
        descont, representedid, abb.completed, situation, abcl.phone1, abcl.phone2, 
        abb.userid, mot_descart, abb.order, abr.represented as representada, abcl.client, abp.id,
        GROUP_CONCAT(abi.id,'*',abp.product,'*',amount,'*',abi.ipi,'*',unitary,'*',abi.total,'*',abp.unit,'*',abp.cod,'*',abi.comission,'*',abi.nf, '') as itensbudget
		FROM budgets as abb
        LEFT JOIN contacts as abc ON abb.contact = abc.id             
        LEFT JOIN itenbudgets as abi ON abb.id = abi.budgetid
        LEFT JOIN products as abp ON abi.product = abp.id 
        LEFT JOIN representeds as abr ON abb.representedid = abr.id
        LEFT JOIN clients as abcl ON abb.client = abcl.id		
		WHERE abb.deleted_at IS NULL AND abi.deleted_at IS NULL
		$and_product $and_client $and_seller $and_nf $and_branch $and_situation $and_type $and_represented";

       //condicional para datas
        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
            $query .= " AND abb.date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
            $query2 .= " AND abb.date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }

        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
            $query .= " AND abb.date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
            $query2 .= " AND abb.date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";

        }

        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
            $query .= " AND abb.date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
            $query2 .= " AND abb.date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";                
        }

        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
            $query .= " AND abb.date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
            $query2 .= " AND abb.date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";                
        }


        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
            $query .= " AND abb.date BETWEEN '$dateam1' AND '$dateam2' ";
            $query2 .= " AND abb.date BETWEEN '$dateam1' AND '$dateam2' ";
        }

        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){

            $currentdata = date('Y-m-d');

            $query .= " AND abb.date = '$currentdata'";
            $query2 .= " AND abb.date = '$currentdata'";
        }

        $query .= " GROUP BY abb.id ORDER BY abb.date DESC ";

        $tenantid = $this->GetTenant();

		if($client != ''){ 
            $clientquery = $this->client::whereNull('deleted_at')
            ->where('id', $client)
            ->get();
		}else{
			$clientquery = '';  
		}

		if($represented != ''){
            $representedquery = $this->represented::whereNull('deleted_at')
            ->where('id', $represented)
            ->get();
		}else{
			$representedquery = '';
		}  

		if($product != ''){
            $productquery = $this->product::whereNull('deleted_at')
            ->where('id', $represented)
            ->get();
		}else{
			$productquery = '';
		}  

    	$budget = DB::select($query); 
        $count = count($budget);

    	if(isset($query2)){
        	echo $query2;
        	$totalfull = DB::select($query2);     		
    	}		

		return response()->json([
				'budget' => $budget, 'totalfull' => $totalfull, 
				'count' => $count, 'representedquery' => $representedquery,
				'clientquery' => $clientquery,
				'represented' => $represented, 'client' => $client,
				'seller' => $seller, 'dados' => $dados,
				'mproduct' => $mproduct, 'mcomission' => $mcomission, 
				'count' => $count, 'totalfull' => $totalfull, 
				'type' => $type, 'date1' => $date1, 
				'date2' => $date2, 'currentdata' => $currentdata, 
				'nf' => $nf, 'branch' => $branch, 
				'product' => $product, 
				'productquery' => $productquery, 
				'situation' => $situation,

				]);


    }

    public function itenbudget(Request $request, $idbudget, $dados, $nf, $product){
        $tenantid = $this->GetTenant();

		if($dados != '-'){
			$queryitens2 = ', itenbudgets.nf, itenbudgets.entregue, DATE_FORMAT(itenbudgets.date, "%d/%m/%Y") as date ';
		}else{
			$queryitens2 = ' ';
		}

		if($nf != '-'){
			$querycomplementsitens = " AND nf = '$nf' ";
		}else{
			$querycomplementsitens = ' ';
		}

		if($product != '-'){
			$querycomplementsitensproduct = " AND products.id = '$product' "; 
		}else{
			$querycomplementsitensproduct = ' '; 
		}	

		echo $queryitens = "SELECT itenbudgets.id, products.unit,  products.cod , products.product, itenbudgets.amount, itenbudgets.nf, itenbudgets.unitary, itenbudgets.ipi, itenbudgets.total"
		.$queryitens2
		.
		"FROM itenbudgets LEFT JOIN products ON itenbudgets.product = products.id WHERE itenbudgets.deleted_at IS NULL AND budgetid = '$idbudget'"
		.$querycomplementsitens
		.$querycomplementsitensproduct;

    	$itensbudget = DB::select($queryitens); 

		 return response()->json($itensbudget); 
    }

    public function billing(Request $request){
        $tenantid = $this->GetTenant(); 
        $userloged = \Auth::guard('api')->user();

        //neste relatório teremos uma composição mes a mes, podendo ser selecionado por vendedor, cliente, e faixa de valor, o relatório inicial será com 6 meses , podendo ser alterada com 0 periodo de data escolhida.
        $client = $request['client'];
        $represented = $request['represented'];
        $seller = $request['seller'];
        $date1 = $request['date1'];
        $date2 = $request['date2'];
        $period = $request['period'];
        $uf = $request['uf'];
        $city = $request['city'];
        $mode = $request['mode'];

        if($mode != ''){
            $query = 'clients.client, total';
        }else{
            $query = "clients.id as id, clients.client, 
                SUM(case month(budgets.date) when 1 then total end) as janeiro, 
                SUM(case month(budgets.date) when 2 then total end) as fevereiro, 
                SUM(case month(budgets.date) when 3 then total end) as março, 
                SUM(case month(budgets.date) when 4 then total end) as abril, 
                SUM(case month(budgets.date) when 5 then total end) as maio, 
                SUM(case month(budgets.date) when 6 then total end) as junho, 
                SUM(case month(budgets.date) when 7 then total end) as julho, 
                SUM(case month(budgets.date) when 8 then total end) as agosto, 
                SUM(case month(budgets.date) when 09 then total end) as setembro, 
                SUM(case month(budgets.date) when 10 then total end) as outubro, 
                SUM(case month(budgets.date) when 11 then total end) as novembro, 
                SUM(case month(budgets.date) when 11 then total end) as dezembro, SUM(itenbudgets.total) as totalmonth";
        }

        $billing = DB::table('itenbudgets')
                ->select(DB::raw($query))
                ->leftJoin('budgets', 'budgets.id', '=', 'budgetid')
                ->leftJoin('clients', 'clients.id', '=', 'budgets.client');
                $billing->where(['situation' => 'concluido', 'budgets.type' => 1]);
                $billing->whereNull('itenbudgets.deleted_at');
                $request['client'] != '' ? $billing->where('budgets.client', $client) : '';
                $request['uf'] != '' ? $billing->where('clients.uf', $uf) : '';
                $request['city'] != '' ? $billing->where('clients.city', $city) : '';
                $request['seller'] != '' ? $billing->where('budgets.userid', $seller) : '';
                
                $isadmin = $userloged['isadmin'].'n';

                if($isadmin == '0n'){
                    $billing->where(['users.id' => $userloged->id]);
                }
                $billing->where(['budgets.tenantid' => $tenantid]);
                $billing->orderByRaw('itenbudgets.date asc')->groupBy('clients.client');

        $billing = $billing->get();

        return response()->json(['billing' => $billing], 200);
    }

    public function commissions(Request $request){
        return 'colocar aqui o total de comissoes';
    }


    public function commissionsMonth(Request $request){
        return 'colocar aqui o total de comissoes do mês';
    }


    public function reportsales1(Request $request){
       $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

        $query = "year(date) as year, 
                  (CASE month(date)
                    when 1 then 'Jan'
                    when 2 then 'Fev'
                    when 3 then 'Mar' 
                    when 4 then 'Abr'
                    when 5 then 'Mai'
                    when 6 then 'Jun'
                    when 7 then 'Jul'
                    when 8 then 'Ago'
                    when 9 then 'Set'
                    when 10 then 'Out'
                    when 11 then 'Nov'
                    when 12 then 'Dez'
                    END) as month,
                    count(id) as totalmonth";

        $sales = DB::table('budgets')
                ->select(DB::raw($query))
                ->orderByRaw('id asc')
                ->groupBy('month');

        $isadmin = $userloged['isadmin'].'n';

        if($isadmin == '0n'){
            $sales->where(['budges.userid' => $userloged->id]);
        }

        $sales->where('budgets.tenantid', $tenantid);
        $sales->where(['situation' => 'concluido', 'budgets.type' => 1]);

        $sales = $sales->get();        
        
        $year = [];
        $month = [];
        $totalmonth = [];
        foreach ($sales as $response) {
            $month[] = $response->month . ' ' . $response->year;
            $totalmonth[] = $response->totalmonth;
        }

        return response()->json([
            'month' => $month,
            'totalmonth' => $totalmonth,
        ]);
    }

    public function reportcommission1(Request $request){
       $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

        $query = "year(budgets.date) as year, (CASE month(budgets.date)
                    when 1 then 'Jan'
                    when 2 then 'Fev'
                    when 3 then 'Mar' 
                    when 4 then 'Abr'
                    when 5 then 'Mai'
                    when 6 then 'Jun'
                    when 7 then 'Jul'
                    when 8 then 'Ago'
                    when 9 then 'Set'
                    when 10 then 'Out'
                    when 11 then 'Nov'
                    when 12 then 'Dez'
                    END) as month, 
                    SUM(itenbudgets.comission) as commission, SUM(total * itenbudgets.comission / 100) as comissionvalue, total";

        $comission = DB::table('itenbudgets')
                ->select(DB::raw($query))
                ->leftJoin('budgets', 'budgets.id', '=', 'budgetid');
        
                $isadmin = $userloged['isadmin'].'n';

                if($isadmin == '0n'){
                    $comission->where(['budges.userid' => $userloged->id]);
                }
                $comission->where(['situation' => 'concluido', 'budgets.type' => 1]);
                $comission->whereNull('budgets.deleted_at');
                $comission->whereNull('itenbudgets.deleted_at');
                $comission->where(['itenbudgets.tenantid' => $tenantid, 'budgets.tenantid' => $tenantid]);
                $comission->orderByRaw('budgets.date asc')->groupBy('month');

        $comissions = $comission->get();

        $year = [];
        $month = [];
        $totalcomission = [];
        $totalrevenue = [];
        $comission = [];
        foreach ($comissions as $response) {
            $month[] = $response->month . ' ' . $response->year;
            $totalcomission[] = $response->comissionvalue;
            $totalrevenue[] = $response->total;
            $comission[] = $response->commission;
        }

        return response()->json([
            'year' => $year,
            'month' => $month,
            'totalcomission' => $totalcomission,
            'totalrevenue' => $totalrevenue,
            'comission' => $comission
        ]);
    }

    public function reportvaluessales(Request $request){
        $tenantid = $this->GetTenant(); 
        $userloged = \Auth::guard('api')->user();
        $isadmin = $userloged['isadmin'].'n';

        $query = "SUM(total) as revenue, SUM(comission) as comission, SUM(total * itenbudgets.comission / 100) as comissionvalue";

        $valueslaes = DB::table('itenbudgets')
                ->select(DB::raw($query))
                ->leftJoin('budgets', 'budgets.id', '=', 'budgetid');
    
        $valueslaes->where(['itenbudgets.tenantid' => $tenantid]);
        $valueslaes->where(['situation' => 'concluido', 'budgets.type' => 1]);
        $valueslaes->whereNull('itenbudgets.deleted_at');
        $valueslaes = $valueslaes->get();

        $minpercent = $valueslaes[0]->comissionvalue / $valueslaes[0]->revenue;
        $percent = $minpercent * 100;

        return response()->json([
            'revenue' => $valueslaes[0]->revenue,
            'comissionvalue' => $valueslaes[0]->comissionvalue,
            'percent' => $percent,
        ]);     
    }
}
