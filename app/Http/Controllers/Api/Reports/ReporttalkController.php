<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Http\Traits\GetTenantid;

use App\Models\Client;
use App\Models\Talk;
use App\Models\Represented;
use App\Models\Contacts;
use App\Models\User;
use App\Models\Legends;

use DB;

class ReporttalkController extends Controller
{
    use GetTenantid;

    private $represented;
    private $user;
    private $talk;
    private $clientt;

    public function __construct(Represented $represented, User $user, Talk $talk, Client $clientt){
        $this->middleware('privilege.reporttalk');  
        $this->represented = $represented;
        $this->user = $user;
        $this->talk = $talk;
        $this->clientt = $clientt;
    }

    public function reporttalk(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

        $client = $request['client']; 
        $uf = $request['uf'];
        $city = $request['city'];        
        $subject = $request['subject'];
        $showcity = $request['showcity'];
        $showcontacts = $request['showcontacts'];
        $showemail = $request['showemail'];
        $represented = $request['represented'];
        $resumelegends = $request['resumelegends'];
        $currentdata = $request['currentdata'];
        $type = $request['type'];
        $seller = $request['seller'];
        $mode = $request['mode'];
        $date1 = $request['date1'];
        $date2 = $request['date2'];
        $period = $request['period'];    
        

        $dateam1 = $request['date1'];
        $dateam2 = $request['date2'];
        $typereport = $request['typereport'];

        $isadmin = $userloged['isadmin'].'n';

        //verificar query das legendas
        if($type != ''){
            $query_legends = DB::table('legends')->select('legends', 'descriptionlegend')->where('legends', $type)->get();
        }else{
            $query_legends = '';
        }

        if($request['mode']){
            $request['showcontacts'] != '' ? $var_seller = ", name " : $var_seller = "";
            $request['showemail'] != '' ? $var_showemail = ", contacts.email as emailcontact " : $var_showemail = "";
            $request['showcity'] != '' ? $var_showcity = ", clients.city, clients.uf " :  $var_showcity = "";

            $query = "talks.id as idtalks, talks.clientid, subject, talks.clientid, talks.id, talks.date as dateact, TIME_FORMAT(hour, '%H:%i') as hour, Day(talks.date) as day, talks.updated_at as hours, descriptionlegend, isactivity, 
            type, contactcl, talks.userid as userid, users.name as name, talks.represented as represented, representeds.represented as representedrep,  description, client, contacts.contact, contacts.email as emailcontact, contacts.id, contacts.phone $var_seller $var_showcity ";

            $talk = DB::table('talks')
                        ->select(DB::raw($query))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $talk->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $talk->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $talk->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $talk->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $talk->where('uf', $uf) : "";
                        $request['city'] != '' ?  $talk->where('city', $city) : "";
                        $seller != '' ? $talk->where('users.id', $seller) : "";
                        $request['type'] != '' ? $talk->where('type', $type) : $talk->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $talk->where('talks.represented', $represented) : "";

                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $talk->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $talk->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $talk->whereRaw('talks.userid', $iduser);
                        }

            //para fazer a contagem 
            $ef = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $ef->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $ef->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $ef->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $ef->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $ef->where('uf', $uf) : "";
                        $request['city'] != '' ?  $ef->where('city', $city) : "";
                        $seller != '' ? $ef->where('users.id', $seller) : "";
                        $request['type'] != '' ? $ef->where('type', $type) : $ef->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $ef->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $ef->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $ef->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $ef->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $ef->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $ef->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $ef->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $ef->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $ef->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $ef->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $ef->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $ef->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $ef->whereRaw('talks.userid', $iduser);
                        }

                        $ef->where('type', 'ef');
                        $ef->where('talks.tenantid', $tenantid);

                        $ef = $ef->get()->count();

            //para fazer a contagem 
            $er = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $er->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $er->whereNull('talks.deleted_at')->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $er->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $er->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $er->where('uf', $uf) : "";
                        $request['city'] != '' ?  $er->where('city', $city) : "";
                        $seller != '' ? $er->where('users.id', $seller) : "";
                        $request['type'] != '' ? $er->where('type', $type) : $er->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $er->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $er->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $er->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $er->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $er->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $er->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $er->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $er->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $er->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $er->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $er->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $er->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $er->whereRaw('talks.userid', $iduser);
                        }

                        $er->where('type', 'er');
                        $er->where('talks.tenantid', $tenantid);
                        $er = $er->get()->count();

            $lf = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $lf->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $lf->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $lf->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $lf->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $lf->where('uf', $uf) : "";
                        $request['city'] != '' ?  $lf->where('city', $city) : "";
                        $seller != '' ? $lf->where('users.id', $seller) : "";
                        $request['type'] != '' ? $lf->where('type', $type) : $lf->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $lf->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $lf->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $lf->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $lf->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $lf->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $lf->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $lf->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $lf->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $lf->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $lf->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $lf->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $lf->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $lf->whereRaw('talks.userid', $iduser);
                        }

                        $lf->where('type', 'lf');
                        $lf->where('talks.tenantid', $tenantid);
                        $lf = $lf->get()->count();

            $lr = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $lr->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $lr->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $lr->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $lr->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $lr->where('uf', $uf) : "";
                        $request['city'] != '' ?  $lr->where('city', $city) : "";
                        $seller != '' ? $lr->where('users.id', $seller) : "";
                        $request['type'] != '' ? $lr->where('type', $type) : $lr->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $lr->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $lr->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $lr->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $lr->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $lr->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $lr->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $lr->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $lr->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $lr->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $lr->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $lr->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $lr->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $lr->whereRaw('talks.userid', $iduser);
                        }

                        $lr->where('type', 'lr');
                        $lr->where('talks.tenantid', $tenantid);
                        $lr = $lr->get()->count();

            $vf = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $vf->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $vf->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $vf->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $vf->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $vf->where('uf', $uf) : "";
                        $request['city'] != '' ?  $vf->where('city', $city) : "";
                        $seller != '' ? $vf->where('users.id', $seller) : "";
                        $request['type'] != '' ? $vf->where('type', $type) : $vf->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $vf->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $vf->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $vf->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $vf->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $vf->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $vf->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $vf->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $vf->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $vf->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $vf->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $vf->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $vf->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $vf->whereRaw('talks.userid', $iduser);
                        }

                        $vf->where('type', 'vf');
                        $vf->where('talks.tenantid', $tenantid);
                        $vf = $vf->get()->count();

            $vr = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $vr->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $vr->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $vr->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $vr->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $vr->where('uf', $uf) : "";
                        $request['city'] != '' ?  $vr->where('city', $city) : "";
                        $seller != '' ? $vr->where('users.id', $seller) : "";
                        $request['type'] != '' ? $vr->where('type', $type) : $vr->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $vr->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $vr->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $vr->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $vr->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $vr->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $vr->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $vr->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $vr->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $vr->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $vr->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $vr->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $vr->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $vr->whereRaw('talks.userid', $iduser);
                        }

                        $vr->where('type', 'vr');
                        $vr->where('talks.tenantid', $tenantid);
                        $vr = $vr->get()->count();

            $oth = DB::table('talks')
                        ->select(DB::raw('type'))
                        ->leftJoin('users', 'users.id', '=', 'talks.userid')
                        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                        ->leftJoin('legends', 'legends', '=', 'talks.type')
                        ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented');

                        $request['represented'] != '' ? $oth->leftJoin('representedclients', 'representedclients.representedid', '=', 'talks.represented') : '';
                        $oth->whereNull('talks.deleted_at')
                        ->where('talks.isactivity', 1); 

                        $request['client'] != '' ? $oth->where('talks.clientid', $client) : '';
                        $request['subject'] != '' ? $oth->where('subject', 'like', '%'.$subject.'%') : "";
                        $request['uf'] != '' ? $oth->where('uf', $uf) : "";
                        $request['city'] != '' ?  $oth->where('city', $city) : "";
                        $seller != '' ? $oth->where('users.id', $seller) : "";
                        $request['type'] != '' ? $oth->where('type', $type) : $oth->where('type', '<>', 'pendente');
                        // ver essa parada aqui que ta travando o relatório
                        $request['represented'] != '' ? $oth->where('talks.represented', $represented) : "";


                        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            $oth->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                             $oth->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
                            $oth->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
                            $vr->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
                            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
                        }
                        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
                            $oth->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
                            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
                        }
                        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
                            $currentdata = date('Y-m-d');
                            $oth->whereRaw("date = '$currentdata'");
                            //$query .= " AND date = '$currentdata'";
                        }

                        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
                            if($period == 1){
                               $oth->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }else if($period == 3){
                               $oth->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $oth->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
                            }else if($period == 6){
                               $oth->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
                            }else if($period == ''){
                               $oth->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
                            }
                        }

                        if($isadmin == '0n'){
                            $oth->whereRaw('talks.userid', $iduser);
                        }

                        $oth->where('type', 'x');
                        $oth->where('talks.tenantid', $tenantid);
                        $oth = $oth->get()->count();

            //email feito 
            $countef = $ef;
            //email recebido
            $counter = $er;
            //ligação feita
            $countlf = $lf;
            //ligação recebida
            $countlr = $lr;
            //visita feita
            $countvf = $vf;
            //visita recebida
            $countvr = $vr;
            
            $countother = $oth;

            $talk->where('talks.tenantid', $tenantid);

            $mode == 'resumido' ? $talk->orderBy('idclient', 'desc') :  $talk->orderBy('date', 'desc');



            if($represented != ''){
                $representedquery = $this->represented::select('id', 'represented', 'reference')
                ->whereNull('deleted_at')
                ->where(['tenantid' => $tenantid, 'id' => $represented])
                ->get();
            }else{
                $representedquery = "";
            }

            if($seller != ''){
                $attribute = 'id';

                $sellerquery = $this->user::select('users.id', 'name', 'user')
                ->whereNull('deleted_at')
                ->where('id', $seller)
                ->get();

            }else{
                $sellerquery = "";
            }

            if($request['client']){
                $attribute = 'clientid';
                $totalcontact = $this->talk::whereNull('deleted_at')
                ->where(['tenantid' => $tenantid, 'id' => $client])
                ->get();

                $countcontacts = count($totalcontact);

                $clientt = $this->clientt::select('id', 'client', 'reference')
                ->whereNull('deleted_at')
                ->where(['tenantid' => $tenantid, 'id' => $client])
                ->get();
            }else{
                $attribute = 'clientid';
                $totalcontact = $this->talk::whereNull('deleted_at')
                ->where(['tenantid' => $tenantid, 'clientid' => $client])
                ->get();

                $countcontacts = count($totalcontact);

                $clientt = '';
            }

            if ($typereport == 1){
                $talk = $talk->paginate(15);                
            } else {
                $talk = $talk->get();
            }

            //$talk = $this->arrayPaginator($talks, $request);

            if($mode == 'resumido'){
                $totalcontact = $this->talk::whereNull('deleted_at')
                ->where(['tenantid' => $tenantid, ''])
                ->get();

                $countcontacts = count($totalcontact);
          
                $totalenterprisequery = "SELECT clientid from talks LEFT JOIN clients ON clients.id = talks.clientid WHERE talks.tenantid = '$tenantid'" ;
                $request['client'] != '' ? $totalenterprisequery .= " AND clients.id = '$client '" : "";   
                $totalenterprisequery .= " GROUP by clients.id  ";

                $totalenterprise = DB::select($totalenterprisequery);

                $countenterprise = count($totalenterprise);

                return response()->json([
                    'talk' => $talk, 'client' => $clientt,
                    'uf' => $uf, 'mode' => $mode, 'city' => $city,
                    'subject' => $subject, 'showcity' => $showcity,
                    'showcontacts' => $showcontacts,'showemail' => $showemail,
                    'represented' => $represented, 'resumelegends' => $resumelegends,
                    'currentdata' => $currentdata, 'type' => $type,
                    'seller' => $seller, 'query' => $query,
                    'representedquery' => $representedquery,
                    'clientquery' => $clientquery,
                    'date1' => $date1, 'date2' => $date2,
                    'sellerquery' => $sellerquery, 
                    'countcontacts' => $countcontacts, 
                    'countenterprise' => $countenterprise, 
                    'countef' => $countef, 'counter' => $counter, 
                    'countlf' => $countlf, 'countlr' => $countlr, 
                    'countvf' => $countvf, 'countvr' => $countvr,
                    'countother' => $countother,
                    'query_legends' => $query_legends

                ]); 
            }else{
  
                return response()->json([
                    'talk' => $talk, 'mode' => $mode, 
                    'showemail' => $showemail, 'countef' => $countef, 
                    'counter' => $counter, 'countlf' => $countlf, 
                    'countlr' => $countlr, 'countvf' => $countvf, 
                    'countvr' => $countvr, 'resumelegends' => $resumelegends, 
                    'client' => $clientt, 'uf' => $uf, 
                    'city' => $city, 'type' => $type, 
                    'representedquery' => $representedquery,  
                    'subject' => $subject, 'currentdata' => $currentdata, 
                    'date1' => $date1, 'date2' => $date2, 
                    'sellerquery' => $sellerquery, 
                    'seller' => $seller, 
                    'showcontacts' => $showcontacts, 
                    'represented' => $represented, 
                    'showcity' => $showcity,
                    'countother' => $countother,
                    'query_legends' => $query_legends
                ]);

                //echo $query;

                //return $talkclient = DB::select($query);                    
                
                //return view('client.reporttalk.reporttalkcl', ['talkclient' => $talkclient, 'mode' => $mode, 'showemail' => $showemail, 'countef' => $countef, 'counter' => $counter, 'countlf' => $countlf, 'countlr' => $countlr, 'countvf' => $countvf, 'countvr' => $countvr, 'resumelegends' => $resumelegends, 'client' => $client, 'uf' => $uf, 'city' => $city, 'type' => $type, 'representedquery' => $representedquery, 'clientquery' => $clientquery, 'subject' => $subject, 'currentdata' => $currentdata, 'date1' => $date1, 'date2' => $date2, 'sellerquery' => $sellerquery, 'seller' => $seller, 'showcontacts' => $showcontacts, 'represented' => $represented, 'showcity' => $showcity]);
                
            }
        }
    }

    public function reporttalkGraphic1(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

        $client = $request['client']; 
        $uf = $request['uf'];
        $city = $request['city'];        
        $subject = $request['subject'];
        $showcity = $request['showcity'];
        $showcontacts = $request['showcontacts'];
        $showemail = $request['showemail'];
        $represented = $request['represented'];
        $resumelegends = $request['resumelegends'];
        $currentdata = $request['currentdata'];
        $type = $request['type'];
        $seller = $request['seller'];
        $mode = $request['mode'];
        $date1 = $request['date1'];
        $date2 = $request['date2'];     
        $period = $request['period'];   
        $dateam1 = $request['date1'];
        $dateam2 = $request['date2'];
        $typereport = $request['typereport'];

        $isadmin = $userloged['isadmin'].'n';

        $query = "year(date) as year, 
                  (CASE month(date)
                    when 1 then 'Jan'
                    when 2 then 'Fev'
                    when 3 then 'Mar' 
                    when 4 then 'Abr'
                    when 5 then 'Mai'
                    when 6 then 'Jun'
                    when 7 then 'Jul'
                    when 8 then 'Ago'
                    when 9 then 'Set'
                    when 10 then 'Out'
                    when 11 then 'Nov'
                    when 12 then 'Dez'
                    END) as month, 
                    count(talks.id) as totalmonth";

        $talk = DB::table('talks')
                ->select(DB::raw($query))
                ->leftJoin('users', 'users.id', '=', 'talks.userid')
                ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                ->leftJoin('legends', 'legends', '=', 'talks.type')
                ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented')
                ->orderByRaw('date asc')
                ->groupBy('month');

        $request['client'] != '' ? $talk->where('talks.clientid', $client) : '';
        $request['subject'] != '' ? $talk->where('subject', 'like', '%'.$subject.'%') : "";
        $request['uf'] != '' ? $talk->where('uf', $uf) : "";
        $request['city'] != '' ?  $talk->where('city', $city) : "";
        $seller != '' ? $talk->where('users.id', $seller) : "";
        $request['type'] != '' ? $talk->where('type', $type) : $talk->where('type', '<>', 'pendente');
        // ver essa parada aqui que ta travando o relatório
        $request['represented'] != '' ? $talk->where('talks.represented', $represented) : "";
        $talk->whereNull('talks.deleted_at')
        ->where('talks.isactivity', 1); 
        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
             $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
        }
        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
            $currentdata = date('Y-m-d');
            $talk->whereRaw("date = '$currentdata'");
            //$query .= " AND date = '$currentdata'";
        }

        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
            if($period == 1){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
            }else if($period == 3){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
            }else if($period == 6){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
            }else if($period == 6){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
            }else if($period == ''){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
            }
        }

        if($isadmin == '0n'){
            $talk->whereRaw('talks.userid', $iduser);
        }
        $talk->where('talks.tenantid', $tenantid);
        $talk = $talk->get();
        ['talk' => $talk];
        $totalmonth = [];
        $year = [];
        $month = [];
        $yearmonth = [];
        foreach ($talk as $response) {
            $totalmonth[] = $response->totalmonth;
            /*$year[] = $response->year;
            $month[] = $response->month;*/
            $yearmonth[] =$response->month . ' ' . $response->year;
        }

        return response()->json([
            'totalmonth' => $totalmonth,
            'yearmonth' => $yearmonth
        ]);
    }

    public function reporttalkGraphic2(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

        $client = $request['client']; 
        $uf = $request['uf'];
        $city = $request['city'];        
        $subject = $request['subject'];
        $showcity = $request['showcity'];
        $showcontacts = $request['showcontacts'];
        $showemail = $request['showemail'];
        $represented = $request['represented'];
        $resumelegends = $request['resumelegends'];
        $currentdata = $request['currentdata'];
        $type = $request['type'];
        $seller = $request['seller'];
        $mode = $request['mode'];
        $date1 = $request['date1'];
        $date2 = $request['date2'];     
        $period = $request['period'];
        $dateam1 = $request['date1'];
        $dateam2 = $request['date2'];
        $typereport = $request['typereport'];

        $isadmin = $userloged['isadmin'].'n';

        $query = "
        UPPER(type) as type, count(type) as typeamount, descriptionlegend, 
        (CASE
        when type = 'ef' then '#104E8B'
        when type = 'er 'then '#4F94CD'
        when type = 'lf' then 'rgb(0,128,128)' 
        when type = 'lr' then 'rgb(32,178,170)'
        when type = 'vf' then '#473C8B'
        when type = 'vr' then '#836FFF'
        when type = 'ew' then 'rgb(255,215,0)'
        when type = 'rw' then 'rgb(250,250,210)'
        when type = 'x' then 'rgb(238,232,170)'
        END) as colors
        ";

        $talk = DB::table('talks')
                ->select(DB::raw($query))
                ->leftJoin('users', 'users.id', '=', 'talks.userid')
                ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
                ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
                ->leftJoin('legends', 'legends', '=', 'talks.type')
                ->leftJoin('representeds', 'representeds.id', '=', 'talks.represented')
                //->orderByRaw('date asc')
                ->groupBy('type');

        $request['client'] != '' ? $talk->where('talks.clientid', $client) : '';
        $request['subject'] != '' ? $talk->where('subject', 'like', '%'.$subject.'%') : "";
        $request['uf'] != '' ? $talk->where('uf', $uf) : "";
        $request['city'] != '' ?  $talk->where('city', $city) : "";
        $seller != '' ? $talk->where('users.id', $seller) : "";
        $request['type'] != '' ? $talk->where('type', $type) : $talk->where('type', '<>', 'pendente');
        // ver essa parada aqui que ta travando o relatório
        $request['represented'] != '' ? $talk->where('talks.represented', $represented) : "";
        $talk->whereNull('talks.deleted_at')
        ->where('talks.isactivity', 1); 
        if(isset($dateam1) && !isset($dateam2) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam1) && !isset($dateam2) && $currentdata != ''){
             $talk->whereRaw("date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam1' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam2) && !isset($dateam1) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam2) && !isset($dateam1) && $currentdata != ''){
            $talk->whereRaw("date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)");
            //$query .= " AND date BETWEEN '$dateam2' AND DATE_ADD(NOW(), INTERVAL +30 DAY)";
        }
        if(isset($dateam1) && isset($dateam2) && $currentdata == ''){
            $talk->whereRaw("date BETWEEN '$dateam1' AND '$dateam2'");
            //$query .= " AND date BETWEEN '$dateam1' AND '$dateam2' ";
        }
        if(!isset($dateam1) && !isset($dateam2) && $currentdata != ''){
            $currentdata = date('Y-m-d');
            $talk->whereRaw("date = '$currentdata'");
            //$query .= " AND date = '$currentdata'";
        }

        if(!isset($dateam1) && !isset($dateam2) && $currentdata == ''){
            if($period == 1){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
            }else if($period == 3){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE()");
            }else if($period == 6){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()");
            }else if($period == 6){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 12 MONTH AND CURDATE()");
            }else if($period == ''){
               $talk->whereRaw("date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()");
            }
        }
        
        if($isadmin == '0n'){
            $talk->whereRaw('talks.userid', $iduser);
        }
        $talk->where('talks.tenantid', $tenantid);
        $talk = $talk->get();
        ['talk' => $talk];

        $legend = [];
        $totallegend = [];
        $typelegend = [];
        $colors = [];
        foreach ($talk as $response) {
            $legend[] = $response->descriptionlegend;
            $totallegend[] = $response->typeamount;
            $typelegend[] = $response->type . '-' . $response->descriptionlegend;
            $colors[] = $response->colors;
        }

        return response()->json([
            'legend' => $legend,
            'totallegend' => $totallegend,
            'typelegend' => $typelegend,
            'colors' => $colors
        ]);
    }
}
