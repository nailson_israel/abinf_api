<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use DB;

use App\Models\Represented;
use App\Models\Comment;
use App\Models\Itemcl;
use App\Models\Contact;
use App\Models\Groupclient;
use App\Models\Client;
use App\Models\Branchclient;

class ReportclientController extends Controller
{
    use GetTenantid;

    private $represented;
    private $comment;
    private $itemcl;
    private $contact;
    private $groupcl;
    private $branch;
    private $pagination;

    public function __construct(Represented $represented, Comment $comment, Itemcl $itemcl, Contact $contact, Groupclient $groupcl, Client $client, Branchclient $branch){
    	$this->middleware('privilege.reportclient');
    	$this->represented = $represented;
    	$this->comment = $comment;
    	$this->itemcl = $itemcl;
    	$this->contact = $contact;
    	$this->groupcl = $groupcl;
    	$this->client = $client;
    	$this->branch = $branch;
        $this->pagination = config('responses.pagination');
    }
	
	public function reportclient(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

		$idclient = $request['client'];
		$represented = $request['represented'];
		$uf = $request['uf'];
		$group = $request['group'];
		$city = $request['city'];
		$route = $request['route'];
		$branch = $request['branch'];
		$datasheet = $request['datasheet'];
		$contacts = $request['contacts'];
		$itemcl = $request['itemcl'];            
		$order = $request['order'];
        $typereport = $request['typereport'];
		$concat =  $contacts .= $itemcl;

        $isadmin = $userloged['isadmin'].'n';

		$request['datasheet'] != '' ? $var_datasheet = "street, cep, neighborhood, cnpjcpf, site, number " : $var_datasheet = ""; 

		$query = "id, cod, client, reference, ie, city, uf, phone1, phone2, clients.email, complements, $var_datasheet";

		$client = DB::table('clients')
				->select(DB::raw($query));

				$request['represented'] != '' ? $client->leftJoin('representedclients', 'representedclients.clientid', '=', 'clients.id') : "";

				$request['client'] != '' ? $client->where('clients.id', $idclient) : "";
				$request['branch'] != '' ?  $client->where('branch', $branch) : "";
				$request['group'] != '' ? $client->where('group', $group) : "";
				$request['route'] != '' ? $client->where('route', $route) : "";
				$request['uf'] != '' ? $client->where('uf', $uf) : "";
				$request['city'] != '' ? $client->where('city', $city) : "";
				$request['represented'] != '' ? $client->where('representedclients.representedid', $represented) : "";

				$client->where('clients.tenantid', $tenantid);

				$client->whereNull('clients.deleted_at');

		        if($isadmin == '0n'){
		            $client->where('clients.userid', $iduser);
		        }

				$client->orderByRaw($order, 'asc');
				$client->groupBy('client');		

	            if ($typereport == 1){
	                $client = $client->paginate(15);                
	            } else {
	                $client = $client->get();
	            }


				if($represented != ''){
					$represent = $this->represented::whereNull('deleted_at')
					->where(['tenantid' => $tenantid, 'id' => $represented])
					->get();
				}else{
					$represent = '';
				}

				if($idclient != ''){
					$clientc = $this->client::whereNull('deleted_at')
					->where(['tenantid' => $tenantid, 'id' => $idclient])
					->get();
				}else{
					$clientc = '';
				}
/*
		$request['represented'] != '' ? $sql_represented = " LEFT JOIN representedclients ON representedclients.clientid = clients.id" : $sql_represented = "";

		$request['datasheet'] != '' ? $var_datasheet = ", street, cep, neighborhood, cnpjcpf, site, number " : $var_datasheet = "";       		
		
		$query = "SELECT id, client, city, uf, phone1, phone2, clients.email $var_datasheet FROM clients $sql_represented WHERE clients.deleted_at IS NULL";
		$request['client'] != '' ? $query .= " AND clients.id = '$idclient' " : "";
		$request['branch'] != '' ? $query .= " AND clients.branch = '$branch' " : "";
		$request['group'] != '' ? $query .= " AND clients.group = '$group' " : "";
		$request['route'] != '' ? $query .= " AND clients.route = '$route' " : "";
		$request['uf'] != '' ? $query .= " AND clients.uf = '$uf' " : "";
		$request['city'] != '' ? $query .= " AND clients.city = '$city' " : "";	
		$request['represented'] != '' ? $query .= " AND representedclients.idrepresented = '$represented' " : "";
      	$query .= " AND clients.tenantid = $tenantid GROUP BY id ORDER BY $order ASC";					
		$client = DB::select($query);

*/

		return response()->json([
			'client' => $client,
			'represent' => $represent,
			'datasheet' => $datasheet,
			'contacts' => $contacts,
			'itemcl' => $itemcl,
			'idclient' => $idclient,
			'represented' => $represented,
			'group' => $group,
			'uf' => $uf,
			'city' => $city,
			'route' => $route,
			'branch' => $branch,
			'clientc' => $clientc
		]);
	}

	public function reportclientGraphic1(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

		$idclient = $request['client'];
		$represented = $request['represented'];
		$uf = $request['uf'];
		$group = $request['group'];
		$city = $request['city'];
		$route = $request['route'];
		$branch = $request['branch'];
		$datasheet = $request['datasheet'];
		$contacts = $request['contacts'];
		$itemcl = $request['itemcl'];            
		$order = $request['order'];
        $typereport = $request['typereport'];
		$concat =  $contacts .= $itemcl;

		$isadmin = $userloged['isadmin'].'n';

		$query = "clients.group, count(id) as qty, 
		(CASE
        when clients.group = 'ativo' then '#20B2AA'
        when clients.group = 'prospecto' then '#87CEFA'
        when clients.group = 'inativo' then '#7B68EE'
        END) as colors";

        $client = DB::table('clients')
                ->select(DB::raw($query))
                ->orderByRaw('cod asc')
                ->groupBy('clients.group');


				$request['represented'] != '' ? $client->leftJoin('representedclients', 'representedclients.clientid', '=', 'clients.id') : "";

				$request['client'] != '' ? $client->where('clients.id', $idclient) : "";
				$request['branch'] != '' ?  $client->where('branch', $branch) : "";
				$request['group'] != '' ? $client->where('group', $group) : "";
				$request['route'] != '' ? $client->where('route', $route) : "";
				$request['uf'] != '' ? $client->where('uf', $uf) : "";
				$request['city'] != '' ? $client->where('city', $city) : "";
				$request['represented'] != '' ? $client->where('representedclients.representedid', $represented) : "";

				$client->where('clients.tenantid', $tenantid);

				$client->whereNull('clients.deleted_at');

		        if($isadmin == '0n'){
		            $client->where('clients.userid', $iduser);
		        }

	            $client = $client->get();

		        $qty = [];
		        $group = [];
		        $colors = [];
		        foreach ($client as $response) {
		            $qty[] = $response->qty;
		            $group[] = $response->group;
		            $colors[] = $response->colors;
		        }

		        return response()->json([
		            'qty' => $qty,
		            'group' => $group,
		            'colors' => $colors
		        ]);
	}

	public function reportclientGraphic2(Request $request){
       $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();
        $iduser = $userloged['id'];

		$idclient = $request['client'];
		$represented = $request['represented'];
		$uf = $request['uf'];
		$group = $request['group'];
		$city = $request['city'];
		$route = $request['route'];
		$branch = $request['branch'];
		$datasheet = $request['datasheet'];
		$contacts = $request['contacts'];
		$itemcl = $request['itemcl'];            
		$order = $request['order'];
        $typereport = $request['typereport'];
		$concat =  $contacts .= $itemcl;

		$isadmin = $userloged['isadmin'].'n';

        $query = "year(clients.created_at) as year, 
                  (CASE month(clients.created_at)
                    when 1 then 'Jan'
                    when 2 then 'Fev'
                    when 3 then 'Mar' 
                    when 4 then 'Abr'
                    when 5 then 'Mai'
                    when 6 then 'Jun'
                    when 7 then 'Jul'
                    when 8 then 'Ago'
                    when 9 then 'Set'
                    when 10 then 'Out'
                    when 11 then 'Nov'
                    when 12 then 'Dez'
                    END) as month,
                    count(id) as totalmonth";

        $client = DB::table('clients')
                ->select(DB::raw($query))
                ->orderByRaw('id asc')
                ->groupBy('month');


				$request['represented'] != '' ? $client->leftJoin('representedclients', 'representedclients.clientid', '=', 'clients.id') : "";

				$request['client'] != '' ? $client->where('clients.id', $idclient) : "";
				$request['branch'] != '' ?  $client->where('branch', $branch) : "";
				$request['group'] != '' ? $client->where('group', $group) : "";
				$request['route'] != '' ? $client->where('route', $route) : "";
				$request['uf'] != '' ? $client->where('uf', $uf) : "";
				$request['city'] != '' ? $client->where('city', $city) : "";
				$request['represented'] != '' ? $client->where('representedclients.representedid', $represented) : "";

				$client->where('clients.tenantid', $tenantid);

				$client->whereNull('clients.deleted_at');

		        if($isadmin == '0n'){
		            $client->where('clients.userid', $iduser);
		        }

	            $client = $client->get();

		        $year = [];
		        $month = [];
		        $totalmonth = [];
		        foreach ($client as $response) {
		            $month[] = $response->month . ' ' . $response->year;
		        	$totalmonth[] = $response->totalmonth;
		        }

		        return response()->json([
		            'year' => $year,
		            'month' => $month,
		            'totalmonth' => $totalmonth
		        ]);
	}


	public function comment(Request $request, $idclient){
		$tenantid = $this->GetTenant();

		$result = $this->comment::whereNull('deleted_at')
		->where(['tenantid' => $tenantid, 'id' => $idclient])
		->get();

		return response()->json($result, 200);
	}

	public function contact(Request $request, $idclient){
		$tenantid = $this->GetTenant();
		
		$result = $this->contact::whereNull('deleted_at')
		->where(['tenantid' => $tenantid, 'id' => $idclient])
		->get();

		return response()->json($result, 200);
	}

	public function itemcl(Request $request, $idclient){
		$tenantid = $this->GetTenant();
		
		$result = $this->itemcl::whereNull('deleted_at')
		->where(['tenantid' => $tenantid, 'id' => $idclient])
		->get();

		return response()->json($result, 200);
	}

	public function emails(Request $request){
		$tenantid = $this->GetTenant();

		$result = $this->contact::whereNull('deleted_at')
		->where('tenantid', $tenantid)
		->get();

		return response()->json($result, 200);	
	}		
}
