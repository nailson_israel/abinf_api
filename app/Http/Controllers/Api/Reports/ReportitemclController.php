<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;
use App\Models\Api\Client;
use DB;
use App\Models\Api\Itemcl;

class ReportitemclController extends Controller
{
    use GetTenantid;

    public function __construct(){
    	$this->middleware('privilege.reportitemcl');    	
    }

    public function reportitemcl(Request $request){
        $tenantid = $this->GetTenant();

    	$client = $request['client'];
    	$groupcl = $request['groupcl'];
    	$brandcl = $request['brandcl'];
    	$order = $request['order'];
    	$uf = $request['uf'];
    	$city = $request['city'];

		$query = "SELECT itemcl, brandcl, groupcl, amountcl, itemcls.id, clientid, client, reference, clients.id
    	FROM itemcls 
    	LEFT JOIN clients ON clients.id = clientid WHERE itemcls.deleted_at IS NULL AND itemcls.tenantid = '$tenantid'";

    	if($client != ''){
    		$query .= " AND clients.id = '$client' ";

    		$queryclient = "SELECT client FROM clients WHERE id = '$client' ";
            $clientquery = DB::select($queryclient);

    	}else{
    		$queryclient = '';
            $clientquery = '';     
    	}

    	$groupcl != '' ? $query .= " AND groupcl = '$groupcl'" : "";
    	$brandcl != '' ? $query .= " AND brandcl = '$brandcl' " : "";
    	$uf != '' ? $query .= " AND uf = '$uf' " : "";
    	$city != '' ? $query .= " AND city = '$city' " : "";

    	if($order == 'm'){
    		$query .= ' ORDER BY brandcl ';
    	}elseif($order == 'i'){
    		$query .= ' ORDER BY itemcl ';
    	}elseif($order == 'e'){
    		$query .= ' ORDER BY client ';
    	}

        $query;
    	$itensclient = DB::select($query);
        $count = count($itensclient);

        return response()->json([
        	'itensclient' => $itensclient,
        	'client' => $client,
        	'clientquery' => $clientquery,
        	'uf' => $uf,
        	'city' => $city,
        	'groupcl' => $groupcl,
        	'brandcl' => $brandcl,
        	'count' => $count,
        ]);
    }
}
