<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;
//Repository
use App\Repositories\Product\ProductRepository as Product;
//Criteria
use App\Repositories\Criteria\Reports\Reportproduct\ReportproductnogroupCriteria;
use App\Repositories\Criteria\Reports\Reportproduct\ReportproductgroupCriteria;


class ReportproductController extends Controller
{
    use GetTenantid;

    private $product;

    public function __construct(Product $product){
    	$this->middleware('privilege.reportproduct');  
    	$this->product = $product;
    }

    public function reportproduct(Request $request){
        $tenantid = $this->GetTenant();

		$ordem = $request['tiporelatorio'];
		$group = $request['groupprod'];
		$min = $request['minvalue'];
		$max = $request['maxvalue'];

		if($group == null){

        	$this->product->pushCriteria(new ReportproductnogroupCriteria($min, $max, $ordem));
        	$reportprod = $this->product->allNoPaginate();
				
			['reportprod' => $reportprod];
			foreach ($reportprod as $dados) {
				$dados->groupproduct;
				$group = '';
			}
		}else{
        	$this->product->pushCriteria(new ReportproductgroupCriteria($min, $max, $ordem, $group));
        	$reportprod = $this->product->allNoPaginate();			

			['reportprod' => $reportprod];

			foreach ($reportprod as $dados) {
				$dados->groupproduct;
				$group = $dados->groupproduct;
			}
		}

		return response()->json([
			'reportprod' => $reportprod,
			'group' => $group,
		]);    	
    }
}
