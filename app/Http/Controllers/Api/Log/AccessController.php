<?php

namespace App\Http\Controllers\Api\Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GetTenantid;

use App\Models\Accessregister;

class AccessController extends Controller
{
    use GetTenantid; 

    private $accessregister;

    public function __construct(Accessregister $accessregister){
      $this->accessregister = $accessregister;
    }

    public function accessregister(){
        $tenantid = $this->GetTenant();
        $attribute = 'accessregisters.tenantid';
        
        $result = $this->accessregister::whereNull('deleted_at')
        ->where(['tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200); 
    }
}
