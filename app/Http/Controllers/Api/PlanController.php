<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Traits\GetTenantid;


use App\Criteria\WhereAttributeCriteria;
use App\Criteria\TenantCriteria;
use App\Criteria\Plan\PlanCriteria;

use App\Models\Tenant;
use App\Models\Plan;

class PlanController extends Controller
{
	use GetTenantid;

	public $tenant;
	public $plan;

	public function __construct(Tenant $tenant, Plan $plan){
        $this->tenant = $tenant;
        $this->plan = $plan;
	}

	public function myplan () {
       	$tenantid = $this->GetTenant();
		$result = $this->plan::whereNull('deleted_at')->where('tenantid', $tenantid)->first();
		return response()->json($result, 200);
	}

	public function amountuserAndprice(){
       	$tenantid = $this->GetTenant();
		$result = $this->tenant::select('tenants.amountuser', 'plans.value as value')
        ->leftJoin('plans', 'tenants.id', '=', 'plans.tenantid')
        ->whereNull('tenants.deleted_at')->where('tenants.id', $tenantid)->first();

		return $result;
	}

}
