<?php

namespace App\Http\Controllers\Api\Pagseguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Traits\Pagseguro;
use App\Http\Traits\GetTenantid;

use App\Events\NotificationPayment;
use App\Events\NotificationAdhesion;

use App\Models\Plan;
use App\Models\Payment;
use App\Models\Tenant;
use App\Models\User;
use App\Models\Company;
use App\Models\States;

use Illuminate\Support\Facades\Mail;
use App\Mail\sendLogsEmail;

class PagseguroController extends Controller
{
    use GetTenantid, Pagseguro;
 
    private $user;
    private $company;
    private $state;
    private $tenant;

    public function __construct(User $user, Company $company, States $state, Tenant $tenant)
    {
        $this->user = $user;
        $this->company = $company;
        $this->state = $state;
        $this->tenant = $tenant;
    }

    /*sessão para poder gerar o tken do cartão de crédito que vem da api js do pagseguro*/
    public function session(){
        $url = $this->Uri() . "v2/sessions?email=".$this->Auth()."&token=".$this->Token();

    	$tenantid = $this->GetTenant();    
        $userloged = \Auth::guard('api')->user();

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '');
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $rtn = "cURL Error #:" . $err;
		} else {
		  	$rtn = $response;
			$xml = simplexml_load_string($rtn);
			//$json = json_encode($xml);
		    activity('payment-session-create')
		     ->causedBy($userloged)
		     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'view' => '0'])
		     ->log('Sessão criada');
		}

		return response()->json($xml);
    }

    /*lista as ordens de pagamento de uma determinada reccorrencia, 
    a recorrencia vé recuperada pelo tenant internament*/
    public function listOrders(Request $request){
    	$status = $request['status'];

    	$tenantid = $this->GetTenant();    
        $userloged = \Auth::guard('api')->user();

    	$preaprovalcode = Plan::where('tenantid', $tenantid)->first();

    	if($preaprovalcode->referenceplan == $this->planFree()){
			return null;
    	}else{

	    	if($request['status']){
				$status = '&status=' . $status;
	    	}else{
	    		$status = '';
	    	}

	        $url = $this->Uri() . "pre-approvals/".$preaprovalcode->codeadesion."/payment-orders?email=".$this->Auth()."&token=".$this->Token() . $status;

	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
	        curl_setopt($curl, CURLOPT_HTTPGET, true);
	 
	        $response = curl_exec($curl);
	        $http = curl_getinfo($curl);

			$err = curl_error($curl);

			curl_close($curl);


			if ($err) {
			  $rtn = "cURL Error #:" . $err;
			} else {
			  	$rtn = $response;
				$xml = simplexml_load_string($rtn);
				//$json = json_encode($xml);

			    activity('payment-list-order')
			     ->causedBy($userloged)
			     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'view' => '0'])
			     ->log('Acessou a lista de ordens');
			}

			return response()->json($xml);
	    	}
    }

    //adere um plano
    public function adhesionP(Request $request){
    	$tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();
		
		$user = $this->user::whereNull('deleted_at')
		->where('users.tenantid', $tenantid)
		->first(); 

		$company = $this->company::whereNull('deleted_at')
		->where('tenantid', $tenantid)
		->first();

		$company_street = $company->address;
		$company_number = $company->number;
		$company_complement = '';
		$company_district = $company->city; //Bairro
		$company_city = $company->city;

		$state = $this->state::where(['nome' => $company->district])
		->first(); 

		if (
			$company->uf == '-' || $company->uf == '' 
			&& $company->city == '-' || $company->city == ''
			&& $company->address == '-' || $company->address == ''
			&& $company->number == '-' || $company->number == '') {
			
			return response()->json(['ERROR' => 'Você precisa adicionar o endereço do seu negócio para aderir ao plano atuals']);
			return '';
		}else{
			$company_state = strtoupper($state->uf); //Estado
			$aux = explode(')', $company->phone1);
			$company_areaCode = RemoveParenths($aux[0]);
			$company_phone = CleanFields(trim($aux[1]));
			
			$company_cep = CleanFields($company->cep);
			$company_type = 'CNPJ';
			$doc = CleanFields($company->cnpj);
			$company_country = 'BRA';

			$user_name =  $user->name;
			$user_email = $user->email;

			$type = 'cpf';

			if (strlen($doc) > 11){
				$type = 'CNPJ';
			}else{
				$type = 'CPF';
			}

			$areaholder = $request['paymentMethod']['creditCard']['holder']['phone']['areaCode'];
			$aux = explode(')', $areaholder);
			$areaholder = RemoveParenths($aux[0]);

			$phoneholder = $request['paymentMethod']['creditCard']['holder']['phone']['number'];
			$phoneholder = CleanFields(trim($phoneholder));


	        $url = $this->Uri() . "pre-approvals?email=".$this->Auth()."&token=".$this->Token();

	 		$data = json_encode(
	 			array(
	 				'plan' => $request['plan'],
	 				'reference' => $request['reference'],
	 				'sender' => [
	 					'name' => $user_name, //$request['sender']['name'],
	 					'email' => $user_email, //$request['sender']['email'],
	 					'ip' => \Request::ip(),
	 					'phone' => [
	 						'areaCode' => $company_areaCode, //$request['sender']['phone']['areaCode'],
	 						'number' => $company_phone //$request['sender']['phone']['number']
	 					],
	 					'address' => [
	 						'street' => $company_street, //$request['sender']['address']['street'],
	 						'number' => $company_number, //$request['sender']['address']['number'],
	 						'complement' => $company_complement, //$request['sender']['address']['complement'],
	 						'district' => $company_district,  //$request['sender']['address']['district'],
	 						'city' => $company_city, //$request['sender']['address']['city'],
	 						'state' => strtoupper($company_state),  //$request['sender']['address']['state'],
	 						'country' => $company_country, //$request['sender']['address']['country'],
	 						'postalCode' => $company_cep, //$request['sender']['address']['postalCode']
	 					],
	 					'documents' => [
	 						0 => [
								'type' => $type,
								'value' => $doc
	 						]
	 					]
	 				],
	 				//essas informações vão na hora de adicionar o cartão do cliente
	 				'paymentMethod' => [
	 					'type' => $request['paymentMethod']['type'],
	 					'creditCard' => [
	 						'token' => $request['paymentMethod']['creditCard']['token'],
	 						'holder' => [
	 							'name' => $request['paymentMethod']['creditCard']['holder']['name'],
	 							'birthDate' => $request['paymentMethod']['creditCard']['holder']['birthDate'],
			 					'documents' => [
			 						0 => [
										'type' => $request['paymentMethod']['creditCard']['holder']['documents'][0]['type'],
										'value' => $request['paymentMethod']['creditCard']['holder']['documents'][0]['value']
			 						]
			 					],
			 					'phone' => [
			 						'areaCode' => $areaholder,
			 						'number' => $phoneholder,
			 					],
			 					'billingAddress' => [
			 						'street' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['street'],
			 						'number' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['number'],
			 						'complement' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['complement'],
			 						'district' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['district'],
			 						'city' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['city'],
			 						'state' => strtoupper($request['paymentMethod']['creditCard']['holder']['billingAddress']['state']),
			 						'country' => $company_country, //$request['sender']['address']['country'],
			 						'postalCode' => $request['paymentMethod']['creditCard']['holder']['billingAddress']['postalCode'],
			 					]
	 						]
	 					],
	 				]
	 			)
	 		);

	 		$dt = $data;

	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
	        curl_setopt($curl, CURLOPT_POST, true);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	 
	        $response = curl_exec($curl);
	        $http = curl_getinfo($curl);

			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  $rtn = "cURL Error #:" . $err;
		      return response()->json(['ERRO' => $rtn]);
			} else {
			  	$rtn = $response;
				$xml = simplexml_load_string($rtn);
				$json = json_encode($xml);

				$decode = json_decode($json, true);

				if(isset($decode['code'])){
					//create payment client db abinf
					$data = [
						'tenantid' => $tenantid,
						'plan' => $request['nameplan'],
						'referenceplan' => $request['reference'],
						'codeadesion' => $decode['code'],
						'codeplan' => $request['plan'],
						'status' => 'ACTIVE',
						'value' => $request['price'],
						'period' => $request['period'],
						'dateadhesion' => date('Y-m-d H:i:s'),
						'typepayment' => $request['paymentMethod']['type']
	 				];

					$payment = Plan::where('tenantid', $tenantid)->update($data);

					$datat = [
						'amountuser' => $request['amountuser'],
					];

					$tenant = $this->tenant::whereNull('deleted_at')
					->where(['id' => $tenantid])
					->update($datat);

				    activity('payment-create adesion')
				     ->causedBy($userloged)
				     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'preaproval' => $decode['code'], 'view' => '0'])
				     ->log('Adesão criada');

			        Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail($dt, $json));

			       	return response()->json(['SUCCES' => 'Parabéns você acabou de escolher seu plano!', $xml]);

				}else{
					return response()->json($xml);
				}
			}			
		}

    }

    public function paymentManual(Request $request){
    	$tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();

        $url = $this->Uri() . "pre-approvals/payment?email=".$this->Auth()."&token=".$this->Token();

        $preaproval = $request['preApprovalCode'];
        $senderip = \Request::ip();
        $reference = 'T_'.$tenantid;
        $itemid1 = $request['itemId1'];
        $itemdescription1 = $request['itemDescription1'];
        $itemamount1 = $request['itemAmount1'];
        $itemQuantity1 = $request['itemQuantity1'];

		$data = "preApprovalCode=".$preaproval."&senderIp=".$senderip."&reference=".$reference."&itemId1=1&itemDescription1=".$itemdescription1."&itemAmount1=".$itemamount1."&itemQuantity1=1";

		$dt = $data;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $rtn = "cURL Error #:" . $err;
	      return response()->json(['ERRO' => $rtn]);
		}else{
		  	$rtn = $response;

		  	if($rtn === 'Internal Server Error'){
		       	return response()->json(['ERRO' => 'Seu pagamento não foi realizado. Está em analise ou aguardando pagamento']);
		  	}else{
		  		$x = strpos($rtn, 'bloqueado');
		  		
		  		if ($x > 0) {
				    Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail('erro', 'Muitas requisicoes'));
		       		return response()->json(['ERRO' => 'Uol bloqueou pelo numero de requisições'], 422);		  			
		  		}else{

					$xml = simplexml_load_string($rtn);
					$json = json_encode($xml);

					$decode = json_decode($json, true);

					if(isset($decode['error'])){
				        Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail('erro', 'assinatura não encontrada'));
						return response()->json(['ERRO' => 'Não existe essa assinatura ou ela foi cancelada']);
					}else{
						$data = [
							'tenantid' => $tenantid,
							'transactioncode' => $decode['transactionCode'],
							'date' => $decode['date'],
							'value' => $request['itemAmount1'],
							'ip' => \Request::ip(),
							'status' => 1
						];

		
						Payment::create($data);

				        Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail($dt, $json));

					    activity('payment-notication-payment')
					     ->causedBy($userloged)
					     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'preaproval' => $preaproval, 'view' => '0'])
					     ->log('Pagamento manual assinatura');

						return response()->json($decode);
					}
		  		}
		  	}
		}
    }

    //function que verificar o status da ultima transição
   /* public function verifyPayment(){
    	$tenantid = $this->GetTenant();
    	
    	$plan  = Plan::where('tenantid', $tenantid)->first();
	    $status = '';

        $url = $this->Uri() . "pre-approvals/".$plan->codeadesion."/payment-orders?email=".$this->Auth()."&token=".$this->Token() . $status;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_HTTPGET, true);
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);


		if ($err) {
		  $rtn = "cURL Error #:" . $err;
		} else {
		  	$rtn = $response;
			$xml = simplexml_load_string($rtn);
			//$json = json_encode($xml);
		}

		$paymentOrder = $xml->paymentOrders->paymentOrder;



		foreach ( $paymentOrder as $order) {
			echo $teste =  $order->status;
		}

		return response()->json($xml);


    }*/


    public function suspendAdhesion(Request $request){
    	$tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();

    	$preaprovalcode = Plan::where('tenantid', $tenantid)->first();

    	$url = $this->Uri() . "pre-approvals/".$preaprovalcode->codeadesion."/status?email=".$this->Auth()."&token=".$this->Token().'&status=' . $request['status'];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_PUT, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS);
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $rtn = "cURL Error #:" . $err;
		} else {
		  	$rtn = $response;
		}

		if($rtn == ''){
		  	$data = [
		  		'status' => $request['status']
		  	];

		  	Plan::where(['tenantid' => $tenantid, 'id' => $preaprovalcode->id])->update($data);

		  	if($request['status'] === 'ACTIVE'){
				$urlinternet = $this->Uri() . "pre-approvals/payment?email=".$this->Auth()."&token=".$this->Token();

		        $preaproval = $preaprovalcode->codeadesion;
		        $senderip = \Request::ip();
		        $reference = 'T_'.$tenantid;
		        $itemid1 = 1;
		        $itemdescription1 = 'Saas Software Abinf Sales ' . $preaprovalcode->period;
		        $itemamount1 = $preaprovalcode->value;
		        $itemQuantity1 = 1;

				$data = "preApprovalCode=".$preaproval."&senderIp=".$senderip."&reference=".$reference."&itemId1=1&itemDescription1=".$itemdescription1."&itemAmount1=".$itemamount1."&itemQuantity1=1";

		        $curl = curl_init($urlinternet);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		 
		        $response = curl_exec($curl);
		        $http = curl_getinfo($curl);

				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  $rtn = "cURL Error #:" . $err;
			      return response()->json(['ERRO' => $rtn]);
				}else{
				  	$rtn = $response;

				  	if($rtn === 'Internal Server Error'){
				       	return response(404)->json(['ERRO' => 'Seu pagamento não foi realizado, provavelmente já existe uma transação sendo analisada ou aguardando pagamento']);
				  	}else{
						$xml = simplexml_load_string($rtn);
						$json = json_encode($xml);

						$decode = json_decode($json, true);

						$data = [
							'tenantid' => $tenantid,
							'transactioncode' => $decode['transactionCode'],
							'date' => $decode['date'],
							'value' =>	$preaprovalcode->value,
							'ip' => \Request::ip(),
							'status' => 1
						];

						Payment::create($data);
						return response()->json($decode);
				  	
					    activity('payment-notication-reativation')
					     ->causedBy($userloged)
					     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'preaproval' => $preaprovalcode->codeadesion, 'view' => '0'])
					     ->log('Notificação de adesão reativada');
				  	}
				}
		  	}

		}else{

			$payment = Payment::where(['tenantid' => $tenantid])->orderBy('id', 'desc')->first();
			if($payment->status == '7'){

				$urlinternet = $this->Uri() . "pre-approvals/payment?email=".$this->Auth()."&token=".$this->Token();

		        $preaproval = $preaprovalcode->codeadesion;
		        $senderip = \Request::ip();
		        $reference = 'T_'.$tenantid;
		        $itemid1 = 1;
		        $itemdescription1 = 'Saas Software Abinf Sales ' . $preaprovalcode->period;
		        $itemamount1 = $preaprovalcode->value;
		        $itemQuantity1 = 1;

				$data = "preApprovalCode=".$preaproval."&senderIp=".$senderip."&reference=".$reference."&itemId1=1&itemDescription1=".$itemdescription1."&itemAmount1=".$itemamount1."&itemQuantity1=1";

		        $curl = curl_init($urlinternet);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		 
		        $response = curl_exec($curl);
		        $http = curl_getinfo($curl);

				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  $rtn = "cURL Error #:" . $err;
			      return response()->json(['ERRO' => $rtn]);
				}else{
				  	$rtn = $response;

				  	if($rtn === 'Internal Server Error'){
				       	return response(404)->json(['ERRO' => 'Seu pagamento não foi realizado, provavelmente já existe uma transação sendo analisada ou aguardando pagamento']);
				  	}else{
						$xml = simplexml_load_string($rtn);
						$json = json_encode($xml);

						$decode = json_decode($json, true);

						$data = [
							'tenantid' => $tenantid,
							'transactioncode' => $decode['transactionCode'],
							'date' => $decode['date'],
							'value' =>	$preaprovalcode->value,
							'ip' => \Request::ip(),
							'status' => 1
						];

						Payment::create($data);
						return response()->json($decode);
				  	
					    activity('payment-notication-reativation')
					     ->causedBy($userloged)
					     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'preaproval' => $preaprovalcode->codeadesion, 'view' => '0'])
					     ->log('Notificação de pgamento com assintura ativa');
				  	}
				}
			}else{
			    activity('payment-notication-suspended')
			     ->causedBy($userloged)
			     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id, 'preaproval' => $preaprovalcode->codeadesion, 'view' => '0'])
			     ->log('Notificação de suspenção de assinatura');
			}
		}
		return $rtn;
    }

    public function methodChangePayment(Request $request){
    	$tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();

		$user = $this->user::whereNull('deleted_at')
		->where(['users.tenantid' => $tenantid])
		->first();

		$company = $this->company::whereNull('deleted_at')->where('tenantid', $tenantid)->first();

		$company_street = $company->address;
		$company_number = $company->number;
		$company_complement = '';
		$company_district = $company->city; //Bairro
		$company_city = $company->city;

		$state = $this->state::where('nome', $company->district)
		->first();

		$company_state = strtoupper($state->uf); //Estado
		$aux = explode(')', $company->phone1);
		$company_areaCode = RemoveParenths($aux[0]);
		$company_phone = CleanFields(trim($aux[1]));

		
		$company_cep = CleanFields($company->cep);
		$company_type = 'CNPJ';
		$company_cnpj = CleanFields($company->cnpj);
		$company_country = 'BRA';

		$user_name =  $user->name;
		$user_email = $user->email;

		$preaprovalcode = Plan::where('tenantid', $tenantid)->first();

    	$url = $this->Uri() . "pre-approvals/".$preaprovalcode->codeadesion."/payment-method?email=".$this->Auth()."&token=".$this->Token();
 		
 		$data = json_encode(
 			array(
 				'sender' => [
 					'ip' => \Request::ip()
 				],
 				//essas informações vão na hora de adicionar o cartão do cliente
 					'type' => $request['type'],
 					'creditCard' => [
 						'token' => $request['creditCard']['token'],
 						'holder' => [
 							'name' => $request['creditCard']['holder']['name'],
 							'birthDate' => $request['creditCard']['holder']['birthDate'],
		 					'documents' => [
		 						0 => [
									'type' => 'CPF',
									'value' => $request['creditCard']['holder']['documents'][0]['value']
		 						]
		 					],
		 					'phone' => [
		 						'areaCode' => $company_areaCode, //$request['sender']['phone']['areaCode'],
		 						'number' => $company_phone //$request['sender']['phone']['number']
		 					],
		 					'billingAddress' => [
		 						'street' => $company_street, //$request['sender']['address']['street'],
		 						'number' => $company_number, //$request['sender']['address']['number'],
		 						'complement' => $company_complement, //$request['sender']['address']['complement'],
		 						'district' => $company_district,  //$request['sender']['address']['district'],
		 						'city' => $company_city, //$request['sender']['address']['city'],
		 						'state' =>$company_state,  //$request['sender']['address']['state'],
		 						'country' => $company_country, //$request['sender']['address']['country'],
		 						'postalCode' => $company_cep, //$request['sender']['address']['postalCode']
		 					]
 						]
 					]
 			)
 		);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $rtn = "cURL Error #:" . $err;
	      return response()->json($rtn);
		} else {
		  	$rtn = $response;
			$xml = simplexml_load_string($rtn);
			$json = json_encode($xml);

		    activity('payment-change-method')
		     ->causedBy($userloged)
		     ->withProperties(['tenantid' => $tenantid, 'view' => '0'])
		     ->log('Mudança de pagamento concluída');	

		    if($rtn){
		    	return response()->json($xml);
		    }else{
		    	$plan = Plan::where('tenantid', $tenantid)->whereNull('deleted_at')->update(['status' => 'PENDING']);
	      		return 'OK';    	
		    }
		}		
    }

    public function cancelAdhesion(Request $request){
    	//corrigir e testar se está funcionando
    	$tenantid = $this->GetTenant();        
        $userloged = \Auth::guard('api')->user();

		$preaprovalcode = Plan::where('tenantid', $tenantid)->first();

    	$url = $this->Uri() . "pre-approvals/".$preaprovalcode->codeadesion."/cancel?email=".$this->Auth()."&token=".$this->Token();

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->Contenttype(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

		$err = curl_error($curl);

		curl_close($curl);    	
		
		if ($err) {
		  $rtn = "cURL Error #:" . $err;
	      return response()->json($rtn);
		} else {
		  	$rtn = $response;
			$xml = simplexml_load_string($rtn);
			$json = json_encode($xml);

		    activity('payment-cancel')
		     ->causedBy($userloged)
		     ->withProperties(['tenantid' => $tenantid, 'view' => '0'])
		     ->log('Usuário cancelou o plano');	

		    if($rtn){
		    	return response()->json($xml);
		    }else{
		    	$plan = Plan::where('tenantid', $tenantid)->whereNull('deleted_at')->update(['status' => 'CANCELLED_BY_RECEIVER']);
	      		return 'OK';  	
		    }
		};
    }

    public function notifications(Request $request){
		header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");

		$typeNotification = $_POST['notificationType'];

		if($typeNotification === 'transaction'){

			$notificationCode = preg_replace('/[^[:alnum:]-]/', '', $_POST['notificationCode']);

	        $url = $this->Uri() . "v3/transactions/notifications/".$notificationCode."/?email=".$this->Auth()."&token=".$this->Token();

	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_HTTPGET, true);
	 
	        $response = curl_exec($curl);
	        $http = curl_getinfo($curl);

			$err = curl_error($curl);

			curl_close($curl);


			if ($err) {
			  $rtn = "cURL Error #:" . $err;
			} else {
			  	$rtn = $response;

			  	if($rtn === 'Not Found'){
			  		return response('Notification not found', 404);
			  	}else{
					$xml = simplexml_load_string($rtn);
					$json = json_encode($xml);
			  	}

				$code = (string) str_replace('-', '', $xml->code);
				$reference = (string) str_replace('T_', '', $xml->reference);
				$statusTransaction = (string) $xml->status;

				$payment = Payment::where(['transactioncode' => $code, 'tenantid' => $reference])->first();

				if($payment){
				 	$resp = event(new NotificationPayment($code, $statusTransaction, $reference));
				}

			    activity('payment-notication-transaction')
			     //->causedBy($userid)
			     ->withProperties(['code' => $code, 'statusTransaction' => $statusTransaction, 'tenantid' => $reference])
			     ->log('Notificação de transação recebida');	
			}

			
		}else if($typeNotification === 'preApproval'){
			$notificationCode = preg_replace('/[^[:alnum:]-]/', '', $_POST['notificationCode']);

	        $url = $this->Uri() . "pre-approvals/notifications/".$notificationCode."/?email=".$this->Auth()."&token=".$this->Token();

	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
	        curl_setopt($curl, CURLOPT_HTTPGET, true);
	 
	        $response = curl_exec($curl);
	        $http = curl_getinfo($curl);

			$err = curl_error($curl);

			curl_close($curl);


			if ($err) {
			  $rtn = "cURL Error #:" . $err;
			} else {
			  	$rtn = $response;

			  	if($rtn === 'Not Found'){
			  		return response('Notification not found', 404);
			  	}else{
					$xml = simplexml_load_string($rtn);
					$json = json_encode($xml);
			  	}

				$code = (string) $xml->code;
				$status = (string) $xml->status;
			
				$plan = Plan::where(['codeadesion' => $code])->first();

				if($plan){
					$resp = event(new NotificationAdhesion($code, $status));
				}

			    activity('payment-notication-preaprovel')
			     //->causedBy($userid)
			     ->withProperties(['code' => $code, 'status' => $status, 'view' => '0'])
			     ->log('Notificação de adesão recebida');

			}
		}

		return response()->json(['SUCCES' => 'Verificação concluída']);
    }



}
