<?php

namespace App\Http\Controllers\Api\Salesfunnel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Traits\GetTenantid;
use App\Http\Requests\Api\RulesPostFunnel;

use App\Criteria\WhereAttributeCriteria;
use App\Criteria\TenantCriteria;
use App\Criteria\Plan\PlanCriteria;

use App\Models\Funnel;
use App\Models\Columnsfunnel;
use App\Models\Opportunity;

class SalesfunnelController extends Controller
{
	use GetTenantid;

	private $funnel;
	private $columnsfunnel;
	private $opportunity;

	public function __construct(Funnel $funnel, Columnsfunnel $columnsfunnel, Opportunity $opportunity){
		$this->funnel = $funnel;
		$this->columnsfunnel = $columnsfunnel;
	}

	public function funnels(){
        $tenantid = $this->GetTenant();
		$result = $this->funnel::whereNull('deleted_at')->where('tenantid', $tenantid)->get();
        return response()->json($result, 200);		
	}

	public function columnsfunnel(Request $request, $id){
        $tenantid = $this->GetTenant();


  		$filter = $this->funnel::whereNull('deleted_at')->where('tenantid', $tenantid)->find($id);
  		if($filter != ''){
			$result = $this->funnel::whereNull('deleted_at')->where('tenantid', $tenantid)->find($id)->columnsf;

	        $stages = [];
			foreach ($result as $rs) {
				$stages[] = $rs->columnfunnel;
			}

	        return response()->json($stages, 200);
  		}else{
			return response()->json([
			    'NONE' => 'Default'
			], 200);  
  		}
	}

	public function addfunnel (RulesPostFunnel $request) {
        $userloged = Auth::guard('api')->user();
        $tenantid = $this->GetTenant();
		$data = [
			'tenantid' => $tenantid,
			'funnel' => $request['funnel'],
		];

		$funnel = $this->funnel::create($data);

		$idfunnel = $funnel->id;

		$column = array_filter($request['column']);


		if($column == null){
			return response()->json(['ERROR' => 'Adicione pelo menos uma coluna ao funil']);
		}else{
			for ($i=0; $i < count($column); $i++) {
				$data = [
					'tenantid' => $tenantid,
					'columnfunnel' => $column[$i],
					'idfunnel' => $idfunnel,
					'iduser' => $userloged['id'],
				];
				$this->columnsfunnel::create($data);
			}
			
			return response()->json([
			    'SUCESS' => 'Funil alvo com sucesso!'
			], 200);
		}
	}
}
