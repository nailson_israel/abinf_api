<?php

namespace App\Http\Controllers\Api\Region;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nation;
use App\Models\States;
use App\Models\Cities;

class RegionController extends Controller
{
    private $nation;
    private $states;
    private $cities;

    public function __construct(Nation $nation, States $states, Cities $cities){
        $this->middleware('auth:api');
        $this->nation = $nation;
        $this->states = $states;
        $this->cities = $cities;
    }

    public function nation(){
        $result = $this->nation->get();
    	return response()->json($result, 200);
    }

    public function states(){	
        $result = $this->states->get();
        return response()->json($result, 200);
    }

    public function cities(Request $request){
        $search = $request['search'];

        $result = $this->cities::select('cidade.id', 'cidade.nome', 'uf', 'estado.nome as estado', 'estado.id as idestado')
        ->leftJoin('estado', 'cidade.estado', '=', 'estado.id')
        ->when($search, function($query) use ($search){
            $query->where(['estado.nome' => $search ]);
        })
        ->get();
        return response()->json($result, 200);
    }
}
