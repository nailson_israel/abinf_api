<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class LogoutapiController extends Controller
{
    public function logoutapi(Request $request){
    	$userloged = \Auth::guard('api')->user();
		$request->user()->token()->revoke();
		DB::table('oauth_access_tokens')
        ->where('user_id', $userloged->id)
        ->update([
            'revoked' => true
        ]);

	    activity('Logout-api')
	     ->causedBy($userloged)
	     ->withProperties(['tenantid' => $userloged->tenantid, 'user' => $userloged->id])
	     ->log('Informações de primeiro acesso salvas.');   


		return 'logged out'; // modify as per your need
    }
}
