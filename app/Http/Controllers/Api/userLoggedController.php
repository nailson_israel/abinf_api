<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Api\RulesPostprofilepassword;
use App\Http\Requests\Api\RulesPostprofileuser;
use App\Http\Traits\GetTenantid;

use App\Models\Plan;
use App\Models\Payment;
use App\Models\Company;
use App\Models\Tenant;
use App\Models\User;

class userLoggedController extends Controller
{
	use GetTenantid;

	public $user;
	public $tenant;
	public $company;

	public function __construct(User $user, Tenant $tenant, Company $company){
        $this->user = $user;
        $this->tenant = $tenant;
        $this->company = $company;
	}

	public function me(){
		$result = \Auth::guard('api')->user();
		return response()->json($result, 200);
	}

	public function isadmin(){
		$result = \Auth::guard('api')->user()->isadmin;
		return response()->json($result, 200);
	}

	public function myuser(){
		$result = \Auth::guard('api')->user()->user;
		return response()->json($result, 200);
	}

	public function myname(){
		$result = \Auth::guard('api')->user()->name;
		return response()->json($result, 200);
	}

	public function groupus(){
		$result = \Auth::guard('api')->user()->groupus;
		return response()->json($result, 200);
	}

	public function responsible(){
       	$tenantid = $this->GetTenant();
		
		$result = $this->user::whereNull('deleted_at')
		->where(['tenantid' => $tenantid, 'responsible' => 1])
		->first();

		return response()->json($result, 200);
	}

	public function savewelcome(Request $request){
    	$tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

		$id = \Auth::guard('api')->user()->id;

		$datacompany = [
			'cnpj' => $request->input('cnpj'), 
	        'reason' => $request->input('reason'),
	        'namefantasy' => $request->input('reason'),
	        'phone1' => $request->input('phone1'),
        	'tenantid' => $tenantid,
		];

		$datauser = [
	        'phone1' => $request->input('phone1'),
		];

		$datatenant = [
			'cnpj' => $request->input('cnpj'), 
	        'phone1' => $request->input('phone1'),
		];

		$update = $this->user::whereNull('deleted_at')
		->where(['tenantid' => $tenantid, 'id' => $id])
		->update($datauser);
		
		$updatetenant = $this->tenant::whereNull('deleted_at')
		->where(['id' => $tenantid])
		->update($datatenant);

		$company = $this->company::where(['tenantid' => $tenantid])->first();

		$idcomp = $company->id;

		$updatecompany = $this->company::where(['tenantid' => $tenantid, 'id' => $idcomp])->update($datacompany);		
		echo $updatecompany.' - '.$updatecompany.' - '.$updatetenant;

	    if($update && $updatetenant && $updatecompany){
		    activity('Logged-save-welcome')
		     ->causedBy($userloged)
		     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id])
		     ->log('Informações de primeiro acesso salvas.');			
			return response()->json([
		    'SUCESS' => 'Ok.'
			], 200);
    	}else{
			return response()->json([
			    'ERROR' => 'Não foi possível concluir.'
			], 422);         		
    	}  	
	}

	public function freePeriod () {
       	$tenantid = $this->GetTenant();
        $result = $this->tenant::whereNull('deleted_at')
        ->where('id', $tenantid)
        ->first();

        /*
        Adhesions - plans

        PENDING pendente
        ACTIVE ativa
        PAYMENT_METHOD_CHANGE cartão sem limite, vencido, suspenso ou bloqueado
        SUSPENDED suspensa pelo vendedor
        CANCELLED cancelada pego pagseguro
        CANCELLED_BY_RECEIVER cancelada pelo vendedor
        CANCELLED_BY_SENDER cancelada pelo usuario
        */

        $plan = Plan::where('tenantid', $tenantid)->first();


        if($plan->status == 'free' || $plan->status == 'Free'){
            $begin = new \DateTime(date("Y-m-d"));
            $finish = new \DateTime($result->datefinishfree); 
            
            $diff = $begin->diff($finish);
            $diff->format('%R');

            $days = $diff->format('%R') . ' ' . $diff->days;

            $ex = explode(" ", $days);
            if($ex[0] == '+'){
               $rst = $ex[1];
            }else{
               $rst = 0;
            }
        }else{

            /*
                1 - Aguardando pagamento / registered
                2 - Em análise / analyzing
                3 - Paga / Paid
                4 - Disponível / Paid
                5 - Em disputa / Dispute
                6 - Devolvida / Returned
                7 - Cancelada / Canceled
            */
            $payment = Payment::whereNull('deleted_at')
                    ->where([
                        ['tenantid', '=', $tenantid]
                    ])->orderBy('id', 'DESC')->first();

           	if($plan->status == 'REGISTERED' || $plan->status == 'PENDING'){
                $rst = 20;
            }else if($plan->status == 'CANCELLED_BY_SENDER' || $plan->status == 'CANCELLED_BY_RECEIVER' || $plan->status == 'CANCELLED' || $plan->status == 'PAYMENT_METHOD_CHANGE'){
                $rst = 30;
            }else if($plan->status == 'SUSPENDED'){
                    $rst = 40;
            }else{

            	/*if(date('Y-m-d', strtotime($payment->date)) < date('Y-m-d')){
            		//futuramente ver se precsa por um valor 45 para quando o pagament está atrasao.
            		//muito dificil acontecer por que a uol já processa o pagamento em minutos, ou paga ou cancelada.
            		//$rst = 40;
            	}else{*/

            		if ($payment != '') {
		                if($payment->status == 1 || $payment->status == 2 || $payment->status == 3){
		                    $rst = 20;
		                }else if($payment->status == 7){
		                    $rst = 35;
		                }else{
		                    $rst = 0;
		                }
            		}else{
            			$rst = 20;
            		}
            	/*}*/
            }
        }
        return $rst;
	}

	public function updateprofile(RulesPostprofileuser $request, $id){
    	$tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

		$data = [
			'name' => $request->input('name'), 
	        'user' => $request->input('user'),
	        'phone1' => $request->input('phone1'),
	        'phone2' => $request->input('phone2'),
	        'phone3' => $request->input('phone3'),
	        'email' => $request->input('email'),
        	'tenantid' => $tenantid,
		];

		$update = $this->user::where(['tenantid' => $tenantid, 'id' => $id])->update($data);

	    if($update){
		    activity('Logged-update-profile')
		     ->causedBy($userloged)
		     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id])
		     ->log('Informações de primeiro acesso salvas.');   

			return response()->json([
		    'SUCESS' => 'Perfil alterado com sucesso.'
			]);        		  
    	}else{
			return response()->json([
			    'ERROR' => 'Perfil não alterado.'
			]);         		
    	}

	}

	public function verifypassowrduser(RulesPostprofilepassword $request, $id){
		$tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();
		$attribute = 'id';
	  	$request->input('actualpassword');
	  	$request->input('newpassword');

	    $hashedPassword = Auth::guard('api')->user()->password;

		if (Hash::check($request->input('actualpassword'), $hashedPassword)) {
			if (Hash::check($request->input('newpassword'), $hashedPassword)) {
	           return response()->json(['ERROR' => 'Sua senha nova não pode ser igual a antiga. Escolha outra.']);
			}else{
				$data = [
			        'password' => bcrypt($request->input('newpassword')),
				];

				$update = $this->user::where(['tenantid' => $tenantid, 'id' => $id])->update($data);
			    if($update == 1){
				    activity('Logged-verfiry-password')
				     ->causedBy($userloged)
				     ->withProperties(['tenantid' => $tenantid, 'user' => $userloged->id])
				     ->log('Informações de primeiro acesso salvas.');   


					return response()->json([
				    'SUCESS' => 'Senha alterada com sucesso.'
					]);        		
		    	}else{
					return response()->json([
					    'ERROR' => 'Senha não alterada.'
					]);         		
		    	}
			}
		}else{
			return response()->json(['ERROR' => 'Senha atual incorrenta, por favor verifique novamente.']);
		}
	}
}
