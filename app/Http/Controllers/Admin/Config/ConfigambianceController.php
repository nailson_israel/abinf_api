<?php

namespace App\Http\Controllers\Admin\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RulesPostAmbiance;
use App\Events\NotificationCreateTenant;
use App\Repositories\TenantRepository as Tenant;
use App\Repositories\Criteria\Admin\Ambiance\WhereAmbiance;

//Repository od Start System
/*use App\Repositories\Config\ChangeRepository as Change;
use App\Repositories\Config\DepartamentRepository as Departament;
use App\Repositories\Config\GroupRepository as Group;
use App\Repositories\Config\PermissionRepository as Permission;
use App\Repositories\User\UserRepository as User;
use App\Repositories\Formatt\FormattRepository as Formatt;
use App\Repositories\Product\GroupproductRepository as Groupproduct;
use App\Repositories\Product\ProductRepository as Product;
use App\Repositories\Company\CompanyRepository as Company;
use App\Repositories\Client\ClientRepository as Client;
use App\Repositories\Client\Selects\GroupclientRepository as Groupclient;
use App\Repositories\Client\Selects\BranchclientRepository as Branchclient;
use App\Repositories\Client\Selects\DepartamentcontactRepository as Departamentcontact;
use App\Repositories\Client\Selects\RouteclientRepository as Routeclient;
use App\Repositories\Client\Comment\CommentRepository as Comment;
use App\Repositories\Client\Itemcl\ItemclRepository as Itemcl;
use App\Repositories\Client\Contact\ContactRepository as Contact;
use App\Repositories\Client\Item\ItemRepository as Item;
use App\Repositories\Client\Talk\TalkRepository as Talk;
use App\Repositories\Client\RepresentedRepository as Represented; //Representadas
use App\Repositories\Budget\MotivedescartRepository as Motivedescart;
use App\Repositories\Budget\GrouporcRepository as Grouporc;
use App\Repositories\Budget\BudgetRepository as Budget;
use App\Repositories\Budget\Itenbudget\ItenbudgetRepository as Itenbudget;*/
//Criteria
use App\Repositories\Criteria\TenantidCriteria;
//Model Custon collection
use App\Models\Api\Permission as PermissionCuston;

use DB;

class ConfigambianceController extends Controller
{
    private $tenant;
    private $key;
    //Start
    private $change;
    private $department;
    private $group;
    private $permission;
    private $formatt;
    private $groupproduct;
    private $product;
    private $company;
    private $groupclient;
    private $client;
    private $branchclient;
    private $departamentcontact;
    private $routeclient;
    private $comment;
    private $itemcl;
    private $contact;
    private $item;
    private $talk;
    private $represented;
    private $motivedescart;
    private $grouporc;
    private $budget;
    private $itenbudget;


    public function index(){
    	return view('admin/configambiance');
    }

    public function ambiance(){
        $key = $this->key;
        $minutes = 60;
        $total = 10;
        $currentPage = \Request::get('page');
        $result = $this->tenant->pushCriteria(new WhereAmbiance())->allWhereNull($currentPage, $key, $minutes, $total);
		
        return response()->json($result, 200);
    }

    public function addambiance(RulesPostAmbiance $request){  
        $key = $this->key;
        $date = date('d/m/Y');
        
        $name = $request['name'];
        $email = $request['email'];
        $phone = $request['phone'];

        $datatenant = [
            'tenant' => $request['tenant'],
            'cnpj' => $request['cnpj'],
            'email' => $request['email'],
            'name' => $request['name'],
            'phone' => $request['phone'],
            'datebegin' => $date,
            'enterprise' => $request['tenant'],                         
        ];
        $createtenant = $this->tenant->create($datatenant, $key);

        $result = $this->tenant->getWith('tenant', $request['tenant']);
        foreach ($result as $response) {$id = $response->id;}

        $start = $this->startSystem($id, $name, $email, $phone);
    
        //event(new NotificationCreateTenant($createtenant));
       
        return 'sucesso';
       // return response()->json(['result' => $result, 'SUCESS' => 'Cliente salvo com sucesso']); 

    }

    public function upambiance(RulesPostAmbiance $request, $id){
    }

    public function delambiance(Request $request, $id){

    }

    public function startSystem($id, $name, $email, $phone){
 
    }
}
