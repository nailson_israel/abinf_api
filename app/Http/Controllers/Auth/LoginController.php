<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;



class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }  

    public function login(Request $request)
    {
        $login = $request->login;

        if (Auth::attempt(['user' => $request->login, 'password' => $request->password, 'active' => '1', 'ismaster' => '1'])){
            return redirect()->intended('home');
        }elseif(Auth::attempt(['email' => $request->login, 'password' => $request->password, 'active' => '1', 'ismaster' => '1'])){
            return redirect()->route('home');
        }elseif(Auth::attempt(['user' => $request->login, 'password' => $request->password, 'active' => '0'])){   
            Auth::logout();
            return redirect()->back()->with('error-login', 'Seu usuário está inativo!');
        }elseif(Auth::attempt(['email' => $request->login, 'password' => $request->password, 'active' => '0'])){   
            Auth::logout();
            return redirect()->back()->with('error-login', 'Seu usuário está inativo!');
        }else{
            //usuário não cadastrado na base de dados não entra
            return redirect()->back()->with('error-login', 'Usuário e senha invalidos!');
        }
    }

}
