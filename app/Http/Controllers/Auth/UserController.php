<?php

namespace App\Http\Controllers\Auth;
/*
	Requeriments inputs
	
	Email, 
	User,
	Password,
	Groupus,
	Active,
	Extern,
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Api\RulesPostUser;
use App\Http\Traits\GetTenantid;

use App\Models\User;
use App\Models\Tenant;

class UserController extends Controller
{
	use GetTenantid;

	private $user;
    private $tenant;
    private $pagination;

    public function __construct(User $user, Tenant $tenant){
        $this->middleware('privilege.config');
        $this->user = $user;
        $this->tenant = $tenant;
        $this->pagination = config('responses.pagination');
    }

    public function Id(){
        return $id = Auth::user()->id;
    }

    public function users(Request $request){
    	$tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'users.tenantid';
        $sort = $request['sort'] != null ? $request['sort'] : 'asc';
        $typesort = $request['typesort'] != null ? $request['typesort'] : 'user';
        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'user';
        $search = $request['search'];
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];
        $limit = $request['limit'] != null ? $request['limit'] : 50;
        $result = $this->user::select('users.id as id', 'isadmin', 'name', 'user', 'email', 'active', 'groupus', 'groups.groupuser as groupuser', 'region', 'extern', 'routeuser', 'phone1', 'estado.nome as uf')
        ->leftJoin('groups', 'users.groupus', '=', 'groups.id')
        ->leftJoin('estado', 'users.region', '=', 'estado.uf')
        ->where('ismaster', '<>', 1)
        ->whereNull('users.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                echo $iduser;
                $query->where('users.id', $iduser);
            }
        })
        ->where(['users.tenantid' => $tenantid])
        ->when($limit, function($query) use ($limit){
            if($limit != 'todos'){
                $query->limit($limit);
            }
        })->get();

        $total = $this->user::whereNull('users.deleted_at')->where(['users.tenantid' => $tenantid])->count();
        $totalsearch = $result->count();

        return response()->json(['data' => $result, 'total' => $total, 'totalsearch' => $totalsearch], 200);	
    }

    public function userid(Request $request, $id){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'users.tenantid';
        $attribute2 = 'users.id';
        
        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->user::select('users.id as id', 'isadmin', 'name', 'user', 'email', 'active', 'groupus', 'groups.groupuser as groupuser', 'region', 'extern', 'routeuser', 'phone1', 'estado.nome as uf')
        ->leftJoin('groups', 'users.groupus', '=', 'groups.id')
        ->leftJoin('estado', 'users.region', '=', 'estado.uf')
        ->where('ismaster', '<>', 1)
        ->whereNull('users.deleted_at')
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0'){
                $query->where('users.id', $iduser);
            }
        })
        ->where(['users.tenantid' => $tenantid])
        ->get();

        return response()->json($result, 200);      
    }

    public function userselectsbox(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $typesearch = $request['typesearch'] != null ? $request['typesearch'] : 'name';
        $sort = 'asc';
        $typesort = 'name';
        $search = $request['search'];

        $isadmin = $userloged['isadmin'].'n';
        $iduser = $userloged['id'];

        $result = $this->user::select('users.id', 'name', 'user')
        ->leftJoin('groups', 'users.groupus', '=', 'groups.id')
        ->leftJoin('estado', 'users.region', '=', 'estado.uf')
        ->where('ismaster', '<>', 1)
        ->whereNull('users.deleted_at')->orderby($typesort, $sort)
        ->when($search, function($query) use ($search, $typesearch){
            if($typesearch){
                $query->where($typesearch, 'like', "%$search%");
            }
        })
        ->when($isadmin, function($query) use ($isadmin, $iduser){
            if($isadmin == '0n'){
                $query->where('users.id', $iduser);
            }
        })
        ->where(['users.tenantid' => $tenantid])
        ->get();
      
        $search == '' ? $result = '' : $result = $result;

		return response()->json($result, 200);		
	  	
    }

    public function adduser(RulesPostUser $request){
    	$tenantid = $this->GetTenant();
        $userloged = Auth::guard('api')->user();

        //mexer aqui amanhã 
        $tenant = $this->tenant::whereNull('deleted_at')->where('id', $tenantid)->first();


        $amountuser = $tenant->amountuser;
       
        $user = $this->user::select('users.id', 'name', 'user')
        ->whereNull('deleted_at')
        ->where('users.tenantid', $tenantid)
        ->get();

        $user = count($user);

        if($user >= $amountuser){
            return response()->json([
                'ERROR' => 'Você não pode adicionar mais usuários com este plano. Veja planos maiores para você ou adicione contrate usuários adicionais'
            ]);
        }else{
            $data = [
                'name' => $request->input('name'),
                'user' => $request->input('user'),            
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'active' => $request->input('active'),
                'isadmin' => $request->input('isadmin'),
                'groupus' => $request->input('groupus'),
                'region' => $request->input('region'),                   
                'extern' => $request->input('extern'),  
                'routeuser' => $request->input('routeuser'),                                        
                'departament' => $request->input('departament'),
                'change' => $request->input('change'),
                'phone1' => $request->input('phone1'),
                'phone2' => $request->input('phone2'),
                'phone3' => $request->input('phone3'),
                'tenantid' => $tenantid,
            ];

            $create = $this->user->create($data);

            activity('user-criou')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'user' => $create->id, 'view' => '1'])
            ->log('Criou o usuário ');

            return response()->json([
                'SUCESS' => 'Usuário salvo com sucesso'
            ], 200);
        }
    }

    public function upuser(RulesPostUser $request, $id){
    	$tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

		if($request['swap'] == 'sim'){
			$data = [
				'name' => $request->input('name'), 
		        'user' => $request->input('user'),
		        'email' => $request->input('email'),
		        'password' => bcrypt($request->input('password')),
                'active' => $request->input('active'),
                'isadmin' => $request->input('isadmin'),
		        'groupus' => $request->input('groupus'),
		        'region' => $request->input('region'),                    
		        'extern' => $request->input('extern'),  
		        'routeuser' => $request->input('routeuser'),                                        
		        'departament' => $request->input('departament'),
		        'change' => $request->input('change'),
		        'phone1' => $request->input('phone1'),
		        'phone2' => $request->input('phone2'),
		        'phone3' => $request->input('phone3'),
	        	'tenantid' => $tenantid,
			];

			$update = $this->user::whereNull('deleted_at')
            ->where(['tenantid' => $tenantid, 'id' => $id])
            ->update($data);
		}else{
			$data = [
				'name' => $request->input('name'), 
		        'user' => $request->input('user'),
		        'email' => $request->input('email'),
                'active' => $request->input('active'),
                'isadmin' => $request->input('isadmin'),
		        'groupus' => $request->input('groupus'),
		        'region' => $request->input('region'),                    
		        'extern' => $request->input('extern'), 
		        'routeuser' => $request->input('routeuser'),                                          
		        'departament' => $request->input('departament'),
		        'change' => $request->input('change'),
		        'phone1' => $request->input('phone1'),
		        'phone2' => $request->input('phone2'),
		        'phone3' => $request->input('phone3'),
	        	'tenantid' => $tenantid,
			];

			$update = $this->user::whereNull('deleted_at')
            ->where(['tenantid' => $tenantid, 'id' => $id])
            ->update($data);
		}

	    if($update){
            activity('user-update')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'user' => $id, 'view' => '1'])
            ->log('Alterou o usuário ');

			return response()->json([
		    'SUCESS' => 'Usuário editado com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Usuário não alterado, este usuário não existe'
			], 422);         		
    	}
    }

    public function deluser(Request $request, $id){
    	$tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

    	$delete = $this->user::where(['tenantid' => $tenantid, 'id' => $id])->delete();
    	
    	if($delete){
            activity('user-delete')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'user' => $id, 'view' => '1'])
            ->log('Deletou o usuário ');

			return response()->json([
			    'SUCESS' => 'Usuário excluido com sucesso'
			], 200);        		
    	}else{
			return response()->json([
			    'ERROR' => 'Usuário não excluido, este Usuário não existe'
			], 422);         		
    	}            	
    }

    public function delalluser(Request $request){
        $tenantid = $this->GetTenant();
        $userloged = \Auth::guard('api')->user();

        $attribute = 'tenantid';
        $checkuser = $request->input('checkuser');

        $delete = $this->user::whereIn('id', $checkuser)->where(['tenantid' => $tenantid])->delete();                

        if($delete){
            activity('user-deleteall')
            ->causedBy($userloged)
            ->withProperties(['tenantid' => $tenantid, 'user' => $userloged['id'], 'user' => $checkuser, 'view' => '1'])
            ->log('Deletou vários usuários ');

            return response()->json([
            'SUCESS' => 'User(s) excluido(s) com sucesso'
            ], 200);             
        }else{
            return response()->json([
            'ERROR' => 'User(s) não excluido, este(s) user(s) não existe(m)'
            ], 422);                 
        } 
    }
}
