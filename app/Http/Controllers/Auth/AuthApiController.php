<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use Psr\Http\Message\ServerRequestInterface;
use \Laravel\Passport\Http\Controllers\AccessTokenController;

class AuthApiController extends AccessTokenController
{
    public function auth(ServerRequestInterface $request)
    {
        $tokenResponse = parent::issueToken($request);
        $token = $tokenResponse->getContent();

        // $tokenInfo will contain the usual Laravel Passort token response.
        $tokenInfo = json_decode($token, true);

        // Then we just add the user to the response before returning it.
        $username = $request->getParsedBody()['username'];
        $user = User::orWhere('email', $username)->orWhere('user', $username)->first();



        if(isset($user)){
            if($user->active != 1 ){
                return response()->json([
                    'error' => 'Usuário Inativo',
                    'message' => 'Oh não, parece que seu usuário não está ativo.'
                ], 401);
                //return response('Este usuário não está ativo', 401);
            }else{
                $tokenInfo = collect($tokenInfo);
                //$tokenInfo->put('user', $user);
                if(isset($tokenInfo['error'])){
                    return response()->json(['error' => 'Usuário invalido', 'message' => 'Ops! Não encontramos nada com este usuário por aqui, desculpe.'], 401);
                }else{
                   return $tokenInfo;    
                }            
            }
        }else{
            return response()->json(['error' => 'Usuário invalido', 'message' => 'Ops! Não encontramos nada com este usuário por aqui, desculpe.'], 401);
        }
    }
}