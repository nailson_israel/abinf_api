<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\RulesRegister;
use Illuminate\Support\Facades\Hash;

use App\Models\Tenant;
use App\Models\User;
use App\Models\Group;
use App\Models\Company;
use App\Models\Groupclient;
use App\Models\Branchclient;
use App\Models\Routeclient;
use App\Models\Departamentcontact;
use App\Models\Represented;
use App\Models\Transport;
use App\Models\Client;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Talk;
use App\Models\Groupproduct;
use App\Models\Product;
use App\Models\Budget;
use App\Models\Itenbudget;
use App\Models\Formattext;
use App\Models\Customorder;
use App\Models\Item;
use App\Models\Groupitem;
use App\Models\Branditem;
use App\Models\Motivedescart;
use App\Models\Itemcl;

use App\Models\Representedclients as CustonRepresentedclients;
use App\Models\Permission as PermissionCuston;
use App\Models\Plan;

use Illuminate\Support\Facades\Mail;
use App\Mail\Welcomeuser;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = 'http://localhost:8080/'; //alterar

    /**+
     * Create a new controller instance.
     *
     * @return void
     */

    private $tenant;
    private $user;
    private $group;
    private $company;
    private $groupclient;
    private $branchclient;
    private $routeclient;
    private $departamentclient;
    private $represented;
    private $transport;
    private $client;
    private $comment;
    private $contact;
    private $item;
    private $groupitem;
    private $branditem;
    private $itemcl;
    private $groupproduct;
    private $product;
    private $motivedescart;
    private $budget;
    private $itensbudget;
    private $formattext;

    public function __construct(Tenant $tenant, User $user, Group $group, Company $company, Groupclient $groupclient, Branchclient $branchclient, Routeclient $routeclient, Departamentcontact $departamentclient, Represented $represented, Transport $transport, Client $client, Comment $comment, Contact $contact, Item $item, Groupitem $groupitem, Branditem $branditem, Itemcl $itemcl, Talk $talk, Groupproduct $groupproduct, Product $product, Motivedescart $motivedescart, Budget $budget, Itenbudget $itensbudget, Formattext $formattext, Customorder $custom)
    {
        $this->tenant = $tenant;
        $this->user = $user;
        $this->group = $group;
        $this->company = $company;
        $this->groupclient = $groupclient;
        $this->branchclient = $branchclient;
        $this->routeclient = $routeclient;
        $this->departamentclient = $departamentclient;
        $this->represented = $represented;
        $this->transport = $transport;
        $this->client = $client;
        $this->comment = $comment;
        $this->contact = $contact;
        $this->item = $item;
        $this->groupitem = $groupitem;
        $this->branditem = $branditem;
        $this->itemcl = $itemcl;
        $this->talk = $talk;
        $this->groupproduct = $groupproduct;
        $this->product = $product;
        $this->motivedescart = $motivedescart;
        $this->budget = $budget;
        $this->itensbudget = $itensbudget;
        $this->formattext = $formattext;
        $this->custom = $custom;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function register(Request $request){
        $tenant = $this->tenant::whereNull('deleted_at')
        ->where('email', $request['email'])
        ->first();

        $user = $this->user::whereNull('deleted_at')
        ->where('name', $request['name'])
        ->first();

        $name = strtolower(str_replace(" ", "_", $request['name']));

        if($user){
            return response()->json([
                'ERROR' => 'Já existe uma conta cadastrada com este nome.'
            ], 422);
        }else if($tenant){
            return response()->json([
                'ERROR' => 'Já existe uma conta cadastrada com este e-mail.'
            ], 422);
        }else{

            $datebegin = date("Y-m-d");
            $timestamp = strtotime("+10 days");
            $datefinishfree = date('Y-m-d', $timestamp);

            $name = strtolower(str_replace(" ", "_", $request['name']));

            //cadastra o tenantid
            $tenantdata = [
                'tenant' => $name,
                'cnpj' => '00.000.000/0001-00',
                'email' => $request['email'],
                'name' => $request['name'],
                'phone1' => '(00) 0 0000-0000',
                'phone2' => '(00) 0 0000-0000',
                'datebeginfree' => $datebegin,
                'datefinishfree' => $datefinishfree,
                'amountuser' => 2
            ];
            
            $tenant = $this->tenant::create($tenantdata);

            //grupo de permissões
            $groupdata = [
                'tenantid' => $tenant->id,
                'groupuser' => 'Administrador'
            ];
            
            $group = $this->group::create($groupdata);

            //permission
            $pages = ['config', 'company', 'product', 'clientselects', 'item', 'client', 'represented', 'budget', 'transport', 'myplan', 'reportrecord', 'reportclient', 'reporttalk', 'reportitemcl', 'reportproduct', 'reportbudget', 'reportbilling'];
            $view = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            $create = [1, 1, 1, 1, 1, 1, 1, 1, 1,  1, null, null, null, null, null, null, null];
            $update = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, null, null, null, null, null, null, null];
            $delete = [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, null, null, null, null, null, null, null];


            for ($i=0;$i<count($pages);$i++){

                $permission = new PermissionCuston(); 
                $permission->tenantid = $tenant->id;                                  
                $permission->groupid = $group->id;
                $permission->page = $pages[$i];
                $permission->view = $view[$i];
                $permission->create = $create[$i];
                $permission->update = $update[$i];
                $permission->delete = $delete[$i];                                              
                $permission->save();
            }

            //custonorder
            $datacustomorder = [
                'tenantid' => $tenant->id,
                'photorepresented' => 0,
                'namerepresented' => 0,
                'inforepresented' => 0,
                'code' => 0,
                'item' => 1,
                'ipi' => 1,
                'percent' => 1,
                'descont' => 1,
                'subtotal' => 1,
                'subtotalproduct' => 1,
                'totalproducttext' => 1,
                'comment' => 1,
            ];
            $custonorder = $this->custom::create($datacustomorder);      

            $aux = explode(' ', $request['name']);

            //usuário novo
            $userdata = [
                'tenantid' => $tenant->id,
                'ismaster' => 0,
                'isadmin' => 1,
                'responsible' => 1,
                'name' => $request['name'],
                'user' => $name,
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'active' => 1,
                'groupus' => $group->id,
                'region' => '',
                'extern' => 1,
                'routeuser' => '',
                'phone1' => '(00) 0 0000-0000',
                'phone2' => '(00) 0 0000-0000',
                'phone3' => '(00) 0 0000-0000'
            ];
            
            $user = $this->user::create($userdata);

            //company

            $companydata = [
                'tenantid' => $tenant->id,
                'cnpj' => '00.000.000/0000-00',
                'reason' => '-',
                'namefantasy' => '-',
                'cep' => '00.000-000',
                'adress' => 'Rua exemplo endereço',
                'number' => '0',
                'city' => '-',
                'district' => '-',
                'phone1' => '(00) 00000-0000',
                'phone2' => '(00) 0000-0000',
                'email' => $request['email'],
                'amountuser' => 2
            ];

            $company = $this->company::create($companydata);

            $groupclientdata2 = [
                'tenantid' => $tenant->id,
                'groupclient' => 'Inativo'
            ];

            $groupclientdata3 = [
                'tenantid' => $tenant->id,
                'groupclient' => 'Prospecto'
            ];

            $groupclientdata4 = [
                'tenantid' => $tenant->id,
                'groupclient' => 'Ativo'
            ];

            $this->groupclient::create($groupclientdata2);
            $this->groupclient::create($groupclientdata3);
            $this->groupclient::create($groupclientdata4);

            
            $branchclientdata1 = [
                'tenantid' => $tenant->id,
                'branchclient' => 'Indústria'
            ];
            $branchclientdata2 = [
                'tenantid' => $tenant->id,
                'branchclient' => 'Fábricas'
            ];
            
            $branchclient = $branchclientdata3 = [
                'tenantid' => $tenant->id,
                'branchclient' => 'Comércio'
            ];

            $this->branchclient::create($branchclientdata1);
            $this->branchclient::create($branchclientdata2);
            $this->branchclient::create($branchclientdata3);
            
            $routeclientdata = [
                'tenantid' => $tenant->id,
                'routeclient' => 'Brasil'
            ];

            $routeclient = $this->routeclient::create($routeclientdata);

            $departamentclientdata1 = [
                'tenantid' => $tenant->id,
                'departamentcontact' => 'Diretoria',
            ];

            $departamentclientdata2 = [
                'tenantid' => $tenant->id,
                'departamentcontact' => 'Administração',
            ];

            $departamentclient = $departamentclientdata3 = [
                'tenantid' => $tenant->id,
                'departamentcontact' => 'Compras',
            ];

            $this->departamentclient::create($departamentclientdata1);
            $this->departamentclient::create($departamentclientdata2);
            $departamentclient = $this->departamentclient::create($departamentclientdata3);

            
            $cnpjfalse = '00000000' . date("Hisu");
            $cnpjfalse = MaskCNPJ($cnpjfalse,'##.###.###/####-##');
            $representesedata = [
                'tenantid' => $tenant->id,
                'cod' => '-',
                'represented' => 'Representada Ltda [Exemplo]',
                'reference' => 'Representada Exemplo',
                'cnpjcpf' => $cnpjfalse,
                'group' => 'representada',
                'ie' => '000000',
                'phone1' => '(41) 0000-0000',
                'phone2' => '(41) 00000-0000',
                'cep' => '00000000',
                'street' => 'Rua Col. Matheus six',
                'complements' => 'Bloco 15',
                'neighborhood' => '',
                'city' => 'São Paulo',
                'uf' => 'São Paulo',
                'email' => 'email@exemplo',
                'site' => 'www.site.com.br',
                'logo' => '',
                'textfix' => 'Este testo é para uso da representada, caso ela queira deixar ao informado nos orçamentos que remetem a ela.'
            ];

            $represented = $this->represented::create($representesedata);
            
            $transportdata = [
                'tenantid' => $tenant->id,
                'cod' => '-',
                'transport' => 'Transportadora Transpack Ltda [Exemplo ]',
                'reference' => 'TR Transpack',
                'cnpjcpf' => $cnpjfalse,
                'ie' => '000000',
                'phone1' => '(41) 0000-0000',
                'phone2' => '(41) 00000-0000',
                'cep' => '00000000',
                'street' => 'Rua Col. Teéofilo Filho',
                'complements' => 'Ala2',
                'neighborhood' => '',
                'city' => 'São Paulo',
                'uf' => 'São Paulo',
                'email' => 'email@exemplo',
                'site' => 'www.site.com.br'          
            ];

            $transport = $this->transport::create($transportdata);
    
            $query = "SELECT MAX(cod)+1 AS cod FROM clients WHERE tenantid = '$tenant->id'";
            $nextnumber = \DB::select($query);

            if($nextnumber[0]->cod === null){
                $next = [
                    'cod' => 1,
                ];
            }else {
                $next = $nextnumber;
            }

            $clientdata = [
                'tenantid' => $tenant->id,
                'cod' => $next['cod'],
                'client' => 'Joias Cds Ltda [Exemplo]',
                'reference' => 'Joias Cds',
                'cnpjcpf' => $cnpjfalse,
                'group' => 'Ativo',
                'branch' => 'Comércio',
                'dateupdate' => date('d/m/Y'),
                'route' => 'Brasil',
                'ie' => '000000',
                'phone1' => '(41) 0000-0000',
                'phone2' => '(41) 00000-0000',
                'cep' => '00000000',
                'street' => 'Rua Argentina',
                'complements' => 'Bloco 20',
                'neighborhood' => '',
                'city' => 'São Paulo',
                'uf' => 'São Paulo',
                'email' => 'email@exemplo',
                'site' => 'www.site.com.br',
                'userid' => $user->id      //mudar aqui depois de terminar as alterações
            ];

            $client = $this->client::create($clientdata);

            $representedclients = new CustonRepresentedclients();            
            $representedclients->clientid = $client->id;
            $representedclients->tenantid = $tenant->id;
            $representedclients->representedid = $represented->id;
            $representedclients->save();


            //comments
            $commentdata = [
                'tenantid' => $tenant->id, 
                'clientid' => $client->id,
                'comment' => 'Observação [Exemplo], O cliente precisa do produto embalado com plastico bolha para nao danificar o produto',
            ];
            $comment = $this->comment::create($commentdata);

            //contacts
            $contactdata = [
                'contact' => 'João [Exemplo]',
                'tenantid' => $tenant->id,
                'clientid' => $client->id,            
                'departament' => '',
                'phone2' => '(00) 0000-0000',
                'phone' => '(00) 00000-0000',
                'email' => 'email@example.com.br' 
            ];
            $contact = $this->contact::create($contactdata);

            $itemdata = [
                'tenantid' => $tenant->id,
                'item' => 'Item [Exemplo]'
            ];        
            $item = $this->item::create($itemdata);

            $groupitemdata = [
                'tenantid' => $tenant->id,
                'group' => 'Grupo [Exemplo]'
            ];        
            $groupitem = $this->groupitem::create($groupitemdata);

            $branditemdata = [
                'tenantid' => $tenant->id,
                'brand' => 'Marca [Exemplo]'
            ];        
            $branditem = $this->branditem::create($branditemdata);

            $itemcldata = [
                'tenantid' => $tenant->id,
                'itemcl' => $item->item,  
                'clientid' => $client->id,                       
                'groupcl' => $groupitem->group,
                'brandcl' => $branditem->brand,
                'amountcl' => '1',             
            ];
            $itemcl = $this->itemcl::create($itemcldata);

            $talkdata = [
                'tenantid' => $tenant->id,
                'clientid' => $client->id,          
                'subject' => 'Primeira vez usando o Abinf Sales',            
                'date' => date('Y-m-d'),
                'hour' => date('H:i'),
                'type' => 'vf',
                'contactcl' => $contact->id,
                'represented' => $represented->id,
                'description' => 'Primeiro contato com o Abinf Sales.',
                'userid' => $user->id,
                'isactivity' => '1'
            ];
            $talk = $this->talk::create($talkdata); 


            $agendadata = [
                'tenantid' => $tenant->id,
                'clientid' => $client->id,          
                'subject' => 'Visitar cliente [Exemplo]',            
                'date' => date('Y-m-d'),
                'hour' => date('H:i'),
                'type' => 'v',
                'contactcl' => $contact->id,
                'represented' => $represented->id,
                'description' => 'Visitar cliente [Exemplo].',
                'userid' => $user->id,
                'isactivity' => '0'
            ];
            $agenda = $this->talk::create($agendadata);

            
            //groupprodut 
           $datagroupproduct = ['tenantid' => $tenant->id, 'groupproduct' => 'Grupo [Exemplo]'];
            $groupproduct = $this->groupproduct::create($datagroupproduct);

            //product

            $dataproduct = [
                'tenantid' => $tenant->id,
                'cod' => 'EXE01',
                'product' => 'Produto [Exemplo]',            
                'ipi' => '10',
                'unit' => 'pç',
                'price' => '200.000',
                'commission' => '0',                
                'groupprod' => $groupproduct->id,
                'idrep' => $represented->id,            
            ];

            $product = $this->product::create($dataproduct);
    
            //budgets
            $datamotivedescart = ['tenantid' => $tenant->id, 'motivedescart' => 'Preços Altos [Exemplo]'];
            $motivedescart = $this->motivedescart::create($datamotivedescart);


            $query = "SELECT MAX(control)+1 AS control FROM budgets WHERE tenantid = '$tenant->id'";
            $nextnumberb = \DB::select($query);

            if($nextnumberb[0]->control === null){
                $nextb = [
                    'control' => 1,
                ];
            }else {
                $nextb = $nextnumberb;
            }

            $databudget = [
                'control' => $nextb['control'],
                'tenantid' => $tenant->id,
                'type' => '1',
                'date' => date('Y-m-d'),          
                'representedid' => $represented->id,
                'client' => $client->id,
                'contact' => $contact->contact,
                'delivery' => '30 Dias',                        
                'situation' => 'concluido', 
                'completed' => date('Y-m-d'),
                'form_pay' => '30/60/90',
                'order' => 'f100',                                                
                'transport' => $transport->transport,                                              
                'comment' => 'Primeiro pedido do cliente [Exemplo]',        
                'seller' => $request['name'], 
                'mot_descart' => '',
                'userid' => $user->id,
            ];

            $budget = $this->budget::create($databudget);


            $dataitensbud = [
                'tenantid' => $tenant->id,   
                'budgetid' => $budget->id,
                'product' => $product->id,           
                'amount' => '1',
                'ipi' => $product->ipi,
                'unitary' => $product->price,                        
                'total' => $product->price + 20, 
                'nf' => 'NF01',
                'date' => date('Y-m-d'),   
                'entregue' => '1',                                            
                'comission' => '0',             
            ];

            $itensbudget = $this->itensbudget::create($dataitensbud);
    

            //fazer formatt
            $dataformatt = [
                'tenantid' => $tenant->id,
                'type' => 'normal'
            ];

            $update = $this->formattext::create($dataformatt);

            //email de bem vindo ao cliente

            //payment talvez

            $datapayment = [
                'tenantid' => $tenant->id,
                'plan' => 'Free',
                'referenceplan' => 'abinfsales_free',
                'codeplan' => 'fr33',
                'codeadesion' => 'fr33',
                'status' => 'Free',
                'value' => '32.00', //começa com zero pois só vai atualizar o valor quando fizer adesão do plano
                'period' => 'monthly' //definir valor do plano inicial
            ];

            $payment = Plan::create($datapayment);

            //quando usar essa rota para aderir ao plano, tem que atualizar também a quantidade de usuários se for mudada

            //$user = User::where('id', $user->id)->first();
            $userw = $this->user::whereNull('deleted_at')
            ->where(['users.tenantid' => $tenant->id, 'users.id' => $user->id])
            ->first();

            $when = now()->addMinutes(1);

            Mail::to($request['email'])->later($when, new Welcomeuser($userw));

            return response()->json([
                'SUCCESS' => 'Bem vindo.'
            ]);
        }
    }
}
