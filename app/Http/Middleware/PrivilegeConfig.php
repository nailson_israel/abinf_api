<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;

class PrivilegeConfig
{
    use GetTenantid;    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();
        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'config')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();

        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }
        /*
            permissions users
        */
        if ($request->route()->named('users') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('adduser') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('upuser') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('deluser') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delalluser') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        /*
            permissions grouppermissions
        */
        if ($request->route()->named('permission') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('groups') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addgroup') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('upgroup') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('delgroup') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallgroup') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if ($request->route()->named('permissionid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('groupuserid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }
        

        /*
            permissions formatt
        */

        if ($request->route()->named('format') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addformat') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('upformat') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('delformat') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        /*
            permissions changes
        */
        if ($request->route()->named('changes') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addchange') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('upchange') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('delchange') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallchange') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }
        /*
            permissions departament
        */
        if ($request->route()->named('departaments') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('adddepartament') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('updepartament') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('deldepartament') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        } 

        if($request->route()->named('delalldepartament') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }  

        if($request->route()->named('upformattext') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        return $next($request);
    }
}
