<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;

class PrivilegeTalk
{
    use GetTenantid;    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();

        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'talk')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();

        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }

        if ($request->route()->named('talk') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('talkid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addtalk') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        if($request->route()->named('addtalkid') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }        

        if($request->route()->named('uptalk') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if($request->route()->named('deltalk') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delalltalk') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('upstatus') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if ($request->route()->named('activity') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('agenda') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('activitypanel') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('agendapanel') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('activityall') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('agendaall') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        return $next($request);
    }
}
