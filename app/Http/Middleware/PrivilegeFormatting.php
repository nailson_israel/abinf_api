<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;


class PrivilegeFormatting
{
    use GetTenantid;    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();

        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'formatt')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();


        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }

        if($request->route()->named('addformat') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        if($request->route()->named('upformat') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if($request->route()->named('delformat') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallformat') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        return $next($request);
    }
}
