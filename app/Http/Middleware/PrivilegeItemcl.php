<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;

class PrivilegeItemcl
{
    use GetTenantid;    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();

        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'itemcl')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();

        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }

        if ($request->route()->named('itemcl') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('itemclid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('additemclid') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('additemcl') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para adicionar']);
        }

        if($request->route()->named('upitemcl') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para alterar']);
        }

        if($request->route()->named('delitemcl') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallitemcl') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        return $next($request);
    }
}
