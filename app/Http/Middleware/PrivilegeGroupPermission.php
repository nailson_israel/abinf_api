<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\ConectionTrait;
use App\Models\Api\Groupuser;
use App\Models\Api\Permission;
use App\Models\User;

class PrivilegeGroupPermission
{
    use ConectionTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
  

        return $next($request);
    }
}
