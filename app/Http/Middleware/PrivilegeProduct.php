<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;

class PrivilegeProduct
{
    use GetTenantid;     
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();

        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'product')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();

        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }


        if($request->route()->named('product') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }


        if($request->route()->named('productid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addproduct') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        if($request->route()->named('upproduct') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if($request->route()->named('delproduct') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallproduct') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('productid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        //grouproduct
        if($request->route()->named('groupproduct') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addgroupproduct') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        if($request->route()->named('upgroupproduct') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if($request->route()->named('delgroupproduct') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delallgroupproduct') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }
        //risegroup
        if($request->route()->named('rise') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if($request->route()->named('addrise') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        return $next($request);
    }
}
