<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\User;

class PrivilegeTransport
{
    use GetTenantid;     
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $tenantid = $this->GetTenant();  
        $userloged = \Auth::user();

        $permission = User::leftJoin('groups', 'users.groupus', '=', 'groups.id')->leftJoin('permissions', 'users.groupus', '=', 'permissions.groupid')->where('users.id', $userloged['id'])->where('permissions.page', 'transport')->where('permissions.tenantid', $tenantid)->whereNull('users.deleted_at')->get();


        foreach ($permission as $pm) {
            $view = $pm->view;
            $create = $pm->create;
            $update = $pm->update;
            $delete = $pm->delete;
        }

        if ($request->route()->named('transport') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }

        if ($request->route()->named('transportid') && $view == null) {
            return response()
            ->json(['permissions' => 'view', 'state' => '0', 'message' => 'Você não tem permissão para acessar esta pagina']);
        }


        if($request->route()->named('addtransport') && $create == null){
            return response()
            ->json(['permissions' => 'create', 'state' => '0', 'message' => 'Você não tem permissão para  adicionar']);
        }

        if($request->route()->named('uptransport') && $update == null){
            return response()
            ->json(['permissions' => 'update', 'state' => '0', 'message' => 'Você não tem permissão para  alterar']);
        }

        if($request->route()->named('deltransport') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }

        if($request->route()->named('delalltransport') && $delete == null){
            return response()
            ->json(['permissions' => 'delete', 'state' => '0', 'message' => 'Você não tem permissão para  deletar']);
        }
        return $next($request);
    }
}
