<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\GetTenantid;
use App\Models\Tenant;
use App\Models\Plan;
use App\Models\Payment;

class PayPlan
{
    use GetTenantid;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantid = $this->GetTenant();
        $result = Tenant::where([
            ['id', $tenantid]
        ])->first();

        /*
        Adhesions - plans

        PENDING pendente
        ACTIVE ativa
        PAYMENT_METHOD_CHANGE cartão sem limite, vencido, suspenso ou bloqueado
        SUSPENDED suspensa pelo vendedor
        CANCELLED cancelada pego pagseguro
        CANCELLED_BY_RECEIVER cancelada pelo vendedor
        CANCELLED_BY_SENDER cancelada pelo usuario
        */

        $plan = Plan::where('tenantid', $tenantid)->first();


        if($plan->status == 'free' || $plan->status == 'Free'){
            $begin = new \DateTime(date("Y-m-d"));
            $finish = new \DateTime($result->datefinishfree); 
            
            $diff = $begin->diff($finish);
            $diff->format('%R');

            $days = $diff->format('%R') . ' ' . $diff->days;

            $ex = explode(" ", $days);
            if($ex[0] == '+'){
                $rst = $ex[1];
                return $next($request);
            }else{
                return $rst = 0;
            }
        }else{
            /*
            INITIATED
            PENDING
            ACTIVE
            PAYMENT_METHOD_CHANGE
            SUSPENDED
            CANCELLED
            CANCELLED_BY_RECEIVER
            CANCELLED_BY_SENDER
            EXPIRED

            */
            $payment = Payment::whereNull('deleted_at')
                    ->where([
                        ['tenantid', '=', $tenantid]
                    ])->orderBy('id', 'DESC')->first();


            if($plan->status == 'REGISTERED' || $plan->status == 'PENDING'){
                $rst = 20;
                return $next($request);                
            }else if($plan->status == 'CANCELLED_BY_SENDER' || $plan->status == 'CANCELLED_BY_RECEIVER' || $plan->status == 'CANCELLED' || $plan->status == 'PAYMENT_METHOD_CHANGE'){
                $rst = 30;
                return response()->json([
                    'unautorized' => 'Oh não, sua conta foi cancelada.'
                ], 403);
            }else if($plan->status == 'SUSPENDED'){
                    $rst = 40;
                    return response()->json([
                        'unautorized' => 'Oh não, sua conta foi suspensa, você suspendeu?.'
                    ], 403);
            }else{
                //status active
                if($payment != ''){
                    if($payment->status == 1 || $payment->status == 2 || $payment->status == 3){
                        $rst = 20;
                         return $next($request);
                    }else if($payment->status == 7){
                        $rst = 35;
                        return response()->json([
                            'unautorized' => 'Oh não, sua conta foi cancelada.'
                        ], 403);
                    }else{
                        $rst = 0;
                        return response()->json([
                            'unautorized' => 'Oh não, vocẽ deixou de pagar e com isso seu usuário foi bloqueado.'
                        ], 403);
                    }
                }else{
                    $rst = 20;
                     return $next($request);
                }
            }
        }

    }
}
