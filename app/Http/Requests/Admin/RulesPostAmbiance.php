<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostAmbiance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [

            'tenant' => ['required', Rule::unique('tenants')->ignore($id)->where(function ($query) {
             $query->whereNull('deleted_at');
            })],
            'cnpj' => 'required|string|max:18|min:14',
            'name' => 'required|string|max:255|min:2',
            'email' => 'required|string|max:255|min:2',
            'phone' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'tenant.required' => 'O cliente é obrigatório',
            'tenant.max'  => 'O cliente deve conter no máximo 255 caracteres',
            'tenant.min' => 'O cliente deve conter no minimo 2 caracteres',
            'tenant.unique' => 'Este cliente já existe',            
            
            'cnpj.required' => 'O cnpj é obrigatório',
            'cnpj.max'  => 'O cnpj deve conter no máximo 255 caracteres',
            'cnpj.min' => 'O cnpj deve conter no minimo 2 caracteres',
            
            'name.required' => 'O nome é obrigatório',
            'name.max'  => 'O nome deve conter no máximo 255 caracteres',
            'name.min' => 'O nome deve conter no minimo 2 caracteres',
            
            'email.required' => 'O email é obrigatório',
            'email.max'  => 'O email deve conter no máximo 255 caracteres',
            'email.min' => 'O email deve conter no minimo 2 caracteres',                               

            'phone.required' => 'O telefone é obrigatório', 
        ];
    }

}
