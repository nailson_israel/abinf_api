<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');

        return [

            'user' => ['required', Rule::unique('users')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
            'email' => 'required|string|email|max:255|min:5',
            'groupus' => 'required',
            'active' => 'required',
            'extern' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user.unique' => 'Este usuário já existe',
            'user.required' => 'O Usuário é obrigatório',
            'user.max'  => 'O Usuário deve conter no máximo 255 caracteres',
            'user.min' => 'O Usuário deve conter no minimo 2 caracteres',
            'email.required' => 'O Email é obrigatório',
            'email.email' => 'Por favor insira o email no formato correto ex: example@example.com.br',
            'email.max' => 'O email deve conter no máximo 255 caracteres',
            'email.min' => 'O email deve conter no minimo 2 caracteres',
            'groupus.required' => 'O Grupo de usuário é obrigatório',
            'active.required' => 'O status é obrigatório',
            'extern.required' => 'O tipo de usuário é obrigatório'
        ];
    }

}
