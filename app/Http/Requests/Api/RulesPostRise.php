<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostRise extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'rise' => 'required',
            'group' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'rise.required' => 'O aumento é obrigatório',
            'group.required' => 'O grupo é obrigatório',
        ];
    } 
}
