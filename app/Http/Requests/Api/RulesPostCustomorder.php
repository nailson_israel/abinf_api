<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostCustomorder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'photorepresented' => 'required|max:1|min:1',
            'namerepresented' => 'required|max:1|min:1',
            'inforepresented' => 'required|max:1|min:1',
            'code' => 'required|max:1|min:1',
            'item' => 'required|max:1|min:1',
            'percent' => 'required|max:1|min:1',
            'descont' => 'required|max:1|min:1',
            'subtotal' => 'required|max:1|min:1'
        ];
    }

    public function messages()
    {
        return [
            'photorepresented.required' => 'Este campo é obrigatório',
            'photorepresented.max' => 'Este campo deve conter no máximo 1 caractere',
            'photorepresented.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'namerepresented.required' => 'Este campo é obrigatório',
            'namerepresented.max' => 'Este campo deve conter no máximo 1 caractere',
            'namerepresented.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'inforepresented.required' => 'Este campo é obrigatório',
            'inforepresented.max' => 'Este campo deve conter no máximo 1 caractere',
            'inforepresented.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'code.required' => 'Este campo é obrigatório',
            'code.max' => 'Este campo deve conter no máximo 1 caractere',
            'code.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'item.required' => 'Este campo é obrigatório',
            'item.max' => 'Este campo deve conter no máximo 1 caractere',
            'item.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'percent.required' => 'Este campo é obrigatório',
            'percent.max' => 'Este campo deve conter no máximo 1 caractere',
            'percent.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'descont.required' => 'Este campo é obrigatório',
            'descont.max' => 'Este campo deve conter no máximo 1 caractere',
            'descont.min' => 'Este campo deve deve conter no minimo 1 caracter',
            'subtotal.required' => 'Este campo é obrigatório',
            'subtotal.max' => 'Este campo deve conter no máximo 1 caractere',
            'subtotal.min' => 'Este campo deve deve conter no minimo 1 caracter',
        ];
    }

}
