<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostFunnel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'funnel' => ['required', Rule::unique('funnels')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }


    public function messages()
    {
        return [
            'funnel.required' => 'O funil deve conter no máximo 255 caracteres',
            'funnel.min' => 'O funil deve conter no minimo 2 caracteres',
            'funnel.unique' => 'Este funil já existe',
        ];
    }
}
