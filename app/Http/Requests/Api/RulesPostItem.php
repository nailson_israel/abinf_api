<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class RulesPostItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            //'change' => 'required|string|max:255|min:2|unique:changes,change,'.$id.',id',
            'item' => ['required', Rule::unique('items')->ignore($id)->where(function ($query) {
             $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
             $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'item.required' => 'O item é obrigatório',
            'item.max'  => 'O item deve conter no máximo 255 caracteres',
            'item.min' => 'O item deve conter no minimo 2 caracteres',
            'item.unique' => 'Este item já existe',
        ];
    }
}
