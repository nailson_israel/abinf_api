<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            //'change' => 'required|string|max:255|min:2|unique:changes,change,'.$id.',id',
            'change' => ['required', Rule::unique('changes')->ignore($id)->where(function ($query) {
             $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
             $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'change.required' => 'O cargo é obrigatório',
            'change.max'  => 'O cargo deve conter no máximo 255 caracteres',
            'change.min' => 'O cargo deve conter no minimo 2 caracteres',
            'change.unique' => 'Este cargo já existe',
        ];
    }

}
