<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostDepartamentcontact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'departamentcontact' => ['required', Rule::unique('departamentcontacts')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'departamentcontact.required' => 'O departamento é obrigatório',
            'departamentcontact.max'  => 'O departamento deve conter no máximo 255 caracteres',
            'departamentcontact.min' => 'O departamento deve conter no minimo 2 caracteres',
            'departamentcontact.unique' => 'Este departamento já existe',
        ];
    }
}
