<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostFormatting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'tables' => ['required', Rule::unique('formatts')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:2', 'min:1'],
            'caixatext' => 'required|max:2|min:1',
            'relatpage' => 'required|max:2|min:1',
            'relatpdf' => 'required|max:2|min:1',
            'header' => 'required|max:2|min:1',
            'pdfbudget' => 'required|max:2|min:1',
        ];
    }

    public function messages()
    {
        return [
            'tables.required' => 'A fonte é da tabela obrigatória',
            'tables.unique' => 'Você já contém formatações para sua aplicação.',
            'tables.max' => 'A fonte da tabela deve conter no máximo 2 caracteres',
            'tables.min' => 'A fonte da tabela deve conter no minimo 1 caracter',
            'caixatext.required' => 'A fonte da caixa de texto é obrigatória',
            'caixatext.max' => 'A fonte da caixa de texto deve conter no máximo 2 caracteres',
            'caixatext.min' => 'A fonte da caixa de texto deve conter no minimo 1 caracter',
            'relatpage.required' => 'A fonte do relatório de pagina é obrigatória',
            'relatepage.max' => 'A fonte do relatório de pagina deve conter no máximo 2 caracteres',
            'relatepage.min' => 'A fonte do relatório de pagina deve conter no minimo 1 caracter',
            'relatpdf.required' => 'A fonte do relatório pdf é obrigatória',
            'relatpdf.max' => 'A fonte do relatório pdf deve conter no máximo 2 caracteres',
            'relatpdf.min' => 'A fonte do relatório pdf deve conter no minimo 1 caracter',
            'header.required' => 'A fonte do cabeçalho é obrigatória',
            'header.max' => 'A fonte do cabeçalho deve conter no máximo 2 caracteres',
            'header.min' => 'A fonte do cabeçalho deve conter no máximo 1 caracter',
            'pdfbudget.required' => 'A fonte do pdf orçamentos é obrigatório',
            'pdfbudget.max' => 'A fonte do pdf orçamentos deve conter no máximo 2 caracteres',
            'pdfbudget.min' => 'A fonte do pdf orçamentos deve conter no máximo 1 caracter',         
        ];
    }

}
