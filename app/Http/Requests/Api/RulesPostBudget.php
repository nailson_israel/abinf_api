<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostBudget extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
       
        return [

            'control' => ['required', Rule::unique('budgets')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            })],
            'type' => 'required',
            'date' => 'required|max:10|min:10', 
            'represented' => 'required',
            'client' => 'required',
            'delivery' => 'required',
            'situation' => 'required',
            'form_pay' => 'required|max:255|min:1',            
        ];
    }

    public function messages()
    {
        return [
            'control.unique' => 'O orçamento já existe',
            'type.required' => 'O tipo é obrigatório',
            'date.required'  => 'A data é obrigatória',
            'date.max'  => 'A data deve conter no máximo 10 caracteres',
            'date.min' => 'A data deve conter no máximo 10 caracteres',
            'represented.required' => 'A representada é obrigatória',
            'client.required' => 'O cliente é obrigatório',
            'delivery.required' => 'A data de entrega é obrigatório',
            'situation.required' => 'A situação é obrigatória',
            'form_pay.required' => 'A forma de pagamento é obrigatória',
            'form_pay.max' => 'A forma de pagamento deve conter no máximo 255 caracteres',
            'form_pay.min' => 'A forma de pagamento deve conter no minimo 1 caracteres',           
        ];
    }
}
