<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostTransport extends FormRequest
{     
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'transport' => ['required', Rule::unique('transports')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'transport.unique' => 'Esta transportadora já existe',
            'transport.required' => 'A transportadora é obrigatório',
            'transport.max'  => 'A transportadora deve conter no máximo 255 caracteres',
            'transport.min' => 'A transportadora conter no minimo 1 caracteres',
        ];
    }

}
