<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostFormatt extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'type' => ['required', Rule::unique('formattexts')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:1']
        ];
    }

    public function messages()
    {
        return [
            'tables.required' => 'O tipo é obrigatório',
            'type.max' => 'O tipo deve conter no máximo 255 caracteres',
            'type.min' => 'O tipo deve conter no minimo 1 caracter',
        ];
    }

}
