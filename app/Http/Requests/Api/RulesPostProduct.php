<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostProduct extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'cod' => ['required', Rule::unique('products')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
            'product' => 'required|string|max:255|min:1',
            'price' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cod.required' => 'O código é obrigatório',
            'cod.max'  => 'O código deve conter no máximo 255 caracteres',
            'cod.min' => 'O código deve conter no minimo 2 caracteres',
            'cod.unique' => 'Este código já existe',
            'product.required' => 'O produto é obrigatório',
            'product.max' => 'O produto deve conter ',
            'price.required' => 'O preço é obrigatório',
        ];
    }    
}
