<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostDepartament extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        echo $id = $this->route('id');

        return [
            //'departament' => 'required|string|max:255|min:2|unique:departaments,departament,'.$id.',id',
            'departament' => ['required', Rule::unique('departaments')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];

    }

    public function messages()
    {
        return [
            'departament.required' => 'O departamento é obrigatório',
            'departament.max'  => 'O departamento deve conter no máximo 255 caracteres',
            'departament.min' => 'O departamento deve conter no minimo 2 caracteres',
            'departament.unique' => 'Este departamento já existe',
        ];
    }

}
