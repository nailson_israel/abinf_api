<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostGroupclient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'groupclient' => ['required', Rule::unique('groupclients')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'groupclient.required' => 'O grupo é obrigatório',
            'groupclient.max'  => 'O grupo deve conter no máximo 255 caracteres',
            'groupclient.min' => 'O grupo deve conter no minimo 2 caracteres',
            'groupclient.unique' => 'Este grupo já existe',
        ];
    }
}
