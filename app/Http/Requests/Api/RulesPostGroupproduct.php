<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostGroupproduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'groupproduct' => [
                'required', 
                Rule::unique('groupproducts')->ignore($id)->where(function ($query) { 
                    $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                    $query->whereNull('deleted_at');
                }),

                'min:2',
                'max:255',
            ],
        ];
    }

    public function messages()
    {
        return [
            'groupproduct.required' => 'O grupo é obrigatório',
            'groupproduct.max'  => 'O grupo deve conter no máximo 255 caracteres',
            'groupproduct.min' => 'O grupo deve conter no minimo 2 caracteres',
            'groupproduct.unique' => 'Este grupo já existe',
        ];
    }
}
