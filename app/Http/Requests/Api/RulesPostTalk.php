<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostTalk extends FormRequest
{       
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [

            'description' => ['required', Rule::unique('talks')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'min:2'],
            'subject' => 'required|string|max:255|min:1',
            'date' => 'required',
            'hour' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'A descrição é obrigatório',
            'description.min' => 'A descrição deve conter no minimo 2 caracteres',
            'description.unique' => 'Esta descrição já existe',
            'date.required' => 'A data é obrigatória',
            'hour.required' => 'A hora é obrigatória',
            'type.required' => 'O tipo é obrigatório',
        ];
    }
}
