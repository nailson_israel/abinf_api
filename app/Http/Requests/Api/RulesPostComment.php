<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostComment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [

            'comment' => ['required', Rule::unique('comments')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'comment.required' => 'A observação é obrigatório',
            'comment.min' => 'A observação deve conter no minimo 2 caracteres',
            'comment.unique' => 'Esta observação já existe',
        ];
    }
}
