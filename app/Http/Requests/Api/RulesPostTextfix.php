<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostTextfix extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'textfix' => 'required|string|min:2',
        ];
    }

    public function messages()
    {
        return [
            'textfix.required' => 'O texto fixo é obrigatório',
            'textfix.min' => 'O texto fixo deve conter no minimo 1 caracteres',
        ];
    }
}
