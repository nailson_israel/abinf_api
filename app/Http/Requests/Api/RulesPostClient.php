<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostClient extends FormRequest
{     
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [

            'cnpjcpf' => [Rule::unique('clients')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            })],
            'client' => 'required|max:255|min:1',
            'group' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cnpjcpf.unique' => 'O cnpj já existe',     
            'client.required' => 'O cliente é obrigatório',
            'client.max'  => 'O cliente deve conter no máximo 255 caracteres',
            'client.min' => 'O cliente deve conter no minimo 1 caracteres',
            
            'group.required' => 'O grupo é obrigatório',
        ];
    }

}
