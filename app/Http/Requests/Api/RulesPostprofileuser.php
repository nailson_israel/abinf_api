<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostprofileuser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');

        return [

            'user' => ['required', Rule::unique('users')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'min:3'],
            'email' => 'required|string|email|max:255|min:3',
            'name' => 'required|min:3',
        ];
    }

    public function messages()
    {
        return [
            'user.unique' => 'Este usuário já existe',
            'user.required' => 'O Usuário é obrigatório',
            'user.min' => 'O Usuário deve conter no minimo 3 caracteres',
            'email.required' => 'O Email é obrigatório',
            'email.email' => 'Por favor insira o email no formato correto ex: example@example.com.br',
            'email.max' => 'O email deve conter no máximo 255 caracteres',
            'email.min' => 'O email deve conter no minimo 3 caracteres',
            'name.required' => 'O nome é obrigatório',
            'name.min' => 'O nome deve conter no minimo 3 caracteres',
        ];
    }

}
