<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostCompany extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'reason' => ['required', Rule::unique('companies')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:3'],
            'cnpj' => 'required|max:18|min:14',
            'namefantasy' => 'required',
            'cep' => 'required',
            'address' => 'required',
            'number' => 'required',
            'city' => 'required',
            'district' => 'required',
            'phone1' => 'required',
            'email' => 'required',                        
        ];
    }

    public function messages()
    {
        return [
            'reason.unique' => 'Esta razão social já existe',
            'cnpj.required' => 'O cnpj é obrigatório',
            'cnpj.max'  => 'O cnpj deve conter no máximo 14 caracteres',
            'cnpj.min' => 'O cargo deve conter no minimo 14 caracteres',
            'namefantasy.required' => 'O nome fantasia da empresa é obrigatório',
            'reason.required' => 'A razão social é obrigatória',
            'reason.max' => 'A razão social deve conter no máximo 255 caracteres',
            'reason.min' => 'A razão social deve conter no minimo 3 caracteres',
            'cep.required' => 'O Cep é obrigatório',
            'address.required' => 'O Endereço é obrigatório',
            'number.required' => 'O numero é obrigatório',
            'city.requied' => 'A cidade é obrigatória',
            'district.required' => 'O estado é obrigatório',
            'phone1' => 'O telefone é obrigatório',
        ];
    }
}
