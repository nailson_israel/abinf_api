<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [

            'contact' => ['required', Rule::unique('contacts')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'contact.required' => 'O contato é obrigatório',
            'contact.max'  => 'O contato deve conter no máximo 255 caracteres',
            'contact.min' => 'O contato deve conter no minimo 2 caracteres',
            'contact.unique' => 'Este contato já existe',
        ];
    }
}
