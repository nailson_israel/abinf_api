<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostGroupuser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            //'groupuser' => 'required|string|max:255|min:2|unique:groups,groupuser,tenantid'.$id.',id',
            'groupuser' => ['required', Rule::unique('groups')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');                
            }), 'max:255', 'min:2'],
            'page' => [Rule::unique('permissions')->ignore($id)->where(function ($query) { $query->where('tenantid', \Auth::guard('api')->user()->tenantid);})],
        ];

    }

    public function messages()
    {
        return [
            'groupuser.required' => 'O grupo é obrigatório',
            'groupuser.max'  => 'O grupo deve conter no máximo 255 caracteres',
            'groupuser.min' => 'O grupo deve conter no minimo 2 caracteres',
            'groupuser.unique' => 'Este grupo já existe',
            'page.unique' => 'esta permissão já existe',
        ];
    }

}
