<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostprofilepassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');

        return [
            'newpassword' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'newpassword.required' => 'A senha nova é obrigatória.',
            'newpassword.min' => 'A senha deve conter mais de 8 caracteres.',
        ];
    }

}
