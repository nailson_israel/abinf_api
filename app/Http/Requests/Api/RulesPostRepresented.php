<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostRepresented extends FormRequest
{     
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'represented' => ['required', Rule::unique('representeds')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'represented.unique' => 'Esta representada já existe',
            'represented.required' => 'A representada é obrigatório',
            'represented.max'  => 'A representada deve conter no máximo 255 caracteres',
            'represented.min' => 'A representada conter no minimo 1 caracteres',
        ];
    }

}
