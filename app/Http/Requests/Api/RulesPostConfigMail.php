<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostConfigMail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'serversmtp' => ['required', Rule::unique('configmails')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:20'],
            'portsmtp' => 'required',
            'serverpop' => 'required|string|max:255|min:1',
            'portpop' => 'required',
            'security' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'serversmtp.required' => 'O serversmtp é obrigatório',
            'serversmtp.max'  => 'O servidor deve conter no máximo 255 caracteres',
            'serversmtp.min' => 'O servidor deve conter no minimo 20 caracteres',
            'serversmtp.unique' => 'Este servidor já existe',
            'portsmtp.required' => 'Esta porta é igatório',
            'serverpop.required' => 'O servidor é obrigatório',
            'serverpop.max'  => 'O servidor deve conter no máximo 255 caracteres',
            'serverpop.min' => 'O servidor deve conter no minimo 20 caracteres',            
            'portpop.required' => 'Esta porta é igatório',   
            'security.required' => 'o tipo de segurança é obrigatório',                        
        ];
    }
}
