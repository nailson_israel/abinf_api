<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostMotivedescart extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'motivedescart' => ['required', Rule::unique('motivedescarts')->ignore($id)->where(function ($query) { 
                $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
                $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];

    }

    public function messages()
    {
        return [
            'motivedescart.required' => 'O motivo é obrigatório',
            'motivedescart.max'  => 'O motivo deve conter no máximo 255 caracteres',
            'motivedescart.min' => 'O motivo deve conter no minimo 2 caracteres',
            'motivedescart.unique' => 'Este motivo já existe',
        ];
    }
}
