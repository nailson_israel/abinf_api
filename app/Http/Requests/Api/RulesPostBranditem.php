<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RulesPostBranditem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            //'change' => 'required|string|max:255|min:2|unique:changes,change,'.$id.',id',
            'brand' => ['required', Rule::unique('branditems')->ignore($id)->where(function ($query) {
             $query->where('tenantid', \Auth::guard('api')->user()->tenantid);
             $query->whereNull('deleted_at');
            }), 'max:255', 'min:2'],
        ];
    }

    public function messages()
    {
        return [
            'brand.required' => 'A marca é obrigatório',
            'brand.max'  => 'A marca deve conter no máximo 255 caracteres',
            'brand.min' => 'A marca deve conter no minimo 2 caracteres',
            'brand.unique' => 'Esta marca já existe',
        ];
    }
}
