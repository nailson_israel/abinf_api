<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \Barryvdh\Cors\HandleCors::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Barryvdh\Cors\HandleCors::class,
        ],

        'api' => [
            //'throttle:60,1',
            'bindings',
            \Barryvdh\Cors\HandleCors::class,            
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        
        'privilege.config' => \App\Http\Middleware\PrivilegeConfig::class,
        'privilege.companypermission' => \App\Http\Middleware\PrivilegeCompanyPermission::class,
        //'privilege.formatting' => \App\Http\Middleware\PrivilegeFormatting::class,
        'privilege.product' => \App\Http\Middleware\PrivilegeProduct::class,
        'privilege.fillselects' => \App\Http\Middleware\PrivilegeFillselects::class, 
        'privilege.item' => \App\Http\Middleware\PrivilegeItem::class,
        'privilege.client' => \App\Http\Middleware\PrivilegeClient::class,
        'privilege.logomarca' => \App\Http\Middleware\PrivilegeLogomarca::class,
        //'privilege.contacts' => \App\Http\Middleware\PrivilegeContacts::class,     
        //'privilege.talk' => \App\Http\Middleware\PrivilegeTalk::class,  
        //'privilege.comment' => \App\Http\Middleware\PrivilegeComment::class,    
        //'privilege.itemcl' => \App\Http\Middleware\PrivilegeItemcl::class, 
        'privilege.represented' => \App\Http\Middleware\PrivilegeRepresented::class,
        'privilege.transport' => \App\Http\Middleware\PrivilegeTransport::class,
        'privilege.budget' => \App\Http\Middleware\PrivilegeBudget::class,  
        //'privilege.itenbudget' => \App\Http\Middleware\PrivilegeItensbudget::class,         
        'privilege.reportrecord' => \App\Http\Middleware\PrivilegeReportrecord::class,
        'privilege.reportclient' => \App\Http\Middleware\PrivilegeReportclient::class,
        'privilege.reporttalk' => \App\Http\Middleware\PrivilegeReporttalk::class,
        'privilege.reportitemcl' => \App\Http\Middleware\PrivilegeReportitemcl::class,
        'privilege.reportproduct' => \App\Http\Middleware\PrivilegeReportproduct::class,
        'privilege.reportbudget' => \App\Http\Middleware\PrivilegeReportbudget::class,                                                
        'payplan' => \App\Http\Middleware\PayPlan::class,
        'corspayment' => \App\Http\Middleware\CorsPayment::class, 
    ];
}
