<?php
if(!function_exists('MaskCNPJ')){
	function MaskCNPJ($val, $mask){
		$maskared = '';
		$k = 0;
		for($i = 0; $i<=strlen($mask)-1; $i++){
			if($mask[$i] == '#'){
				if(isset($val[$k]))
					$maskared .= $val[$k++];
			}else{
				if(isset($mask[$i]))
				$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}

	function CleanFields($value){
		 $value = trim($value);
		 $value = str_replace(".", "", $value);
		 $value = str_replace(",", "", $value);
		 $value = str_replace("-", "", $value);
		 $value = str_replace("/", "", $value);
		 return $value;
	}

	function RemoveParenths($value){
		$value = str_replace(array( '(', ')' ), '', $value);
		return $value;
	}
}
