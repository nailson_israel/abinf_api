<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Laravel\Passport\Events\AccessTokenCreated' => [
            'App\Listeners\Log\AccessRegister',
        ],

        'App\Events\NotificationCreateTenant' => [
            'App\Listeners\NotifiesnewTenant',
        ],

        'App\Events\NotificationPayment' => [
            'App\Listeners\Payment\ListenerPayment',
        ],

        'App\Events\NotificationAdhesion' => [
            'App\Listeners\Adhesion\ListenerPlan',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
