<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationPayment
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $code;
    public $statusTransaction;
    public $reference;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($code, $statusTransaction, $reference)
    {
        $this->code = $code;
        $this->statusTransaction = $statusTransaction;
        $this->reference = $reference;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('payment');
    }
}
