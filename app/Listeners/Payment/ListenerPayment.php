<?php

namespace App\Listeners\Payment;

use App\Events\NotificationPayment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\JobUpdatePayPlan;


class ListenerPayment{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NotificationPayment $event)
    {

        $code = $event->code;
        $statusTransaction = $event->statusTransaction;
        $tenantid = $event->reference;

        dispatch(new JobUpdatePayPlan($code, $statusTransaction, $tenantid))->delay(now()->addMinutes(1));
    }
}
