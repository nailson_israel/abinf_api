<?php

namespace App\Listeners\Adhesion;

use App\Events\NotificationAdhesion;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\JobUpdatePlan;


class ListenerPlan{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NotificationAdhesion $event)
    {

        $code = $event->code;
        $status = $event->status;

        dispatch(new JobUpdatePlan($code, $status))->delay(now()->addMinutes(1));
    }
}
