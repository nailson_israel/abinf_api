<?php

namespace App\Listeners\Log;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class AccessRegister
{

    private $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;     
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {   
        $result = $this->user::where('id', $event->userId)->first();
        $tenantid = $result->tenantid;
        $user = $event->userId;
        $ip = $_SERVER["REMOTE_ADDR"];
        $action = 'Acessou o Abinf Pedidos';
        
        $data = [
            'userid' => $user,
            'tenantid' => $tenantid,
            'ip' => $ip,         
            'action' => $action

        ];        

        activity('login')
         ->causedBy($result)
         ->withProperties($data)
         ->log('Login');
    }
}
