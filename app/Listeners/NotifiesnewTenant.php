<?php

namespace App\Listeners;

use App\Events\NotificationCreateTenant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifiesnewTenant implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationCreateTenant  $event
     * @return void
     */
    public function handle(NotificationCreateTenant $event)
    {
        return $event;
    }
}
