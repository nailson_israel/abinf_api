<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Jobs\JobSendPayment;


class PaymentManualPeriodically extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paymentmanual:periodically';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar diariamente ou com período a ser escolhido, para quem será mandado o próximo pagamento com base em seus períodos de pagamentos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        dispatch(new JobSendPayment())->delay(now()->addMinutes(1));

       $this->info('Verificação dos período para enviar pagamentos aos clientes');
    }
}
