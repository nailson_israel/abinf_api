<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\JobVerifiySendAgenda;

class SendAgendaPeriodically extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendagenda:periodically';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar e enviar as agendas que estão marcadas com notificar e para vencer contando com 3 dias de antecedência, depois 2, 1, de 8 a 2 horas, de 2 a 1, de 1 a 0, enviando apenas uma vez o email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new JobVerifiySendAgenda())->delay(now()->addMinutes(1));        
       $this->info('Envia notificação das mensagem marcadas para ser notificada');
    }
}
