<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Plan;
use App\Models\Payment;
use App\Models\User;
use App\Jobs\JobSendPaymentPeriodic;
use App\Jobs\JobSendLastTransactionsnotPay;


class JobSendPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Job execute once per day at 05:00 am 
        $day = date('d');
        $today = date('Y-m-d');

        /*Verifica quais os planos que tem adesão para o dia atual se não, não executa as features necessárias para cobrança ou aviso de pagamento atrasado*/
        $plans = Plan::whereNull('deleted_at')
                /*->whereDay('dateadhesion', $day)*/
                ->where([
                    ['status', '=', 'ACTIVE'],
                ])->get();


        if($plans != '[]'){
            foreach ($plans as $key => $plan) {
                $tenantid =  $plan->tenantid;
                $period = $plan->period;
                $value = $plan->value;
                $codeadhesion = $plan->codeadesion;
                $dataadhesion = $plan->dateadhesion;

                $payment = Payment::whereNull('deleted_at')
                        ->where([
                            ['tenantid', '=', $tenantid]
                        ])
                        ->orderBy('id', 'DESC')
                        ->first();

                /*
                    1 - Aguardando pagamento / registered
                    2 - Em análise / analyzing
                    3 - Paga / Paid
                    4 - Disponível / Paid
                    5 - Em disputa / Dispute
                    6 - Devolvida / Returned
                    7 - Cancelada / Canceled
                */

                $aux = explode('T', $payment['date']);
                $date = $aux[0];
                $hours = isset($aux[1]) ? $aux[1] : ''; 

                $dateLastPayment = date($date);
                $datelast = new \DateTime($dateLastPayment);
                $daytoday = new \DateTime($today);

                $interval = $datelast->diff($daytoday);
                $days = $interval->days;

                if($period == 'monthly'){
                    $daymin = 28;
                    $daymax = 31;
                    $month = 1;

                    $monthsAhead = $interval->m + ($interval->y * 12);
     
                    $timestamp = strtotime($dateLastPayment . "+1 months");
                    $datepayment = date('Y-m-d', $timestamp);

                }else if ($period == 'trimonthly'){
                    $daymin = 88;
                    $daymax = 93;
                    $month = 3;

                    $monthsAhead = $interval->m + ($interval->y * 12);

                    $timestamp = strtotime($dateLastPayment . "+3 months");
                    $datepayment = date('Y-m-d', $timestamp);
                
                }else if($period == 'semiannually'){
                    $daymin = 178;
                    $daymax = 186;
                    $month = 6;

                    $monthsAhead = $interval->m + ($interval->y * 12);

                    $timestamp = strtotime($dateLastPayment . "+6 months");
                    $datepayment = date('Y-m-d', $timestamp);
                }

                if($dateLastPayment <= date('Y-m-d')){
                    if($monthsAhead <= $month){
                        if($today == $datepayment){
                            if($payment->status == '3' || $payment->status == '4'){
                                //echo 'é dia de pagamento';
                                dispatch(new JobSendPaymentPeriodic($tenantid, $value, $period, $codeadhesion))->delay(now()->addMinutes(1));
                            }else{
                                //echo 'é dia de pagamento e a ultima transação não foi paga, tem que pagar';
                                dispatch(new JobSendLastTransactionsnotPay($tenantid))->delay(now()->addMinutes(1));
                            }
                        }else if($datepayment == ''){
                            //echo 'quanto o plano ainda não tem fatura, pode nunca acontecer';
                        }else{
                            //echo 'não é dia de cobrança';
                        }
                    }else{
                        //echo 'atrasado';
                        dispatch(new JobSendLastTransactionsnotPay($tenantid));
                    }
                }else{
                    //echo 'data de pagamento não bate com a data atual';
                }
            }
        }else{
            //echo 'não acha plano de acordo com os parametros passados';
        }
    }
}
