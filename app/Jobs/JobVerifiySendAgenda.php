<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Jobs\JobSendEmailAgenda;

use App\Models\Talk;
use App\Models\Sendagenda;


class JobVerifiySendAgenda implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $month = date('m');
        $agendas = Talk::select(
            'talks.id as id', 'tenantid', 'date as dateag', 'hour', 'userid'
        )->whereMonth('date', $month)->where([
            ['isactivity', '=', '0'],
            ['notification', '=', '1'],
            ['notification_complet', '<>', '1']
        ])
        // talvez fazer leftjoin do contato do cliente
        ->get();

        foreach ($agendas as $agd) {

            $id = $agd->id;
            $tenant = $agd->tenantid;
            $dateag = $agd->dateag . ' ' . $agd->hour;
            $userid = $agd->userid;

            $today = date('Y-m-d H:i');
            $yearactual = date('Y');
            $dateagd = date($dateag);
            $yearag = date('Y', strtotime($dateag));

            $data1 = new \DateTime($dateagd);
            $data2 = new \DateTime($today);

            $interval = $data1->diff($data2);

            $verifyagenda = Sendagenda::where(['agendaid' =>  $id])->first();

            if(!$verifyagenda){
                $create = Sendagenda::create(['agendaid' => $id]);
            }

            $hoursend = date('06:00');
            $hourcurrent = date('H:i');
            if($yearactual === $yearag){
                if($interval->d == 3){
            
                    $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                    ->where('threedays', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
                    if(!isset($sendagenda)){
                        $moment = '3days';
                        if($hoursend == $hourcurrent){
                            dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                            $update = Sendagenda::where('agendaid', $id)->update(['threedays' => 1]);
                        }
                    }

                }else if($interval->d == 2){
                    $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                    ->where('twodays', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

                    if(!isset($sendagenda)){
                        $moment = '2days';
                        if($hoursend == $hourcurrent){
                            dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                            $update = Sendagenda::where('agendaid', $id)->update(['twodays' => 1]);
                            //dispara job para enviar o aviso por email
                        }
                    }       
                }else if($interval->d == 1){
                    $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                    ->where('oneday', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

                    if(!isset($sendagenda)){
                        $moment = '1day';
                        if($hoursend == $hourcurrent){
                            dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                            $update = Sendagenda::where('agendaid', $id)->update(['oneday' => 1]);
                            //dispara job para enviar o aviso por email
                        }
                    }
                    //um job que execute ao dia
                }else if($interval->d == 0){
                    $entrada = strtotime(date('H:i'));
                    $saida   = strtotime($agd->hour);
                    $diferenca = $saida - $entrada;
                    $missinghours = sprintf( '%d:%d', $diferenca/3600, $diferenca/60%60);

                    $eight = '8:00';
                    $tow = '2:00';
                    $one = '1:00';
                    $minutes = '0:30';
                    $finish = '0:00';

                    if(strtotime($missinghours) > strtotime($finish)){
                        if(strtotime($missinghours) >= strtotime($tow)){
                            if(strtotime($missinghours) <= strtotime($eight)){
                                $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                                ->where('interval8hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

                                if(!isset($sendagenda)){
                                    $moment = '8hours';
                                    dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                                    $update = Sendagenda::where('agendaid', $id)->update(['interval8hours' => 1]);
                                }
                            }
                        }
                        if(strtotime($missinghours) >= strtotime($one)){
                            if(strtotime($missinghours) <= strtotime($tow)){
                                $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                                ->where('interval2hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
                                $moment = '2hours';
                                if(!isset($sendagenda)){
                                    dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                                    $update = Sendagenda::where('agendaid', $id)->update(['interval2hours' => 1]);
                                }                               
                            }               
                        }

                        /*este é o valor que o missinghours buga 12:50*
                            a cima de 9:50 ele dá esse problema
                        /

                        /*verificar isso aqui, é a unica que ferra quando tem hora igual 17:00 = 5:00*/

                        if(strtotime($missinghours) >= strtotime($minutes)){
                            if(strtotime($missinghours) <= strtotime($one)){
                                $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                                ->where('interval1hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
                                if(!isset($sendagenda)){
                                    $moment = '1hour';
                                    dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                                    $update = Sendagenda::where('agendaid', $id)->update(['interval1hours' => 1]);
                                }   
                            }
                        }


                        if(strtotime($missinghours) >= strtotime($finish)){
                            if(strtotime($missinghours) <= strtotime($minutes)){
                                $sendagenda = Sendagenda::where(['agendaid' =>  $id])
                                ->where('interval0hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

                                if(!isset($sendagenda)){
                                    $moment = '0hour';
                                    dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                                    $update = Sendagenda::where('agendaid', $id)->update(['interval0hours' => 1]);
                                }
                            }
                        }
                    }else{
                        if($missinghours === '0:-1'){
                                                                                            echo 'asdsa';
                            $moment = '-0hour';
                            dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
                        }
                    }

                    echo '<br><br>';
                }else{
                    echo 'nem chego a entrar';
                }
            }
        }
    }
}
