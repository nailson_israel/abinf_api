<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Traits\Pagseguro;

use App\Models\Payment;

class JobSendPaymentPeriodic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Pagseguro;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $tenantid;
    protected $value;
    protected $period;
    protected $codeadhesion;

    public function __construct($tenantid, $value, $period, $codeadhesion)
    {
        $this->tenantid = $tenantid;
        $this->value = $value;
        $this->period = $period;
        $this->codeadhesion = $codeadhesion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        
        $url = $this->Uri() . "pre-approvals/payment?email=".$this->Auth()."&token=".$this->Token();

        $preaproval = $this->codeadhesion;
        $senderip = \Request::ip();
        $reference = 'T_'. $this->tenantid;
        $itemid1 = 1;
        $itemdescription1 = 'Saas Software Abinf Sales ' . $this->period;
        $itemamount1 = $this->value;
        $itemQuantity1 = 1;

        $data = "preApprovalCode=".$preaproval."&senderIp=".$senderip."&reference=".$reference."&itemId1=1&itemDescription1=".$itemdescription1."&itemAmount1=".$itemamount1."&itemQuantity1=1";
        $dt = $data;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( $this->ContenttypeTwo(), $this->Accept() ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
 
        $response = curl_exec($curl);
        $http = curl_getinfo($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          $rtn = "cURL Error #:" . $err;
          return response()->json(['ERRO' => $rtn]);
        }else{
            $rtn = $response;

            if($rtn === 'Internal Server Error'){
                $resp = response()->json(['ERRO' => 'Seu pagamento não foi realizado. Está em analise ou aguardando pagamento']);
            }else{
                $x = strpos($rtn, 'bloqueado');
                
                if($x > 0){
                    return response()->json(['ERRO' => 'Uol bloqueou pelo numero de requisições'], 422);                    
                }else{
                    $xml = simplexml_load_string($rtn);
                    $json = json_encode($xml);

                    $decode = json_decode($json, true);


                    $decode = json_decode($json, true);

                    if($decode['error']){
                        Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail('Assinatura cancelada na entrada', $decode));
                        //return response()->json(['ERRO' => 'Não existe essa assinatura ou ela foi cancelada']);
                    }else{
                        $data = [
                            'tenantid' => $this->tenantid,
                            'transactioncode' => $decode['transactionCode'],
                            'date' => $decode['date'],
                            'value' => $this->value,
                            'ip' => \Request::ip(),
                            'status' => 1
                        ];


                        Mail::to('nailson.ivs@codens.com.br')->send(new sendLogsEmail($dt, $json));

                        $payment = Payment::create($data);
                    }
                }
            }
        }

    }
}
