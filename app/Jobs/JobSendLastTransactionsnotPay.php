<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Mail;
use App\Mail\LastTransctionnotPay;

use App\Models\User;

class JobSendLastTransactionsnotPay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $tenantid;

    public function __construct($tenantid)
    {
        $this->tenantid = $tenantid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $user_responsible = User::where(['tenantid' => $this->tenantid, 'responsible' => 1])->whereNull('deleted_at')->first();

        $email = $user_responsible->email;

        Mail::to('nailson.ivs@gmail.com.br')->send(new LastTransctionnotPay($user_responsible));
    }
}
