<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Mail;
use App\Mail\ActiveEmail;
use App\Mail\PaidPaymentEmail;
use App\Mail\PaymentMethodChange;
use App\Mail\Suspended;
use App\Mail\Canceled;
use App\Mail\CanceledReceiver;

use App\Models\Plan;
use App\Models\User;

class JobUpdatePlan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $code;
    protected $status;

    public function __construct($code, $status)
    {
        $this->code = $code;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /*
        INITIATED
        PENDING
        ACTIVE
        PAYMENT_METHOD_CHANGE
        SUSPENDED
        CANCELLED
        CANCELLED_BY_RECEIVER
        CANCELLED_BY_SENDER
        EXPIRED

        */

        $plan = Plan::where('codeadesion', $this->code)->whereNull('deleted_at')->first();

        $user_responsible = User::where(['tenantid' => $plan->tenantid, 'responsible' => 1])->whereNull('deleted_at')->first();

        $email = $user_responsible->email;
        if($this->status == 'ACTIVE'){
            Mail::to($email)->send(new ActiveEmail($user_responsible));
        }
        if($this->status == 'PAYMENT_METHOD_CHANGE'){
            Mail::to($email)->send(new PaymentMethodChange($user_responsible));
        }else if($this->status == 'SUSPENDED'){
            Mail::to($email)->send(new Suspended($user_responsible));
        }else if($this->status == 'CANCELLED'){
            //Mail::to($email)->send(new Canceled($user_responsible));
        }else if($this->status == 'CANCELLED_BY_RECEIVER'){
            //Mail::to($email)->send(new CanceledReceiver($user_responsible));
        }

        $plan = Plan::where(['codeadesion' => $this->code, 'tenantid' => $plan->tenantid])->whereNull('deleted_at')->update(['status' => $this->status]);
    }
}
