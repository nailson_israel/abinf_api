<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Mail;
use App\Mail\PaidPaymentEmail;
use App\Mail\AnalyzingPaymentEmail;
use App\Mail\CanceledPaymentEmail;
use App\Mail\ReturnedPaymentEmail;
use App\Mail\DisputePaymentEmail;
use App\Mail\RegisteredPaymentEmail;

use App\Models\Plan;
use App\Models\Payment;
use App\Models\User;

class JobUpdatePayPlan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $code;
    protected $statusTransaction;
    protected $tenantid;

    public function __construct($code, $statusTransaction, $tenantid)
    {
        $this->code = $code;
        $this->statusTransaction = $statusTransaction;
        $this->tenantid = $tenantid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /*
            1 - Aguardando pagamento / registered
            2 - Em análise / analyzing
            3 - Paga / Paid
            4 - Disponível / Paid
            5 - Em disputa / Dispute
            6 - Devolvida / Returned
            7 - Cancelada / Canceled
        */

        $user_responsible = User::where(['tenantid' => $this->tenantid, 'responsible' => 1])->whereNull('deleted_at')->first();

        $email = $user_responsible->email;
        
        if($this->statusTransaction == 4 || $this->statusTransaction == 3){
            Mail::to($email)->send(new PaidPaymentEmail($user_responsible));

        }else if($this->statusTransaction == 1){
            //Mail::to($email)->send(new RegisteredPaymentEmail($user_responsible));
            //Notification email in register user
        }else if($this->statusTransaction == 2){
            Mail::to($email)->send(new AnalyzingPaymentEmail($user_responsible));

        }else if($this->statusTransaction == 5){
            //Mail::to($email)->send(new DisputePaymentEmail($user_responsible));

        }else if ($this->statusTransaction == 6){
            //Mail::to($email)->send(new ReturnedPaymentEmail($user_responsible));

        }else if ($this->statusTransaction == 7){
            Mail::to($email)->send(new CanceledPaymentEmail($user_responsible));
        }

        $payment = Payment::where(['tenantid' => $this->tenantid, 'transactioncode' => $this->code])->whereNull('deleted_at')->update(['status' => $this->statusTransaction]);
    }
}
