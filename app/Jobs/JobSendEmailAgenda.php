<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationAgenda;

use App\Models\User;
use App\Models\Talk;

class JobSendEmailAgenda implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $tenantid;
    protected $id;
    protected $iduser;
    protected $moment;

    public function __construct($tenantid, $id, $iduser, $moment)
    {
        $this->tenantid = $tenantid;
        $this->id = $id;
        $this->iduser = $iduser;
        $this->moment = $moment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where(['tenantid' => $this->tenantid, 'id' => $this->iduser])->whereNull('deleted_at')->first();
        $email = $user->email;

        $agenda = Talk::select(
            'talks.tenantid as tnt', 'name', 'users.email as uemail', 
            'subject', 'date as dateag', 'hour', 'represented', 
            'description', 'type', 'descriptionlegend', 'client', 'reference', 'cod', 'clients.phone1 as cphone1', 'clients.phone2 as cphone2', 'cep', 'street', 'neighborhood', 'complements', 'number', 'city', 'uf', 'clients.email as cemail',
            'contact', 'contacts.departament as departament', 'contacts.phone as ctphone', 'contacts.phone2 as ctphone2', 'contacts.email as cmail'
        )->where(['talks.id' => $this->id])
        ->leftJoin('users', 'users.id', '=', 'talks.userid')
        ->leftJoin('legends', 'legends.legends', '=', 'talks.type')
        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
        ->first();

        $subject = '';
        $titleemail = '';
        $complement = '';

        if($this->moment == '3days'){
            $subject = 'Você tem uma agenda daqui 3 dias!';
            $titleemail = ' Você tem agenda em 3 dias.';
            $complement = 'para daqui 3 dias'; 
        }else if ($this->moment == '2days'){
            $subject = 'Você tem uma agenda daqui 2 dias!';
            $titleemail = ' Você tem agenda em 2 dias.';
            $complement = 'para daqui 2 dias'; 
        }else if($this->moment == '1day'){
            $subject = 'Você tem uma agenda daqui 1 dia!';
            $titleemail = ' Você tem agenda em 1 dia.';
            $complement = 'para daqui 1 dia';
        }else if($this->moment == '8hours'){
            $subject = 'Você tem uma agenda daqui algumas horas!';
            $titleemail = ' Você tem agenda daqui algumas horas';
            $complement = ' daqui algumas horas, mas fique tranquilo, ainda falta bastante tempo';            
        }else if($this->moment == '2hours'){
            $subject = 'Você tem uma agenda em menos de 2 horas!';
            $titleemail = ' Você tem agenda em menos de 2 horas';
            $complement = ' em menos de 2 horas';            
        }else if($this->moment == '1hour'){
            $subject = 'Você tem uma agenda em menos de 1 hora!';
            $titleemail = ' Você tem agenda em menos de 1 hora';
            $complement = ' em menos de 1 hora, se prepare';            
        }else if($this->moment == '0hour'){
            $subject = 'Você tem uma agenda em alguns minutos!';
            $titleemail = ' Você tem agenda em alguns minutos, se prepare para cumprir sua agenda';
            $complement = ' em alguns minutos';
        }else if($this->moment == '-0hour'){
            $subject = 'Chegou a hora da sua agenda!';
            $titleemail = ' Você tem agenda neste exato momento.';
            $complement = ' neste exato momento';
        }

        Mail::to($email)->send(new NotificationAgenda($user, $agenda, $subject, $titleemail, $complement));
    }
}
