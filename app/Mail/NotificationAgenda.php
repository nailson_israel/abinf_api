<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class NotificationAgenda extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $agenda, $subject, $titleemail, $complement)
    {
        $this->user = $user;
        $this->agenda = $agenda;
        $this->subject = $subject;
        $this->titleemail = $titleemail;
        $this->complement = $complement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notificationagenda')
        //subject personalizado
        ->subject('[#Abinf Sales] ' . $this->subject)
        ->with([
          'user' => $this->user,
          'agenda' => $this->agenda,
          'titleemail' => $this->titleemail,
          'complement' => $this->complement
        ]);
    }
}
