<?php
Route::group(['middleware' => ['payplan']], function () {	
	Route::middleware('auth:api')->prefix('/transports')->group(function (){
		Route::prefix('transport')->group(function (){
			Route::get('transport', 'Api\Client\TransportController@transport')->name('transport');
			Route::get('transport/{id}', 'Api\Client\TransportController@transportid')->name('transportid');
			Route::get('/transportselectsbox', 'Api\Client\TransportController@transportselectsbox')->name('transportselectsbox');
			Route::post('addtransport', 'Api\Client\TransportController@addtransport')->name('addtransport');
			Route::put('uptransport/{id}', 'Api\Client\TransportController@uptransport')->name('uptransport');
			Route::delete('deltransport/{id}', 'Api\Client\TransportController@deltransport')->name('deltransport');
			Route::post('delalltransport', 'Api\Client\TransportController@delalltransport')->name('delalltransport');
		});
	});
});
