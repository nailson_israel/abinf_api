<?php
Route::middleware('auth:api')->prefix('/report')->group(function (){
	Route::group(['middleware' => ['payplan']], function () {	
		//Record client
		Route::post('/recordclient', 'Api\Reports\ReportrecordController@recordclient')->name('reportrecord');

		Route::post('/reportclient', 'Api\Reports\ReportclientController@reportclient')->name('reportclient');
		Route::prefix('/reportclient')->group(function (){
			//observações
			Route::get('/comment/{idclient}', 'Api\Reports\ReportclientController@comment');
			//contacts
			Route::get('/contact/{idclient}', 'Api\Reports\ReportclientController@contact');
			//itencl
			Route::get('/itencl/{idclient}', 'Api\Reports\ReportclientController@itemcl');
			//emails all system
			Route::get('/emails', 'Api\Reports\ReportclientController@emails');
			Route::post('/graphic1', 'Api\Reports\ReportclientController@reportclientGraphic1')->name('reportgraphic1');
			Route::post('/graphic2', 'Api\Reports\ReportclientController@reportclientGraphic2')->name('reportgraphic2');
		});

		Route::post('/reporttalk', 'Api\Reports\ReporttalkController@reporttalk')->name('reporttalk');
		Route::post('/reporttalk/graphic1', 'Api\Reports\ReporttalkController@reporttalkGraphic1')->name('reportgraphictalk1');
		Route::post('/reporttalk/graphic2', 'Api\Reports\ReporttalkController@reporttalkGraphic2')->name('reportgraphictalk2');

		Route::post('/reportitemcl', 'Api\Reports\ReportitemclController@reportitemcl')->name('reportitemcl');
		Route::post('/reportproduct', 'Api\Reports\ReportproductController@reportproduct')->name('reportproduct');

		Route::post('/reportbudget', 'Api\Reports\ReportbudgetController@reportbudget')->name('reportbudget');
		Route::prefix('/reportbudget')->group(function (){
			Route::get('/itenbudget/{idbudget}/{dados?}/{nf?}/{product?}', 'Api\Reports\ReportbudgetController@itenbudget');
		});

		Route::prefix('/reportsales')->group(function (){
			Route::prefix('/revenues')->group(function () {			
				Route::post('/billing', 'Api\Reports\ReportbudgetController@billing')->name('billing');
			});

			Route::prefix('/sales')->group(function () {
				Route::post('/graphisales1', 'Api\Reports\ReportbudgetController@reportsales1')->name('reportgraphicsales1');
				Route::post('/reportvaluessales', 'Api\Reports\ReportbudgetController@reportvaluessales')->name('reportvaluessales');
			});

			Route::prefix('/commission')->group(function (){
				Route::post('/graphiccomission1', 'Api\Reports\ReportbudgetController@reportcommission1')->name('reportgraphiccommission1');
			});
		});
	});
});