<?php
Route::group(['middleware' => ['payplan']], function () {	
	Route::middleware('auth:api')->prefix('/notification')->group( function(){
		Route::get('/notification', 'Api\NotificationController@notification')->name('notification');
		Route::post('/addnotification', 'Api\NotificationController@addnotification')->name('addnotification');
		Route::put('/upnotification/{id}', 'Api\NotificationController@upnotification')->name('upnotification');
		Route::delete('/delnotification/{id}', 'Api\NotificationController@delnotification')->name('delnotification');
	});
});