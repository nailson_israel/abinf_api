<?php
Route::middleware('auth:api')->prefix('/uslog')->group(function (){
	Route::get('/me', 'Api\userLoggedController@me')->name('me');
	Route::get('/isadmin', 'Api\userLoggedController@isadmin')->name('isadmin');
	Route::get('/groupus', 'Api\userLoggedController@groupus')->name('groupus');
	Route::get('/myuser', 'Api\userLoggedController@myuser')->name('myuser');
	Route::get('/myname', 'Api\userLoggedController@myname')->name('me');
    Route::post('/verifypassowrduser/{id}', 'Api\userLoggedController@verifypassowrduser')->name('verifypassowrduser');
    Route::put('/updateprofile/{id}', 'Api\userLoggedController@updateprofile')->name('updateprofile');

	Route::get('/freeperiod', 'Api\userLoggedController@freeperiod')->name('freeperiod');
	Route::get('/responsible', 'Api\userLoggedController@responsible')->name('responsible');

	Route::post('/savewelcome', 'Api\userLoggedController@savewelcome')->name('savewelcome');
});