<?php
Route::group(['middleware' => ['payplan']], function () {	
	Route::middleware('auth:api')->prefix('/logs')->group(function (){
		Route::prefix('access')->group(function () {
			Route::get('accessregister', 'Api\Log\AccessController@accessregister')->name('accessregister');
		});
	});
});
