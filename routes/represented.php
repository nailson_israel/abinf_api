<?php
Route::group(['middleware' => ['payplan']], function () {	
	Route::middleware('auth:api')->prefix('/representeds')->group(function (){
		Route::prefix('represented')->group(function (){
			//represented
			Route::get('representedselectbox', 'Api\Client\RepresentedController@representedselectbox');		
			Route::get('representedcl/{id}', 'Api\Client\RepresentedController@representedcl')->name('representedcl');
			Route::get('representedclient/{id}', 'Api\Client\RepresentedController@representedclient')->name('representedclient');
			Route::get('represented', 'Api\Client\RepresentedController@represented')->name('represented');
			Route::get('represented/{id}', 'Api\Client\RepresentedController@representedid')->name('representedid');
			Route::post('addrepresented', 'Api\Client\RepresentedController@addrepresented')->name('addrepresented');
			Route::put('uprepresented/{id}', 'Api\Client\RepresentedController@uprepresented')->name('uprepresented');
			Route::delete('delrepresented/{id}', 'Api\Client\RepresentedController@delrepresented')->name('delrepresented');
			Route::post('delallrepresented', 'Api\Client\RepresentedController@delallrepresented')->name('delallrepresented');

			Route::get('productrepresented/{idrep}', 'Api\Client\RepresentedController@productrepresented')->name('productrepresented');

			Route::get('logorepresented/{idrep}', 'Api\Client\RepresentedController@logorepresented')->name('logorepresented');
			Route::put('updatelogorepresented/{idrep}', 'Api\Client\RepresentedController@updatelogorepresented')->name('updatelogorepresented');
			
		    //textfix
			Route::get('textfix/{id}', 'Api\Client\RepresentedController@textfix')->name('textfix');
			Route::post('addtextfix', 'Api\Client\RepresentedController@addtextfix')->name('addtextfix');
			Route::put('uptextfix/{id}', 'Api\Client\RepresentedController@uptextfix')->name('uptextfix');
			Route::delete('deltextfix/{id}', 'Api\Client\RepresentedController@deltextfix')->name('deltextfix');
		});
	});
});