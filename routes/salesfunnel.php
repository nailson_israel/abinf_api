<?php
Route::middleware('auth:api')->prefix('/salesfunnel')->group(function (){
	Route::post('/addfunnel', 'Api\Salesfunnel\SalesfunnelController@addfunnel')->name('addfunnelnew');
	Route::get('/funnels', 'Api\Salesfunnel\SalesfunnelController@funnels')->name('funnels');
	Route::get('/columnsfunnel/{id}', 'Api\Salesfunnel\SalesfunnelController@columnsfunnel')->name('columnsfunnel');
	Route::get('/funnel/{id}', 'Api\Salesfunnel\SalesfunnelController@funnel')->name('funnel');
});