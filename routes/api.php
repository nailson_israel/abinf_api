<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('oauth/token', 'Auth\AuthApiController@auth');
Route::middleware('auth:api')->post('logoutapi', 'Api\LogoutapiController@logoutapi');

Route::post('register', 'Auth\RegisterController@register');

require('client.php'); // clients, talks(activity), contact, comment, logoclient, itenscl, 
require('represented.php');
require('transport.php');
require('product.php');
require('budget.php'); //budgets and itens budget
require('item.php');
require('users.php'); //Users and Groups of users
require('config.php');
require('datalogged.php'); //User logged in momment
require('report.php');
require('notification.php');
require('route.php');
require('log.php');
require('pagseguro.php');
require('plan.php');
require('salesfunnel.php');

//mexer nisso aqui amanhã, por o middler por rota...
Route::group(['middleware' => ['payplan']], function () {	
});

