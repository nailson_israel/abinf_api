<?php
Route::middleware('auth:api')->prefix('/configs')->group(function (){
	Route::group(['middleware' => ['payplan']], function () {	
		//financer
		Route::prefix('/formatting')->group(function (){
			Route::get('/format', 'Api\Formatting\FormattingController@format')->name('format');
			Route::post('/addformat', 'Api\Formatting\FormattingController@addformat')->name('addformat');
			Route::put('/upformat/{id}', 'Api\Formatting\FormattingController@upformat')->name('upformat');
			Route::delete('/delformat/{id}', 'Api\Formatting\FormattingController@delformat')->name('delformat');	
		});
		//financer
		Route::prefix('/financer')->group(function (){
			Route::get('/financer', 'Api\Company\ConfigfinancerController@financer')->name('financer');
			Route::post('/addfinancer', 'Api\Company\ConfigfinancerController@addfinancer')->name('addfinancer');
			Route::put('/upfinancer/{id}', 'Api\Company\ConfigfinancerController@upfinancer')->name('upfinancer');
			Route::delete('/delfinancer/{id}', 'Api\Company\ConfigfinancerController@delfinancer')->name('delfinancer');	
		});		
		//config_email(Configuração de email)
		Route::prefix('/configmail')->group(function (){
			Route::get('/configmail', 'Api\Company\ConfigmailController@configmail');
			Route::post('/addconfigmail', 'Api\Company\ConfigmailController@addconfigmmail')->name('addconfigmail');
			Route::put('/upconfigmail/{id}', 'Api\Company\ConfigmailController@upconfigmail')->name('upconfigmail');
			Route::delete('/delconfigmail/{id}', 'Api\Company\ConfigmailController@delconfigmail')->name('delconfigmail');	
		});	
	});

	//company
	Route::prefix('/company')->group(function (){
		Route::get('/company', 'Api\Company\CompanyController@company');
		Route::post('/addcompany', 'Api\Company\CompanyController@addcompany')->name('addcompany');
		Route::put('/upcompany/{id}', 'Api\Company\CompanyController@upcompany')->name('upcompany');
		Route::delete('/delcompany/{id}', 'Api\Company\CompanyController@delcompany')->name('delcompany');
		//imagecompany
		Route::get('/imagecompany', 'Api\Company\ImgcompanyController@imagecompany');
		Route::post('/addimagecompany', 'Api\Company\ImgcompanyController@addimagecompany')->name('addimagecompany');
		Route::post('/upimagecompany/{id}', 'Api\Company\ImgcompanyController@upimagecompany')->name('upimagecompany');
		Route::delete('/delimagecompany/{id}', 'Api\Company\ImgcompanyController@delimagecompany')->name('delimagecompany');
		//Api for save in db the name image
		Route::get('/nameimagecompany', 'Api\Company\ImgcompanyController@nameimagecompany')->name('nameimagecompany');
		Route::post('/addnameimagecompany', 'Api\Company\ImgcompanyController@addnameimagecompany')->name('addnameimagecompany');
		Route::put('/upnameimagecompany/{id}', 'Api\Company\ImgcompanyController@upnameimagecompany')->name('upnameimagecompany');
		Route::delete('/delnameimagecompany/{id}', 'Api\Company\ImgcompanyController@delnameimagecompany')->name('delnameimagecompany/{id}');
	});
	//formatt text
	Route::prefix('/formattext')->group(function (){
		Route::get('/formattext', 'Api\Formatting\FormattingController@formattext');
		Route::put('/upformattext/{id}', 'Api\Formatting\FormattingController@upformattext')->name('upformattext');
	});
});