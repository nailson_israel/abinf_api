<?php
Route::middleware('auth:api')->prefix('/clients')->group(function (){
	Route::prefix('/client')->group(function (){
		Route::get('/clientselectbox', 'Api\Client\ClientController@clientselectbox')->name('clientselectbox');
	});

	Route::group(['middleware' => ['payplan']], function () {
		//item cliente
		Route::prefix('itemcl')->group(function (){
			Route::get('/itemcl/{idclient}', 'Api\Client\Itemcl\ItemclController@itemcl')->name('itemcl');
			Route::post('/additemcl', 'Api\Client\Itemcl\ItemclController@additemcl')->name('additemcl');
			Route::post('/additemclid/{idclient}', 'Api\Client\Itemcl\ItemclController@additemclid')->name('additemclid');
			Route::put('/upitemcl/{id}', 'Api\Client\Itemcl\Itemcl\ItemclController@upitemcl')->name('upitemcl');
			Route::delete('/delitemcl/{id}', 'Api\Client\Itemcl\Itemcl\ItemclController@delitemcl')->name('delitemcl');
		});
		//comment
		Route::prefix('comment')->group(function(){
			Route::get('/comment/{idclient}', 'Api\Client\Comment\CommentController@comment')->name('comment');
			Route::post('/addcomment', 'Api\Client\Comment\CommentController@addcomment')->name('addcomment');
			Route::post('/addcommentid/{idclient}', 'Api\Client\Comment\CommentController@addcommentid')->name('addcommentid');
			Route::put('/upcomment/{id}', 'Api\Client\Comment\CommentController@upcomment')->name('upcomment');
			Route::delete('/delcomment/{id}', 'Api\Client\Comment\CommentController@delcomment')->name('delcomment');
			Route::post('/delallcomment', 'Api\Client\Comment\CommentController@delallcomment')->name('delallcomment');
		});
		//talk
		Route::prefix('talk')->group(function (){
			Route::get('/talk/{id}', 'Api\Client\Talk\TalkController@talk')->name('talk');
			Route::post('/addtalk', 'Api\Client\Talk\TalkController@addtalk')->name('addtalk');
			Route::post('/addtalkid/{idclient}', 'Api\Client\Talk\TalkController@addtalkid')->name('addtalkid');
			Route::put('/uptalk/{id}', 'Api\Client\Talk\TalkController@uptalk')->name('uptalk');
			Route::delete('/deltalk/{id}', 'Api\Client\Talk\TalkController@deltalk')->name('deltalk');
			Route::post('/delalltalk', 'Api\Client\Talk\TalkController@delalltalk')->name('delalltalk');

			Route::put('/upstatus/{id}', 'Api\Client\Talk\TalkController@upstatus')->name('upstatus');

			Route::get('/activity/{id}', 'Api\Client\Talk\TalkController@activity')->name('activity');
			Route::get('/agenda/{id}', 'Api\Client\Talk\TalkController@agenda')->name('agenda');

			Route::get('/activitypanel/{id}', 'Api\Client\Talk\TalkController@activitypanel')->name('activitypanel');
			Route::get('/agendapanel/{id}', 'Api\Client\Talk\TalkController@agendapanel')->name('agendapanel');

			Route::get('/activityall', 'Api\Client\Talk\TalkController@activityall')->name('activityall');
			Route::get('/agendaall', 'Api\Client\Talk\TalkController@agendaall')->name('agendaall');
		});
		//contacts
		Route::prefix('contact')->group(function (){
			Route::get('/contactclient/{idclient}', 'Api\Client\Contact\ContactController@contactidclient')->name('contactidclient');
			Route::post('/addcontat', 'Api\Client\Contact\ContactController@addcontact')->name('addcontact');
			Route::post('/addcontactid/{idclient}', 'Api\Client\Contact\ContactController@addcontactid')->name('addcontactid');
			Route::put('/upcontact/{id}', 'Api\Client\Contact\ContactController@upcontact')->name('upcontact');
			Route::delete('/delcontact/{id}', 'Api\Client\Contact\ContactController@delcontact')->name('delcontact');
			Route::post('/delallcontact', 'Api\Client\Contact\ContactController@delallcontact')->name('delallcontact');
			//off
			Route::get('/contact/{id}', 'Api\Client\Contact\ContactController@contact')->name('contactid');
			Route::get('/contactselectbox/{id}', 'Api\Client\Contact\ContactController@contactselectbox')->name('contactselectbox');
		});

		//logo marca cliente
		/*Route::prefix('/logomarca')->group(function (){
			Route::get('/logomarca/{id}', 'Api\Client\Logoclient\LogomarcaController@logomarca')->name('logomarca');
			Route::post('/addlogomarca', 'Api\Client\Logoclient\LogomarcaController@addlogomarca')->name('addlogomarca');
			Route::post('/uplogomarca/{id}', 'Api\Client\Logoclient\LogomarcaController@uplogomarca')->name('uplogomarca');
		});	*/

		//client
		Route::prefix('/client')->group(function (){	
			Route::get('/client', 'Api\Client\ClientController@client')->name('client');
			Route::get('/client/{id}', 'Api\Client\ClientController@clientid')->name('clientid');		
			Route::post('/addclient', 'Api\Client\ClientController@addclient')->name('addclient');
			Route::put('/upclient/{id}', 'Api\Client\ClientController@upclient')->name('upclient');
			Route::delete('/delclient/{id}', 'Api\Client\ClientController@delclient')->name('delclient');
			Route::post('/delallclient', 'Api\Client\ClientController@delallclient')->name('delallclient');
			//get client for selectsbox
			Route::get('/nextnumber', 'Api\Client\ClientController@nextnumber')->name('nextnumber');

			Route::get('/totalclientmonth', 'Api\Client\ClientController@totalclientmonth')->name('totalclientmonth');
			Route::get('/clientsmonth', 'Api\Client\ClientController@clientsmonth')->name('clientsmonth');
			Route::get('/prospects', 'Api\Client\ClientController@prospects')->name('prospects');
			Route::get('/others', 'Api\Client\ClientController@others')->name('others');
			
		});	
		//itemcl
		Route::prefix('/itemcl')->group(function (){
			Route::get('/itemcl', 'Api\Client\Itemcl\ItemclController@itemcl')->name('itemcl');
			Route::post('/additemcl', 'Api\Client\Itemcl\ItemclController@additemcl')->name('additemcl');
			Route::put('/upitemcl/{id}', 'Api\Client\Itemcl\ItemclController@upitemcl')->name('upitemcl');
			Route::delete('/delitemcl/{id}', 'Api\Client\Itemcl\ItemclController@delitemcl')->name('delitemcl');
			Route::post('/delallitemcl', 'Api\Client\Itemcl\ItemclController@delallitemcl')->name('delallitemcl');
		});

		//*FILL SELECTS GROUP*//
		//routeclient	
		Route::prefix('/routeclient')->group(function (){
			Route::get('/routeclient', 'Api\Client\Fillselects\RouteclientController@routeclient')->name('routeclient');
			Route::post('/addrouteclient', 'Api\Client\Fillselects\RouteclientController@addrouteclient')->name('addrouteclient');
			Route::put('/uprouteclient/{id}', 'Api\Client\Fillselects\RouteclientController@uprouteclient')->name('uprouteclient');
			Route::delete('/delrouteclient/{id}', 'Api\Client\Fillselects\RouteclientController@delrouteclient')->name('delrouteclient');
			//get routeclient for selects box
			Route::get('/routeclientselectsbox', 'Api\Client\Fillselects\RouteclientController@routeclientselectsbox');
			
		});
		//groupclient	
		Route::prefix('/groupclient')->group(function (){
			Route::get('/groupclient', 'Api\Client\Fillselects\GroupclientController@groupclient')->name('groupclient');
			Route::post('/addgroupclient', 'Api\Client\Fillselects\GroupclientController@addgroupclient')->name('addgroupclient');
			Route::put('/upgroupclient/{id}', 'Api\Client\Fillselects\GroupclientController@upgroupclient')->name('upgroupclient');
			Route::delete('/delgroupclient/{id}', 'Api\Client\Fillselects\GroupclientController@delgroupclient')->name('delgroupclient');
			//get groupclient for seleects box
			Route::get('/groupclientselectsbox', 'Api\Client\Fillselects\GroupclientController@groupclientselectsbox');

		});
		//branch client (rotas)
		Route::prefix('/branchclient')->group(function (){
			Route::get('/branchclient', 'Api\Client\Fillselects\BranchclientController@branchclient')->name('branchclient');
			Route::post('/addbranchclient', 'Api\Client\Fillselects\BranchclientController@addbranchclient')->name('addbranchclient');
			Route::put('/upbranchclient/{id}', 'Api\Client\Fillselects\BranchclientController@upbranchclient')->name('upbranchclient');
			Route::delete('/delbranchclient/{id}', 'Api\Client\Fillselects\BranchclientController@delbranchclient')->name('delbranchclient');
			//get branchclient for selects box
			Route::get('/branchclientselectsbox', 'Api\Client\Fillselects\BranchclientController@branchclientselectsbox');		
		});
		//departament contacts client
		Route::prefix('/departamentcontact')->group(function (){
			Route::get('/departamentcontact', 'Api\Client\Fillselects\DepartamentcontactController@departamentcontact')->name('departamentcontact');
			Route::post('/adddepartamentcontact', 'Api\Client\Fillselects\DepartamentcontactController@adddepartamentcontact')->name('adddepartamentcontact');
			Route::put('/updepartamentcontact/{id}', 'Api\Client\Fillselects\DepartamentcontactController@updepartamentcontact')->name('updepartamentcontact');
			Route::delete('/deldepartamentcontact/{id}', 'Api\Client\Fillselects\DepartamentcontactController@deldepartamentcontact')->name('deldepartamentcontact');
			//get departamentcontact for selects box
			Route::get('/departamentcontactselectsbox', 'Api\Client\Fillselects\DepartamentcontactController@departamentcontactselectsbox');		
		});
	});
});
