<?php
Route::middleware('auth:api')->prefix('/region')->group(function (){
	Route::get('/nation', 'Api\Region\RegionController@nation');	
	Route::get('/states', 'Api\Region\RegionController@states');
	Route::get('/cities', 'Api\Region\RegionController@cities');	
});
