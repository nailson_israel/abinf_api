<?php
Route::middleware('auth:api')->prefix('/item')->group(function (){
	Route::group(['middleware' => ['payplan']], function () {	
		Route::get('/item', 'Api\Item\ItemController@item')->name('item');
		Route::post('/additem', 'Api\Item\ItemController@additem')->name('additem');
		Route::put('/upitem/{id}', 'Api\Item\ItemController@upitem')->name('upitem');
		Route::delete('/delitem/{id}', 'Api\Item\ItemController@delitem')->name('delitem');
		//get shows for selects box
		Route::get('/showlistitem', 'Api\Item\ItemController@showlistitem');
		Route::get('/showlistgroupitem', 'Api\Item\ItemController@showlistgroupitem');
		Route::get('/showlistbranditem', 'Api\Item\ItemController@showlistbranditem');
		
		Route::get('/itemselectbox', 'Api\Item\ItemController@itemselectbox');
		
		//group
		Route::prefix('/group')->group(function (){
			Route::get('/group', 'Api\Item\GroupitemController@group')->name('group');
			Route::post('/addgroup', 'Api\Item\GroupitemController@addgroup')->name('addgroup');
			Route::put('/upgroup/{id}', 'Api\Item\GroupitemController@upgroup')->name('upgroup');
			Route::delete('/delgroup/{id}', 'Api\Item\GroupitemController@delgroup')->name('delgroup');
			//get shows for selects box

			Route::get('/groupselectbox', 'Api\Item\GroupitemController@groupselectbox')->name('groupselectbox');
		});

		//brand
		Route::prefix('/brand')->group(function (){
			Route::get('/brand', 'Api\Item\BranditemController@brand')->name('brand');
			Route::post('/addbrand', 'Api\Item\BranditemController@addbrand')->name('addbrand');
			Route::put('/upbrand/{id}', 'Api\Item\BranditemController@upbrand')->name('upbrand');
			Route::delete('/delbrand/{id}', 'Api\Item\BranditemController@delbrand')->name('delbrand');
			//get shows for selects box
			Route::get('/brandselectbox', 'Api\Item\BranditemController@brandselectbox')->name('delbrand');
		});
	});
});