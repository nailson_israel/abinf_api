<?php
Route::middleware('auth:api')->prefix('/users')->group(function (){
	Route::prefix('group')->group(function () {
		Route::get('/permissionid/{id}', 'Api\Config\GroupuserController@permissionid')->name('permissionid');
	});

	Route::group(['middleware' => ['payplan']], function () {	
		//user 
		Route::prefix('/usersistem')->group(function (){
			Route::get('/user', 'Auth\UserController@users')->name('users');
			Route::post('/adduser', 'Auth\UserController@adduser')->name('adduser');
			Route::put('/upuser/{id}', 'Auth\UserController@upuser')->name('upuser');
			Route::delete('/deluser/{id}', 'Auth\UserController@deluser')->name('deluser');
		    Route::post('/delalluser', 'Auth\UserController@delalluser')->name('delalluser');
		    //get departament for selects box
		    Route::get('/userselectsbox', 'Auth\UserController@userselectsbox');	
		   	Route::get('userid/{id}', 'Auth\UserController@userid')->name('userid');
		});	

		//group and permissions
		Route::prefix('group')->group(function () {
			Route::get('/group', 'Api\Config\GroupuserController@groups')->name('groups');
			Route::get('/permission', 'Api\Config\GroupuserController@permission')->name('permission');
			Route::post('/addgroup', 'Api\Config\GroupuserController@addgroup')->name('addgroup');
			Route::put('/upgroup/{id}', 'Api\Config\GroupuserController@upgroup')->name('upgroup');
			Route::delete('/delgroup/{id}', 'Api\Config\GroupuserController@delgroup')->name('delgroup');
		    //get departament for selects box
		    Route::get('/groupselectsbox', 'Api\Config\GroupuserController@groupselectsbox');
		   	Route::get('/groupuserid/{id}', 'Api\Config\GroupuserController@groupuserid')->name('groupuserid'); 			
		});

		//departament
		Route::prefix('departament')->group(function () {
			Route::get('/departaments', 'Api\Config\DepartamentController@departaments')->name('departaments');
			Route::post('/adddepartament', 'Api\Config\DepartamentController@adddepartament')->name('adddepartament');
			Route::put('/updepartament/{id}', 'Api\Config\DepartamentController@updepartament')->name('updepartament');
			Route::delete('/deldepartament/{id}', 'Api\Config\DepartamentController@deldepartament')->name('deldepartament');
		    Route::post('/delalldepartament', 'Api\Config\DepartamentController@delalldepartament')->name('delalldepartament');
		    //get departament for selects box
		    Route::get('/departamentselectsbox', 'Api\Config\DepartamentController@departamentselectsbox');		
		});
		//changes
		Route::prefix('change')->group(function () {
		    Route::get('/changes', 'Api\Config\ChangeController@changes')->name('changes');
		    Route::post('/addchange', 'Api\Config\ChangeController@addchange')->name('addchange');	
		    Route::put('/upchange/{id}', 'Api\Config\ChangeController@upchange')->name('upchange');	
		    Route::delete('/delchange/{id}', 'Api\Config\ChangeController@delchange')->name('delchange');
		    Route::post('/delallchange', 'Api\Config\ChangeController@delallchange')->name('delallchange');	
		    //get change for selects box
		    Route::get('/changeselectsbox', 'Api\Config\ChangeController@changeselectsbox');
		});
	});
});