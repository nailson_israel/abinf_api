<?php
Route::middleware('auth:api')->prefix('/plan')->group(function (){
	Route::get('/myplan', 'Api\PlanController@myplan')->name('myplan');
	Route::get('/amountuserprice', 'Api\PlanController@amountuserAndprice')->name('amountuser');
});