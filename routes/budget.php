<?php
Route::middleware('auth:api')->prefix('/budgets')->group(function (){
	Route::group(['middleware' => ['payplan']], function () {
		//motive descart 
		Route::prefix('/motivedescart')->group(function (){
			Route::get('/motivedescart', 'Api\Budget\MotivedescartController@motivedescart')->name('motivedescart');
			Route::post('/addmotivedescart', 'Api\Budget\MotivedescartController@addmotivedescart')->name('addmotivedescart');
			Route::put('/upmotivedescart/{id}', 'Api\Budget\MotivedescartController@upmotivedescart')->name('upmotivedescart');
			Route::delete('/delmotivedescart/{id}', 'Api\Budget\MotivedescartController@delmotivedescart')->name('delmotivedescart');
			//get motivedescart for selects box
			Route::get('/motivedescartselectsbox', 'Api\Budget\MotivedescartController@motivedescartselectsbox');
		});
	});

	Route::prefix('/customorder')->group(function (){
		Route::get('/customorder', 'Api\Budget\CustomorderController@customorder')->name('customorder');
	});
	Route::prefix('/customorder')->group(function (){
		Route::post('/addcustomorder', 'Api\Budget\CustomorderController@addcustomorder');
		Route::put('/upcustomorder/{id}', 'Api\Budget\CustomorderController@upcustomorder')->name('customorderput');
		Route::delete('/delcustomorder/{id}', 'Api\Budget\CustomorderController@delcustomorder');
	});
	//Budget
	Route::prefix('/budget')->group(function (){
		Route::get('/showcountbudget', 'Api\Budget\BudgetController@showcountbudget');
		Route::get('/showcountorder', 'Api\Budget\BudgetController@showcountorder');
		Route::get('/showtotalmonth', 'Api\Budget\BudgetController@showtotalmonth');
		Route::get('/showtotalorderv', 'Api\Budget\BudgetController@showtotalorderValue');

		Route::group(['middleware' => ['payplan']], function () {
			Route::get('/budget', 'Api\Budget\BudgetController@budget')->name('budget');
			Route::get('/budgetid/{id}', 'Api\Budget\BudgetController@budgetid')->name('budgetid');
			Route::post('/addbudget','Api\Budget\BudgetController@addbudget')->name('addbudget');
			Route::put('/upbudget/{id}', 'Api\Budget\BudgetController@upbudget')->name('upbudget');
			Route::delete('/delbudget/{id}', 'Api\Budget\BudgetController@delbudget')->name('delbudget');
			Route::post('/delallbudget', 'Api\Budget\BudgetController@delallbudget')->name('delallbudget');
			//oof
			Route::put('/uptypebudget/{id}', 'Api\Budget\BudgetController@uptypebudget')->name('uptypebudget');
			Route::get('/nextnumber', 'Api\Budget\BudgetController@nextnumber');

			//oof
			Route::get('/showtotal/{id}', 'Api\Budget\BudgetController@showtotal');
			Route::get('/showclientbuy', 'Api\Budget\BudgetController@showclientbuy');

			Route::get('/showtotalbudget', 'Api\Budget\BudgetController@showtotalbudget');
			Route::get('/showtotalaguard', 'Api\Budget\BudgetController@showtotalaguard');
			Route::get('/showtotalconcluido', 'Api\Budget\BudgetController@showtotalconcluido');
			Route::get('/showtotaldescarted', 'Api\Budget\BudgetController@showtotaldescarted');

			Route::get('/showtotalorder', 'Api\Budget\BudgetController@showtotalorder');
			Route::get('/showtotalaguardorder', 'Api\Budget\BudgetController@showtotalaguardOrder');
			Route::get('/showtotalconcluidoorder', 'Api\Budget\BudgetController@showtotalconcluidoOrder');
			Route::get('/showtotaldescartedorder', 'Api\Budget\BudgetController@showtotaldescartedOrder');

			Route::get('/sellerclient/{idclient}', 'Api\Budget\BudgetController@sellerClient');
			Route::get('/topseller/{idclient}', 'Api\Budget\BudgetController@topSeller');
			Route::get('/bottomseller/{idclient}', 'Api\Budget\BudgetController@bottomSeller');
			Route::get('/totalorderclient/{idclient}', 'Api\Budget\BudgetController@totalorderClient');
			Route::get('/budgetclientorder/{idclient}', 'Api\Budget\BudgetController@budgetClientOrder');
			Route::get('/budgetclientorder/{idclient}', 'Api\Budget\BudgetController@budgetClientOrder');
			Route::get('/budgetpending/{idclient}', 'Api\Budget\BudgetController@budgetPending');		
			
			//Itensbudget
			Route::get('/itenbudget/{idbudget}', 'Api\Budget\Itenbudget\ItenbudgetController@itenbudget')->name('itenbudget');
			Route::get('/itenbudgetorder/{idbudget}', 'Api\Budget\Itenbudget\ItenbudgetController@itenbudgetorder')->name('itenbudgetorder');
			Route::post('/additenbudget', 'Api\Budget\Itenbudget\ItenbudgetController@additenbudget')->name('additenbudget');
			Route::put('/upitenbudget/{iditenbudget}', 'Api\Budget\Itenbudget\ItenbudgetController@upitenbudget')->name('upitenbudget');
			Route::delete('/delitenbudget/{iditenbudget}', 'Api\Budget\Itenbudget\ItenbudgetController@delitenbudget')->name('delitenbudget');
			Route::post('/delallitenbudget/', 'Api\Budget\Itenbudget\ItenbudgetController@delallitenbudget')->name('delallitenbudget');
		});
	});
	//group budgets 
	/*Route::prefix('/grouporc')->group(function (){
		Route::get('/grouporc', 'Api\Budget\GrouporcController@grouporc')->name('grouporc');
		Route::post('/addgrouporc', 'Api\Budget\GrouporcController@addgrouporc')->name('addgrouporc');
		Route::put('/upgrouporc/{id}', 'Api\Budget\GrouporcController@upgrouporc')->name('upgrouporc');
		Route::delete('/delgrouporc/{id}', 'Api\Budget\GrouporcController@delgrouporc')->name('delgrouporc');
		//get grouporc for selects box
		Route::get('/grouporcselectsbox', 'Api\Budget\GrouporcController@grouporcselectsbox');
	});*/
});