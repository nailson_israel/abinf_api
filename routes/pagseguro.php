<?php
Route::prefix('/pgsgro/recurrence')->group(function (){
	//notifications
	Route::post('/notifications', 'Api\Pagseguro\PagseguroController@notifications')->middleware('corspayment')->name('notification');	
});

Route::middleware('auth:api')->prefix('/pgsgro')->group(function (){
	Route::prefix('/recurrence')->group(function (){
		//AdhesionPlan
		Route::post('/session', 'Api\Pagseguro\PagseguroController@session')->name('session');
		Route::get('/listorders', 'Api\Pagseguro\PagseguroController@listOrders')->name('listorders');
		Route::post('/adhesionp', 'Api\Pagseguro\PagseguroController@adhesionP')->name('adhesionp');
		Route::post('/paymentmanual', 'Api\Pagseguro\PagseguroController@paymentManual')->name('paymentm');
		Route::post('/sendpayments', 'Api\Pagseguro\PagseguroController@sendPayments')->name('sendpayments');
		Route::put('/suspendadhesion', 'Api\Pagseguro\PagseguroController@suspendadhesion')->name('suspendadhesion');
		Route::put('/methodchangepayment', 'Api\Pagseguro\PagseguroController@methodChangePayment')->name('methodchangepayment');
		Route::put('/canceladhesion', 'Api\Pagseguro\PagseguroController@cancelAdhesion')->name('canceladhesion');		
	});
});