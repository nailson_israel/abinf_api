<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('api', ['ambiance' => '']);
});


Auth::routes();

//home
Route::get('/home', 'Admin\HomeController@index')->name('home');
Route::get('tenantdash', 'Admin\HomeController@tenantdash')->name('tenant');

//ambiance
Route::get('/configambiance', 'Admin\Config\ConfigambianceController@index')->name('configambiance')->middleware('auth:web');
Route::get('/ambiance', 'Admin\Config\ConfigambianceController@ambiance')->name('ambiance')->middleware('auth:web');
Route::post('addambiance', 'Admin\Config\ConfigambianceController@addambiance')->name('addambiance')->middleware('auth:web');
Route::put('upambiance/{id}', 'Admin\Config\ConfigambianceController@upambiance')->name('upambiance')->middleware('auth:web');
Route::delete('delambiance/{id}', 'Admin\Config\ConfigambianceController@delambiance')->name('delambiance')->middleware('auth:web');


use App\Models\Plan;
use App\Models\Payment;
use App\Models\User;
use App\Models\Talk;
use App\Jobs\JobSendPaymentPeriodic;
use App\Jobs\JobSendLastTransactionsnotPay;

use App\Jobs\JobSendEmailAgenda;
use App\Models\Sendagenda;

require('salesfunnel.php');

Route::get('mail', function (){
		return view('emails.notificationagenda');

       /* $agenda = Talk::select(
            'talks.tenantid as tnt', 'name', 'users.email as uemail', 
            'subject', 'date as dateag', 'represented', 
            'description', 'type', 'descriptionlegend', 'client', 'reference', 'cod', 'clients.phone1 as cphone1', 'clients.phone2 as cphone2', 'cep', 'street', 'neighborhood', 'complements', 'number', 'city', 'uf', 'clients.email as cemail',
            'contact', 'contacts.departament as departament', 'contacts.phone as cphone', 'contacts.phone2 as cphone2', 'contacts.email as cmail'
        )->where(['talks.id' => '6'])
        ->leftJoin('users', 'users.id', '=', 'talks.userid')
        ->leftJoin('legends', 'legends.legends', '=', 'talks.type')
        ->leftJoin('clients', 'clients.id', '=', 'talks.clientid')
        ->leftJoin('contacts', 'contacts.id', '=', 'talks.contactcl')
        // talvez fazer leftjoin do contato do cliente
        ->first();        

return $agenda;*/
});

Route::get('/testeagenda', function (){	
	$month = date('m');
	$agendas = Talk::select(
		'talks.id as id', 'tenantid', 'date as dateag', 'hour', 'userid'
	)->whereMonth('date', $month)->where([
		['isactivity', '=', '0'],
		['notification', '=', '1'],
		['notification_complet', '<>', '1']
	])
	// talvez fazer leftjoin do contato do cliente
	->get();
	foreach ($agendas as $agd) {

		$id = $agd->id;
		$tenant = $agd->tenantid;
		$dateag = $agd->dateag . ' ' . $agd->hour;
		$userid = $agd->userid;

		$today = date('Y-m-d H:i');
		$yearactual = date('Y');
		$dateagd = date($dateag);
		$yearag = date('Y', strtotime($dateag));

		$data1 = new DateTime($dateagd);
		$data2 = new DateTime($today);

		$interval = $data1->diff($data2);

		$verifyagenda = Sendagenda::where(['agendaid' =>  $id])->first();

		if(!$verifyagenda){
			$create = Sendagenda::create(['agendaid' => $id]);
		}

		$hoursend = date('06:00');
		$hourcurrent = date('H:i');
		if($yearactual === $yearag){
			if($interval->d == 3){
		
				$sendagenda = Sendagenda::where(['agendaid' =>  $id])
				->where('threedays', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
				if(!isset($sendagenda)){
					$moment = '3days';
					if($hoursend == $hourcurrent){
						dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
						$update = Sendagenda::where('agendaid', $id)->update(['threedays' => 1]);
					}
				}

			}else if($interval->d == 2){
				$sendagenda = Sendagenda::where(['agendaid' =>  $id])
				->where('twodays', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

				if(!isset($sendagenda)){
					$moment = '2days';
					if($hoursend == $hourcurrent){
						dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
						$update = Sendagenda::where('agendaid', $id)->update(['twodays' => 1]);
						//dispara job para enviar o aviso por email
					}
				}		
			}else if($interval->d == 1){
				$sendagenda = Sendagenda::where(['agendaid' =>  $id])
				->where('oneday', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

				if(!isset($sendagenda)){
					$moment = '1day';
					if($hoursend == $hourcurrent){
						dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
						$update = Sendagenda::where('agendaid', $id)->update(['oneday' => 1]);
						//dispara job para enviar o aviso por email
					}
				}
				//um job que execute ao dia
			}else if($interval->d == 0){
				$entrada = strtotime(date('H:i'));
        		$saida   = strtotime($agd->hour);
        		$diferenca = $saida - $entrada;
				$missinghours = sprintf( '%d:%d', $diferenca/3600, $diferenca/60%60);

 				$eight = '8:00';
 				$tow = '2:00';
 				$one = '1:00';
 				$minutes = '0:30';
 				$finish = '0:00';

 				if(strtotime($missinghours) > strtotime($finish)){
	 				if(strtotime($missinghours) >= strtotime($tow)){
	        			if(strtotime($missinghours) <= strtotime($eight)){
							$sendagenda = Sendagenda::where(['agendaid' =>  $id])
							->where('interval8hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

							if(!isset($sendagenda)){
								$moment = '8hours';
								dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
								$update = Sendagenda::where('agendaid', $id)->update(['interval8hours' => 1]);
							}
	        			}
	 				}
                    if(strtotime($missinghours) >= strtotime($one)){
	 					if(strtotime($missinghours) <= strtotime($tow)){
							$sendagenda = Sendagenda::where(['agendaid' =>  $id])
							->where('interval2hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
							$moment = '2hours';
							if(!isset($sendagenda)){
								dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
								$update = Sendagenda::where('agendaid', $id)->update(['interval2hours' => 1]);
							}	 	 						
	 					}				
					}

					/*este é o valor que o missinghours buga 12:50*
						a cima de 9:50 ele dá esse problema
					/

					/*verificar isso aqui, é a unica que ferra quando tem hora igual 17:00 = 5:00*/

	        		if(strtotime($missinghours) >= strtotime($minutes)){
	        			if(strtotime($missinghours) <= strtotime($one)){
							$sendagenda = Sendagenda::where(['agendaid' =>  $id])
							->where('interval1hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();
							if(!isset($sendagenda)){
								$moment = '1hour';
								dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
								$update = Sendagenda::where('agendaid', $id)->update(['interval1hours' => 1]);
							}	
	        			}
	        		}


	        		if(strtotime($missinghours) >= strtotime($finish)){
	        			if(strtotime($missinghours) <= strtotime($minutes)){
							$sendagenda = Sendagenda::where(['agendaid' =>  $id])
							->where('interval0hours', '=', '1')->orderBy('id', 'desc')->limit(1)->first();

							if(!isset($sendagenda)){
								$moment = '0hour';
								dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
								$update = Sendagenda::where('agendaid', $id)->update(['interval0hours' => 1]);
							}
	        			}
	        		}
 				}else{
	        		if($missinghours === '0:-1'){
	        																			echo 'asdsa';
						$moment = '-0hour';
						dispatch(new JobSendEmailAgenda($tenant, $id, $userid, $moment));
	        		}
 				}

 				echo '<br><br>';
			}else{
				echo 'nem chego a entrar';
			}
		}
	}
});

use App\Http\Traits\Pagseguro;

//use App\Models\Payment;


//teste de envio de pagamento
Route::get('/teste', function () {
	//Job execute once per day at 05:00 am 
	$day = date('d');
    $today = date('Y-m-d');

    /*Verifica quais os planos que tem adesão para o dia atual se não, não executa as features necessárias para cobrança ou aviso de pagamento atrasado*/
	$plans = Plan::whereNull('deleted_at')
			/*->whereDay('dateadhesion', $day)*/
			->where([
				['status', '=', 'ACTIVE'],
			])->get();


	if($plans != '[]'){
		foreach ($plans as $key => $plan) {
			$tenantid =  $plan->tenantid;
			$period = $plan->period;
			$value = $plan->value;
			$codeadhesion = $plan->codeadesion;
			$dataadhesion = $plan->dateadhesion;

			$payment = Payment::whereNull('deleted_at')
					->where([
						['tenantid', '=', $tenantid]
					])
					->orderBy('id', 'DESC')
					->first();

	        /*
	            1 - Aguardando pagamento / registered
	            2 - Em análise / analyzing
	            3 - Paga / Paid
	            4 - Disponível / Paid
	            5 - Em disputa / Dispute
	            6 - Devolvida / Returned
	            7 - Cancelada / Canceled
	        */

			$aux = explode('T', $payment['date']);
			$date = $aux[0];
			$hours = isset($aux[1]) ? $aux[1] : ''; 

			$dateLastPayment = date($date);
            $datelast = new DateTime($dateLastPayment);
		    $daytoday = new DateTime($today);

			$interval = $datelast->diff($daytoday);
		 	$days = $interval->days;

			if($period == 'monthly'){
				$daymin = 28;
				$daymax = 31;
				$month = 1;

				$monthsAhead = $interval->m + ($interval->y * 12);
 
	            $timestamp = strtotime($dateLastPayment . "+1 months");
	            $datepayment = date('Y-m-d', $timestamp);


			}else if ($period == 'trimonthly'){
				$daymin = 88;
				$daymax = 93;
				$month = 3;

				$monthsAhead = $interval->m + ($interval->y * 12);

	            $timestamp = strtotime($dateLastPayment . "+3 months");
	            $datepayment = date('Y-m-d', $timestamp);
			
			}else if($period == 'semiannually'){
				$daymin = 178;
				$daymax = 186;
				$month = 6;

				$monthsAhead = $interval->m + ($interval->y * 12);

	            $timestamp = strtotime($dateLastPayment . "+6 months");
	            $datepayment = date('Y-m-d', $timestamp);
			}

		 	if($dateLastPayment <= date('Y-m-d')){
			 	if($monthsAhead <= $month){
					if($today == $datepayment){
						if($payment->status == '3' || $payment->status == '4'){
							echo 'é dia de pagamento' .$tenantid.'<br>';
							//dispatch(new JobSendPaymentPeriodic($tenantid, $value, $period, $codeadhesion));//->delay(now()->addMinutes(1));
						}else{
							echo 'é dia de pagamento e a ultima transação não foi paga, tem que pagar';
							//dispatch(new JobSendLastTransactionsnotPay($tenantid));//->delay(now()->addMinutes(1));
						}
					}else if($datepayment == ''){
						echo 'quanto o plano ainda não tem fatura, pode nunca acontecer';
					}else{
						echo 'não é dia de cobrança<br>';
					}
			 	}else{
			 		echo 'atrasado';
					//dispatch(new JobSendLastTransactionsnotPay($tenantid));
		 		}
			}else{
				echo 'data de pagamento não bate com a data atual';
			}
		}
	}else{
		echo 'não acha plano de acordo com os parametros passados';
	}
});