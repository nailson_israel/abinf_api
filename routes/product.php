<?php
Route::middleware('auth:api')->prefix('/products')->group(function (){
	Route::group(['middleware' => ['payplan']], function () {
		//product
		Route::prefix('/product')->group(function (){
			Route::get('/product', 'Api\Product\ProductController@product')->name('product');
			Route::post('/addproduct', 'Api\Product\ProductController@addproduct')->name('addproduct');		
			Route::put('/upproduct/{id}', 'Api\Product\ProductController@upproduct')->name('upproduct');
			Route::delete('/delproduct/{id}', 'Api\Product\ProductController@delproduct')->name('delproduct');
			//get vars for selects box
			Route::get('/productbox', 'Api\Product\ProductController@productbox');
			Route::get('/productid/{id}', 'Api\Product\ProductController@productid')->name('productid');
			Route::get('/productmoreseller/{id}', 'Api\Product\ProductController@productmoreseller');
			Route::post('/productcod', 'Api\Product\ProductController@productcod');
			Route::post('/delallproduct', 'Api\Product\ProductController@delallproduct')->name('delallproduct');		
		});	
		//grouproduct
		Route::prefix('/groupproduct')->group(function (){
			Route::get('/groupproduct', 'Api\Product\GroupproductController@groupproduct')->name('groupproduct');
			Route::post('/addgroupproduct', 'Api\Product\GroupproductController@addgroupproduct')->name('addgroupproduct');
			Route::put('/upgroupproduct/{id}', 'Api\Product\GroupproductController@upgroupproduct')->name('upgroupproduct');
			Route::delete('/delgroupproduct/{id}', 'Api\Product\GroupproductController@delgroupproduct')->name('delgroupproduct');
			//delete all groupproduct
			Route::post('/delallgroupproduct', 'Api\Product\GroupproductController@delallgroupproduct')->name('delallgroupproduct');		
			//get grouprod for selects box 
			Route::get('/grouproductselectsbox', 'Api\Product\GroupproductController@grouproductselectsbox');		
		});
	    //Risegroup
		Route::prefix('/risergroup')->group(function (){
			Route::get('rise', 'Api\Product\RiseController@rise')->name('rise');
			Route::post('addrise', 'Api\Product\RiseController@addrise')->name('addrise');
		});
	});
});
