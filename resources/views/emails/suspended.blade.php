@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2><span class="user">{{ $user->name }}</span>, sua assinatura foi suspensa. 🥺</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p>Você optou por suspender sua conta e estamos aqui só para avisar e dizer que sentimos muito, mas entendemos você.
        Sua conta nunca será excluída, e cuidaremos muito bem dos seus dados até você decidir voltar a usa a plataforma.
        <span style="font-size:25px !important;">&#128074;&#127996;</span>
        <br>
        Fique tranquilo, não haverá nenhuma cobrança em todo o período de suspensão da assinatura. E para voltar a usar é simples,
        basta ir em "Plano" e clicar em "Reativar minha conta".</p>
        <br>
        <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Para mais informações sobre sua assinatura, acesse o Abinf Sales na aba "Plano".</span>
        <br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
