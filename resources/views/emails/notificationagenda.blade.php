@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf">
      <h2><span class="user">{{ $user->name }}</span>, {{ $titleemail }} 📅</h2>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        Olá, tudo bem? Como andam as vendas por aí?
        <br>
        Esperamos que muito boas.
        <br><br>
        Só passando para lembrar que você tem um compromisso agendado {{ $complement }}.
        <br><br>
        Veja as informações da agenda abaixo:

        <p class="text-email">
        </p>        
        <div class="infos">
          <p class="">
            <p><b class="title">{{ $agenda->subject }}</b></p>
            <p class="activity">{{ $agenda->descriptionlegend }}</p>
            <span class="data">📅 <?php echo date('d/m/Y', strtotime($agenda->dateag)); ?></span> 
            <span class="time">⏰ <?php echo date('H:i', strtotime($agenda->hour)); ?>h</span>
            <br>
            <p>{{ $agenda->client }} - <b>{{ $agenda->contact }}</b></p>
            <br>
            <p class="description">
              {{ $agenda->description }}
            </p>
          </p> 
          <br>
          <p class="final-text final-info">Para mais informações de sua agenda, acesse sua conta Abinf Sales.</p> 
        </div>        
        <br><br><br><br>
        <span class="final-text">Conte conosco - <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
