@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
    </div>
    <div class="div-text-email">
      <p class="text-email">
        {{ $request }}
        <br><br><br>
        {{ $response }}
      </p>
    </div>  
    <br>
  </div>
@endsection
