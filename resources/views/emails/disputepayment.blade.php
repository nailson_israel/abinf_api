@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2>O pagamento da sua assinatura <br>está em disputa! &#x1F624;</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p><span class="hello">Olá <span class="user">{{ $user->name }}</span>, calma! Entenda por que o pagamento da sua assinatura está em disputa.</span>
        <br>
        <br>
        <span class="infoimp">Por que meu pagamento está em disputa?</span>
        <br><br>
        A Disputa é o bloqueio do pagamento realizado pelo comprador. O bloqueio de pagamento não resulta no cancelamento da transação, e sim na suspensão temporária do pagamento, até que as partes envolvidas cheguem a um acordo.
        <br>
        <br></p>
         <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
