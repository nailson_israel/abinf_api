@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2>O pagamento da sua assinatura <br>foi cancelado! &#x1F624;</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p><span class="hello">Olá <span class="user">{{ $user->name }}</span>, Entenda por que o pagamento da sua assinatura foi cancelado.</span>
        <br>
        <br>
        O seu pagamento foi cancelado, entre em contato conosco para saber o que pode ter acontecido.
        <br><br>
        <span class="infoimp">Como?</span>
        Acesse a plataforma e vá na aba "Planos" e clique no botão "reenviar pagamento".
        <br>
        Com o pagamento realizado você poderá usar novamente a plataforma com todas as suas funcionalidades.
        <br><br>
        <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br></p>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
