@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2><span class="user">{{ $user->name }}</span>, o pagamento da sua assinatura <br>foi retornado! &#x1F624;</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <span class="infoimp">Por que meu pagamento foi retornado?</span>
        <br><br>
        Você pediu o retorno do valor pago pois foi alegado que seu cartão teve um uso indevido.
        <br><br>
        Com o pagamento realizado você poderá usar novamente a plataforma com todas as suas funcionalidades.
        <br><br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
