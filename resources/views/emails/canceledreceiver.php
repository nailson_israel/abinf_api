@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2><span class="user">{{ $user->name }}</span>, Sua assinatura foi cancelada. 🥺</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p>Você cancelou sua assinatura, e estamos aqui apenas para avisar que você pode voltar a usar a hora que desejar, estaremos esperando você.
        <br>
        Sua assinatura continuará a existir conosco, nada será cobrado enquanto sua assinatura estiver cancelada.</p>
        <br>
        <p class="final-text">- Sempre informaremos a você cada status de sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Para mais informações sobre sua assinatura, acesse o Abinf Sales na aba "Plano".</span>
        <br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
