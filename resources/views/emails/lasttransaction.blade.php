@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2>Não conseguimos enviar a cobrança periódica de seu plano. &#128577;</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p><span class="hello"><span class="user">{{ $user->name }}</span>, provavelmente você não consegue acessar sua conta no Abinf Sales, pois a última cobrança ainda não foi paga.</span>
        <br>
        <br>
        <span class="infoimp">Como assim?</span>
        <br><br>
        Nossos sistemas detectaram que a última cobrança foi cancelada. Esse cancelamento pode ser causado principalmente pela falta de limite em seu cartão de crédito ou um boleto gerado que não foi pago. Mas não tem problema, você pode regularizar sua assinatura acessando o Abinf Sales com sua conta. <br> 
        <br>
        Na plataforma, vá na aba "Planos" e clique no botão "Reativar meu plano" ou, se preferir, mude o cartão escolhido para o envio periódico da cobrança do seu plano e altere em "Alterar método de pagamento".
        <br>
        <br>
        </p>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
