@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2>Parabéns <span class="user">{{ $user->name }}</span>, agora sua representada vai decolar! 🚀</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <span class="hello">Olá <span class="user">{{ $user->name }}</span>,</span>
        <br>
        <br>
        Estamos muito felizes em ter você conosco, agora você é parte da família Abinf Sales.
        <br>
        Nós queremos ajudar você e mais tantos representantes e vendedores deste mundo que precisam gerir melhor suas informações, ter métricas e saber exatamente como agir com o cliente na próxima venda.
        <br><br>
        Informações centralizadas, gestão e força das vendas, funil de vendas, tecnologia e velocidade. Faça a gestão de seus pedidos, entenda o seu cliente com apenas uma plataforma, Abinf Sales.
        <br>
        Abinf Sales é a plataforma do representante comercial e vendedor.
        <br><br><br><br>
        <span class="final-text">Conte conosco - <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
