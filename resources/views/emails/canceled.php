@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2><span class="user">{{ $user->name }}</span>, Sua assinatura foi cancelada. 🥺</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p>Sua assinatura foi cancelada pela administradora de pagamentos.
        <br>
        Fique tranquilo, entre em contato conosco pelos canais de atendimento e nós te ajudaremos.</p>
        <br>
        <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Para mais informações sobre sua assinatura, acesse o Abinf Sales na aba "Plano".</span>
        <br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
