@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2>O pagamento da sua assinatura <br>está em análise! &#129300;</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <span class="hello">Olá <span class="user">{{ $user->name }}</span>, calma! Entenda por que o pagamento da sua assinatura está em análise.</span>
        <br>
        <br>
        <span class="infoimp">Por que meu pagamento está em análise?</span>
        <br><br>
        <p>As vezes o processo de pagamento leva alguns minutos para devolver o status de que a assinatura foi paga, logo após o processo de análise
        os sistemas liberam e aprovam o pagamento que você fez, então fique tranquilo(a), logo você receberá um e-mail com a situação atual da
        sua assinatura, OK? <span style="font-size:25px !important;">&#129309;&#127996;</span></p>
        <br>
        <br>
         <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
