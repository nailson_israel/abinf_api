@extends('layouts.emailsend')

@section('contentemail')
  <div class="email">
    <div class="top">
    </div>
    <div class="welcome">
      <img src="https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i" alt="Logo Abinf Sales">
      <h2><span class="user">{{ $user->name }}</span>, não conseguimos enviar a cobrança periódica em seu cartão. 💳</h2>
      <br><br>
    </div>
    <div class="div-text-email">
      <p class="text-email">
        <p>Ao tentar realizar a recorrência vigente do mês, foi indentificado que seu cartão está impossibilitado de receber cobranças. Para isso temos 3 possibilidades: 
        <ul>
          <li>Seu cartão de crédito venceu.</li>
          <li>Seu cartão foi cancelado.</li>
          <li>Seu cartão foi bloqueado.</li>
        </ul> 
        Neste caso você ficará impossibilitado(a) de usar a plataforma no dia a dia, mas muita calma,
        para resolver isso é simples, basta acessasr a sua conta, ir em "Plano" e alterar as informações do seu cartão de crédito.
        <br><br>
        Ao trocar o cartão de crédito, você receberá uma cobrança referente a última que não foi enviada.
        <br>
        Sim, é só isso que você precisa fazer. Fácil, não é?

        <span style="font-size:25px !important;">&#128079;&#127996;</span>
        </p>
        <br>
        <br>
        <p class="final-text">- Sempre informaremos a você cada atualização de status da sua assinatura na plataforma Abinf Sales.</p>
        <br><br>
        <span class="final-text">Para mais informações sobre sua assinatura, acesse o Abinf Sales na aba "Plano".</span>
        <br>
        <span class="final-text">Dúvidas? Acesse a aba de suporte e dúvidas. <a class="link" href="https://abinf.com.br">Abinf Sales</a></span>
      </p>
    </div>
    <br>
    <div class="button_action">
      <a href="https://app.abinf.com.br">Ir para Abinf Sales!</a>
      <br><br>
    </div>
  </div>
@endsection
