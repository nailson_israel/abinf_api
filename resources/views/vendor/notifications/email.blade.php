@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        <img src="{!! asset('https://drive.google.com/uc?id=18yk2e2cjdYGY1yw4RtEg3ditIDYPv97i') !!}" width="150px">
        @endcomponent
    @endslot
{{-- Body --}}
    <img src="{!! asset('https://drive.google.com/uc?id=1XOZzNj5cluVTL2hcJHzp9FmT2YQ698Ve') !!}" width="15px"> Olá! <br><br>
        Você está recebendo este e-mail porque recebemos uma solicitação de redefinição de senha para sua conta.    
{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

Se você não solicitou uma redefinição de senha, nenhuma ação adicional é necessária.
<br><br>
Até mais,

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
@lang(
    "Se você estiver com problemas para clicar no \":actionText\" botão, copie e cole o URL abaixo\n".
    'no seu navegador da web: [:actionURL](:actionURL)',
    [
        'actionText' => $actionText,
        'actionURL' => $actionUrl,
    ]
)
@endcomponent
@endisset
{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. Todos os direitos reservados a Codens.
        @endcomponent
    @endslot
@endcomponent