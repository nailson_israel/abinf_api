@extends('layouts.portal')
@section('content')
<div class="top-section">
	<h1 class="text-abinf">
		<span class="text-abinf-part1">Abinf Sales <br> A plataforma do vendedor.</span>
		<br><br>
		Descubra como organizar suas vendas, <br> Obtenha indicadores e saiba o que fazer e quando fazer.
	</h1>
	<div class="container-button">
		<a  href="register" class="btn btn-success i-want">Experimente gratuitamente</a>
		<br><br>
		<img src="image/logo-a.png" class="logo-a">
	</div>
</div>
@endsection
