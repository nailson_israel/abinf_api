@extends('layouts.resets')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <p class="forgot center-text">Altere senha sua senha</p>
            <div class="card card-box">
                <div class="card-body box-reset">
                    <form method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}
                        <h6 class="warning-reset"><b>Para sua segurança, você deve digitar seu email para redefinir sua senha. é rápido.</b></h6>
                        <input type="hidden" name="token" value="{{ $token }}">
                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" placeholder='Seu e-mail' required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder='Senha' required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" placeholder='Confirmar Senha' class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary w-100 button-buttons">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
