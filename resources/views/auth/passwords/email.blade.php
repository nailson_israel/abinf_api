@extends('layouts.resets')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <p class="forgot center-text">Ah não! Esqueceu sua senha?</p>
            <div class="card card-box">
                <div class="card-body box-reset">
                    <h6 class="warning-reset"><b>Informe seu e-mail e nós enviaremos um link para redefinir sua senha.</b></h6>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <div class="col">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder='E-mail' required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary w-100 button-buttons">
                                    Redefinir minha senha
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
