<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <style>
        @import url('https://fonts.googleapis.com/css?family=Work+Sans:200');
        html, body { margin:0 !important; height: 100% !important; }

        .email{
          margin:0 auto !important;
          background:#fff;
          width:700px;
          text-align:center !important:
        }

        .top{
          padding:4px;
          background:;
        }

        .email img{
          width:170px;
          margin-bottom:20px;
        }

        .welcome{
          margin-top:50px;
          font-size:15px;
          font-family: 'product_sansregular', sans-serif !important;
          color:#565656;
          text-align:center !important;
        }

        .div-text-email{
          background:#fff;
          padding:1px 1px;
          width:550px;
          margin:0 auto !important;
        }

        .text-email{
          margin:25px;
          text-align:justify;
          font-family: 'product_sansregular', sans-serif !important;
        }

        .hello{
          font-size:16px;
        }

        .final-text{
          font-size:13px;
        }

        .linktrine-text{
          color: !important;
        }

        .user{
          color:#0073C4;
          font-weight:bold;
          text-transform:capitalize !important;
        }

        .infoimp{
          color:#0073C4;
          font-weight:bold;
        }

        .button_action{
          text-align:center !important;
          margin-top:20px;
        }

        .button_action a{
          background:#0073C4;
          padding:10px 20px;
          text-decoration:none !important;
          color:#fff;
          font-family: 'product_sansregular', sans-serif !important;
        }

        .link{
          color:#0073C4;
          font-weight:bold;
        }

        b, .activity, .final-info{
          color:#a5a5a5;
        }

        .title{
          color:#0073c4;
          text-transform: capitalize !important;
        }

        .infos{
          width:90% !important;
          background:#efefef;
          padding:15px !important;
          font-family: 'product_sansregular', sans-serif !important;
        }

        .description{
        }
      </style>
  </head>
  <body>
    <section class="has-text-centered">
      @yield('contentemail')
    </section>
  </body>
</html>
