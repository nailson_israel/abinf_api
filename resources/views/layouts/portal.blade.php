<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Portal Abinf Sales Crm</title>
<!-- Latest compiled and minified CSS -->
<!-- Optional theme -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
        <!-- Styles -->
        <link rel="stylesheet" href="css/portal.css"crossorigin="anonymous">
        <link rel="icon" href="{{ asset('image/logo-a.png') }}" type="image/x-icon"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-light navbar-portal">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="image/logo.png" class="logoportal">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @if(!Route::current()->getName() == 'register')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">A plataforma</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Planos e valores</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Sobre nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Muito mais</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Representei.blog</a>
                        </li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest('api')
                            @if(!Route::current()->getName() == 'register')
                            <li class="nav-item">
                                <a class="btn btn-primary w-100 button-buttons-overlap border-radius-custon-positive"  href="{{ route('register') }}">{{ __('Eu quero!') }}</a>
                            </li>
                            @else
                            <li class="nav-item">
                                <a class="nav-link tryefree w-100" href="{{ route('register') }}">{{ __('Já possui uma conta?') }}</a>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a class="btn btn-primary w-100 button-buttons-border"  href="http://localhost:8080">{!! trans("Acessar") !!}</a>
                            </li>
                        @else
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </body>
</html>
