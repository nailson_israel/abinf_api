<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Abinf Sales</title>
<!-- Latest compiled and minified CSS -->
<!-- Optional theme -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
        <!-- Styles -->
        <link rel="stylesheet" href="/css/portal.css"crossorigin="anonymous">
        <link rel="icon" href="{{ asset('image/logo-a.png') }}" type="image/x-icon"/>
    </head>
    <body>
        <div  class="center-text">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('image/logo-a.png') }}" class="image-icon-reset">
            </a>
        </div>
        <main class="py-4">
            @yield('content')
        </main>
    </body>
</html>
