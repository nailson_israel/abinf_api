window.axios = require('axios');

const config = axios.create({
  baseURL: 'http://abinfrapi.codens.com.br/',
  timeout: 1000,
  headers: {'Content-Type': 'application/json',}
});

export default config;