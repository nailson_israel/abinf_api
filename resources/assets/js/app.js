/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.VueForm = require('vue-form');
window.VueTheMask = require('vue-the-mask');

import Notifications from 'vue-notification';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.filter('uppercase', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toLowerCase() + value.slice(1)
})

//Uses
Vue.use(VueForm);
Vue.use(VueTheMask);
Vue.use(Notifications);

//Components
//Vue.component('example-component', require('./components/Example.vue'));
Vue.component('ab-dashboard', require('./components/Dashboard.vue'));
Vue.component('ab-ambiance', require('./components/config/Ambiance.vue'));
Vue.component('ab-modal', require('./components/Modaldelete.vue'));
//Components Packages
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('cds-pagination', require('cds-pagination-laravel'));

const app = new Vue({
    el: '#content',

    data () {
      return {
        teste: 'teste',   
      }
    },


});
